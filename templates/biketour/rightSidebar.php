<div class="mainRightSidebar">

	<div class="topSection">
		<? if ($this->emagid->route['controller'] == 'products') { ?>
		<?}else{?>
			<div class="lft">
				<div id="shopping_bag_toggle" class="shopping_bag_toggle sidebar_view_toggle" data-sidebar_view="cart">
					<p style="pointer-events: none" class="">shopping bag</p>
					<icon style="pointer-events: none" class="media"></icon>
				</div>
				<div id="my_account_header_link" class="sidebar_view_toggle" data-sidebar_view="account">
					<?if($model->user){?>
						<a href="/account" class="" id="accountOverviewLinkDontOpenSidebar"><p><?=$model->user->full_name()?></p></a>
					<?}else{?>
						<p style="pointer-events: none" class="">my account</p>
					<?}?>
				</div>
			</div>
		<?}?>

		<div class="rght">
			<div id="shopping_bag_count" class="shopping_bag_toggle"><?=$model->cart ? count($model->cart->products): 0?></div>
			<!-- 			<div id="favorites_bar_toggle">
                            <icon class="media"></icon>
                        </div> -->


		</div>
	</div>
	<div class="activeSection">
		<div id="closeRightSidebar" class="show510">
			<icon style="background-image:url(<?=FRONT_IMG?>chevronLeft.png)"></icon>
		</div>
		<?if(isset($model->cart->cart)&&($model->cart->cart != null)){?>
		<div class="sidebar_view_cart sidebar_view" style="display:none;">
			<div class="header_fixed">
				<a class="btn full_width_btn shoppingBagBtn" href="/cart"><p>View Shopping Bag</p></a>
				<div class="row row_of_2">
					<div class="col discount">
						<label class="as_m">discount</label>
						<p class="coupon_container"><?if(isset($model->cart->discount) && $model->cart->discount){?>
								<span class="couponDiscountAmount">- $<?=number_format($model->cart->discount,2)?></span>
								<span class="couponCode">
									<?if(($coupon=\Model\Coupon::checkDiscount())){?>
										<?=$coupon->discount_type == 1 ? "-$coupon->discount_amount OFF": "$coupon->discount_amount% OFF"?>
									<?}?>
								</span>
							<?}else{?>
								<input type="text" placeholder="Coupon Code" class="rightSidebarCouponInput couponInput">
								<a class="rightSidebarCouponBtn addCoupon">Go</a>
						<?}?></p>
					</div>
					<div class="col subtotal">
						<label class="as_m">subtotal</label>
						<p class="updateSubtotal"><?=$model->cart ? '$'.number_format($model->cart->total - $model->cart->discount,2): 'No items in bag'?></p>
					</div>
				</div>
			</div>
		<? }else{?>
		<div class="sidebar_view_cart sidebar_view sidebar_view_cart_noitems" style="display:none;">
			<div id="closeRightSidebar" class="noItemsCloseSidebarBtn">
				<icon style="background-image:url(<?=FRONT_IMG?>chevronLeft.png)"></icon>
			</div>
			<div class="header_fixed">
				<a class="btn full_width_btn shoppingBagBtn" href="/cart"><p>View Shopping Bag</p></a>
				<div class="row row_of_2">
					<div class="col discount">
						<label class="as_m">discount</label>
						<p class="coupon_container"><?if(isset($model->cart->discount) && $model->cart->discount){?>
								<span class="couponDiscountAmount">- $<?=number_format($model->cart->discount,2)?></span>
								<span class="couponCode">
									<?if(($coupon=\Model\Coupon::checkDiscount())){?>
										<?=$coupon->discount_type == 1 ? "-$coupon->discount_amount OFF": "$coupon->discount_amount% OFF"?>
									<?}?>
								</span>
							<?}else{?>
								<input type="text" placeholder="Coupon Code" class="rightSidebarCouponInput couponInput">
								<a class="rightSidebarCouponBtn addCoupon">Go</a>
						<?}?></p>
					</div>
					<div class="col subtotal">
						<label class="as_m">subtotal</label>
						<p class="updateSubtotal"><?=$model->cart ? '$'.number_format($model->cart->total,2): 'No items in bag'?></p>
					</div>
				</div>
			</div>
		<?}?>
			<div class="floatingCheckoutButton">
				<?if($model->user){?>
				<a class="col btn_oreo shoppingBagBtnCheckbox" href="/checkout/delivery">
				<?}else{?>
				<a class="col btn_oreo shoppingBagBtnCheckbox" href="/checkout/email">
					<?}?>
					<p>Checkout</p>
				</a>
			</div>
			<div class="tabbedView">
				<header class="tabController">
					<div class="title row">
						<h4 class="col as_l shopping_bag_title tab active" data-tab_title="bag"><icon></icon>Shopping Bag <span class="count"><?=$model->cart ? count($model->cart->products): 0?></span></h4>
						<a class="as_l col inv_btn tab favorites" data-tab_title="favorites">
							Saved
							<icon>
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="0 0 241.3 224.7" enable-background="new 0 0 241.3 224.7" xml:space="preserve">
	                                <path d="M120.6,34.6c0,0,0.5-0.5,0.8-0.8c9.8-11.2,30.1-30,54.2-30c33.6,0,61.9,28.4,61.9,62c0,22-14.5,50.1-43.3,83.7
	                                c-10.8,12.6-23.5,25.8-37.6,39.3c-10,9.5-18.7,17.1-24.2,21.9c-3.4,3-11.9,10.5-11.9,10.5s-8.3-7.5-11.6-10.4
	                                C77.2,183.6,3.6,115.4,3.6,65.9c0-33.6,28.3-62,61.9-62c24.2,0,44.4,18.7,54.3,29.9L120.6,34.6z"/>
	                            </svg>
							</icon>
						</a>
					</div>
				</header>
				<div class="sidebarBagScrollContainer">
					<div class="shoppingBag_items_wrapper tab_content tab_content_bag tab_content_active">
						<ul class="items">
							<?if(isset($model->cart->cart)&&($model->cart->cart != null)){
								foreach($model->cart->cart as $cart){
									$product = \Model\Product::getItem($cart->product_id);
									$color_id = json_decode($cart->variation,true)['color'];
									$size_id = json_decode($cart->variation,true)['size'];
									$defaultcolor = \Model\Color::getItem($color_id);
									$size = \Model\Size::getItem($size_id)?>
									<li>
										<div class="item_container">
											<div class="bagEditItem">
												<div class="editClose circle_x">
													<circle>
														<span></span>
														<span></span>
													</circle>
												</div>
												<div class="mediaWrapper item_img">
													<a class="cartImg" href="/products/<?=$product->slug?>">
														<img src="<?=UPLOAD_URL.'products/'.$cart->featuredImage()?>">
													</a>
													<div class="remove_item_cart btn btn_black" data-cart_id="<?= $cart->id?>">
														<p>Remove</p>
													</div>
												</div>
												<div class="item_info">
													<div class="itemName">
														<h4 class="as_l"><?=$product->name?></h4>
													</div>
													<div class="product_price">
														<h4 class="as_l">$<?=number_format($product->getPrice($color_id),2)?></h4>
													</div>
												</div>
												<div class="item_info accordionUI">
													<a class="visible btn btn_gray edit_btn">Edit</a>
													<div class="hidden">
														<div class="customizationOption customizationOptionColor accordionUI accordionUI_active">
															<a class="toggleCustomizationOptions visible"><label><span id="color-name">Color: <span><?=$defaultcolor->name?></span></span><icon></icon></label></a>
															<div class="hidden customizationOptionsModule color_selection_module selection_module radioOptionsModule">
																<div class="row color-content">
																	<? if ($product->color) {
																		foreach (json_decode($product->color, true) as $item) {
																			if ($color = \Model\Color::getItem($item)) {?>
																				<div class="col optionBox optionBoxRadio <?=$color->id == $defaultcolor->id ? 'selected': ''?>">
																					<a href="#">
																						<div class="media colorSwatch"
																							 style="background-image:url(<?=$color->swatch()?>)"></div>
																						<p><?= $color->name ?></p>
																						<input type="checkbox" value="<?= $color->id ?>"
																							   data-color="<?= $color->name ?>" <?=$color->id == $defaultcolor->id ? 'checked="checked"': ''?>>
																					</a>
																				</div>
																			<? } ?>
																		<? } ?>
																	<? } else { ?>
																		<div class="col optionBox optionBoxRadio">
																			<a href="#">
																				<div class="media colorSwatch"
																					 style="background-image:url(<?= FRONT_IMG ?>color_swatch_.png)"></div>
																				<p>Default</p>
																				<input type="checkbox" value="0"
																					   data-color="0">
																			</a>
																		</div>
																	<? } ?>
																</div>
															</div>
														</div>
														<div class="customizationOption customizationOptionSize accordionUI accordionUI_active">
															<a class="toggleCustomizationOptions visible"><label><span id="size-name">Size: <span>EUR <?=floatval($size->eur_size)?></span><span>US <?=floatval($size->us_size)?></span></span><icon></icon></label></a>
															<div class="hidden customizationOptionsModule size_selection_module radioOptionsModule">
																<!-- <a><p>Size Guide</p></a> -->
																<div class="row row_of_3 size-content">
																	<?foreach($product->getSizes() as $item){
																		if($item != ''){?>
																			<div class="col optionBox optionBoxRadio <?=$item->id == $size->id ? 'selected': ''?>">
																				<!--									<h3 class="as_r">--><?//='US:'.floatval($item->us_size).'|EU:'.floatval($item->eur_size)?><!--</h3>-->
																				<h3 class="as_r"><span>EUR <?=floatval($item->eur_size)?></span><span>US <?=floatval($item->us_size)?></span></h3>
																				<input type="checkbox" value="<?=$item->id?>" <?=$item->id == $size->id ? 'checked': ''?>>
																			</div>
																		<?}?>
																	<?}?>
																</div>
															</div>
														</div>
														<h4 class="update_cart btn_black btn" data-cart_id="<?=$cart->id?>">Update</h4>
													</div>
												</div>
											</div>
										</div>
									</li>
								<?}?>
							<?} else {?>
								<li class="noItemsInCartWrapper">
									<div class="item_container">
										<div class="bagEditItem">
											<div class="item_info">
												<div class="itemName absTransCenter">

													<h4 class="as_l">Your shopping bag<br>is empty</h4>
													<div class="cfBox row shippingBox">
														<icon class="icon col" style="background-image:url('/content/frontend/assets/img/shippingIcon.png')"></icon>
														<p class="as_l">Free 5-day delivery on all in-stock items not made to order.</p>
													</div>													
												</div>
											</div>
										</div>
									</div>
								</li>
							<? } ?>
						</ul>
					</div>
					<div class="shoppingBag_items_wrapper tab_content tab_content_favorites">
						<ul class="items">
							<? if ($model->user && ($fav = \Model\User_Favorite::getList(['where' => "user_id = {$model->user->id}"]))) {
								$uFavorite = array_map(function ($item) {
									return $item->product_id;
								}, $fav);
							} else if (isset($_COOKIE['favorite']) && $uFavorite = json_decode($_COOKIE['favorite'], true)['product']) {
							} else {
								$uFavorite = [];
							}
							foreach ($uFavorite as $uf) {
								$prod = \Model\Product::getItem($uf) ?>
								<li>
									<div class="item_container">
										<div class="bagEditItem">
											<div class="editClose circle_x">
												<circle>
													<span></span>
													<span></span>
												</circle>
											</div>
											<div class="mediaWrapper item_img">
												<a class="cartImg" href="<?= SITE_URL . 'products/' . $prod->slug ?>">
													<img src="<?= UPLOAD_URL . 'products/' . $prod->featuredImage() ?>">
												</a>

												<div class="remove_item_favorite btn btn_black" data-product_id="<?=$uf?>" data-user_id="<?=$model->user?$model->user->id:0?>">
													<p>Remove</p>
												</div>
											</div>
											<div class="item_info">
												<div class="itemName">
													<h4 class="as_l"><?= $prod->name ?></h4>
												</div>
												<div class="product_price">
													<h4 class="as_l">$<?= number_format($prod->getPrice(), 2) ?></h4>
												</div>
											</div>
											<div class="item_edit_options">
												<a class="btn btn_oreo">Add to Bag</a>
											</div>
										</div>
									</div>
								</li>
							<? } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="sidebar_view sidebar_view_account" style="display:none;">
			<div class="tabbedView">
				<header class="tabController">
					<div class="title row">
						<?if($model->user == null){?>
							<h4 class="col as_l shopping_bag_title login_title tab active" data-tab_title="login">Log In</h4>
							<a class="as_l col inv_btn tab register_title" data-tab_title="register">Register</a>
						<?} else {?>
							<h4 id="signout" class="col as_l shopping_bag_title login_title tab active" data-tab_title="login">Sign Out</h4>
						<?}?>
					</div>
				</header>
				<div class="sidebarBagScrollContainer">
					<div class="shoppingBag_items_wrapper tab_content tab_content_login tab_content_active">
						<div class="row loginMainRow">
							<h5 class="as_r"><?=$model->user?'Welcome':'Log In'?></h5>
							<h8 class="as_l"><?=$model->user?$model->user->full_name():'Made in NYC'?></h8>
							<?if(!$model->user){?>
								<div class="formWrapper accountFormWrapper">
									<form method="post" action="/login">
										<div class="row">
											<input name="email" class="dark_text_input" type="text" placeholder="Email" id="sidebarEmailInput">
										</div>
										<div class="row">
											<input name="password" class="dark_text_input" type="password" placeholder="Password">
										</div>
										<div class="row">
											<input type="submit" class="btn btn_black btn_full_width" value="Sign In">
											<input hidden name="redirect-url" value="<?=$this->emagid->uri?>">
										</div>
										<div class="footer_links">
											<a id="toggleSignupTab">Create Account</a>
											<a id="closeRightSidebar">Return to Store</a>
											<a>Forgot your Password?</a>
										</div>
									</form>
								</div>
							<?} else {?>
								<div class="formWrapper accountFormWrapper">
									<a class="row" href="/account">View Account</a>
								</div>
							<?}?>
						</div>
					</div>
					<?if(!$model->user){?>
						<div class="shoppingBag_items_wrapper tab_content tab_content_register">
							<div class="row loginMainRow">
								<h5 class="as_r">Register</h5>
								<h8 class="as_l">Made in NYC</h8>
								<div class="separator">
								</div>
								<div class="formWrapper accountFormWrapper">
									<form method="post" action="/login/register">
										<div class="row">
											<div class="row">
												<input name="first_name" class="dark_text_input" type="text" id="firstRegisterField" placeholder="First Name *">
											</div>
											<div class="row">
												<input name="last_name" class="dark_text_input" id="lname" type="text" placeholder="Last Name *">
											</div>
											<div class="row">
												<input name="email" class="dark_text_input" type="email" id="email" placeholder="Email Address *">
											</div>
											<div class="row">
												<input name="phone" class="dark_text_input" type="tel" id="phone" placeholder="Phone *">
											</div>
											<div class="row">
												<input name="password" class="dark_text_input" type="password" id="password" placeholder="Password *">
												<input hidden name="redirect-url" value="<?=$this->emagid->uri?>">
											</div>
										</div>
										<div class="row">
											<input type="submit" class="btn btn_black btn_full_width" value="Create Account">
										</div>
										<div class="footer_links">
											<a id="closeRightSidebar">Return to Store</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.addCoupon').on('click',function(){
			var container = $(".coupon_container");
			var subtotal = $(".updateSubtotal");
			var code = $('.couponInput').val();
			$.post('/coupon/add',{code:code,subtotal:<?=$model->cart->total?>},function(data){
				var json = $.parseJSON(data);
				if(json.status == 'success'){
					container.html('<span class="couponDiscountAmount">- $'+json.discount+'</span> <span class="couponCode">'+json.discountText+'</span>');
					subtotal.html(json.discountTotal);
				} else {
					alert(json.message);
				}
			});
		});
		$('#signout').on('click',function(){
			$.post('/login/logout','',function(){
				window.location.replace('/');
			})
		});
		$('.update_cart').on('click',function(){
			var color_id = $(this).parent().find('.color-content').find('input[type=checkbox]:checked').val();
			var size_id = $(this).parent().find('.size-content').find('input[type=checkbox]:checked').val();
			var cart_id = $(this).attr('data-cart_id');
			$.post('/cart/updateCart',{color_id:color_id,size_id:size_id,cart_id:cart_id},function(data){
				if(data.status == 'success'){
					window.location.replace('<?=$this->emagid->uri?>');
				}
			});
		});
		$(document).on('click','.remove_item_cart',function(){

			var cart_id = $(this).attr('data-cart_id');
			$.post('/cart/deleteItem',{cart_id:cart_id},function(obj){
				if(obj.status=='success'){
					window.location.replace('<?=$this->emagid->uri?>');
				}else{
					alert(obj.message);
				}
			})
		});
		$(document).on('click','.remove_item_favorite',function(){
			var user_id = $(this).attr('data-user_id');
			var product_id = $(this).attr('data-product_id');
			var data = {'user_id': user_id, 'product_id': product_id};
			var th = $('.favorite_btn');
			$.post('/product/changeFavorite', data, function (item) {
				if (item.status == 'success') {
					window.location.replace('<?=$this->emagid->uri?>');

				}else {
					alert(item.message);
				}
			})

		});
	})
</script>

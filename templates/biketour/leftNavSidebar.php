<div id="leftNavSidebar">
	<? if($this->emagid->route['controller'] == 'account' || $this->emagid->route['controller'] == 'pages'){?>
		<?foreach($model->categories as $mainCategory){?>
			<div class="accordionUI accordionUI_active">
				<a class="visible">
					<h3 class="as_l"><?=$mainCategory->name?></h3>
					<icon>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
							<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
							L-321,348.1z"/>
						</svg>
					</icon>
				</a>
				<div class="hidden">
					<a href="<?=SITE_URL."collections/$mainCategory->slug/all"?>">
						<h5 class="as_l">All</h5>
					</a>
					<?php $sql = "select * from subnav where active = 1 and category_id = {$mainCategory->id} order by column_num ASC, display_order ASC;";
					$subCat = \Model\Subnav::getList(['sql'=>"$sql"]); $list = [];?>
					<?foreach($subCat as $category){
						$category = \Model\Category::getItem($category->sub_category_id);?>
						<a href="<?=SITE_URL."collections/$mainCategory->slug/$category->slug"?>">
							<h5 class="as_l"><?=$category->name?></h5>
						</a>
					<?}?>
				</div>
			</div>
		<?}

	}else{?>


		<?foreach($model->categories as $mainCategory){
			if($model->category == 'all'){?>
				<div class="accordionUI <?=$mainCategory->slug == $model->parent?'accordionUI_active': ''?>">
			<?} else {?>
				<div class="accordionUI <?=$mainCategory->slug == \Model\Category::getItem($model->category->parent_category)->slug?'accordionUI_active': ''?>">
			<?}?>
			<a class="visible">
				<h3 class="as_l"><?=$mainCategory->name?></h3>
				<icon>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
						<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
						L-321,348.1z"/>
					</svg>
				</icon>
			</a>
			<div class="hidden">
				<?$slug = $model->category == 'all'? $model->category : $model->category->slug; ?>
				<a href="<?=SITE_URL."collections/$mainCategory->slug/all"?>" <?=$slug == 'all' ? 'class="active_accordion_tab"': ''?>>
					<h5 class="as_l">All</h5>
				</a>
				<?php $sql = "select * from subnav where active = 1 and category_id = {$mainCategory->id} order by column_num ASC, display_order ASC;";
				$subCat = \Model\Subnav::getList(['sql'=>"$sql"]); $list = [];?>
				<?foreach($subCat as $category){
					$category = \Model\Category::getItem($category->sub_category_id);
					$activeAccordion = $slug == $category->slug ? 'class="active_accordion_tab"': ''?>
					<a href="<?=SITE_URL."collections/$mainCategory->slug/$category->slug"?>" <?=$activeAccordion?>>
						<h5 class="as_l"><?=$category->name?></h5>
					</a>
				<?}?>
			</div>
		</div>
		<?}
	}?>
</div>
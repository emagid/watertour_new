
<script src="<?=FRONT_JS?>moment.js"></script>
<script src="<?=FRONT_JS?>rome.standalone.js"></script>
<script src="<?= FRONT_JS ?>headroom.min.js"></script>
<script src="<?= FRONT_JS ?>core.js"></script>
<script src="<?= FRONT_JS ?>jquery.easing.1.3.js"></script>
<script src="<?= FRONT_JS ?>jquery-ui-sliderAccess.js"></script>
<script src="<?= FRONT_JS ?>jquery-ui-timepicker-addon.js"></script>
<script src="<?= FRONT_JS ?>jquery.mCustomScrollbar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.js"></script>
<script src="<?= FRONT_JS ?>js.cookie.js"></script>
 <script src="<?=FRONT_JS?>extractImgColors.js"></script>
<script src="<?= FRONT_LIBS ?>slick/slick.min.js"></script>

<script src="<?= FRONT_LIBS ?>flickity.min.js"></script>
<!-- PACKET MANAGER FOR imagesLoaded() npm install imagesloaded -->

<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>


<? if ($this->emagid->route['controller'] == 'packages' || $this->emagid->route['controller'] == 'attractions') { ?>
    
     <script src="<?=FRONT_JS?>datepicker.js"></script>
    
    <script src="<?=FRONT_JS?>typeahead.js"></script>
    <script src="<?=FRONT_JS?>bootstrap-datepaginator.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?=FRONT_JS?>packages.js"></script>
<? } ?>


<? if ($this->emagid->route['controller'] == 'attractions') { ?>
    
    <script src="<?=FRONT_JS?>typeahead.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?=FRONT_JS?>packages.js"></script>
<? } ?>



<? if ($this->emagid->route['controller'] == 'products') { ?>
    <script src="<?=FRONT_JS?>product.js"></script>
<? } ?>


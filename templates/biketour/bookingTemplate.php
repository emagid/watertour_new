<div class="reservationBarWrapper reservationBarTandemIncluded">
    <div class="container">
        <form action="/reservation/resRental" method="post" id="book_form">
            <?$sessionSelect = $model->sessionSelect?>
            <? $firstPackage = \Model\Package::getItem('',['where'=>'Status = 1','orderBy'=>'display_order ASC']);
            if(isset($_SESSION[$sessionSelect]->$cartStr)) {
                $firstPackage = \Model\Package::getItem($_SESSION[$sessionSelect]->$cartStr);
            }

            ?>
            <div class="stepHours step">
                <div class="timepickerInputWrapper">
                    <select class="btSelect reservationTime" name="reservationTime">
                        <?$selectedType = '';
                        foreach($model->booking_selection as $mod=>$items){
                            $modelStr = strtolower($mod);
                            $cartStr = $modelStr.'s'; ?>
                            <option value="-1" disabled selected hidden>Packages</option>
                            <optgroup label="<?=$mod?>s">
                                <?foreach ($items as $item){?>
                                    <option data-type="<?=$modelStr?>" data-cart="<?=$cartStr?>" value="<?=$item->id?>" <?if($_SESSION[$sessionSelect]->$cartStr == $item->id){$selectedType = $cartStr; echo 'selected';}?>><?=$item->name?></option>
                                <?}?>
                            </optgroup>
                        <? } ?>
                    </select>
                    <input type="hidden" name="reservationTimeType" value="<?=$selectedType?>">
                    <?php $datetime = $_SESSION['cart']->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $_SESSION['cart']->datetime)->format('M j, Y g:i a') : ''; ?>
                    <input type="text" class="triggerDatepicker" hidden name="reservationDateTime" placeholder="When?" value="<?= strtotime($datetime) ?>">
                    <span class="dropdownArrow">
                        <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
                    </span>
                </div>
            </div>
            <div class="step countStep">
                <div class="countDropdownTrigger_data">
                    <?$str = [];
                    if($_SESSION[$sessionSelect]->adults){
                        $str[] = "<b>{$_SESSION[$sessionSelect]->adults}</b> ". pluralize($_SESSION[$sessionSelect]->adults,'Adult');
                    }
                    if($_SESSION[$sessionSelect]->kids){
                        $str[] = "<b>{$_SESSION[$sessionSelect]->kids}</b> ". pluralize($_SESSION[$sessionSelect]->kids,'Kid');
                    }
                    ?>
                    <div class="text <?=!implode(' & ',$str) ? 'placeholderActive': 'placeholderInactive'?>">
                        <p class="placeholder val gray">Who's going?</p>
                        <p class="data">
                            <?=implode(' & ',$str)?>
                        </p>
                    </div>
                    <span class="dropdownArrow">
                        <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
                    </span>
                </div>
                <div class="countDropdownMenu">
                    <a class="cancelDropdown">
                        <span></span>
                        <span></span>
                    </a>
                    <div class="adultsCount">
                        <div class="inc_dec_wrapper">
                            <div class="dec button disabled" data-attribute="adult"><span></span></div>
                            <input type="text" class="adultsCountVal" value="<?=$_SESSION[$sessionSelect]->adults?>" pattern="[0-9]*"
                                   name="reservationQuantity">
                            <div class="inc button" data-attribute="adult"><span></span><span></span></div>
                        </div>
                        <div class="adultPrice">
                            $<?=(integer)$firstPackage->getPrice("adult")?>
                        </div>
                        <span class="key">Adults</span>
                    </div>
                    <div class="kidsCount">
                        <div class="inc_dec_wrapper">
                            <div class="dec button disabled" data-attribute="kid"><span></span></div>
                            <input type="text" class="kidsCountVal" value="<?=$_SESSION[$sessionSelect]->kids?>" pattern="[0-9]*"
                                   name="reservationKidQuantity">
                            <div class="inc button" data-attribute="kid"><span></span><span></span></div>
                        </div>
                        <div class="kidPrice">
                            $<?=(integer)$firstPackage->getPrice("kid")?>
                        </div>
                        <span class="key">Kids</span>
                    </div>
<!--                    <div class="tandemsCount">-->
<!--                        <div class="inc_dec_wrapper">-->
<!--                            <div class="dec button disabled" data-attribute="tandem"><span></span></div>-->
<!--                            <input type="text" class="tandemCountVal" value="--><?//=$_SESSION[$sessionSelect]->tandem?><!--" pattern="[0-9]*"-->
<!--                                   name="reservationTandemQuantity">-->
<!--                            <div class="inc button" data-attribute="tandem"><span></span><span></span></div>-->
<!--                        </div>-->
<!--                        <div class="tandemCountValWrapper">-->
<!---->
<!--                            <span class="key">Tandem Bikes</span>-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
                </div>
            </div>
            <div class="stepDate step">
                <div class="datepickerInputWrapper">

                   <!-- <input type="text" class="triggerDatepicker" hidden name="reservationDateTime" placeholder="When?" value="<?=$_SESSION[$sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION[$sessionSelect]->datetime)->format('M j, Y g:i a') : ''?>">-->
                    <!--<div class="datepickerCellUI whenSelectorParent"></div>-->
                    <div class="<?=$_SESSION[$sessionSelect]->datetime ? 'placeholderInactive ': 'placeholderActive '?>dateText">
                        <p class="placeholder val gray">Use anytime within 6 months</p>
                        <p class="data whenSelectorResult">Book Online & Save</p>
                    </div>
                    <!--<span class="dropdownArrow">
                            <icon style="background-image:url('<?/*= FRONT_IMG */?>dropdown_arrow.png')"></icon>
                        </span>-->
                </div>
            </div>
            <div class="step finalStep">
                <a class="btn primaryBtn reserveBtn">
                    <p><?=($total = \Model\Cart::getSessionTotal()) > 8? 'Book it <span class="middot"></span> $'.number_format($total,0): 'Starting at $29'?></p>
                </a>
            </div>
        </form>
    </div>
</div>
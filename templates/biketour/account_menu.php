<ul class="nav nav-pills myaccount_nav">
	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && $this->emagid->route['action'] == 'index')?'active':''?>"><a href="<?=SITE_URL?>user">Customer Profile</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'index')?'active':''?>"><a href="<?=SITE_URL?>orders">Order History</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && ($this->emagid->route['action'] == 'payment_methods' || $this->emagid->route['action'] == 'pm_update'))?'active':''?>"><a href="<?=SITE_URL?>user/payment_methods">Payment Information</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && ($this->emagid->route['action'] == 'addresses' || $this->emagid->route['action'] == 'ad_update'))?'active':''?>"><a href="<?=SITE_URL?>user/addresses">Stored Addresses</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && $this->emagid->route['action'] == 'wishlist')?'active':''?>"><a href="<?=SITE_URL?>user/wishlist">Wish List</a></li>
</ul>
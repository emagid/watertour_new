<div class="inventoryFilterBar">
	<div class="content_width_172 row">
		<div class="left_float row">
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="color"><a><span class="lyon_r"><em>Color</em></span> <icon></icon></a></div>
			</div>
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="size"><a><span class="lyon_r"><em>Size</em></span> <icon></icon></a></div>
			</div>
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="heel"><a><span class="lyon_r"><em>Heel Height</em></span> <icon></icon></a></div>
			</div>
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions" id="applyFilter"><a><span class="lyon_r"><em>Apply</em></span></a></div>
			</div>
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions" id="clearFilter"><a><span class="lyon_r"><em><span></span><span></span></em></span><div class="tooltip">Clear All</div></a></div>
			</div>
<!--			<div class="col">-->
<!--				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="price"><a><span class="lyon_r"><em>Price</em></span> <icon></icon></a></div>-->
<!--			</div>-->
		</div>
		<div class="right_float">
			<label class="lyon_r dg"><em>Sort by</em></label>
			<div class="dropdown_ui">
				<?$arr = ['Newest','Best Sellers','Price: High to Low','Price: Low to High']?>
				<div class="visible"><p class="as_r sort_by_main" data-value="0"><?=isset($model->sortBy) && $model->sortBy ? $arr[$model->sortBy]: 'Newest'?></p><icon></icon></div>
				<div class="hidden">
					<?foreach($arr as $i=>$a){
						$active = isset($_GET['sort_by'])? 'selected': 'test';
						?>
						<div class="option <?=$active?>"><p class="as_l sort_by_group" data-value="<?=$i?>"><?=$a?></p></div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
	<div class="filterMenus">
		<div class="content_width_172">
			<div class="filterOption filterOption_color">
				<!-- <div class="right_float">
					<a class="inv_btn tiny_inv_btn">Reset</a>
					<a class="closeFilterMenu cancelIcon">
						<span></span>
					</a>
				</div> -->
				<div class="row">
					<?
//					$count = count($model->filters->colors);
//					$count = ($c = count($model->filters->colors)) < 12 ? $c: 12;
					foreach($model->filters->colors as $color){
//					for($i = 0; $i < $count; $i++){
//						$color = $model->filters->colors[$i];
						if(isset($_GET['color']) && $_GET['color']){$selectable = explode(',',urldecode($_GET['color']));?>
						<a class="col colorSwatchBox">
							<div class="optionBox <?=in_array($color->id,$selectable) ? 'selected': ''?>">
								<div>
									<div class="media" style="background-image:url('<?=UPLOAD_URL?>colors/<?=$color->swatch?>')"></div>
									<h5 class="as_t"><?=$color->name?></h5>
								</div>
								<input type="checkbox" value="<?=$color->id?>" class="colorFilter" <?=in_array($color->id,$selectable) ? 'checked="checked"': ''?>>
							</div>
						</a>
					<?} else {?>
						<a class="col colorSwatchBox">
							<div class="optionBox">
								<div>
									<div class="media" style="background-image:url('<?=UPLOAD_URL?>colors/<?=$color->swatch?>')"></div>
									<h5 class="as_t"><?=$color->name?></h5>
								</div>
								<input type="checkbox" value="<?=$color->id?>" class="colorFilter">
							</div>
						</a>
					<?}?>
					<?}?>
				</div>
			</div>
			<div class="filterOption filterOption_size">
				<!-- <div class="right_float">
					<a class="inv_btn tiny_inv_btn">Reset</a>
					<a class="closeFilterMenu cancelIcon">
						<span></span>
					</a>
				</div> -->
				<div class="row">
					<?foreach($model->filters->size as $size){
						if(isset($_GET['size']) && $_GET['size'] != ''){$sizeList = explode(',',urldecode($_GET['size']));?>
						<a class="col sizeBox">
							<div class="optionBox <?=in_array($size->id,$sizeList) ? 'selected': ''?>">
								<h5 class="as_t"><?=$size->name()?></h5>
								<input type="checkbox" value="<?=$size->id?>" class="sizeFilter" <?=in_array($size->id,$sizeList) ? 'checked="checked"': ''?>>
							</div>
						</a>
					<?} else {?>
						<a class="col sizeBox">
							<div class="optionBox">
								<h5 class="as_t"><?=$size->name()?></h5>
								<input type="checkbox" value="<?=$size->id?>" class="sizeFilter">
							</div>
						</a>
					<?}?>
					<?}?>
				</div>
			</div>
			<div class="filterOption filterOption_heel">
				<!-- <div class="right_float">
					<a class="inv_btn tiny_inv_btn">Reset</a>
					<a class="closeFilterMenu cancelIcon">
						<span></span>
					</a>
				</div> -->
				<div class="row">
					<?foreach($model->filters->heel_height as $key=>$value){
						if(isset($_GET['heel_height']) && $_GET['heel_height'] != ''){$heelList = explode(',',urldecode($_GET['heel_height']))?>
						<a class="col sizeBox">
							<div class="optionBox <?=in_array($key,$heelList) ? 'selected': ''?>">
								<h5 class="as_t"><?=$value?></h5>
								<input type="checkbox" value="<?=$key?>" class="heelFilter" <?=in_array($key,$heelList) ? 'checked="checked"': ''?>>
							</div>
						</a>
					<?} else {?>
						<a class="col sizeBox">
							<div class="optionBox">
								<h5 class="as_t"><?=$value?></h5>
								<input type="checkbox" value="<?=$key?>" class="heelFilter">
							</div>
						</a>
					<? } ?>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
<!-- 		<div class="activeFilters row">
		<div class="content_width_172 row">
			<div class="right_float">
				<a class="inv_btn tiny_inv_btn">Clear All</a>
			</div>
			<div class="activeFiltersBoxesWrapper">
				<label>Selected Filters:</label>
			</div>
		</div>
	</div> -->
</div>
<script>
	$(document).ready(function(){
		$('.inventoryFilterBar').on('touchstart click','.filterOption .optionBox',function(event){
			var colors = [];
			$.each($('.colorFilter:checked'),function(index,item){
				colors.push(item.value);
			});
			var size = [];
			$.each($('.sizeFilter:checked'),function(index,item){
				size.push(item.value);
			});
			var heel = [];
			$.each($('.heelFilter:checked'),function(index,item){
				heel.push(item.value);
			});
			var term = '<?=isset($_GET['term']) ? $_GET['term']: ''?>';
			var sortBy = $(".sort_by_main").data('value');
			var colorStr = colors.join(',');
			var sizeStr = size.join(',');
			var heelStr = heel.join(',');
			var data = {term:term,color:colorStr,size:sizeStr,heel_height:heelStr, sort_by:sortBy};
			$.each(data,function(key,value){
				if(value == ''){
					delete data[key];
				}
			});
			var queryString;
			if($.param(data)){
				queryString = '?'+ $.param(data);
			} else {
				queryString = '';
			}
			window.location.replace('<?=parse_url($this->emagid->uri)['path']?>'+ queryString);
		});

		$('#applyFilter').on('click',function(){
			var colors = [];
			$.each($('.colorFilter:checked'),function(index,item){
				colors.push(item.value);
			});
			var size = [];
			$.each($('.sizeFilter:checked'),function(index,item){
				size.push(item.value);
			});
			var heel = [];
			$.each($('.heelFilter:checked'),function(index,item){
				heel.push(item.value);
			});
			var term = '<?=isset($_GET['term']) ? $_GET['term']: ''?>';
			var sortBy = $(".sort_by_main").data('value');
			var colorStr = colors.join(',');
			var sizeStr = size.join(',');
			var heelStr = heel.join(',');
			var data = {term:term,color:colorStr,size:sizeStr,heel_height:heelStr, sort_by:sortBy};
			$.each(data,function(key,value){
				if(value == ''){
					delete data[key];
				}
			});
			var queryString;
			if($.param(data)){
				queryString = '?'+ $.param(data);
			} else {
				queryString = '';
			}
			window.location.replace('<?=parse_url($this->emagid->uri)['path']?>'+ queryString);
		});
		$('#clearFilter').on('click',function(){
			window.location.replace("<?=parse_url($this->emagid->uri)['path']?>");
		});

		$(".sort_by_group").on('click', function(){
			$(".sort_by_main")
				.text($(this).text())
				.data('value', $(this).data('value'));

		})
	})
</script>
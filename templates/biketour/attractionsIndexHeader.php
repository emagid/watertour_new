<style>
    .ui-autocomplete-loading {
        background: white url("https://media.giphy.com/media/xTk9ZvMnbIiIew7IpW/giphy.gif") right center no-repeat;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <div class="fixedReserveRow fixedReserveRow_AttractionsIndex">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>main_logo.png">
                </a>
            </div>
        </div>
        <div class="show860 fixedReserveTriggerWrapper">
        <? $fixedCartCount = count(\Model\Cart::getActiveCart($model->user ? $model->user->id: 0,session_id()));
        if($fixedCartCount>=1){?>
        
            <a class="primaryBtn btn triggerFixedReserveBox cartIconPresent">Start Booking!</a>
        <?}else{?>
            <a class="primaryBtn btn triggerFixedReserveBox">Start Booking!</a>
        <?}?>
        </div>
        <div class="absHorizCenter reserveBarFixed">
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
        </div>
        <div class="right_float_hidden">
            <? $fixedCartCount = count(\Model\Cart::getActiveCart($model->user ? $model->user->id: 0,session_id()));
            if($fixedCartCount>=1){?>
            <a class="cartLink" href="<?=SITE_URL?>reservation/confirm">
                <span class="cartCount"><?=$fixedCartCount?></span>
                <icon class="cartIcon" style="background-image:url('<?=FRONT_IMG?>cartIcon.png')"></icon>
            </a>
            <?}?>        
            <a class="menuBtn globalSideMenuTrigger">
                <icon>
                    <div class="menu-container">
                        <div class="menu-circle">
                            <div class="line-wrapper">
                                <span class="line"></span>
                                <span class="line"></span>
                                <span class="line"></span>
                            </div>
                        </div>
                    </div>
                </icon>
                <span class="text">
                    <span class="open">Menu</span>
                    <span class="close">Close</span>
                </span>
            </a>
        </div>        
    </div>
    <div class="globalSideMenu">
        <ul class="header_link_list left_float as_r tab sidebar_link_list">
            <?foreach(\Model\Nav::getList(['orderBy'=>'display_order']) as $item){
                $subnav = \Model\Sub_Nav::getList(['where'=>'nav_id = '.$item->id, 'orderBy'=>'display_order'])?>
                <li class="<?= $subnav ? 'triggerNavSubRow' : '' ?> aboutTab">
                    <div>
                        <a href="<?= $item->url ?>"><?= $item->name ?></a>
                    </div>
                    <? if($subnav){ ?>
                    <div class="subNavWrap">
                        <div class="subNavLinksList">
                            <? foreach ($subnav as $sn) { ?>
                                <a href="<?= $sn->url ?>"><?=$sn->name?></a>
                            <? } ?>
                        </div>
                    </div>
                    <? } ?>
                </li>
            <?}?>
            <li>
                <div>
                    <a href="#" class="footerLink">
                        <p><icon style="background-image:url('<?=FRONT_IMG?>locationIcon.png')" class="locationIcon"></icon><span>892 9th Ave., off of 58 St. New York, NY 10019</span></p>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="mailto:info@bikerentalcentralpark.com" class="footerLink">
                        <p><icon style="background-image:url('<?=FRONT_IMG?>mailIcon.png')" class="mailIcon"></icon><span>info@bikerentalcentralpark.com</span></p>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <div class="socialRow row">
                        <div class="col">
                            <a href="#">
                                <icon style="background-image:url(<?=FRONT_IMG?>twitterIcon.png)" class="twitter"></icon>
                            </a>
                        </div>
                        <div class="col">
                            <a href="#">
                                <icon style="background-image:url(<?=FRONT_IMG?>instaIcon.png)" class="insta"></icon>
                            </a>
                        </div>
                        <div class="col">
                            <a href="#">
                                <icon style="background-image:url(<?=FRONT_IMG?>facebookIcon.png)" class="facebook"></icon>
                            </a>
                        </div>
                        <div class="col">
                            <a href="tel:2126640300">
                                <icon style="background-image:url(<?=FRONT_IMG?>phoneIcon.png)"></icon>
                                <p>(212) 664 0300</p>
                            </a>
                        </div>
                    </div>
                </div>
            </li>           
        </ul>
    </div>


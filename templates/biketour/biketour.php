<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <title><?= $this->configs['Meta Title']; ?></title>

        <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">

        <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

        <link rel = "shortcut icon" href = "<?=SITE_URL?>content/frontend/assets/img/favicon.png">
        <meta property="og:title" content="<?= SITE_NAME ?>"/>

        <meta property="og:type" content="website"/>

        <meta property="og:url" content="<?= SITE_URL ?>"/>

        <? require_once('includes.php'); ?>
        <? require_once('includeScripts.php'); ?>

    </head>

    <?
        $bodyClass = $this->emagid->route['controller'];
        $bodyClass2 = $this->emagid->route['action'];
    ?>

    <body class="<?= $bodyClass ?> <?=$bodyClass2?>">
        <div class="mainPageContainer row">
            <div class="responsiveMenuCurtain"></div>

            <? if ($this->emagid->route['controller'] == 'attractions' && $this->emagid->route['action'] == 'index') { ?>
                <?require_once('attractionsIndexHeader.php'); ?>
            <?}else{?>
                <?require_once('header.php'); ?>
            <?}?>
                <?display_notification()?>

                <?php $emagid->controller->renderBody($model); ?>


                <?require_once('footer.php'); ?>
        </div>


    </body>
</html>
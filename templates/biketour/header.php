<style>
    .ui-autocomplete-loading {
        background: white url("https://media.giphy.com/media/xTk9ZvMnbIiIew7IpW/giphy.gif") right center no-repeat;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <div class="mainHeader headroom" id="mainHeader">
        <div class="topRow">
            <div class="gonyUniverse">



                <div class="media tour_red_bg">

                    <div class="topLevelHeader">
                        <div class="leftBlock">
    
                            <h6 class="valB">EASY & FUN HOP-ON, HOP-OFF ADVENTURES</h6>

                        </div>
                        <div class="rightBlock">
                            <img src="<?= FRONT_IMG ?>ph-icon.png">
                            <h6 class="valB"><a href="tel:+12126430080">(212) 643-0080</a></h6>
                        </div>
                    </div>

                </div>
            </div>
        </div>  
        <div class="row middleRow">
            <div class="sub_header">
                <div class="logo">
                    <a href="/">
                        <img src="<?= FRONT_IMG ?>tp_logo_new.png">
                    </a>
                </div>
                <div class="menu">
            <?foreach(\Model\Nav::getList(['orderBy'=>'display_order']) as $item){
                $subnav = \Model\Sub_Nav::getList(['where'=>'nav_id = '.$item->id, 'orderBy'=>'display_order'])?>
                <li class="<?= $subnav ? 'triggerNavSubRow' : '' ?> aboutTab">
                    <div>
                        <h6 class="valB"><a href="<?= $item->url ?>"><?= $item->name ?></a></h6>
                    </div>
                    <? if($subnav){ ?>
                    <div class="subNavWrap">
                        <div class="subNavLinksList">
                            <? foreach ($subnav as $sn) { ?>
                                <h6 class="valB"><a href="<?= $sn->url ?>"><?=$sn->name?></a><h6>
                            <? } ?>
                        </div>
                    </div>
                    <? } ?>
                </li>
            <?}?>
                </div>

            
            <div class="right_float">
                <div class="cart_indicator">
                    <? $cartCount = count(\Model\Cart::getActiveCart($model->user ? $model->user->id: 0,session_id())); ?>
                    <div class="cartCheckoutCol col row float_right">
                        <div class="col cartCol">
                            <a class="cartLink" href="<?=SITE_URL?>reservation/confirm">
                                <span class="cartCount"><?=$cartCount?></span>
                                <icon class="cartIcon" style="background-image:url('<?=FRONT_IMG?>cartIcon1.png')"></icon>
                            </a>
                            <a href="<?=SITE_URL?>checkout/payment" class="btn checkoutBtn orange">
<!--                            <p class="valB">CHECKOUT  </p>-->
                        </a>
                        </div>

                    </div>

                </div>
            </div>
                
                <div class="phoneRow" style="display:none;">
                    <a href="tel:2126430080">
                        <icon style="background-image:url(<?=FRONT_IMG?>phoneIcon.png)"></icon>
                        <p>(212) 643-0080</p>
                    </a>
                </div>
                <div class="socialRow row row_of_3" style="display:none;">
                    <div class="col">
                        <a href="https://twitter.com/GoNewYorkTours">
                            <icon style="background-image:url(<?=FRONT_IMG?>twitterIcon.png)" class="twitter"></icon>
                        </a>
                    </div>
                    <div class="col">
                        <a href="https://www.instagram.com/gony_tours/">
                            <icon style="background-image:url(<?=FRONT_IMG?>instaIcon.png)" class="insta"></icon>
                        </a>
                    </div>
                    <div class="col">
                        <a href="https://www.facebook.com/GoNewYorkTours/">
                            <icon style="background-image:url(<?=FRONT_IMG?>facebookIcon.png)" class="facebook"></icon>
                        </a>
                    </div>
                </div>
            </div>
            <div class="right_float_hidden hidden show770">
                <a class="menuBtn globalSideMenuTrigger">
                    <icon>
                        <div class="menu-container">
                            <div class="menu-circle">
                                <div class="line-wrapper">
                                    <span class="line"></span>
                                    <span class="line"></span>
                                    <span class="line"></span>
                                </div>
                            </div>
                        </div>
                    </icon>
                    <span class="text">
                        <span class="open">Menu</span>
                        <span class="close">Close</span>
                    </span>
                </a>
            </div>
        </div>
    </div>
<?if($_SERVER['REQUEST_URI'] != '/reservation/confirm'){?>
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>main_logo.png">
                </a>
            </div>
        </div>
         <? $cartCount = count(\Model\Cart::getActiveCart($model->user ? $model->user->id: 0,session_id())); 
        if($cartCount>=1){?>
        <div class="show860 fixedReserveTriggerWrapper cartIconPresent">
        <?}else{?>
        <div class="show860 fixedReserveTriggerWrapper">
        <?}?>
            <a class="primaryBtn btn triggerFixedReserveBox">Start Booking!</a>
        </div>
        <div class="absHorizCenter">
            <? if ($this->emagid->route['controller'] == 'rentals') { ?>
                <?include(__DIR__.'/bookingTemplate.php')?>
            <?} else if(($this->emagid->route['controller'] == 'tours' && $this->emagid->route['action'] == 'tour') || ($this->emagid->route['controller'] == 'tours' && $this->emagid->route['action'] == 'reservetour')) { ?>
            <div class="reservationBarWrapper tourDetailReservationBar">
                <div class="container">
                    <form action="/reservation/resRental" method="post" id="book_form">
                        <div class="stepHours step">
                            <div class="timepickerInputWrapper">
                                <div class="loaderBox">
                                    <style type='text/css'>@-webkit-keyframes uil-default-anim {
                                        0% {
                                            opacity: 1
                                        }
                                        100% {
                                            opacity: 0
                                        }
                                    }

                                    @keyframes uil-default-anim {
                                        0% {
                                            opacity: 1
                                        }
                                        100% {
                                            opacity: 0
                                        }
                                    }

                                    .uil-default-css > div:nth-of-type(1) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: -0.5s;
                                        animation-delay: -0.5s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(2) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: -0.4166666666666667s;
                                        animation-delay: -0.4166666666666667s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(3) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: -0.33333333333333337s;
                                        animation-delay: -0.33333333333333337s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(4) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: -0.25s;
                                        animation-delay: -0.25s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(5) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: -0.16666666666666669s;
                                        animation-delay: -0.16666666666666669s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(6) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: -0.08333333333333331s;
                                        animation-delay: -0.08333333333333331s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(7) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: 0s;
                                        animation-delay: 0s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(8) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: 0.08333333333333337s;
                                        animation-delay: 0.08333333333333337s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(9) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: 0.16666666666666663s;
                                        animation-delay: 0.16666666666666663s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(10) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: 0.25s;
                                        animation-delay: 0.25s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(11) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: 0.33333333333333337s;
                                        animation-delay: 0.33333333333333337s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    .uil-default-css > div:nth-of-type(12) {
                                        -webkit-animation: uil-default-anim 1s linear infinite;
                                        animation: uil-default-anim 1s linear infinite;
                                        -webkit-animation-delay: 0.41666666666666663s;
                                        animation-delay: 0.41666666666666663s;
                                    }

                                    .uil-default-css {
                                        position: relative;
                                        background: none;
                                        width: 200px;
                                        height: 200px;
                                    }

                                    </style>
                                    <div class='uil-default-css' style='transform:scale(0.2);'>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
                                        <div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>

                                        </div>
                                    </div>
                                </div>
                                <div class="selectedTourImage media" style="background-image:url('<?=UPLOAD_URL.'tours/'.$model->tour->featuredImage()?>')">
                                    <icon class="checkmark" style="background-image:url('<?=FRONT_IMG?>checkmarkWhite.png')"></icon>

                                </div>
                                <div class="tourName"><h5 class="valB" style="font-size: 18px;font-weight: 500;"><?=$model->tour->name?></h5></div>
                                <input type="hidden" name="reservationTour" value="<?=$model->tour->id?>"/>
                            </div>
                        </div>
                        <div class="step countStep">
                            <div class="adultsCount">
                                <div class="dec button <?=$_SESSION['cart']->adults > 0 ? '': 'disabled'?>"><span></span></div>
                                <div class="adultsCountValWrapper">
                                    <input type="text" class="adultsCountVal" value="<?=$_SESSION['cart']->adults?>" pattern="[0-9]*"
                                           name="reservationQuantity">
                                    <span class="key"><?if(($_SESSION['cart']->adults)==1){?>Adult<?}else{?>Adults<?}?></span>
                                </div>
                                <div class="inc button"><span></span><span></span></div>
                            </div>
                            <div class="kidsCount">
                                <div class="dec button <?=$_SESSION['cart']->kids > 0 ? '': 'disabled'?>"><span></span></div>
                                <div class="kidsCountValWrapper">
                                    <input type="text" class="kidsCountVal" value="<?=$_SESSION['cart']->kids?>" pattern="[0-9]*"
                                           name="reservationKidQuantity">
                                    <span class="key"><?if(($_SESSION['cart']->kids)==1){?>Kid<?}else{?>Kids<?}?></span>
                                </div>
                                <div class="inc button"><span></span><span></span></div>
                            </div>
                        </div>
                        <div class="stepDate step">
                            <div class="datepickerInputWrapper">
                                <input type="text" class="triggerDatepicker" hidden name="reservationDateTime" placeholder="When?" value="<?=$_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : ''?>">
                                <div class="datepickerCellUI whenSelectorParent"
                                     data-hours="<?=implode(',', $model->tour->getHours())?>"
                                     data-week="<?=implode(',', $model->tour->getWeek())?>"
                                     data-day="<?=implode(',', $model->tour->getDay())?>">
                                </div>
                                <div class="<?=$_SESSION[$model->sessionSelect]->datetime ? 'placeholderInactive ': 'placeholderActive '?>dateText">
                                    <p class="placeholder val gray">When?</p>
                                    <p class="data whenSelectorResult"><?=$_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : ''?></p>
                                </div>
                                <span class="dropdownArrow">
                                    <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
                                </span>
                            </div>
                        </div>
                        <div class="step finalStep">
                            <a class="btn primaryBtn reserveBtn">
                                <p><?=($total = \Model\Cart::getSessionTotal()) ? 'Book this Tour &middot; $'.number_format($total,0): 'Starting at $'.floatval($model->tour->getPrice())?></p>
                            </a>
                        </div>
                    </form>
                </div>
            </div>

            <?}else if($this->emagid->route['controller'] == 'packages' && $this->emagid->route['action'] == 'reservepackage'){}else{?>
                <?include (__DIR__.'/bookingTemplate.php')?>
            <?}?>
        </div>
        <div class="right_float_hidden">
            <? $fixedCartCount = count(\Model\Cart::getActiveCart($model->user ? $model->user->id: 0,session_id()));
            if($fixedCartCount>=1){?>
            <a class="cartLink" href="<?=SITE_URL?>reservation/confirm">
                <span class="cartCount"><?=$fixedCartCount?></span>
                <icon class="cartIcon" style="background-image:url('<?=FRONT_IMG?>cartIcon.png')"></icon>
            </a>
            <?}?>
            <a class="menuBtn globalSideMenuTrigger">
                <icon>
                    <div class="menu-container">
                        <div class="menu-circle">
                            <div class="line-wrapper">
                                <span class="line"></span>
                                <span class="line"></span>
                                <span class="line"></span>
                            </div>
                        </div>
                    </div>
                </icon>
                <span class="text">
                    <span class="open">Menu</span>
                    <span class="close">Close</span>
                </span>
            </a>
        </div>
    </div>
<?}?>
    <div class="globalSideMenu">
        <ul class="header_link_list left_float as_r tab sidebar_link_list">
            <?foreach(\Model\Nav::getList(['orderBy'=>'display_order']) as $item){
                $subnav = \Model\Sub_Nav::getList(['where'=>'nav_id = '.$item->id, 'orderBy'=>'display_order'])?>
                <li class="<?= $subnav ? 'triggerNavSubRow' : '' ?> aboutTab">
                    <div>
                        <a href="<?= $item->url ?>"><?= $item->name ?></a>
                    </div>
                    <? if($subnav){ ?>
                    <div class="subNavWrap">
                        <div class="subNavLinksList">
                            <? foreach ($subnav as $sn) { ?>
                                <a href="<?= $sn->url ?>"><?=$sn->name?></a>
                            <? } ?>
                        </div>
                    </div>
                    <? } ?>
                </li>
            <?}?>
                <div class="cart_indicator">
                    <? $cartCount = count(\Model\Cart::getActiveCart($model->user ? $model->user->id: 0,session_id())); ?>
                    <div class="cartCheckoutCol col row">
                        <div class="col cartCol">
                            <a class="cartLink" href="<?=SITE_URL?>reservation/confirm">
                                <span class="cartCount"><?=$cartCount?></span>
                                <icon class="cartIcon" style="background-image:url('<?=FRONT_IMG?>cartIcon.png')"></icon>
                            </a>
                            <a href="<?=SITE_URL?>checkout/payment" class="btn checkoutBtn orange">
                            CHECKOUT 
                        </a>
                        </div>

                    </div>

                </div>

            <div class="sideMenuSocial">
                <li><a href="https://www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </div>
            <li>
                <div>
                    <a href="#" class="footerLink">
                        <p><icon style="background-image:url('<?=FRONT_IMG?>locationIcon.png')" class="locationIcon"></icon><span>2 East 42nd Street New York, NY 10017</span></p>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="mailto:info@topviewtours.com" class="footerLink">
                        <p><icon style="background-image:url('<?=FRONT_IMG?>mailIcon.png')" class="mailIcon"></icon><span>info@topviewtours.com</span></p>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <div class="socialRow row">

                        <div class="col">
                            <a href="tel:2126640300">
                                <icon style="background-image:url(<?=FRONT_IMG?>phoneIcon.png)"></icon>
                                <p>(212) 664 0300</p>
                            </a>
                        </div>
                    </div>
                </div>
            </li>           
        </ul>
    </div>
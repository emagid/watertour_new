<div id="responsiveMenu">
	<div class="mainLinksListWrapper">
		<ul>
			<li>
				<a href="/"><h3 class="as_r">Home</h3></a>
			</li>
				<? foreach ($model->categories as $mainCategory) { ?>
					<li>
						<div class="accordionUI accordionUI_leftSidebar">
							<a class="visible">
								<h3 class="as_r"><?=$mainCategory->name?></h3>
							</a>

							<div class="hidden">
								<ul>
									<li class="leftSidebar_emph">
										<a href="<?="/collections/{$mainCategory->slug}/all"?>">All <?=$mainCategory->name?></a>
									</li>
									<?php $sql = "select * from subnav where active = 1 and category_id = {$mainCategory->id} order by column_num ASC, display_order ASC;";
									$subCat = \Model\Subnav::getList(['sql' => "$sql"]);
									$list = []; ?>
									<? foreach ($subCat as $category) {
										$category = \Model\Category::getItem($category->sub_category_id); ?>
										<li>
											<a href="<?= SITE_URL . "collections/$mainCategory->slug/$category->slug" ?>">
												<h5 class="as_l"><?= $category->name ?></h5>
											</a>
										</li>
									<? } ?>
								</ul>
							</div>
						</div>
					</li>
				<? } ?>
			<li>
				<a href="/privatelabel"><h3 class="as_r">Private Label</h3></a>
			</li>
			<li>
				<a href="/nycstore"><h3 class="as_r">NYC Store</h3></a>
			</li>
		</ul>
	</div>
	<div class="separator"></div>
	<div class="footer">
		<div>
			<a class="sidebar_view_toggle" id="responsiveMenuAccountLink" data-sidebar_view="account"><i class="material-icons">account_box</i><p class="as_l">My Account</p></a>
			<a href="tel:212 777 1851" class="telStyle"><i class="material-icons">local_phone</i><p class="as_l">212 777 1851</p></a>
		</div>
	</div>
</div>
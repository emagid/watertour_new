<div class="mod-container">
    <div class="mod-content">
        <span class="close-mod">&times;</span>
        <form action="/quotes/add" method="post" id="submit_form">
            <input hidden name="redirect" value="<?=$this->emagid->uri?>">
        <h1 class="valB">Submit your Quote</h1>
            <div class="mod-form">
                <div class="mod-element">
                    <input type="text" name="first_name" placeholder="First Name*" required style="width: 100%;">
                </div>
                <div class="mod-element">
                    <input type="text" name="last_name" placeholder="Last Name*" required style="width: 100%;">
                </div>
                <div class="mod-element">
                    <input type="text" name="company" placeholder="Company / Organization*" required style="width: 100%;">
                </div>
                <div class="mod-element">
                    <input type="email" name="email" placeholder="Email Address*" required style="width: 100%;">
                </div>
                <div class="mod-element">
                    <input type="text" name="phone" placeholder="Phone Number*" required style="width: 100%;">
                </div>
                <div class="mod-element">
                    <input type="textarea" name="comment" placeholder="Comment" style="width: 100%;">
                </div>
                <div class="mod-element">
                    <input type="textarea" name="route" placeholder="Route"  style="width: 100%;">
                </div>

            </div>
            <div class="mod-form">
                <div class="mod-element">
                    <input type="text" name="pickup_location" placeholder="Pick-Up Location*" style="width: 100%;">
                </div>

                <div class="mod-element element_float">
                    <input type="text" name="pickup_date" id="pickup_date" placeholder="Pick-Up Date*" required style="width: 49%;">

                    <input type="text" name="pickup_time" id="pickup_time" placeholder="Pick-Up Time*" required style="width: 49%;">
                </div>

                <div class="mod-element">
                    <input type="text" name="dropoff_location" placeholder="Drop-Off Location*" style="width: 100%;">
                </div>

                <div class="mod-element element_float">
                    <input type="text" name="dropoff_date" id="dropoff_date" placeholder="Drop-Off Date*" required style="width: 49%;">

                    <input type="text" name="dropoff_time" id="dropoff_time"placeholder="Drop-Off Time*" required style="width: 49%;">
                </div>

                <div class="mod-element">
                    <input type="number" name="guest" placeholder="Number of Guests*" style="width: 100%;">
                </div>

                <div class="mod-element">
                    <input type="text" name="language_request" placeholder="Specific Language Request" style="width: 100%;">
                </div>

                <div class="mod-element">
                    <input type="text" name="special_request" placeholder="Special Request" style="width: 100%;">
                </div>
            </div>
            <div class="mod-button">
                <input type="submit" value="Submit Request" class="btn primaryBtn">
            </div>

        </form>
    </div>
</div>
<div class="mod-container-a">
    <div class="mod-content">
        <span class="close-mod">&times;</span>
            <iframe name="NR4BranchIFrame" id="NR4BranchIFrame" style="width:100%;height:1000px;border-style:none;" src="https://nr4.me/GoNewYork"></iframe>
    </div>
</div>

<div class="mainFooterWrapper">
	<div class="footerSiteIndexSection">
		<div class="left_float row row_of_3">
            <div class="col">
                <h6 class="valB gray" id="contact-us"><a href="/about/location">Contact Us</a></h6>
                <ul class="footer_links_list">
                    <li>
                        <div>
                            <a class="footerLink">
                                <p>New York Water Tours<br> 2 East 42nd Street<br> New York, NY 10017<br> (212) 643-0080</p>
                            </a>
                        </div>
                    </li>
                    <hr class="styleone">
                    <li>
                        <div>
                            <a class="footerLink">
                                <p>Looking for groups and charters?</p>
                            </a>
                            <h6 class="valB gray request-quote">Request A Quote</h6>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col">
                <h6 class="valB gray feedback" id="feedback">Feedback</h6>
                <ul class="footer_links_list social_links_list">

                    <li><a href="https://www.facebook.com/newyorkwatertours" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/nycwater_tours" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
<!--                    <li><a href="https://www.pinterest.com/TopViewNYC/topview-nyc/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>-->
                    <li><a href="https://www.instagram.com/newyorkwatertours/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
<!--                    <li><a href="https://plus.google.com/110776737645195721264" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>-->

                </ul>
            </div>
        </div>
        <div class="right_float">
                <div class="col">
                    <h6 class="valB gray">Site Navigation</h6>
                                <?foreach($model->footer as $item){?>
                    <ul class="footer_links_list">
                        <?foreach($item->children as $child){?>
                            <li>
                                <div>
                                    <a href="<?=$child->url?>" class="footerLink"><?=$child->name?></a>
                                </div>
                            </li>
                        <?}?>
                    </ul>
                </div>
            <?}?>

<!--             <div>
                <div>
                    <a class="copyright footer_text">&#xa9; Bike Rental Central Park</a>
                    <//?foreach($model->pages as $page){?>
                        <span class="middot">&#xb7;</span>
                        <a class="legal footer_text" href="<//?="/pages/$page->slug"?>"><//?=$page->title?></a>
                    <//?}?>
                </div>
            </div> -->
        </div>
	</div>
</div>
<script>
    $(document).ready(function(){
        $('.request-quote').on('click',function(){
            $('.mod-container').show();
        });
        $('.close-mod').on('click',function(){
            $('.mod-container').hide();
        });
        window.onclick = function(event) {
            if (event.target == $('.mod-container')[0]) {
                $('.mod-container').hide();
            }
        };

        /*$('.submit-quote').on('click',function(){
            var firstname = $('[name=fname]').val();
            var lastname = $('[name=lname]').val();
            var email = $('[name=email]').val();
            var phone = $('[name=phone]').val();
            var company = $('[name=company]').val();
            var comment = $('[name=comment]').val();
            var route = $('[name=route]').val();
            var pickup_date = $("[name=pudate]").val();
            var pickup_time = $("[name=putime]").val();
            var pickup_location = $('[name=pulocation]').val();
            var dropoff_date = $("[name= dodate]").val();
            var dropoff_time = $("[name=dotime]").val();
            var dropoff_location = $('[name=dolocation]').val();
            var guest = $('[name=guests]').val();
            var language_request = $('[name=langrequest]').val();
            var special_request = $('[name=sprequest]').val();

            $.post('/quotes/add',{
                first_name:firstname,
                last_name:lastname,
                email:email,
                phone:phone,
                company:company,
                route:route,
                comment:comment,
                pickup_date:pickup_date,
                pickup_time:pickup_time,
                pickup_location:pickup_location,
                dropoff_date:dropoff_date,
                dropoff_time:dropoff_time,
                dropoff_location:dropoff_location,
                guest:guest,
                language_request:language_request,
                special_request:special_request
            },function(ret){
                alert(ret.msg);
            });
        })*/
    })
</script>

<script>
    $(document).ready(function() {
        $("#pickup_date").datepicker({
           dateFormat: 'yy-m-d'
        });
    });
    $('#pickup_time').timepicker({ 'timeFormat': 'h:mm:ss p'  });

    $(document).ready(function() {
        $("#dropoff_date").datepicker({
            dateFormat: 'yy-m-d'
        });
    });
    $('#dropoff_time').timepicker({ 'timeFormat': 'h:mm:ss p' });

</script>

<script>
    $(document).ready(function(){
        $('.feedback').on('click',function(){
            $('.mod-container-a').show();
        });
        $('.close-mod').on('click',function(){
            $('.mod-container-a').hide();
        });
        window.onclick = function(event) {
            if (event.target == $('.mod-container-a')[0]) {
                $('.mod-container-a').hide();
            }
        };
        $('.submit-quote').on('click',function(){
            var name = $('[name=name]').val();
            var email = $('[name=email]').val();
            var quote = $('[name=quote]').val();
            $.post('/quotes/add',{name:name,email:email,content:quote},function(ret){
                alert(ret.msg);
            });
        })
    })
</script>
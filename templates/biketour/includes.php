<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?= FRONT_LIBS ?>jquery.sticky.js"></script>

<? if ($this->emagid->route['controller'] == 'packages' || $this->emagid->route['controller'] == 'attractions' || $this->emagid->route['controller'] == 'tours') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>packages.css">
    <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>datepaginator.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<? } ?>

<link rel="stylesheet" type="text/css" href="<?= FRONT_CSS ?>core.css">
<link rel="stylesheet" type="text/css" href="<?= FRONT_CSS ?>topview.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?= FRONT_CSS ?>coreResponsive.css">
<link rel="stylesheet" type="text/css" href="<?= FRONT_LIBS ?>slick/slick.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?= FRONT_LIBS ?>flickity.css">
<!-- PACKET MANAGER FOR imagesLoaded() npm install imagesloaded -->

<link href="https://vjs.zencdn.net/5.9.2/video-js.css" rel="stylesheet">


<script src="<?=FRONT_JS?>jssor.slider-23.1.1.min.js"></script>

<? if ($this->emagid->route['controller'] == 'account') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>account.css">
<? } ?>

<? if ($this->emagid->route['controller'] == 'products') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>product.css">
<? } ?>

<? if (($this->emagid->route['controller'] == 'cart')||($this->emagid->route['controller'] == 'checkout')) { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>checkoutFlow.css">
<? } ?>

<div class="mdl mdl_mv mdl_plApplication" id="mdl_plApplication">
	<div class="header">
		<div class="left_float"><h2 class="as_l mdl_title">Want to Partner with Bike Tour?</h2></div>
		<div class="right_float">
			<a id="mdl_exit">
				<icon>
					<span></span>
					<span></span>
				</icon>
				<p>Cancel the Application</p>
			</a>
		</div>
	</div>
	<div class="formWrapper">
		<form id="inquiry_form" action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1469287000001235007 method='POST' enctype='multipart/form-data' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>
			<!-- Do not remove this code. -->
			<input type='text' style='display:none;' name='xnQsjsdp' value='326d81381f3fed83cfe208f3030d4f0412b473a9775fc1518fc3092147ffa642'/>
			<input type='hidden' name='zc_gad' id='zc_gad' value=''/>
			<input type='text' style='display:none;' name='xmIwtLD' value='77121bf2c836b1d0f897b12946e4ac9296197f2928384d4b70a05e160bcbccde'/>
			<input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>

			<input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;<?=$_SERVER['SERVER_NAME']?>&#x2f;privatelabel' />
			<!-- Do not remove this code. -->
			<div class="labelInputWrapper">
				<label for="name">Name</label>
				<input type="text" id="name" name="Last Name" placeholder="John Smith" required>
				<span></span>
			</div>
			<div class="labelInputWrapper">
				<label for="email">Email</label>
				<input type="email" id="email" name="Email" required>
				<span></span>
			</div>
			<div class="labelInputWrapper">
				<label for="phone">Phone</label>
				<input type="tel" id="phone" name="Phone" required>
				<span></span>
			</div>
			<div class="labelInputWrapper">
				<label for="estimatedBudget">Estimated Budget</label>
				<input type="number" id="estimatedBudget" name="Annual Revenue" required>
				<span></span>
			</div>
			<div class="labelInputWrapper">
				<label for="photo">Upload a Photo</label>
				<input type="file" id="photo" name="theFile">
				<span></span>
			</div>
			<div class="labelInputWrapper">
				<label for="desc">Description</label>
				<textarea id="desc" name="Description"></textarea>
				<span></span>
			</div>
			<div class="submitRow">
				<input type="submit" class="btn" value="Submit">
			</div>
		</form>
	</div>
</div>

<script>
	var mndFileds=new Array('Last Name');
	var fldLangVal=new Array('Name');
	var name='';
	var email='';

	$('#inquiry_form').on('submit',function(){
		var name = $('#name').val();
		var email = $('input[name=Email]').val();
		var phone = $('input[name=Phone]').val();
		var estimated_budget = $('#estimatedBudget').val();
		var featured_image = $('#photo').val();
		var description = $('#desc').val();
		var data = {name:name,email:email,phone:phone,estimated_budget:estimated_budget,featured_image:featured_image,description:description};
		$.post('/privatelabel/submitInquiry',data,function(){
			alert('Thank you for your submission!');
		});	
	});

	function checkMandatory() {
		for(i=0;i<mndFileds.length;i++) {
			var fieldObj=document.forms['WebToLeads1469287000001235007'][mndFileds[i]];
			if(fieldObj) {
				if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
					if(fieldObj.type =='file')
					{
						alert('Please select a file to upload');
						fieldObj.focus();
						return false;
					}
					alert(fldLangVal[i] +' cannot be empty');
					fieldObj.focus();
					return false;
				}  else if(fieldObj.nodeName=='SELECT') {
					if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
						alert(fldLangVal[i] +' cannot be none');
						fieldObj.focus();
						return false;
					}
				} else if(fieldObj.type =='checkbox'){
					if(fieldObj.checked == false){
						alert('Please accept  '+fldLangVal[i]);
						fieldObj.focus();
						return false;
					}
				}
				try {
					if(fieldObj.name == 'Last Name') {
						name = fieldObj.value;
					}
				} catch (e) {}
			}
		}
		return validateFileUpload();

	}
	function validateFileUpload(){
		var uploadedFiles = document.getElementById('theFile');
		var totalFileSize =0;
		if(uploadedFiles.files.length >3){
			alert('You can upload a maximum of 3 files at a time.');
			return false;
		}
		if ('files' in uploadedFiles) {
			if (uploadedFiles.files.length != 0) {
				for (var i = 0; i < uploadedFiles.files.length; i++) {
					var file = uploadedFiles.files[i];
					if ('size' in file) {
						totalFileSize = totalFileSize + file.size;
					}
				}
				if(totalFileSize > 20971520){
					alert('Total File(s) size should not exceed 20 MB.');
					return false;
				}
			}
		}
	}
</script>
<div class="mdl mdl_mv mdl_newItems" id="mdl_collection">
    <div class="header">
		<div class="left_float">
			<div class="categoryShareRow productShareRow">
				<ul>
					<li>
						<div>
							<a href="https://www.facebook.com/watertour" class="facebook footer_social_icon" style="background-image:url(<?=FRONT_IMG?>facebook_icon.png)"></a>
						</div>
					</li>
					<li>
						<div>
							<a href="https://twitter.com/watertour" class="twitter footer_social_icon" style="background-image:url(<?=FRONT_IMG?>twitter_icon.png)"></a>
						</div>
					</li>
					<li>
						<div>
							<a href="https://www.instagram.com/watertour/" class="instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>instagram_icon.png)"></a>
						</div>
					</li>
					<li>
						<div>
							<a href="https://www.instagram.com/watertour/" class="pinterest instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>pinterest_icon.png)"></a>
						</div>
					</li>
				</ul>
			</div>
		</div>
        <div class="right_float">
            <a id="mdl_exit">
                <icon>
                    <span></span>
                    <span></span>
                </icon>
                <p>Close</p>
            </a>
        </div>
        <h2 class="as_l mdl_title">Latest <?= $model->category == 'all' ? 'All':$model->category->name ?> <span><?=count($model->new_arrivals)?></span></h2>
    </div>
    

    <div class="noGutter_content_width product_inventory_gridui">
        <div class="row row_of_4 product_grid_ui">
            <? if($model->new_arrivals) {
                foreach ($model->new_arrivals as $product) { ?>
                    <div class="col productGridItem">
                        <a href="/products/<?= $product->slug ?>">
                            <div class="mediaWrapper">
                                <div class="media" style="background-image:url(<?= UPLOAD_URL . 'products/' . $product->featuredImage() ?>)"></div>
                            </div>
                            <div class="dataWrapper">
                                <h4 class="product_name"><?= $product->name ?></h4>
                                <h4 class="product_price">
                                    <? if ($product->msrp > 0) {
                                        $percent = round(($product->price - $product->msrp) * 100 / $product->price) ?>
                                        <span class="full_price">
                                        <span class="currency">$</span><?= number_format($product->price, 2) ?>
                                    </span>
                                        <span class="markdown">-<?= $percent ?>%</span>
                                        <span class="value">$<?= number_format($product->msrp, 2) ?></span>
                                    <? } else { ?>
                                        $<?= number_format($product->price, 2) ?>
                                    <? } ?>
                                </h4>
                            </div>
                        </a>
                    </div>
                <? } ?>
            <? } else {?>
                <div>No new arrivals</div>
            <? } ?>
        </div>
    </div>
</div>
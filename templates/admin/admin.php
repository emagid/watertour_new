<?php
$logged_admin = $model->logged_admin;
$admin_sections = $model->admin_sections;
$flash = isset($model->flash) ? $model->flash : [];
$title = (isset($model->meta_title) ? $model->meta_title . " | " : "") . "Admin | Bike Tour";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    

    <?php
    if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == 'localhost') {
        define('WP_ENV', 'development');
    } else {
        define('WP_ENV', 'production');
    }
    if (WP_ENV == 'development') { ?>
        <?php css('main.css', ADMIN_CSS); ?>
        <!--<link rel="stylesheet" href="<?= ADMIN_CSS ?>main.css">-->
    <?php } else { ?>
        <?php css('main.css', ADMIN_CSS); ?>
        <!--<link rel="stylesheet" href="<?= ADMIN_CSS ?>main.min.css">-->
    <?php } ?>

    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js"></script>

    <script>
        WebFont.load({
            google: {families: ['Noto Sans:400,700,400italic,700italic', 'Roboto Slab:400,300,700']}
        });
    </script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= FRONT_CSS ?>core.css">
</head>
<body class="skin-default skin-sbe fixed">
<header class="header">
    <a class="navbar-brand logo" href="<?= ADMIN_URL ?>">
        <img style="width: 90%!important;background-color: #7f8cc1!important;" src="<?= FRONT_IMG ?>main_logo.png" alt="" class="logotop">
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="navbar-btn sidebar-toggle icon-left-open-mini" data-toggle="offcanvas"
           role="button" <?php if ($logged_admin == null) {
            echo "style='display:none'";
        } ?>>
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>


        <?php if ($logged_admin != null) { ?>
        <div style="padding-top:27px; font-size:20px;">
            <?= count(\Model\Order::getList(["where" => "viewed != 'TRUE'"])) ?> <a href="<?= ADMIN_URL ?>orders">New
                Orders</a> |
            <? } ?>


            <div class="navbar-right">

                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <?php if ($logged_admin != null) { ?>
                            <a style="margin-top: -27px;" href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span><?= $logged_admin->full_name(); ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?= ADMIN_URL . 'login/logout'; ?>" class="btn btn-default btn-flat">Sign
                                            out</a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                    </li>
                </ul>
            </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <aside class="left-side sidebar-offcanvas" <?php if ($logged_admin == null) {
        echo "style='display:none'";
    } ?> >
        <section class="sidebar">
            <ul class="sidebar-menu">
                <?if($logged_admin && $logged_admin->username == 'emagid'){?>
                    <li>
                        <a href="<?= ADMIN_URL . 'dashboard/index'; ?>">
                            <i class="fa fa-home">
                                <span>Dashboard</span>
                            </i>
                        </a>
                    </li>
                <?}?>
                <?php

                $compareTo = $this->emagid->route['controller'];
                $compareTo = str_replace('_', ' ', $compareTo);

                switch ($compareTo) {
                    case 'users':
                        $compareTo = 'customer accounts';
                        break;
                    case 'pages':
                        $compareTo = 'cms';
                        break;
                    case 'banners':
                        $compareTo = $this->emagid->route['action'];
                        $compareTo = str_replace('_', ' ', $compareTo);
                        switch ($compareTo) {
                            case 'main update':
                                $compareTo = 'main';
                                break;
                            case 'featured update':
                                $compareTo = 'featured';
                                break;
                            case 'deal of the week update':
                                $compareTo = 'deal of the week';
                                break;
                        }
                        break;
                    case 'configs':
                        $compareTo = 'configurations';
                        break;
                }

                foreach ($admin_sections as $parent => $children):
                    $parentLowerCase = strtolower($parent);
                    $parentLowerCase = str_replace(' ', '_', $parentLowerCase);
                    ?>
                    <li id="<?php echo $parentLowerCase; ?>"
                        class="admin_sections <?php echo (count($children) > 0) ? 'treeview' : ''; ?> <?php //echo ((stripos($_SERVER['REQUEST_URI'],$parentLowerCase) !== false)?'active':'')
                        ?> active">
                        <a href="<?php echo (count($children) > 0) ? '#' : ADMIN_URL . $parentLowerCase; ?>"><i
                                class="fa fa-cog"><span><?php echo $parent; ?></span></i></a>
                        <?php if (count($children) > 0) { ?>
                            <ul class="treeview-menu" style='display:block;'>
                                <?php foreach ($children as $child):
                                    $childLowerCase = strtolower($child);
                                    $childLowerCase = str_replace(' ', '_', $childLowerCase);

                                    if ($child == 'Users') {
                                        $label = 'Customer Accounts';
                                    } else if ($child == 'Pages') {
                                        $label = 'CMS';
                                    } else if ($child == 'Configs') {
                                        $label = 'Configurations';
                                    } else if ($child == 'Navs') {
                                        $label = 'Main Navigation';
                                    } else if ($child == 'Sub Navs') {
                                        $label = 'Sub Navigation';
                                    } else if ($child == 'Equipments') {
                                        $label = 'Equipment';
                                    } else {
                                        $label = $child;
                                    }
                                    ?>
                                    <li id="<?php echo $childLowerCase; ?>"
                                        class="admin_sections <?php echo((strtolower($label) == $compareTo) ? 'active' : '') ?>">
                                        <a href="<?= ADMIN_URL . (($parent == 'Banners') ? 'banners' . DS : '') . (($parent == 'Elements') ? 'elements' . DS : '') . $childLowerCase; ?>"><?php echo $label; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </aside>
    <aside class="right-side" <?php if ($logged_admin == null) {
        echo "style='margin-left:0'";
    } ?>>
        <? $static = ""; ?>
        <? if (($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'update') || ($this->emagid->route['controller'] == 'products' && $this->emagid->route['action'] == 'update')) {
            $static = 'style="position: fixed;min-width: 100%;"';
        } ?>
        <section class="content-header navbar-static-top" <?= $static ?>  <?php if ($logged_admin == null) {
            echo "style='display:none'";
        } ?>>
            <?php if (isset($model->hasCreateBtn) and $model->hasCreateBtn) { ?>
                <div class="btn-group">
                    <?
                    $updateUrl = ADMIN_URL . $emagid->route['controller'] . '/update';
                    if ($updateUrl == ADMIN_URL . 'banners/update') {
                        $updateUrl = ADMIN_URL . 'banners/' . $emagid->route['action'] . '_update';
                    }
                    if($emagid->route['controller'] == 'orders'){
                        $updateUrl = ADMIN_URL .'orders/create_order';
                    }
                    ?>
                    <a class="btn btn-default" href="<?php echo $updateUrl; ?>">
                        Create new&nbsp;
                        <i class="icon-plus"></i>
                    </a>
                </div>
            <?php } ?>
            <h1>
                <?= isset($model->page_title) ? $model->page_title : ""; ?> <? if ($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'update') {
                    ?>#<?= $model->order->id ?> / <?= date("M d Y  g:ia", strtotime($model->order->insert_time)) ?> / <?= $model->order->status ?>

                <? } ?>

                <? if ($this->emagid->route['controller'] == 'products' && $this->emagid->route['action'] == 'update') {
                    ?>

                    <? if ($model->product->id > 0) {
                        ?>MPN:<?= $model->product->mpn ?> / Price $<?= $model->product->price ?>  MSRP $<?= $model->product->msrp ?><? } ?>

                <? } ?>
            </h1>
        </section>

        <? if (($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'update')) {
            echo '  <section class="content" style="padding: 70px 15px;">';
        } else {
            echo '  <section class="content">';
        } ?>

        <? if ($this->emagid->route['controller'] == 'products' && $this->emagid->route['action'] == 'update') {
            echo '  <section class="content" style="padding: 70px 15px;">';
        } else {
            echo '  <section class="content">';
        } ?>
        <? display_notification(); ?>
        <div id="custom_notifications"></div>

        <?php $emagid->controller->renderBody($model); ?>
        </section>
    </aside>
</div>

<?php function footer()
{ ?>

    <?php script('jquery.min.js', ADMIN_JS); ?>
    <?php
    if (WP_ENV == 'development') { ?>
        <?php script('main.js', ADMIN_JS); ?>

    <?php } else { ?>
        <?php script('main.min.js', ADMIN_JS); ?>

    <?php } ?>
    <?php script('ckeditor/ckeditor.js', ADMIN_JS); ?>
    <?php script('Chart.js', ADMIN_JS); ?>

    <?php
} ?>

<script type='text/javascript'>
    function slug_async(in_elem, out_elem) {
        in_elem.on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            out_elem.val(val.toLowerCase());
        });
    }

    var params = <?php echo (isset($model->params)) ? json_encode($model->params) : json_encode((object)array()); ?>;
    // in case result is an array, change it to object
    if (params instanceof Array) {
        params = {};
    }

    $(document).ready(function () {


        /**
         * builds a url with params from a params object passed to it
         * @param {type} url: url of page
         * @param {type} params: params object with key as param key name and value as param value
         * @param {type} redirect: true or false if we want to redirect to the url
         * @returns {Boolean|String}
         */

        <?if ($this->emagid->route['controller'] == 'orders' || $this->emagid->route['controller'] == 'products'){?>
        function build_url(url, params, redirect) {

            var params_arr = [];
            $.each(params, function (i, e) {
                params_arr.push(i + "=" + e);
            });
            if (redirect) {
                window.location.href = url + "" + params_arr.join("&");
                return false;
            } else {
                return url + "?" + params_arr.join("&");
            }
        }

        <?} else {?>
        function build_url(url, params, redirect) {

            var params_arr = [];
            $.each(params, function (i, e) {
                params_arr.push(i + "=" + e);
            });
            if (redirect) {
                window.location.href = url + "?" + params_arr.join("&");
                return false;
            } else {
                return url + "?" + params_arr.join("&");
            }
        }

        <? }?>


        if (typeof total_pages !== 'undefined' && typeof page !== 'undefined') {
            // the variable is defined

            $(function () {
                $('div.paginationContent').pagination({
                    pages: total_pages,
                    currentPage: page,
                    cssStyle: 'light-theme',
                    onPageClick: function (pageNumber, event) {
                        var url_params = params || {};
                        url_params.page = parseInt(pageNumber);
                        var full_url = site_url;
                        build_url(full_url, url_params, true);
                        //window.location.href = full_url+"?page="+page;
                    }
                });
            });
        }
        /*$('div.paginationContent').paginate({
         count: total_pages,
         start: page,
         text_color:'#333',
         background_color:'#FFF',
         text_hover_color:'#333',
         background_hover_color:'#FFF',
         border: true,
         border_color: '#CCC',
         border_hover_color: '#333',
         images:false,
         mouse:'press',
         onChange: function(){
         var page = parseInt($('.jPag-current').html());
         var full_url = site_url;
         window.location.href = full_url+"?page="+page;
         }
         });    */
    });
</script>
</body>
</html>

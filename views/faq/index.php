<div class="pageWrapper faqPageWrapper cmsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
            <!-- <h7 class="valB">Reserve online for up to <span>60% off!</span></h7> -->
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php');?>
        </div>
    </div>
    <h1 class="uppercase valB big">FAQ</h1>
    <div class="questionsSeachWrapper">
        <input type="text" class="searchFaqField" id="searchFaqField" placeholder="Search frequently asked questions">
<!--         <p class="placeholder"><icon style="background-image:url('<?=FRONT_IMG?>searchIcon.png')"></icon><span></span></p> -->
    </div>

    <div class="qa_wrapper">
    <?$i=0; foreach($model->faqs as $fc=>$faq){ $i++; ?>
        <div class="row row_of_3">
            <div class="panel-group" id="accordion">

                
            <div class="panel-heading">
            <h6 class="panel-title" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1<?=$i?>"><?= $fc ?>
            </a>
            </h6>
            </div>
            <div id="collapse1<?=$i?>" class="panel-collapse">
                <div class="panel-body">     
            <? foreach ($faq as $outerFaq) {
                foreach ($outerFaq as $f) { ?>
                            <div class="col">
                            <h4 class="futura futura_m"><?= $f->question ?></h4>

                            <p class="futura navy"><?= $f->answer ?></p>
                            </div>
                <? } ?>
            <? } ?>
                </div>
                </div>
            </div>
        </div>
    <? } ?>
    </div>
</div>
</div>
</div>
<script>
    /*$(document).ready(function(){
        $('.searchFaqField').on('keyup',function(){
            var val = $(this).val();
            var qUI = $('.questionUI');
            $.each(qUI,function(i,e){
                var jE = $(e);
                var ques = jE.find('.futura_m');
                var ans = jE.find('.navy');
                if(ques.html().toLowerCase().indexOf(val) >= 0 || ans.html().toLowerCase().indexOf(val) >= 0){
                    jE.parent('.col').show();
                } else {
                    jE.parent('.col').hide();
                }
            });
        })
    })*/
    $("#searchFaqField").keyup(function(){

        // Retrieve the input field text
        var filter = $(this).val();

        // Loop through the comment list
        $(".col").each(function(){

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();

                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).fadeIn();
            }
        });
    });
</script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
$("h6.panel-title > a").click(function(){
    $("img.open-sesame", this).toggle();
    $("img.close-shut", this).toggle();
});

</script>
<script type="text/javascript">
    $(function(){
        $("#accordion").multiAccordion({ header: "h6", collapsible: true, active: false });
    });
</script>


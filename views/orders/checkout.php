<div class="banner cms">
	<div class="container">
		<div class="inner">
			<h1>Checkout</h1>
		</div>
	</div>
</div>
<div class="cart">
	<div class="container">
		<form action="<?=$this->emagid->uri?>" method="POST">
			<div class="row">



				<!-- Billing Info -->
				<div class="col-sm-11" id="bill_data">
					<h3>Billing Address</h3>
					
					<? if (!is_null($model->payment_profiles) && count($model->payment_profiles) > 0) { ?>
						<div class="form-group">
							<div class="custom-select">
								<select name="payment_profile" id="payment_profiles" class="form-control">
									<option value="">SAVED BILLING ADDRESSES</option>
									<? foreach($model->payment_profiles as $payment_profile) { ?>
										<option value="<?=$payment_profile->id?>"><?=(!is_null($payment_profile->label)?$payment_profile->label:$payment_profile->address)?></option>
									<? } ?>
								</select>
							</div>
						</div>
					<? } ?>

					<div class="form-group">
						<div class="custom-select">
							<select name="bill_country" id="country" class="form-control">
								<option value="">COUNTRY</option>
								<? foreach(get_countries() as $key=>$country) { ?>
								<?if(isset($_SESSION['post'])){ if($_SESSION['post']['bill_country']==$key){$selected = "selected";}else{$selected="";}}?>
									<option value="<?=$key?>" <?if(isset($_SESSION['post'])){echo $selected;}?>><?=$country?></option>
								<? } ?>
							</select>
						</div>
					</div>

									<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<input name="bill_first_name" id="bill_first_name" type="text" class="form-control" placeholder="First Name" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['bill_first_name'];}?>"/>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<input name="bill_last_name" type="text" class="form-control" placeholder="Last Name" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['bill_last_name'];}?>"/>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input name="bill_address" type="text" class="form-control" placeholder="Address" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['bill_address'];}?>" />
					</div>
					<div class="form-group">
						<input name="bill_address2" type="text" class="form-control" placeholder="Address 2" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['bill_address2'];}?>"  />
					</div>
					<div class="form-group">
						<input name="bill_city" type="text" class="form-control" placeholder="City/Town" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['bill_city'];}?>"/>
					</div>
					<div class="row checkout_location">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="custom-select">
									<select name="bill_state" id="state" class="form-control">
										<option value="">STATE</option>
										<? foreach(get_states() as $key=>$state) { ?>
										<?if(isset($_SESSION['post'])){ if($_SESSION['post']['bill_state']==$key){$selected = "selected";}else{$selected="";}}?>
											<option value="<?=$key?>" <?if(isset($_SESSION['post'])){echo $selected;}?>><?=$state?></option>
										<? } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<input name="bill_zip" type="text" class="form-control" placeholder="ZIP"  value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['bill_zip'];}?>"/>
							</div>
						</div>
					</div>
				</div> 

				<!-- Shipping Info -->
				<div class="col-sm-2"></div>
				<div class="col-sm-11" id="ship_data">
					<!--a href="#" id="clone">ASD</a-->
					<label class="pull-right">
						<!-- <input id="same_button" type="checkbox"> <em>Same as Billing? </em> -->
						<a href="#" id="clone">
							<button class="btn-primary"> Same as Billing? (click here)</button>
						</a>
					</label>
					
					<h3>Shipping Address</h3>

					<? if (!is_null($model->addresses) && count($model->addresses) > 0) { ?>
						<div class="form-group">
							<div class="custom-select">
								<select name="shipping_address" id="shipping_addresses" class="form-control">
									<option value="">SAVED SHIPPING ADDRESSES</option>
									<? foreach($model->addresses as $address) { ?>
										<option value="<?=$address->id?>"><?=((!is_null($address->label) && $address->label != "")?$address->label:$address->address);?></option>
									<? } ?>
								</select>
							</div>
						</div>
					<? } ?>

					<div class="form-group">
						<div class="custom-select">
							<select name="ship_country" id="country" class="form-control">
								<option value="">COUNTRY</option>
								<? foreach(get_countries() as $key=>$country) { ?>
								<?if(isset($_SESSION['post'])){ if($_SESSION['post']['ship_country']==$key){$selected = "selected";}else{$selected="";}}?>
									<option value="<?=$key?>" <?if(isset($_SESSION['post'])){echo $selected;}?>><?=$country?></option>
								<? } ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<input name="ship_first_name" id="ship_first_name"type="text" class="form-control" placeholder="First Name" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['ship_first_name'];}?>"/>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<input name="ship_last_name" type="text" class="form-control" placeholder="Last Name" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['ship_last_name'];}?>"/>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input name="ship_address" type="text" class="form-control" placeholder="Address" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['ship_address'];}?>"/>
					</div>
					<div class="form-group">
						<input name="ship_address2" type="text" class="form-control" placeholder="Address 2" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['ship_address2'];}?>"/>
					</div>
					<div class="form-group">
						<input name="ship_city" type="text" class="form-control" placeholder="City/Town" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['ship_city'];}?>"/>
					</div>
					
					<div class="row checkout_location">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="custom-select">
									<select name="ship_state" id="state" class="form-control">
										<option value="">STATE</option>
										<? foreach(get_states() as $key=>$state) { ?>
											<?if(isset($_SESSION['post'])){ if($_SESSION['post']['ship_state']==$key){$selected = "selected";}else{$selected="";}}?>
											<option value="<?=$key?>" <?if(isset($_SESSION['post'])){echo $selected;}?>><?=$state?></option>
										<? } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<input name="ship_zip" type="text" class="form-control" placeholder="ZIP" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['ship_zip'];}?>"/>
							</div>
						</div>
					</div>
				</div>


				<div class="col-sm-24">
					<br /><br /><br />
					<hr />
				</div>
			</div> 



			<div class="row">
				<div class="col-sm-11">
					<h3>Payment Method</h3>
					<div class="form-group payment_method">
						<ul class="list-unstyled">
							<li><label for="s1"><input type="radio"  checked="checked" name="payment_method" value="1" id="s1" />Credit Card</label></li>
							<div class="form-group">
								<input name="cc_number" id="cc_number" type="text" class="form-control" placeholder="Card Number" maxlength="16"/>
							</div>
						</ul>
					</div>


					<div class="row checkout_date">
						<div class="col-sm-24 checkout_date">
							<h4>Expiration Date</h4>
						</div>
						<div class="col-sm-24 col-md-8 expiration_field">
							<div class="form-group">
								<div class="custom-select">
									<select name="cc_expiration_month" id="cc_expiration_month" class="form-control">
										<option value="">MONTH</option>
										<? foreach(get_month() as $key=>$month) { ?>
											<option value="<?=$key?>"><?=$key.' / '.$month?></option>
										<? } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-24 col-md-8 expiration_field">
							<div class="form-group">
								<div class="custom-select">
									<select name="cc_expiration_year" id="cc_expiration_year" class="form-control">
										<option value="">YEAR</option>
										<? foreach(range(date('Y'), date('Y')+9) as $year){ ?>
											<option value="<?=$year?>"><?=$year?></option>
										<? } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-24 col-md-8 expiration_field">
							<div class="form-group">
								<input name="cc_ccv" id="cc_ccv" type="text" class="form-control" placeholder="CCV" maxlength="3"/>
								<span class="ccv"><a tabindex="0" role="button" class="link-blue" data-toggle="popover" data-placement="left" data-trigger="focus" title="CCV #" data-content="The CCV number is a 3 digit code printed on the BACK of your card, for American Express, the CCV is a 4 digit code on the front.">?</a></span>
							</div>
						</div>




					<div class="col-sm-18">
						<div class="cc_icons">
							<img src="/content/frontend/img/visa_icon.png" alt="Pay with Visa">
							<img src="/content/frontend/img/mc_icon.png" alt="Pay with MasterCard">
							<img src="/content/frontend/img/amex_icon.png" alt="Pay with American Express">
							<img src="/content/frontend/img/discover_icon.png" alt="Pay with Discover">
							<img src="/content/frontend/img/wire_icon.png" alt="Pay via Wire">
						</div>
					</div>



						<div class="form-group bank_wire">
						<ul class="list-unstyled">
							<li><label for="s2"><input type="radio" name="payment_method" value="2" id="s2" />Bank Wire</label></li>
							<p>You will be contacted by a representative shortly after placing order.</p>
						</ul>
					</div>
						<div class="us-shipping-group">
						<h3>Shipping Options</h3>
						<div class="form-group">
							<ul id="shipping-list" class="list-unstyled">
								<?
								$selected_method = $model->shipping_method;
								foreach($model->shipping_methods as $shipping_method) {
									?>
									<li><label for="m<?=$shipping_method->id?>" style="display:<?=$shipping_method->international == false?'block':'none';?>" ><input <?=($shipping_method->id == $selected_method)?'checked="checked"':'';?> type="radio" class="shipping-item" name="shipping" value="<?=$shipping_method->id?>" data-price="<?=$shipping_method->cost?>" />$<?=$shipping_method->cost?> <?=$shipping_method->name?></label></li>
								<? } ?>
							</ul>
						</div>
						</div>
					</div>
				</div>

				<div class="col-sm-2"></div>

				<div class="col-sm-11">

					
					<div class="row" style="margin-bottom:15px;">
						<h3>Contact</h3>
						<? if ($model->guest_checkout) { ?>
							<div class="form-group">
								<input name="email" type="email" class="form-control" placeholder="Email" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['email'];}?>"/>
							</div>
						<? } ?>
						<div class="form-group">
							<input name="phone" type="text" class="form-control" placeholder="Phone Number" value="<?if(isset($_SESSION['post'])){echo $_SESSION['post']['phone'];}?>"/>
						</div>
						<div class="form-group">
							<textarea name="comment" type="text" class="form-control" placeholder="Comment"></textarea>
							 
						</div>
					</div>
					

					<h3>Order Summary</h3>

					<div class="checkout-products">
						<? foreach($model->order_products as $order_product) { ?>
						<div class="row">
							<div class="col-sm-18">
								<?=$order_product->product->name?>
								<small><?=$order_product->product->mpn?></small>
								<small><?php $final_sale = $order_product->product->final_sale; 
								If ($final_sale==1){echo 'This is final sale!';} else{}?>
								</small>
								<small><?php $day_delivery = $order_product->product->day_delivery; If ($day_delivery==1){echo 'Ships in 1 business day';} else{}?></small>
							</div>
							<div class="col-sm-6 text-right">
								<?if(isset($_SESSION['price'])){
				 		$code= $_SESSION['code'];
						$new_price = \Model\Price_Alert::getItem(null,['where'=>" code =  '$code'"]);
						if($order_product->product->id == $new_price->product_id){
							$price = $new_price->price*$order_product->quantity;
						}else{
							$price = $order_product->product->price*$order_product->quantity;
						}
						}
						else{
							$price = $order_product->product->price*$order_product->quantity;
						}?>
								$<?=number_format($price, 2)?>
								<small><?=$order_product->quantity?></small>
							</div>
						</div>
						<? } ?>
					</div>

					<table class="table table-cart table-checkout">
						<tbody>
							<tr>
								<th>Subtotal:</th>
									<?if(isset($_SESSION['price'])){
				 		$code= $_SESSION['code'];
				 		$subtotal = 0;
				 		foreach($model->order_products as $order_product) { 

				 		$new_price = \Model\Price_Alert::getItem(null,['where'=>" code =  '$code'"]);
						if($order_product->product->id == $new_price->product_id){
							$subtotal += $new_price->price*$order_product->quantity;
						}else{
							$subtotal += $order_product->product->price*$order_product->quantity;
						}
				 		}
						
						}
						else{
							$subtotal = $model->order->subtotal;
						}?>
								<td>$<span id="subtotal-value"><?=number_format($subtotal, 2)?></span></td>
								<input type="hidden" id="subtotal" value="<?=$subtotal?>" disabled="disabled" />
							</tr>
							<tr>
								<th>Savings:</th>
								<td>$<?=$model->savings?></td>
								<? if (!is_null($model->coupon)) { ?>
									<input type="hidden" name="coupon_code" value="<?=$model->coupon->code?>" />
								<? } ?>
								<input type="hidden" id="coupon_amount" value="<?=$model->savings?>" disabled="disabled" />
							</tr>
							<tr>
								<th>Shipping:</th>
								<td>$<span id="shipping-value"><?=number_format($model->order->shipping_cost, 2)?></span></td>
								<input type="hidden" name="shipping_method" value="<?=$model->order->shipping_method?>" />
								<input type="hidden" id="shipping_cost" value="<?=$model->order->shipping_cost?>" disabled="disabled" />
							</tr>
							<tr>
								<th>Tax:</th>
								<td>$<span id="tax-amount">0.00</span></td>
							</tr>
						</tbody>
					</table>

					<table class="table table-cart table-checkout">
						<tbody>
							<tr>
								<th><h3>Grand Total</h3></th>
									<?if(isset($_SESSION['price'])){
				 		$code= $_SESSION['code'];
				 		$total = 0;
				 		foreach($model->order_products as $order_product) { 

				 		$new_price = \Model\Price_Alert::getItem(null,['where'=>" code =  '$code'"]);
						if($order_product->product->id == $new_price->product_id){
							$total += $new_price->price*$order_product->quantity;
						}else{
							$total += $order_product->product->price*$order_product->quantity;
						}
				 		}
						$total = $total+$model->order->shipping_cost+$model->savings;
						}
						else{
							$total = $model->order->total;
						}?>
								<td><h3>$<span id="grand-total-value"><?=number_format($total, 2)?></span></h3></td>
							</tr>
						</tbody>
					</table>

					<div class="form-group">
						<ul class="list-unstyled">
							<li>
								<label for="terms">
									<input type="checkbox" name="terms" id="terms" />I accept the <a href="<?=SITE_URL?>page/terms-conditions" target="_blank">Terms & Conditions</a>
								</label>
							</li>
						</ul>
					</div>

					<button type="submit" class="btn btn-primary btn-block">Place Order</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?=FRONT_JS?>plugins/jquery.number.min.js"></script>

<script>
	$(function(){
		$('#country').on('change',function(){
			var country = $(this).find('option:selected').val();
			if(country == "0" || country == "1" || country == '') {
				<?foreach($model->shipping_methods as $shipping_method){
					if($shipping_method->international == false){?>
						$("label[for=m<?=$shipping_method->id?>]").show();
					<?} else {?>
						$("label[for=m<?=$shipping_method->id?>]").hide();
					<?}?>
				<?}?>
			} else {
				<?foreach($model->shipping_methods as $shipping_method){
					if($shipping_method->international == true){?>
						$("label[for=m<?=$shipping_method->id?>]").show();
					<?} else {?>
						$("label[for=m<?=$shipping_method->id?>]").hide();
					<?}?>
				<?}?>
			}
		});
		$('#country').trigger('change');
		$('input.shipping-item').on('ifChecked', function(){
			var value = $(this).attr('data-price');
			var id = $(this).val();
			$('#shipping-value').text($.number(value, 2));
			$('#grand-total-value').text($.number(parseFloat($('#subtotal-value').html().replace(/,/g,'')) + value*1, 2));
			$.ajax({
				url: '<?=SITE_URL?>cart/updateShipping',
				data: 'id='+id
			})
		});
		$('input[name=cc_number]').focusout(function(){
			sessionStorage.setItem('cc_number',$(this).val());
		});
		$('input[name=cc_ccv]').focusout(function(){
			sessionStorage.setItem('cc_ccv',$(this).val());
		});
		$('select[name=cc_expiration_month]').change(function(){
			sessionStorage.setItem('cc_expiration_month',$(this).find('option:selected').val());
		});
		$('select[name=cc_expiration_year]').change(function(){
			sessionStorage.setItem('cc_expiration_year',$(this).find('option:selected').val());
		});

		function getCC(){
			<?$input = ['cc_number','cc_ccv'];
			$select = ['cc_expiration_month','cc_expiration_year'];
			foreach($input as $item){?>
				if(sessionStorage.getItem("<?=$item?>") != null){
					$("#<?=$item?>").val(sessionStorage.getItem("<?=$item?>"));
				}
			<?}
			foreach($select as $item){?>
				if(sessionStorage.getItem("<?=$item?>") != null){
					var num = sessionStorage.getItem("<?=$item?>");
					$("#<?=$item?>").find('option[value='+num+']').prop('selected',true);
				}
			<?}?>
		}
		getCC().trigger();
		
		var billing_addresses = <?=json_encode($model->payment_profiles);?>;
		$('#payment_profiles').change(function(){
			for (key in billing_addresses){
				if (billing_addresses[key].id == $(this).val()){
					$('input[name="bill_first_name"]').val(billing_addresses[key].first_name);
					$('input[name="bill_last_name"]').val(billing_addresses[key].last_name);
					$('input[name="bill_address"]').val(billing_addresses[key].address);
					$('input[name="bill_address2"]').val(billing_addresses[key].address2);
					$('input[name="bill_city"]').val(billing_addresses[key].city);
					$('input[name="bill_zip"]').val(billing_addresses[key].zip);

					$('select[name="bill_country"] option').each(function(){
						if ($(this).text() == billing_addresses[key].country){
							$(this).attr('selected', 'selected');
						} else {
							$(this).removeAttr('selected');
						}
					})
					$('select[name="bill_state"] option').each(function(){
						if ($(this).val() == billing_addresses[key].state){
							$(this).attr('selected', 'selected');
						} else {
							$(this).removeAttr('selected');
						}
					})
				}
			};
		});

		var shipping_addresses = <?=json_encode($model->addresses);?>;
		$('#shipping_addresses').change(function(){
			for (key in shipping_addresses){
				if (shipping_addresses[key].id == $(this).val()){
					$('input[name="ship_first_name"]').val(shipping_addresses[key].first_name);
					$('input[name="ship_last_name"]').val(shipping_addresses[key].last_name);
					$('input[name="ship_address"]').val(shipping_addresses[key].address);
					$('input[name="ship_address2"]').val(shipping_addresses[key].address2);
					$('input[name="ship_city"]').val(shipping_addresses[key].city);
					$('input[name="ship_zip"]').val(shipping_addresses[key].zip);

					$('select[name="ship_country"] option').each(function(){
						if ($(this).text() == shipping_addresses[key].country){
							$(this).attr('selected', 'selected');
						} else {
							$(this).removeAttr('selected');
						}
					})
					$('select[name="ship_state"] option').each(function(){
						if ($(this).val() == shipping_addresses[key].state){
							$(this).attr('selected', 'selected');
						} else {
							$(this).removeAttr('selected');
						}
					})
					$('select[name="ship_state"]').change();
				}
			};
		});

		$('select[name="ship_state"]').change(function(){
			var tax = 0;
			if($(this).val() == 'NY'){
				tax = ($('#subtotal').val()-$('#coupon_amount').val().replace(/,/g,'').replace(/\$/g,'').replace(/%/g,''))*(8.775/100);
			}
			$('#tax-amount').html($.number(tax, 2));
			var total = $('#subtotal').val()*1 - $('#coupon_amount').val().replace(/,/g,'').replace(/\$/g,'').replace(/%/g,'')*1 + $('#shipping_cost').val()*1 + tax*1;
			$('#grand-total-value').text($.number(total,2));
		});

		var subtotal = $('#subtotal');
		$('#s2').on('ifChecked', function(){
			subtotal.val(subtotal.val()*0.98);
			subtotal.prev().find('#subtotal-value').text($.number(subtotal.val(),2));
			$('select[name="ship_state"]').change();
		});
		$('#s2').on('ifUnchecked', function(){
			subtotal.val(<?=$subtotal?>);
			subtotal.prev().find('#subtotal-value').text($.number(subtotal.val(),2));
			$('select[name="ship_state"]').change();
		});
	})
</script>




































<div class="banner cms myaccount_banner">
	<div class="container">
		<div class="inner">
			<h1>My Account</h1>
		</div>
	</div>
</div>

<div class="container container_myaccount">
	<div class="myaccount_table nav_table">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/account_menu.php'); ?>
			<? if (count($model->orders) > 0) { ?>
			<div class="account_info">
				<table class="table table-products">
					<thead>
						<tr>
							<th width="25%">Date</th>
							<th width="16%">Order #</th>
							<th width="31%">Order Details</th>
							<th width="16%">Total</th>
						</tr>
					</thead>
				    <? foreach ($model->orders as $order) { ?>
						<tbody>
							<tr>
								<td><?=$order->date?></td>
								<td><?=$order->id?></td>
								<td><p><a href="<?=SITE_URL.'order/'.$order->id?>">View Receipt</a></p></td>
								<td><?=number_format($order->total,2)?></td>
							</tr>
						</tbody>
					<? } ?>
				</table>
			</div>
			<? } else { ?>
			<h4>No orders found on your account</h4>
			<? } ?>			
	</div><!-- .account_info -->
</div><!-- .table-responsive -->
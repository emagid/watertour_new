<div class="pageWrapper cartPageWrapper">
	<div class="row content_width_1000">
		<div class="col bagItemsCol">
			<div class="header">
				<h1 class="as_l">Shopping Bag</h1>
				<div class="right_float">
					<?if(isset($model->cart->cart)){?>
						<a class="btn_black btn" href="<?='/checkout/email'?>">Proceed to Checkout</a>
					<?} else {?>
						<a class="btn_black btn" href="<?='/'?>">Check out our catalog</a>
					<?}?>
				</div>
			</div>
			<div class="bagItemsListWrapper">
				<ul>
					<?if(isset($model->cart->cart) && $model->cart){
					foreach($model->cart->cart as $cart){
						$product = \Model\Product::getItem($cart->product_id);
						$color = $cart->getColor()?>
					<li>
						<div class="row" id="<?=$cart->id?>">
							<div class="col bagItemImg">
								<a>
									<img src="<?=UPLOAD_URL.'products/'.$cart->featuredImage()?>">
									<p class="as_l"><a href="<?='/products/'.$product->slug?>">Go to Product Page</a></p>
								</a>
							</div>
							<div class="col bagItemInfo">
								<h6 class="as_l bagItemInfoName"><?=$product->name?></h6>
								<?$arr = \Model\Variation::$lowerVariations;
								foreach(json_decode($cart->variation,true) as $key=>$value){
									if(in_array($key,$arr) && $value){?>
									<div class="bagItemInfoItem row row_of_2">
										<div class="col">
											<h8 class="as_l"><?=$key?>:</h8>
										</div>
										<div class="col">
											<h8 class="as_l">
												<?$ucKey = ucfirst($key);
												$mode = "\Model\\$ucKey"?>
												<?=class_exists($mode) ? $mode::getItem($value)->name(): $value?>
											</h8>
										</div>
									</div>
								<?}?>
								<?}?>
								<h8 class="as_l bagItemInfoName expandQuantityOptions">Quantity:<input class="quantityValue" type="number" name="quantity" min="1" max="5" value="1"><icon class="expansion_icon"></icon></h8>
							</div>
							<div class="col bagItemPrice">
								<h6 class="as_l bagItemInfoPrice">$<?=$product->getPrice($color)?></h6>
								<div class="footer">
									<a class="btn btn_oreo" href="<?='/cart/remove_product/'.$cart->id?>">Remove</a>
								</div>
							</div>
							
						</div>
					</li>
					<?}?>
					<?}?>
				</ul>
<!-- 				<p class="as_r">For orders including at least one item not manufactured in our NYC factory, you can only choose standard or express shipping.</p> -->
			</div>
			<div class="orderTotalsWrapper">
<!-- 				<div class="promoInputWrapper inputBtnUI">
					<label class="inputLabel" for="promoCode">Promotional Code</label>
					<input type="text" id="promoCode" class="whiteTxtInput" placeholder="Promotional Code">
					<button class="btn btn_black">Apply</button>
				</div> -->
				<div class="orderTotalsData">
					<div class="row row_right">
						<div class="col orderDataLabel">
							<p>Subtotal:</p>
						</div>
						<div class="col orderDataValue">
							<p>$<?=number_format($model->cart->total,2)?></p>
						</div>
					</div>
					<div class="row row_right">
						<div class="col orderDataLabel">
							<p>Estimated Sales Tax:</p>
						</div>
						<div class="col orderDataValue">
							<p>To be Calculated</p>
						</div>
					</div>
					<div class="row row_right">
						<div class="col orderDataLabel">
							<p>Shipping:</p>
						</div>
						<div class="col orderDataValue">
							<p>To be Calculated</p>
						</div>
					</div>
					<div class="row row_right finalTotal">
						<span class="triangleBorder"></span>
						<div class="col orderDataLabel">
							<p>Estimated Total</p>
						</div>
						<div class="col orderDataValue">
							<p>$<?=number_format($model->cart->total,2)?></p>
						</div>
					</div>
				</div>
			</div>
			<?if(isset($model->products) && count($model->products) > 0){?>
				<a class="btn btn_black btn_full_width" href="<?='/checkout/email'?>">Proceed to Checkout</a>
			<?}?>
		</div>
		<div class="shoppingInfoCol col">
			<div class="labelDataBox">
				<label>Delivery and Returns</label>
				<p>You can choose from different delivery service levels:</p>
				<p><b>Standard Shipping</b>4-8 working days<br>Complimentary</p>
				<p><b>Express Shipping</b>2-4 working days<br>$20.00</p>
				<span class="line_sep"></span>
				<p><b>Easy Returns</b>Returns service: you have 20 days from delivery to follow our quick and easy return procedure.</p>
			</div>
			<div class="labelDataBox">
				<label>Secure Payments</label>
				<p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
				<img src="<?=FRONT_IMG?>creditCardOptions.png">
			</div>
			<div class="labelDataBox">
				<label>Gift Option</label>
				<p>Gift wrap your order, removing all prices, and add a greeting card.</p>
			</div>
			<div class="labelDataBox">
				<label>Customer Care</label>
				<p>You can reach us by:</p>
				<p><b>Phone</b>877 804 7645</p>
				<p><b>Toll-Free</b>M-F 9AM-9PM ET</p>
				<p><b>Email and Chat</b><a>Send a Message</a></p>
			</div>
		</div>
	</div>
</div>

<style>
    @media only screen and (max-width: 385px){
        .reservationDetails .summaryDetailsItem.labelInputWrapper h5.valB{
            width: 100%;
        }
        .reservationDetails .summaryDetailsReservation .selectedTourImage.media{
            display: none;
        }
    }
</style>
<div class="loading-overlay">
    <div id="white-loading-img"></div>
</div>
<div class="pageWrapper paymentPageWrapper">

	<div class="row">
		<div class="col checkoutFormCol">
			<div class="contentWidth">
				<h1 class="uppercase valB big">Checkout</h1>
				<div class="show860 responsiveCheckoutSummary">
					<h3 class="medium_big">Reservation Summary</h3>
					<div class="checkoutSummaryData">
						<div class="summaryDataItem">
							<h5 class="valB uppercase gray"><span>Sub-total:</span>$<?=intval(number_format($model->subtotal,2))?></h5>
						</div>
						<div class="summaryDataItem">
							<h5 class="valB uppercase gray"><span>Tax:</span>$<?=number_format($model->tax,2)?></h5>
						</div>
						<div class="summaryDateItem discountInputWrapper">
							<input type="text" placeholder="Coupon code">
							<a class="primaryBtn btn"><icon class="rightChev" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon></a> 
						</div>
						<div class="summaryDataItem total">
							<h4 class="valB uppercase gray"><span>Total:</span>$<?=number_format($model->total,2)?></h4>
						</div>
					</div>
				</div>				
				<div class="formWrapper">
					<form action="/checkout/payment_post" method="post" id="paymentForm">
						<div class="formSection row row_of_2">
							<!--SHOULD ALREADY BE FILLED OUT FOR THE MOST PART-->
							<div class="col">
								<h3 class="valB gray medium">Contact Info</h3>
								<div class="labelAboveInputWrapper">
									<label for="emailAddress">Email Address</label>
									<input type="email" name="email" id="emailAddress" placeholder="name@domain.com" value="<?=isset($_SESSION['email']) && $_SESSION['email'] ? $_SESSION['email']: '';?>" data-ErrorText="Please enter your email address">
									<span class="validationSuccess"></span>
									<span class="validationError"></span>
								</div>
								<div class="labelAboveInputWrapper">
									<label for="fullName">Full Name</label>
									<input type="text" name="fullName" id="fullName" placeholder="John Smith" value="<?=isset($_SESSION['fullName']) && $_SESSION['fullName'] ? $_SESSION['fullName']: '';?>" data-ErrorText="Please enter your full name">
									<span class="validationSuccess"></span>
									<span class="validationError"></span>
								</div>
								<div class="labelAboveInputWrapper">
									<label for="phoneNumber">Phone Number <span>(optional)</span></label>
									<input type="tel" id="phoneNumber" name="phone" placeholder="(XXX)-XXX-XXXX" value="<?=isset($_SESSION['phone']) && $_SESSION['phone'] ? $_SESSION['phone']: '';?>">
									<span class="validationSuccess"></span>
									<span class="validationError"></span>
								</div>
							</div>
							<div class="col reservationSummaryNested">
								<h3 class="valB gray medium">Reservation Summary</h3>
								<div class="checkoutSummaryData">
									<div class="summaryDataItem subtotalItem">
										<h5 class="valB uppercase gray"><span>Sub-total:</span>$<?=intval(number_format($model->subtotal,2))?></h5>
									</div>
									<div class="summaryDataItem taxItem">
										<h5 class="valB uppercase gray"><span>Tax:</span>$<?=number_format($model->tax,2)?></h5>
									</div>
									<div class="summaryDataItem couponItem" <?=$model->discount ? '': 'style="display: none;"'?>>
										<h5 class="valB uppercase gray"><span>Coupon:</span>-$<?=number_format($model->discount,2)?></h5>
									</div>
									<div class="summaryDateItem discountInputWrapper couponInput" <?=$model->discount ? 'style="display: none;"':''?>>
										<input type="text" placeholder="Coupon code" id="couponCode">
										<a class="primaryBtn btn addCoupon"><icon class="rightChev" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon></a>
									</div>
									<div class="summaryDataItem total">
										<h4 class="valB uppercase gray"><span>Total:</span>$<?=number_format($model->total,2)?></h4>
									</div>
								</div>
							</div>
						</div>
						<div class="formSection">
							<h3 class="valB gray medium">Payment Info<span class="cardsWeAccept"><h6 class="valB uppercase">We accept</h6> <icon style="background-image:url('<?=FRONT_IMG?>creditCardOptions.png')"></icon></span> <span class="secureTransaction"><p><em>Secure Transaction</em><icon style="background-image:url('<?=FRONT_IMG?>checkoutLockIcon.png')"></icon></p></span></h3>
							<div class="labelAboveInputWrapper creditCardInputWrapper">
								<label for="ccnumber">Credit Card Number</label>
								<input type="tel" name="cc_number" id="ccnumber" maxlength="16" tabindex="4" placeholder="" data-ErrorText="Please enter your credit card number">
								<input type="hidden" id="cc_type" name="cc_type">
								<div id="creditCardIcons">
		                            <div class="media"></div>
		                        </div>
								<span class="validationSuccess"></span>
								<span class="validationError"></span>
							</div>
							<div class="labelAboveInputWrapper">
								<label for="creditCardName">Name on Card</label>
								<input type="text" name="card_name" id="creditCardName" placeholder="" data-ErrorText="Please enter your name as it appears on your credit card">
								<span class="validationSuccess"></span>
								<span class="validationError"></span>
							</div>
							<div class="row">
								<div class="labelAboveInputWrapper col expDateCol">
									<label for="expirationDate1">Expiration Date</label>
									<input type="tel" id="expirationDate1" name="cc_expiration_month" placeholder="MM" maxlength="2" data-ErrorText="Please enter the expiration date month">
									<span class="slash">/</span>
									<input type="tel" id="expirationDate2" name="cc_expiration_year" placeholder="YY" maxlength="2" data-ErrorText="Please enter the expiration date year">
									<span class="validationSuccess"></span>
									<span class="validationError"></span>
								</div>
								<div class="labelAboveInputWrapper col cvvCol">
									<label for="securityCode">CVV <div class="cvvHelperWrapper"><span class="cvvHelperTrigger"><p>?</p></span><div class="cvvHelperBox"><p>CVV stands for Credit Card Verification Value. The CVV is a 3 or 4 digit code embossed or imprinted on the signature panel on the reverse side of Visa, MasterCard and Discover cards and on the front of American Express cards. This code is used as an extra security measure to ensure that you have access and/or physical possession of the credit card itself.</p></div></div></label>
									<input type="tel" id="securityCode" name="cc_ccv" placeholder="" maxlength="4" data-ErrorText="Please enter your credit card's security code">
									<span class="validationSuccess"></span>
									<span class="validationError"></span>
								</div>
							</div>
						</div>
						<div class="formSection paymentBtnSection">
							<p class="val gray">By clicking "Submit Payment" below, you agree to our<br>terms of service and privacy policy.</p>
							<div class="btnWrapper">
								<a class="btn primaryBtn btnBig paymentBtn">Submit Payment</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col checkoutSummaryCol">
			<div class="rightColResSummaryWrapper">
				<h3 class="medium_big">Reservation Summary</h3>
				<div class="checkoutSummaryData">
					<div class="summaryDataItem">
						<h5 class="valB uppercase gray"><span>Sub-total:</span>$<?=intval(number_format($model->subtotal,2))?></h5>
					</div>
					<div class="summaryDataItem">
						<h5 class="valB uppercase gray"><span>Tax:</span>$<?=number_format($model->tax,2)?></h5>
					</div>
					<div class="summaryDateItem discountInputWrapper">
						<input type="text" placeholder="Coupon code">
						<a class="primaryBtn btn"><icon class="rightChev" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon></a> 
					</div>
					<div class="summaryDataItem total">
						<h4 class="valB uppercase gray"><span>Total:</span>$<?=number_format($model->total,2)?></h4>
					</div>
				</div>
			</div>
			<?$increment = 1;
			foreach($model->cart as $cart){?>
				<div class="reservationDetails">
				<div class="summaryDetailsReservation">
					<p>Reservation #<?=$increment?></p>
					<div class="summaryDetailsItem labelInputWrapper">
						<label>Which activity?</label>
                        <?if($cart->tour_id) {
                            $tour = \Model\Tour::getItem($cart->tour_id); ?>
                            <div class="tourSelection">
                                <div class="selectedTourImage media">
                                    <icon class="checkmark" style="background-image:url('<?= FRONT_IMG ?>checkmarkWhite.png')"></icon>
                                </div>
                                <input type="hidden" value="<?=$tour->id?>">
                                <h5 class="valB"><?=$tour->name?></h5>
                            </div>
                        <? } ?>
                        <?if($cart->duration) {
                            $rental = \Model\Rental::getItem($cart->duration); ?>
                            <div class="tourSelection">
<!--
                                <div class="selectedTourImage media" style="background-image:url('<?= '/' ?>')">
                                    <icon class="checkmark" style="background-image:url('<//?= FRONT_IMG ?>checkmarkWhite.png')"></icon>
                                </div>
-->
                                <input type="hidden" value="<?=$rental->id?>">
                                <h5 class="valB"><?=$rental->name?></h5>
                            </div>
                        <? } ?>
                        <?if($cart->package_id) {
                            $package = \Model\Package::getItem($cart->package_id);
							$details = $cart->package_details ? json_decode($cart->package_details,true): [];?>
                            <div class="tourSelection">
<!--
                                <div class="selectedTourImage media" style="background-image:url('<//?= UPLOAD_URL.'packages/'.$package->featuredImage() ?>')">
                                    <icon class="checkmark" style="background-image:url('<//?= FRONT_IMG ?>checkmarkWhite.png')"></icon>
                                </div>
-->
                                <input type="hidden" value="<?=$package->id?>">
                                <h2 class="valB"><?=$package->name?></h2>
                            </div>
                            <div class="packageContents">
							<? foreach ($details as $name => $detail) {?>
								<?foreach ($detail as $items) {
									$m = "\\Model\\" . $name;
									$obj = $m::getItem($items['id']);
									$img = $name == 'tour' ? $obj->featuredImage() : '';?>
									<div class="tourSelection">
										<div class="selectedTourImage media">
											<icon class="checkmark" style="background-image:url('<?= FRONT_IMG ?>checkmarkWhite.png')"></icon>
										</div>
										<input type="hidden" value="<?=$obj->id?>">
										<h5 class="valB"><?=$obj->getName()?></h5>
									</div>
								<? } ?>
							<? } ?>
							</div>
                        <? } ?>
					</div>
					<div class="summaryDetailsItem labelInputWrapper">
						<label>Who's going?</label>
						<?$whosGoing = [];
						if($cart->adult_count){
							$adult = $cart->adult_count; $adult .= $cart->adult_count == 1 ? ' Adult': ' Adults';
							$whosGoing[] = $adult;
						}
						if($cart->kid_count){
							$kid = $cart->kid_count; $kid .= $cart->kid_count == 1 ? ' Kid': ' Kids';
							$whosGoing[] = $kid;
						}
						if($cart->tandem_count){
							$tandem = $cart->tandem_count .' Tandem';
							$whosGoing[] = $tandem;
						}
						$goingStr = implode(' and ',$whosGoing)?>
						<h5 class="valB"><?=$goingStr?></h5>
					</div>
					<?if(!$cart->package_id){?>
						<div class="summaryDetailsItem labelInputWrapper">
							<label>When?</label>
							<h5 class="valB"><?=date('M j Y g:ia',strtotime($cart->reserve_date))?></h5>
						</div>
					<? } else {
						foreach (json_decode($cart->package_details, true) as $name => $details) {
							foreach ($details as $detail) {
								$m = "\\Model\\" . $name;
								$obj = $m::getItem($detail['id']); ?>
								<div class="summaryDetailsItem labelInputWrapper">
									<label><?= $obj->getName() ?></label>
									<? if ($name == "Tour") {?>
										<label>Duration:</label>
										<h5 class="valB"><?=$obj->duration_hour > 0 ? $obj->duration_hour." Hours" : ''?><?=$obj->duration_minute > 0 ? " ".$obj->duration_minute." Minutes" : '' ?></h5>
									<?} ?>
									<label>Valid Until</label>
									<h5 class="valB"><?= date('M j Y g:ia', strtotime($detail['datetime'])) ?></h5>
								</div>
							<? } ?>
						<? } ?>
					<? } ?>
					<?if(count(json_decode($cart->equipment,true))>=1){?>
					<div class="summaryDetailsItem labelInputWrapper">
						<label>Add-ons</label>
                        <div class="selectedEquipmentList">
                            <?foreach(json_decode($cart->equipment,true) as $id=>$attr){
                                $equipment = \Model\Equipment::getItem($id)?>
                            <div class="selectedI">
                                <div class="equipmentImage media" style="height:56px;">
                                    <p class="whiteOut"><?=$equipment->name?> x<?=$attr['qty']?></p>
                                </div>
                                <div class="comparativePricingFormat centeredCols">
                                    <div class="col">
                                        <h4 class="valB">Walk-In</h4>
                                        <div class="co_price">
                                            <span></span>
                                            <p>$<?=$equipment->getPrice('adult',$cart->duration)?></p>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <h4 class="valB">Online</h4>
                                        <div class="online_price">
                                            <p>$<?=$equipment->getPrice('adult',$cart->duration)?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?}?>
							<?if($cart->add_ons && $cart->add_ons != 'false'){foreach(json_decode($cart->add_ons,true) as $type=>$id){foreach($id as $key=>$qty){
								$m = "\\Model\\".$type; $obj = $m::getItem($key)?>
								<div class="selectedI">
									<div class="equipmentImage media" style="height:56px;">
										<p class="whiteOut"><?=$obj->getName()?> x<?=$qty['qty']?></p>
									</div>
									<div class="comparativePricingFormat centeredCols">
										<div class="col">
											<h4 class="valB">Walk-In</h4>
											<div class="co_price">
												<span></span>
												<p>$<?=$obj->getPrice()?></p>
											</div>
										</div>
										<div class="col">
											<h4 class="valB">Online</h4>
											<div class="online_price">
												<p>$<?=$obj->getPrice()?></p>
											</div>
										</div>
									</div>
								</div>
								<?}?>
								<?}?>
							<?}?>
                        </div>
                    </div>	
                    <?}?>			
				</div>
			</div>
			<?$increment++;}?>
		</div>
	</div>

</div>
<!--<link href="--><?//=FRONT_CSS.'jquery.datetimepicker.css'?><!--" rel="stylesheet" type="text/css"/>-->
<!--<script src="--><?//=FRONT_JS.'jquery.datetimepicker.full.min.js'?><!--"></script>-->
<script src="<?=FRONT_JS.'cc_type.js'?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[name=cc_number]').on('input',function(){
			$('#cc_type').val(creditCardType($(this).val())[0].type);
		});
		function updateACart() {
			var inputs = $('#paymentForm :input');
			var values = {};
			inputs.each(function(){
				if(this.name != "") {
					values[this.name] = $(this).val();
				}
			});
			$.post('/checkout/logCheckout', values, function () {
				console.log('123');
			});
		}

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('#emailAddress,#fullName,[name=phone],#ccnumber,#creditCardName,#expirationDate1,#expirationDate2,#securityCode').on('blur autocompletechange', function () {
				updateACart()
			});
		} else {
			$(window).on('beforeunload', function () {
				updateACart()
			});
		}
		$('.addCoupon').on('click',function(){
            $('.loading-overlay').show();
		    var code = $('#couponCode').val();
		    var email = $('#emailAddress').val();
		    var couponHtml = $('.couponItem');
		    $.post('/coupons/add',{code:code,email:email},function(data){
		        if(data.status == 'success'){
                    $('.subtotalItem').find('h5').html('<span>Sub-total:</span>$'+data.subtotal);
                    $('.taxItem').find('h5').html('<span>Tax:</span>$'+data.tax);
                    $('.total').find('h4').html('<span>Total:</span>$'+data.total);
                    couponHtml.show();
                    couponHtml.find('h5').html('<span>Coupon:</span>-$'+data.discount);
                    $('.couponInput').hide();
                } else {
		            alert(data.msg);
                }
            });
            $('.loading-overlay').hide();
        });
		$('select[name=bill_country]').on('change',function(){
			var us = $('#us_states');
			var foreign = $('#foreign_states');
			if($(this).find(':selected').val() === 'United States'){
				us.show();
				us.find('select').prop('disabled',false);

				foreign.hide();
				foreign.find('input').prop('disabled',true);
			} else {
				us.hide();
				us.find('select').prop('disabled',true);

				foreign.show();
				foreign.find('input').prop('disabled',false);
			}
		});
        $('.paymentBtn').on('click',function(){
			var email = $('[name=email]');
			var fullName = $('[name=fullName]');
			var cc_number = $('[name=cc_number]');
			var card_name = $('[name=card_name]');
			var cc_expiration_month = $('[name=cc_expiration_month]');
			var cc_expiration_year = $('[name=cc_expiration_year]');
			var cc_ccv = $('[name=cc_ccv]');
			if(formValidator([email,fullName,cc_number,card_name,cc_expiration_month,cc_expiration_year,cc_ccv])){
				$('#paymentForm').submit();
			}
        });
	})
</script>
<div class="pageWrapper confirmationPageWrapper">
    <div class="promoBar">
        <p class="valB">Book your Reservation online now to save <span>50%</span> compared to our walk-in price.</p>
    </div>
    <section class="topSection">
        <h1 class="big uppercase valB">Thank you for booking with us!</h1>
        <p class="valB">You will receive your e-ticket very shortly.</p>
    </section>
    <section class="reservationDetailsSection middleSection">
        <div class="thinGrayBar">
            <div class="checkoutContentWidth">
                <div class="left_float">
                    <p class="valB">Order Information</p>
                </div>
                <div class="right_float">
                    <p class="valB">Total: <span id="subtotal">$<?=number_format($model->total,2)?></span></p>
                </div>
            </div>
        </div>
        <div class="checkoutContentWidth row_of_2 row reservationDetailsContent">
            <div class="row row_of_2 reservationItemDetails">
                <p>Details</p>
                <div class="col">
                    <div class="labelInputWrapper">
                        <label>Name</label>
                        <h5 class="valB"><?=ucwords($model->order->billName())?></h5>
                    </div>
                    <div class="labelInputWrapper">
                        <label>Reference #</label>
                        <h5 class="valB"><?=$model->order->ref_num?></h5>
                    </div>
                    <div class="labelInputWrapper">
                        <label>Location</label>
                        <h5 class="valB">892 9th Ave (Between 57th st and 58th st) New York NY 10019</h5>
                    </div>
                </div>
                <div class="col">
                    <div class="labelInputWrapper">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.71590191942!2d-73.98734708459334!3d40.76827287932592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25858f013c221%3A0x5545db8ad48f740d!2s892+9th+Ave%2C+New+York%2C+NY+10019!5e0!3m2!1sen!2sus!4v1480952788955" width="400" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div>
                    <div class="labelInputWrapper">
                        <label>Redeem Your Ticket at the TopView Visitor Centers Located at:</label>
                            <h5 class="valB">
                                2 East 42nd Street (corner of 42nd St and 5th Ave)
                            </h5>
                        <br />
                        or
                        <br /><br />
                            <h5 class="valB">
                                Bike Rental Central Park
                                <br />
                                892 9th Ave, btwn 57th and 58th Street
                            </h5>
                        <br />
                        <br />
                        <label>
                            You may also redeem your tickets with any of our TopView ticket agents or supervisors.
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <?$increment = 1;
        foreach($model->order_products as $order_product){?>
        <div class="checkoutContentWidth row_of_2 row reservationDetailsContent">
            <div class="row row_of_2 reservationItemDetails">
                <!--IF MORE THAN 1 RESERVATION -->
                <p>Reservation #<?=$increment?></p>
                <div class="col">
                    <div class="labelInputWrapper">
                        <label><!--IF just one activity:"activity", multiple:"activities"-->Activity</label>
                        <!--IF TOUR-->
                        <?if($order_product->tour){
                            $tour = \Model\Tour::getItem($order_product->tour)?>
                            <div class="selectedActivity">
                                <div class="tourSelection">
                                    <input name="tourName" type="hidden" value="<?=$tour->id?>">
                                    <h5 class="valB"><?=$tour->name?></h5>
                                </div>
                            </div>
                        <?}?>
                        <?if($order_product->package){
                            $package = \Model\Package::getItem($order_product->package);
                            $details = $order_product->package_details ? json_decode($order_product->package_details,true): [];?>
                            <div class="selectedActivity">
                                <div class="tourSelection">
                                    <input name="tourName" type="hidden" value="<?=$package->id?>">
                                    <h5 class="valB"><?=$package->name?></h5>
                                </div>
                            </div>
                            <div class="packageContents">
                            <? foreach ($details as $name => $detail) {?>
                                <?foreach ($detail as $items) {
                                    $m = "\\Model\\" . $name;
                                    $obj = $m::getItem($items['id']);
                                    $img = $name == 'Tour' ? $obj->featuredImage() : '';?>
                                    <div class="selectedActivity" style="margin-left: 15px;">
                                        <div class="tourSelection">
                                            <input name="tourName" type="hidden" value="<?= $obj->id ?>">
                                            <h5 class="valB"><?= $obj->getName() ?></h5>
                                        </div>
                                    </div>
                                <? } ?>
                            <? } ?>
                            </div>
                        <?}?>
                        <?if($order_product->rental){ //todo replace duration with rental id
                            $rental = \Model\Rental::getItem($order_product->rental)?>
                            <div class="selectedActivity">
                                <div class="tourSelection">
                                    <h5 class="valB"><?=$rental->name?></h5>
                                </div>
                            </div>
                        <?}?>
                    </div>
                    <div class="labelInputWrapper">
                        <label>People Going</label>
                        <div class="adultsCount">
                            <div class="adultsCountValWrapper">
                                <input type="text" readonly class="adultsCountVal" value="<?=$order_product->adult_count ? : 0?>" pattern="[0-9]*"
                                       name="reservationQuantity">
                                <span class="key"><?=pluralize($order_product->adult_count,'Adult')?></span>
                            </div>
                        </div>
                        <span class="separator">and</span>
                        <div class="kidsCount">
                            <div class="kidsCountValWrapper">
                                <input type="text" readonly class="kidsCountVal" data-name="kid_count" value="<?=$order_product->kid_count ? : 0?>" pattern="[0-9]*"
                                       name="reservationKidQuantity">
                                <span class="key"><?=pluralize($order_product->kid_count,'Kid')?></span>
                            </div>
                        </div>
<!--                        --><?//if($order_product->rental){?>
<!--                            <span class="separator">and</span>-->
<!--                            <div class="tandemsCount">-->
<!--                                <div class="tandemsCountValWrapper">-->
<!--                                    <input type="text" readonly class="tandemCountVal" data-name="tandem_count" value="--><?//=$order_product->tandem_count ? : 0?><!--" pattern="[0-9]*"-->
<!--                                           name="reservationTandemQuantity">-->
<!--                                    <span class="key">Tandem</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        --><?//}?>
                    </div>

                    <?if(!$order_product->package){?>
                        <div class="labelInputWrapper whenLabelInputWrapper">
                            <label>Date & Time</label>
                            <div class="datepickerInputWrapper">
                                <div class="placeholderInactive dateText">
                                    <p class="placeholder val gray">When?</p>
                                    <p class="data whenSelectorResult"><?= \Carbon\Carbon::createFromTimestamp(strtotime($order_product->reservation_date))->format('M d, Y g:i a') ?></p>
                                </div>
                            </div>
                        </div>
                    <? } else {
                        foreach (json_decode($order_product->package_details, true) as $name => $details) {
                            foreach ($details as $detail) {
                                $m = "\\Model\\" . $name;
                                $obj = $m::getItem($detail['id']);
                                $dataDay = $dataWeek = $dataHour = ''; ?>
                                <div class="labelInputWrapper whenLabelInputWrapper">
                                    <label><?=$obj->getName()?></label>
                                    <label>Valid Until</label>
                                    <p ><?= \Carbon\Carbon::createFromTimestamp(strtotime($detail['datetime']))->format('M d, Y g:i a') ?></p>
                                </div>
                            <? } ?>
                        <? } ?>
                    <? } ?>
                    <div class="labelInputWrapper">
                        <label>Reservation Price</label>
                        <h5 class="valB reservationPrice">$<?=number_format($order_product->total,2)?></h5>
                    </div>
                </div>
                <div class="col">
                    <div class="labelInputWrapper equipmentCustomizationReservationInfo">

                        <? if(count(json_decode($order_product->equipment,true))>=1){?>
                            <label>Add-Ons</label>
                            <!--Bikes added to cart-->
                            <div class="selectedEquipmentList row">
                                <?foreach(json_decode($order_product->equipment,true) as $id=> $attr){
                                    $equipment = \Model\Equipment::getItem($id);?>
                                    <div class="col selectedI">
                                        <div class="equipmentImage media" style="background-image:url('<?=UPLOAD_URL.'equipments/'.$equipment->featuredImage()?>')">
                                            <p class="whiteOut"><?=$equipment->name?></p>
                                        </div>

                                        <div class="comparativePricingFormat centeredCols">
                                            <div class="col">
                                                <h4 class="valB">Walk-In</h4>
                                                <div class="co_price">
                                                    <span></span>
                                                    <p>$<?=intval($equipment->getPrice('adult',$order_product->rental))?></p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <h4 class="valB">Online</h4>
                                                <div class="online_price">
                                                    <p>$<?=intval($equipment->getPrice('adult',$order_product->rental))?></p>
                                                </div>
                                            </div>
                                            <div class="col numberTogglerCol">
                                                <div class="numericalToggler">
                                                    <div class="numberCount">
                                                        <div class="numberCountValWrapper">
                                                            <input name="reserveRental[]" type="text" class="numberCountVal" value="<?=$attr['qty']?>" pattern="[0-9]*" data-id="<?=$id?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?}?>
                                <?if($order_product->add_ons){foreach(json_decode($order_product->add_ons,true) as $type=> $id){foreach($id as $key=> $qty){
                                    $m = "\\Model\\".$type; $obj = $m::getItem($key)?>
                                    <div class="col selectedI">
                                        <div class="equipmentImage media" style="background-image:url('<?=UPLOAD_URL.strtolower($type).'/'.$obj->featuredImage()?>')">
                                            <p class="whiteOut"><?=$obj->getName()?></p>
                                        </div>

                                        <div class="comparativePricingFormat centeredCols">
                                            <div class="col">
                                                <h4 class="valB">Walk-In</h4>
                                                <div class="co_price">
                                                    <span></span>
                                                    <p>$<?=intval($obj->getPrice())?></p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <h4 class="valB">Online</h4>
                                                <div class="online_price">
                                                    <p>$<?=intval($obj->getPrice())?></p>
                                                </div>
                                            </div>
                                            <div class="col numberTogglerCol">
                                                <div class="numericalToggler">
                                                    <div class="numberCount">
                                                        <div class="numberCountValWrapper">
                                                            <input name="reserveRental[]" type="text" class="numberCountVal" value="<?=$qty['qty']?>" pattern="[0-9]*" data-id="<?=$key?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?}?>
                                <?}?>
                                <?}?>
                            </div>
                        <?}?>
                        <div class="bodyCurtain"></div>
                        <!--Modal equipment selection-->
                    </div>
                    <?if($order_product->package && ($package = \Model\Package::getItem($order_product->package)) && $order_product->upgrade_option){?>
                        <div class="labelInputWrapper updateOptions">

                            <label>Upgrade Options</label>
                            <div class="selectedEquipmentList row">
                                <?foreach (json_decode($package->upgrade_option,true) as $type => $array) {
                                    foreach ($array as $value) {
                                        $mod = "\\Model\\".ucfirst($type);
                                        $obj = $mod::getItem($value);
                                        $upgrade = json_decode($order_product->upgrade_option,true);
                                        ?>
                                        <div class="col selectedI">
                                            <div class="equipmentImage media" style="background-image:url('<?=method_exists($obj,'featuredImage') ? UPLOAD_URL.$type.DS.$obj->featuredImage() : ''?>')">
                                                <p class="whiteOut"><?=$obj->name .'<br>('. ucfirst($type).')'?></p>
                                            </div>

                                            <div class="comparativePricingFormat center edCols">
                                                <div class="col">
                                                    <h4 class="valB">Walk-In</h4>
                                                    <div class="co_price">
                                                        <span></span>
                                                        <p>$<?=intval(method_exists($obj,'getWalkin') ? $obj->getWalkin(): $obj->getPrice())?></p>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <h4 class="valB">Online</h4>
                                                    <div class="online_price">
                                                        <p>$<?=intval($obj->getPrice())?></p>
                                                    </div>
                                                </div>
                                                <div class="col numberTogglerCol">
                                                    <div class="numericalToggler">
                                                        <div class="numberCount">
                                                            <div class="numberCountValWrapper">
                                                                <input name="reserveRental[]" type="text" class="numberCountVal" value="<?=isset($upgrade[$type]) && isset($upgrade[$type][$value]) ? $upgrade[$type][$value]['qty']: 0?>" pattern="[0-9]*" data-id="<?=$value?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?}?>
                                <? } ?>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
            <? $increment++;}?>
        </div>
    </section>
<!--     <section class="deals confirmationDealsRow row">
        <div class="equipmentFreeSection row col">
            <div class="text col">
                <h1 class="valB">Free</h1>
                <h4 class="val gray">with your bus tour<br>reservation tickets</h4>
            </div>
            <div class="col">
                <icon class="lockIcon" style="background-image:url('<//?=FRONT_IMG?>poncho.png')"></icon>
                <h6 class="valB">Poncho</h6>
            </div>
            <div class="col">
                <icon class="helmetIcon" style="background-image:url('<//?=FRONT_IMG?>earphones.png')"></icon>
                <h6 class="valB">Earphones</h6>
            </div>
        </div>
    </section> -->
    <section class="reservationQuestionsSection" style="display:none;pointer-events: none;opacity:0;visibility: hidden;">
        <h1 class="big valB">Questions</h1>
        <div class="questionsBox">
            <div class="row row_of_3">
                <div class="col">
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
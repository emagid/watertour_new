<div class="pageWrapper aboutPageWrapper cmsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
        </div>
    </div>
    <h1 class="valB big blogDetailsTitle"><?=$model->blog->name?></h1>
    <div class="blogDetailsContentWidth contentWidth">
        
        <h6 class="valUpper uppercase gray">by <?=$model->blog->author?></h6>
        <div class="blogDescriptionWrapper">
            <p class="valB"><?=$model->blog->description?></p>
        </div>
    </div>
</div>
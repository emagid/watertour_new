<div class="pageWrapper aboutPageWrapper cmsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
        </div>
    </div>

    <h1 class="valB big blogTitle">Blog</h1>
<!--    <p class="valB">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
    <div class="contentWidth">
        <div class="tiling row squareContents blogTiling">
            <? $alternator = true;
            foreach ($model->blog as $value) { ?>
                <a href="<?= SITE_URL ?>blog/<?=$value->slug?>" class="row row_of_2">
                    <div class="col float_<?=$alternator ? 'right': 'left'?>">
                        <div class="media" style="background-image:url('<?= UPLOAD_URL .'blogs/'. $value->featured_image ?>')"></div>
                    </div>
                    <div class="col float_left">
                        <div class="absTransCenter">
                            <h4 class="uppercase valB gray"><?=$value->name?></h4>

                            <p class="val"><?=$value->summary?></p>
                            <div class="btn secondary_btn">Read More</div>
                        </div>
                    </div>
                </a>
            <? $alternator = $alternator ? false: true;} ?>
        </div>
    </div>
</div>
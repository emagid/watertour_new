<?php
$page = $model->page;
?>
<div class="pageWrapper cmsPageWrapper locationsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
        </div>
    </div>
    <h1 class="valB big"><?= $page->title; ?></h1>
    <div class="banner <?= is_null($page->featured_image) ? 'cms' : ''; ?>" <?= (is_null($page->featured_image) ? '' : 'style="background-image:url(\'' . UPLOAD_URL . 'pages/' . $page->featured_image . '\')"') ?> >
    </div>

    <section class="contentWidthGutter contentWidth content cmsPagesContentWidth">
        <div class="row">
            <div class="cms-pages-text">
                <?= $page->description; ?>
            </div>
        </div>
    </section>
</div>
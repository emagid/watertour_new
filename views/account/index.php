<div class="pageWrapper accountPageWrapper pageLeftNavSidebar">
    <div class="content">
        <h1 class="as_t">Account Info</h1>
        <form method="post" action="/login/logout">
            <input class="btn btn_oreo signout_btn" type="submit" value="Sign out"/>
        </form>
        <form method="post" action="<?= $this->emagid->uri ?>">
            <input hidden name="id" value="<?= $model->user->id ?>">

            <div>
                <label>First Name</label>
                <input name="first_name" type="text" value="<?= $model->user->first_name ?>"/>
            </div>
            <div>
                <label>Last Name</label>
                <input name="last_name" type="text" value="<?= $model->user->last_name ?>"/>
            </div>
            <div>
                <label>Email</label>
                <input name="email" type="text" value="<?= $model->user->email ?>"/>
            </div>
            <div>
                <label>Phone</label>
                <input name="phone" type="text" value="<?= $model->user->phone ?>"/>
            </div>
            <div>
                <label>Password</label>
                <input name="password" type="password"/>
            </div>
            <input type="submit" name="submit" class="btn btn_full_width save_account_info_btn" value="Save Account Info"/>
        </form>

        <h1 class="as_t">Order History</h1>
            <?if($model->orderHistory){
                foreach($model->orderHistory as $order){
                    $orderProduct = \Model\Order_Product::getList(['where'=>"order_id = $order->id"])?>
                    <label>Id</label>
                    <div><?=$order->id?></div>
                    <label>Purchased</label>
                    <div><?=date('F d, Y h:i a',strtotime($order->insert_time))?></div>
                    <label>Total</label>
                    <div>$<?=number_format($order->total,2)?></div>
                    <label>Products</label>
                    <div>
                    <?foreach($orderProduct as $op){
                        $product = \Model\Product::getItem($op->product_id)?>
                        <img src="<?=UPLOAD_URL.'products/'.$product->featuredImage()?>" width="100px"/>
                    <?}?>
                    </div>
                <?}?>
            <?} else {?>
                <div>No active history</div>
            <?}?>
        <h1 class="as_t">Favorites</h1>
        <? if ($model->favorites) {
            foreach ($model->favorites as $favorite) {
                $prod = \Model\Product::getItem($favorite->product_id)?>
                <div><a href="<?=SITE_URL.'products/'.$prod->slug?>"><?=$prod->name?></a></div>
                <div><a href="<?=SITE_URL.'products/'.$prod->slug?>">$<?=number_format($prod->price,2)?></a></div>
                <div><a href="<?=SITE_URL.'products/'.$prod->slug?>"><img src="<?=UPLOAD_URL.'products/'.$prod->featuredImage()?>" width="100px"/></a></div>
            <? } ?>
        <? } ?>
    </div>
</div>
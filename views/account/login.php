<div class="pageWrapper loginPageWrapper accountPageWrapper">
	<div class="row loginMainRow">
		<div class="col">
			<icon class="circleLogoOreo">
			</icon>
			<h5 class="as_r">Log In</h5>
			<h8 class="as_l">Made in NYC</h8>
		</div>
		<div class="col">
			<div class="formWrapper accountFormWrapper">
				<div class="row row_of_2">
					<div class="col">
						<input type="text" placeholder="Email">
					</div>
					<div class="col">
						<input type="password" placeholder="Password">
					</div>
				</div>
				<div class="row">
					<input type="submit" class="btn btn_black btn_full_width" value="Sign In">
				</div>
				<div class="footer_links">
					<a>Create Account</a>
					<a>Return to Store</a>
					<a>Forgot your Password?</a>
				</div>
			</div>
		</div>

	</div>
</div>
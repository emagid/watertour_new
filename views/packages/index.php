<style>
	.packageGridItem.row{
		display: none;
	}
</style>
<div class="pageWrapper packagesPageWrapper">

	<section class="topSection">
<!-- 		<div class="promoBar">
			<p class="valB">Don't stress over planning the perfect itinerary. We've got your covered with a great deal!</p>
		</div> -->	
		<div class="heroContainer shortHeroContainer">
			<div class="heroSlideshow heroSlideshowBikeTours">
				<div class="slide">		
<!-- 					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<//?=FRONT_IMG?>tsquare.jpg');"></div>
					</div> -->				
					<div class="heroText">
						<h1 class="biggest valB">Tickets and Packages</h1>
						 <h3 class="valB valUpper">Explore New York Without Missing A Beat</h3>
					</div>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>downtown_liberty.jpg'); background-position:center center;"></div>
				</div>
<!--
				<div class="slide">
 					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<//?=FRONT_IMG?>met.jpg');"></div>
					</div> 				
					<div class="heroText">
						<h1 class="biggest valB">Explore Museums</h1>
						 <h3 class="valB valUpper">and other legendary attractions along with your NYC bike rental</h3>
					</div>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>met.jpg')"></div>
				</div>
-->
<!--
				<div class="slide">
 					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<//?=FRONT_IMG?>empiresb.jpg');"></div>
					</div>	 			
					<div class="heroText">
						<h1 class="biggest valB">Visit Skyscrapers</h1>
						 <h3 class="valB valUpper">and other legendary attractions along with your NYC bike rental</h3>
					</div>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>empiresb.jpg')"></div>
				</div>
-->
			</div>
		</div>
	</section>	
	<section class="middleSection">
<!--		<div class="datePaginatorButtonsRow row">-->
<!--			<div class="left_float row col">-->
<!--				<div class="col threePrev">-->
<!--					<a class="btn secondary_btn">-->
<!--						<p></p>-->
<!--					</a>-->
<!--				</div>-->
<!--				<div class="col twoPrev">-->
<!--					<a class="btn secondary_btn">-->
<!--						<p></p>-->
<!--					</a>-->
<!--				</div>-->
<!--				<div class="col onePrev">-->
<!--					<a class="btn secondary_btn">-->
<!--						<p></p>-->
<!--					</a>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="right_float row col">-->
<!--				<div class="col onePost">-->
<!--					<a class="btn secondary_btn">-->
<!--						<p></p>-->
<!--					</a>-->
<!--				</div>-->
<!--				<div class="col twoPost">-->
<!--					<a class="btn secondary_btn">-->
<!--						<p></p>-->
<!--					</a>-->
<!--				</div>-->
<!--				<div class="col threePost">-->
<!--					<a class="btn secondary_btn">-->
<!--						<p></p>-->
<!--					</a>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		<div id="paginator"></div>-->
		<div class="packageSortingRow_content_width" style="display:none;">
            <?$p = isset($_GET['sort']) ? $_GET['sort']: null?>
			<?if(isset($_GET['sort']) && $_GET['sort'] == 'htl'){
				$_GET['sort'] = 'lth'?>
				<a class="faux_dropdown highToLow" href="/packages?<?=http_build_query($_GET)?>"><p>Price: <span>High to Low</span></p><icon class="dropdownArrow"></icon></a>
			<?}else{
				$_GET['sort'] = 'htl'?>
				<a class="faux_dropdown lowToHigh" href="/packages?<?=http_build_query($_GET)?>"><p>Price: <span>Low to High</span></p><icon class="dropdownArrow"></icon></a>
			<?}
            if(!$p){ unset($_GET['sort']); } else {$_GET['sort'] = $p;}?>
            <?$d = isset($_GET['day']) ? $_GET['day']: null;unset($_GET['day'])?>
            <!--<a href="/packages<?=http_build_query($_GET) ?'?'.http_build_query($_GET) : ''?>"><button>No Day Filter</button></a>-->
            <?/**foreach (\Model\Package::getDays() as $day){
                $_GET['day'] = $day?>
                <a href="/packages?<?=http_build_query($_GET)?>"><button><?=$day.' '.pluralize($day,'Day')?></button></a>
            <?}
            if(!$d){ unset($_GET['day']); } else {$_GET['day'] = $d;}**/?>
            <?foreach (\Model\Package::getDays() as $day){
                $_GET['day'] = $day?>
                <a href="" data-durationfilter='<?=$day?>' class='durationFilter'><button><?=$day == 0 ? 'Single Entry' : $day.' '.pluralize($day,'Day')?></button></a>
            <?}
            if(!$d){ unset($_GET['day']); } else {$_GET['day'] = $d;}?>
		</div>
		<div class="packagesGrid">
			<?foreach($model->packages as $package){?>
				<div class="packageGridItem row" data-duration = '<?=$package->days?>'>
					<a href="<?='/packages/package/'.$package->slug?>"><div class="media col" style="background-image:url('<?=UPLOAD_URL.'packages/'.$package->banner?>');"></div></a>
					<div class="textualData col">
						<a href="<?='/packages/package/'.$package->slug?>"><h2 class="val"><?=$package->name?></h2></a>
						<h4 class="val gray">
<!--							<span class="time"><i class="material-icons">access_time</i>11:00 AM</span>-->
<!--							<span class="duration"><i class="material-icons">timelapse</i>--><?//=$package->getDurationFormatted()?><!--</span>-->
							<span class="date"><i class="material-icons">date_range</i>Use within 6 months of purchase</span>
						</h4>
						<h4 class="val grounded">
                            <?=$package->tickets_sold?>
                        </h4>
						<div class="lineSep"></div>
						<a class="invBtn btn arrowBtn learnMore" href="<?='/packages/package/'.$package->slug?>">
							Learn More
							<!-- <icon style="background-image:url('<//?=FRONT_IMG?>btnLinkArrow.png')"></icon> -->
						</a>
					</div>
					<div class="pricing col">
						<div class="absTransCenter">
							<h6 class="gray uppercase valB">Adults</h6>
							<div class="online_price circle">
								<p class="gray uppercase valB">online</p>
								<h2 class="price"><span>$</span><?=intval($package->getPrice())?></h2>
							</div>
							<div class="walkin circle">
								<p class="gray uppercase valB">walkin</p>
								<h2 class="price"><span>$</span><?=intval($package->getWalkin())?></h2>
							</div>
						</div>
					</div>
					<div class="bookBtnWrapper col">
						<a class="btn fullBtn primaryBtn" href="<?='/packages/package/'.$package->slug?>"><p class="absTransCenter">Book this Package</p></a>
					</div>
				</div>
			<?}?>
		</div>
	</section>
	<section class="bottomSection">
		<div class="row row_of_3 featuredLinks">
			<?foreach($model->tour_banner as $value){?>
				<div class="col">
					<h4 class="valB medium"><?=$value->title?></h4>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'banners/'.$value->image?>')"></div>
					<p class="valB"><?=$value->description?></p>
					<a class="invBtn btn arrowBtn learnMore" href="<?=$value->url?>">
						<?=isset(json_decode($value->details,true)['url_text']) ? json_decode($value->details,true)['url_text']: 'Explore our tours here'?>
						<!-- <icon style="background-image:url('<//?=FRONT_IMG?>btnLinkArrow.png')"></icon> -->
					</a>
				</div>
			<?}?>
		</div>
	</section>
</div>
<script>
$(document).ready(function(){
	function packageSearch(duration=0){
		if(duration == 0){
			$('.packageGridItem').show();
		}
		else{
			$('.packageGridItem').each(function(){
				if($(this).data('duration') == duration){
					$(this).show();
				}
				else{
					$(this).hide();
				}
			});	
		}
	}
	var duration = "<?=$_GET['day']?>";
	packageSearch(duration);
	$('.durationFilter').click(function(e){
		e.preventDefault();
		var durationFilter = $(this).data('durationfilter');

		packageSearch(durationFilter);
		var currentURI = window.location.href;
		var replaceURI = '';
		if(currentURI.indexOf('day=') > 0){
			replaceURI = currentURI.replace(/day=[0-9]/, 'day='+durationFilter);	
		}
		else{
			if(currentURI.indexOf('sort') > 0){
				replaceURI = currentURI+'&day='+durationFilter;
			}
			else{
				replaceURI = currentURI+'?day='+durationFilter;	
			}
			
		}
		

		history.pushState(currentURI, "", replaceURI);
	});
	window.onpopstate = function(event){
		var url = new URL(window.location.href);
		var day = url.searchParams.get("day");
		packageSearch(day);
		
	}
});
</script>
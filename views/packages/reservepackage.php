<style>
	.placeholderActive.dateText p.placeholder.val.gray {
		display: inline-block;
		font-size: 18px;
		vertical-align: top;
	}
</style>
<div class="pageWrapper reservePackagePageWrapper">

	<section class="topSection">
		<div class="promoBar">
			<div class="float_left">
				<a href="<?=SITE_URL?>packages" class="promoBarLeftBtn"><icon class="whiteArrowIcon" style="background-image:url('<?=FRONT_IMG?>whiteArrowIcon.png');"></icon>Browse Packages</a>
			</div>
			<p class="valB">Don't stress over planning the perfect itinerary. We've got your covered with a great package deal!</p>
            <div class="right_float_hidden hidden show770">
                <a class="menuBtn globalSideMenuTrigger">
                    <icon>
                        <div class="menu-container">
                            <div class="menu-circle">
                                <div class="line-wrapper">
                                    <span class="line"></span>
                                    <span class="line"></span>
                                    <span class="line"></span>
                                </div>
                            </div>
                        </div>
                    </icon>
                    <span class="text">
                        <span class="open">Menu</span>
                        <span class="close">Close</span>
                    </span>
                </a>
            </div>
		</div>	
		<div class="heroContainer shortHeroContainer">
			<div class="heroSlideshow heroSlideshowBikeTours">
				<div class="slide">		
					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<?=FRONT_IMG?>empiresb.jpg');"></div>
					</div>				
					<div class="heroText row">
						<div class="col logoBlock">
			                <a class="homeLink" href="/">
			                    <img src="<?= FRONT_IMG ?>tp_logo_new.png">
			                </a>
			            </div>
						<?
						$duration = 'Unspecified';
						if($model->package->getDuration()){
							$duration = [];
							foreach($model->package->getDuration(false) as $item=>$value){
								$duration[] = $value.' '.$item;
							}
							$duration = implode(', ',$duration);
						}
						?>
			            <div class="col">
							<h1 class="biggest valB"><?=$model->package->name?> Tour + Bike Rental Package</h1>
<!--							<h3 class="valB valUpper">--><?//=date('l F j,Y',$model->time)?><!-- at 10:00 AM <span class="middot"></span>--><?//=$duration?><!--</h3>-->
						</div>
					</div>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'packages/'.$model->package->banner?>')"></div>
				</div>
			</div>
		</div>
	</section>	
	<section class="middleSection selectTickets">
		<form class="row reservePackageForm" action="/reservation/resRental" method="post">
			<input type="hidden" name="reservationPackage" value="<?=$model->package->id?>"/>
<!--			<input type="hidden" name="reservationDateTime" value="--><?//=$model->time?><!--"/>-->
			<div class="row row_of_3">
				<?foreach($model->items as $item) { ?>
					<div class="datepickerInputWrapper" style="z-index: 9999;">
						<?php $datetime = $_SESSION['cart']->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $_SESSION['cart']->datetime)->format('M j, Y g:i a') : ''; ?>
						<input type="text" class="triggerDatepicker" hidden name="reservationDateTime[]" placeholder="When?" value="<?= strtotime($datetime) ?>">
						<input type="text" class="triggerDatepicker" hidden name="reservationPackageType[]" placeholder="When?" value="<? $n = new ReflectionClass($item);echo $n->getShortName() ?>">
						<input type="text" class="triggerDatepicker" hidden name="reservationPackageId[]" placeholder="When?" value="<?= $item->id ?>">
						<!--<div class="datepickerCellUIConfirm whenSelectorParent" <?/*= 'data-hours="' . implode(',', $item->hours) . '"' */?> <?/*= 'data-week="' . implode(',', $item->weeks) . '"' */?>></div>-->
						<div class="<?= $datetime ? 'placeholderInactive ' : 'placeholderActive ' ?>dateText">
							<label class="valB gray uppercase"><?= $item->getName() ?>:</label>
							<p class="placeholder val gray">Valid for 6 months</p>
							<p class="data whenSelectorResult"><?= $datetime ?></p>
						</div>
					<span class="dropdownArrow">
						<icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
					</span>
					</div>
				<? } ?>
				<div class="col quantity">
					<div class="centered">
						<h6 class="uppercase valB">How many <span>Adults</span>?</h6>
						<div class="adultsCount">
	                        <div class="dec button disabled" data-attribute="adult"><span></span></div>
	                        <div class="adultsCountValWrapper">
	                            <input type="text" class="adultsCountVal" value="<?=$_SESSION['cart']->adults?>" pattern="[0-9]*"
	                                   name="reservationQuantity">
	                            <span class="key">Adults</span>
	                        </div>
	                        <div class="inc button" data-attribute="adult"><span></span><span></span></div>
	                    </div>
						<h1 class="val"><span>$</span><?=floatval($model->package->adults_price)?></h1>
					</div>
				</div>
				<div class="col quantity">
					<div class="centered">
						<h6 class="uppercase valB">How many <span>Kids</span>?</h6>
						<div class="kidsCount">
	                        <div class="dec button disabled" data-attribute="kid"><span></span></div>
	                        <div class="kidsCountValWrapper">
	                            <input type="text" class="kidsCountVal" value="<?=$_SESSION['cart']->kids?>" pattern="[0-9]*"
	                                   name="reservationKidQuantity">
	                            <span class="key">kids</span>
	                        </div>
	                        <div class="inc button" data-attribute="kid"><span></span><span></span></div>
	                    </div>
						<h1 class="val"><span>$</span><?=floatval($model->package->kids_price)?></h1>
	                </div>
				</div>
				<div class="col subtotal">
					<div class="centered">
						<h6 class="valB uppercase">Sub total</h6>
						<h1 class="val packageTotal">$<?=$model->total?></h1>
					</div>
				</div>
			</div>
			<div class="continueBar row">
				<div class="left_float">
<!-- 					<label for="discountCode">Do you have a discount code?</label>
					<div class="inputBtnRow">
						<input type="text" id="discountCode">
						<input type="submit" value="Apply" class="btn secondary_btn">
					</div> -->
				</div>
				<div class="right_float resPackage">
					<div class="inputBtnWrapper">
						<input type="submit" value="Continue" class="primaryBtn btn">
					</div>
				</div>
			</div>
		</form>
	</section>
</div>
<script>
	$(document).ready(function(){
		var adultPrice = <?=$model->package->adults_price?>;
		var kidPrice = <?=$model->package->kids_price?>;
		$('.inc,.dec').on('click',function(){
			var adults = $('.adultsCountVal').val();
			var kids = $('.kidsCountVal').val();
			var adultTotal = adults * adultPrice;
			var kidTotal = kids * kidPrice;
			$('.packageTotal').html('$'+(adultTotal+kidTotal));
		});
		$('.reservePackageForm').on('submit',function(e){
            var adults = $('.adultsCountVal').val();
            var kids = $('.kidsCountVal').val();
            if(adults + kids == 0){
                e.preventDefault();
                $('.resPackage').append('<div class="resBar_error resBar_error_count"><p class="errorText">Please select how many adults or kids</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
            }
        });
		function setupRome() {
			var datepickerCellUI = document.getElementsByClassName('datepickerCellUIConfirm');
			var whenSelectorResult = $('.whenSelectorResult');
			for (var i = 0; i < datepickerCellUI.length; i++) {
				var date = moment(Math.ceil((Date.now()/1000)/(30*60))*(30*60)*1000);
				var availableHours = datepickerCellUI[i].dataset.hours ? datepickerCellUI[i].dataset.hours : '';
				var availableWeeks = datepickerCellUI[i].dataset.week ? datepickerCellUI[i].dataset.week : '';
				var availableDate = datepickerCellUI[i].dataset.day ? datepickerCellUI[i].dataset.day : '';
				var datepick = true;
				var timepick = true;
				(function(e){
					rome(datepickerCellUI[i], options = {
						"appendTo": document.body,
						"autoClose": true,
						"autoHideOnBlur": true,
						"autoHideOnClick": true,
						"date": true,
						"dateValidator": function (d) {
//                    var index = typeof datepickerCellUI[1] === 'undefined' ? 0 : 1;
							var index = e;
							var availableWeeks = datepickerCellUI[index].dataset.week ? datepickerCellUI[index].dataset.week : '';
							var m = moment(d);
							if (availableWeeks) {
								var weekArray = availableWeeks.split(',');
								var weekMap = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
								var indexMap = weekArray.map(function (obj) {
									if (weekMap.indexOf(obj.valueOf()) != -1) {
										return weekMap.indexOf(obj.valueOf());
									}
								});
								return indexMap.indexOf(m.day()) !== -1;
							} else {
								return true;
							}
						},
						"dayFormat": "D",
						"initialValue": $(datepickerCellUI[i]).siblings('[name=reservationDateTime]').val(),
						"inputFormat": "MMM D, YYYY h:mm a",
						"invalidate": true,
						"max": null,
						"min": date.format('MMM DD, YYYY h:mm a'),
						"monthFormat": "MMMM 'YY",
						"monthsInCalendar": 1,
						"required": false,
						"strictParse": false,
						"styles": {
							"back": "rd-back",
							"container": "rd-container",
							"date": "rd-date",
							"dayBody": "rd-days-body",
							"dayBodyElem": "rd-day-body",
							"dayConcealed": "rd-day-concealed",
							"dayDisabled": "rd-day-disabled",
							"dayHead": "rd-days-head",
							"dayHeadElem": "rd-day-head",
							"dayRow": "rd-days-row",
							"dayTable": "rd-days",
							"month": "rd-month",
							"next": "rd-next",
							"positioned": "rd-container-attachment",
							"selectedDay": "rd-day-selected",
							"selectedTime": "rd-time-selected",
							"time": "rd-time",
							"timeList": "rd-time-list",
							"timeOption": "rd-time-option"
						},
						"time": true,
						"timeFormat": "h:mm a",
						"timeInterval": 1800,
						timeValidator: function (d) {
//                    var index = typeof datepickerCellUI[1] === 'undefined' ? 0 : 1;
							var index = e;
							var availableHours = datepickerCellUI[index].dataset.hours ? datepickerCellUI[index].dataset.hours : '';
							var m = moment(d);
							if (availableHours) {
								var availableHoursArray = availableHours.toString().split(',');
								var hoursStringArray = [];
								for (var i = 0; i < availableHoursArray.length; i++) {
									var thisMins = parseFloat(availableHoursArray[i]) * 60;
									var h = m.clone().startOf('day').minute(thisMins).second(0);
									hoursStringArray.push(h.toString());
								}
								var comparedM = m.second(0);
								return hoursStringArray.indexOf(comparedM.toString()) != -1;
							} else {
								var start = m.clone().hour(7).minute(59).second(59);
								var end = m.clone().hour(20).minute(0).second(1);
								return m.isAfter(start) && m.isBefore(end);
							}
						},
						"weekdayFormat": "min",
						"weekStart": moment().weekday(0).day()

					}).on('data', function (value) {
						if (window.location.pathname == '/reservation/confirm') {
							var ref_num = this.associated.dataset.ref;
							var data = {ref_num: ref_num, reserve_date: value, upgrade: false};
							$.post('/reservation/updateCart', data);
						}
						var whenSelectorResult = $(this.associated).siblings('.dateText').find('.whenSelectorResult');
						var validateTime = moment.duration(moment(value).format('HH:mm')).asHours();
						var availableHours = datepickerCellUI[0].dataset.hours ? datepickerCellUI[0].dataset.hours : '';
						var availableHoursArray = [];
						if (availableHours) {
							availableHoursArray = availableHours.toString().split(',');
						} else {
							for (var i = 8; i < 20.5; i += .5) {
								availableHoursArray.push(i + "");
							}
						}
						if (availableHoursArray.indexOf(validateTime.toString()) != -1) {
							//time is good
						} else {
							//time is bad - set to nearest available
							var newTime = moment(value);
							if (newTime.isSame(moment(), 'day')) {
								var self = this;
								$.each(availableHoursArray, function (i, e) {
									if (e > validateTime) {
										var h = newTime.startOf('day').minute(parseFloat(e) * 60).second(0);
										self.setValue(h);
										value = h.format('MMM D, YYYY h:mm a');
										return false;
									}
								})
							} else {
								var h = newTime.startOf('day').minute(availableHoursArray[0] * 60).second(0);
								this.setValue(h);
								value = h.format('MMM D, YYYY h:mm a');
							}
						}

						whenSelectorResult.html(value);
						$('[name="reservationDateTime[]"]')[e].value = value;
						if ($('.rd-time-selected').hasClass("timeChosenbyUser")) {
							$(".dateTimeConfirmButton").removeClass("disabled");
						}
					}).on('time', function (time) {
						if ($(".rd-time-list").is(":visible")) {
							$('.rd-time-selected').addClass('timeChosenbyUser');
							$(".dateTimeConfirmButton").removeClass("disabled");
						}
						$(".rd-time").toggleClass('activeTimeDropdown');
					}).on('ready', function () {
						if ($('[name="reservationDateTime[]"]').val().length > 0) {
							$('.rd-time-selected').addClass('timeChosenbyUser');
						}
					});
				})(i);
			}
		}
		setupRome();
	})
</script>
<div class="bookNowHeader">
    <div class="bookNowContain">
        <div class="bookNowHeaderTitle">
            <h4 class="biggest valB"><?=$model->package->name?></h4>
        </div>

    <div class="bookNowForm">
				<form class="row reservePackageForm" action="/reservation/resRental" method="post">
					<input type="hidden" name="reservationPackage" value="<?=$model->package->id?>"/>
					<?foreach($model->items as $item) { ?>
						<?php $datetime = $_SESSION['cart']->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $_SESSION['cart']->datetime)->format('M j, Y g:i a') : ''; ?>
						<input type="text" class="triggerDatepicker" hidden name="reservationDateTime[]" placeholder="When?" value="<?= strtotime($datetime) ?>">
						<input type="text" class="triggerDatepicker" hidden name="reservationPackageType[]" placeholder="When?" value="<? $n = new ReflectionClass($item);echo $n->getShortName() ?>">
						<input type="text" class="triggerDatepicker" hidden name="reservationPackageId[]" placeholder="When?" value="<?= $item->id ?>">
					<? } ?>
					<div class="col quantity">
							<div class="centered">
								<div class="adultsCount">
			                        <div class="dec button disabled" data-attribute="adult"><span></span></div>
			                        <div class="adultsCountValWrapper">
			                            <input type="text" class="adultsCountVal" value="<?=$_SESSION['cart']->adults?>" pattern="[0-9]*"
			                                   name="reservationQuantity">
			                            <span class="key">Adults</span>
			                        </div>
			                        <div class="inc button" data-attribute="adult"><span></span><span></span></div>
			                    </div>
							</div>
						</div>
						<div class="col quantity">
							<div class="centered">
								<div class="kidsCount">
			                        <div class="dec button disabled" data-attribute="kid"><span></span></div>
			                        <div class="kidsCountValWrapper">
			                            <input type="text" class="kidsCountVal" value="<?=$_SESSION['cart']->kids?>" pattern="[0-9]*"
			                                   name="reservationKidQuantity">
			                            <span class="key">kids</span>
			                        </div>
			                        <div class="inc button" data-attribute="kid"><span></span><span></span></div>
			                    </div>
			                </div>
						</div>
						<div class="col subtotal">
							<div class="centered">
								<h6 class="valB uppercase">Sub total</h6>
								<p class="val packageTotal">$<?=$model->total?></p>
							</div>
						</div>
					<div class="continueBar row">
						<div class="resPackage">
							<div class="inputBtnWrapper">
								<input type="submit" value="Book Now" class="primaryBtn btn">
							</div>
						</div>
					</div>
				</form>
        </div>
</div>
    </div>


<div class="pageWrapper attractionPageWrapper">

	<section class="topSection">
		<div class="heroContainer">
			<div class="absTransCenter heroTextFixed heroText">
				<h1 class="biggest valB"><?=$model->package->name?></h1>
				<h3 class="valB valUpper"><?=$model->package->subtitle?></h3>
				<a class="btn fullBtn primaryBtn bookPkge"><p>Book this Package</p></a>
			</div>
			<div class="heroSlideshow heroSlideshowBikeTours">
				<?foreach($model->package_images as $image){?>
					<div class="slide">
						<div class="media" style="background-image:url('<?=UPLOAD_URL.'packages/'.$image->image?>')"></div>
					</div>
				<?}?>
			</div>
		</div>
	</section>
	<section class="middleSection">
		<div class="row attractionInfo packageDetailInfo">
			<div class="col right_float imagesCalls">
				<div class="verticalMedia media">
					<img src="<?=UPLOAD_URL.'packages/'.$model->package->banner?>">
				</div>
<!--				<div class="attractionDetailMap">-->
<!--					<iframe src="https://www.google.com/maps/embed/v1/place?key=--><?//=GOOGLE_MAPS_API_KEY?><!--&q=--><?//=htmlentities($model->package->name)?><!--" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>-->
<!--				</div>-->
				<div class="availablePackagesWrapper">
					<h6 class="val uppercase gray valB">Related Packages</h6>
					<? foreach ($model->related as $item) {?>
						<div class="availablePackagesGrid">
							<div class="row packageGridItem">
								<div class="textualData">
									<h2 class="val"><?=$item->name?></h2>
									<h4 class="val gray"><?=date('l M d, Y')?><span class="middot"></span><i class="material-icons">timelapse</i><?=$item->getDurationFormatted()?></h4>
									<div class="lineSep"></div>
								</div>
								<div class="row row_of_2">
									<div class="pricing col">
										<h6 class="gray uppercase valB">Starting at</h6>

										<h2 class="price"><span>$</span><?=$item->getPrice()?></h2>
										<h6 class="gray uppercase valB">per person</h6>
									</div>
									<div class="bookBtnWrapper col">
										<a class="btn fullBtn primaryBtn" href="/packages/package/<?=$item->slug?>">View this Package</a>
									</div>
								</div>
							</div>
						</div>
					<? } ?>
					<div class="footer">
						<a class="btn primaryBtn" href="/packages">View all Packages</a>
					</div>
					<?php $options = json_decode($model->package->add_ons, true);?>
					<?php if(isset($options['options'])){ ?>
					<div style="font-size: 20px;margin-top: 30px;line-height: 1.5;">
						<h3>Available Options</h3>
						<ul>
							<?php foreach($options['options'] as $option){ ?>
							<li><?=$option?></li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>
				</div>
					<section class="reviewsSection reviewsHorizontalScroller">
		<div class="inPageCurtain"></div>
		<h2 class="big valB">Reviews<a class="writeReview">Write a Review</a></h2>
		<div class="reviewFormExpander">
			<span class="expanderArrow"></span>
			<div class="reviewFormWrapper">
				<form action="/reviews/add" method="post" id="submit_form">
					<input hidden name="redirect" value="<?=$this->emagid->uri?>">
					<input hidden name="review_type" value="3">
					<input hidden name="review_id" value="<?=$model->package->id?>">
					<div class="inputRow">
						<label>Name</label>
						<input name="customer_name" id="reviewCustomerName">
					</div>
					<div class="inputRow">
						<label>Title</label>
						<input name="title">
					</div>
					<div class="inputRow">
						<label>Body</label>
						<textarea name="description"></textarea>
					</div>
					<div class="inputRow">
						<label>City</label>
						<input name="city">
					</div>
					<div class="inputRow">
						<label>State</label>
						<select name="state">
							<? foreach (get_states() as $short => $long) { ?>
								<option value="<?= $short ?>"><?= $long ?></option>
							<? } ?>
						</select>
					</div>
					<div class="inputRow">
						<label>Rating</label>
						<select name="rating">
							<?for($i = 5; $i >= 1; $i -= .5){?>
								<option value="<?=$i?>"><?=$i?></option>
							<?}?>
						</select>
					</div>
					<div class="inputRow submitRow">
						<input type="submit" value="Submit" class="btn primaryBtn submit_review">
					</div>
				</form>
			</div>
		</div>
		<div class="reviewScroller">
			<?foreach($model->reviews as $review){?>
				<div class="reviewItem">
					<div class="quoteMark"></div>
					<div class="quoteText">
						<h6 class="medium val"><?=$review->description?></h6>
						<div class="starRating">
							<?for($i = 0; $i < 5; $i++){
								$sub = $review->rating - $i;
								switch($sub){
									case $sub > .5:
									case $sub == 0:
										$rating = 'star_rate';
										break;
									case $sub == .5:
										$rating = 'star_half';
										break;
									case $sub < 0:
									default:
										$rating = 'star_border';
										break;
								}?>
								<i class="material-icons"><?=$rating?></i>
							<?}?>
						</div>
						<p><strong>- <?=$review->customer_name?></strong><span class="middot">&#xb7;</span><?=date('M jS, Y',strtotime($review->create_date))?></p>
					</div>
					<div class="reviewHelpful">
						<a>
							<h6 class="valB uppercase">Helpful</h6>
							<icon class="thumbsUp" style="background-image:url('<//?=FRONT_IMG?>reviewThumbsUp.png')">
								<span><?=$review->helpful ? : 0?></span>
							<!-- </icon> -->
						</a>
					</div>
				</div>
			<?}?>
		</div>

	</section>
	                <a class="btn fullBtn primaryBtn bookPkge">Book this Package</a>
			</div>
		<div class="col left_float textualInfo">
				<div class="packagePricing">
					<h3 class="val"><?=$model->package->name?></h3>
					<br>
					<!-- <p>Pass valid for: <//?=$item->getDurationFormatted()?></p> -->
					<div>
						<h6 class="gray uppercase valB">Adults</h6>
						<div class="row rowCentered">

							<div class="online_price circle">
								<p class="gray uppercase valB">online</p>
								<h2 class="price"><span>$</span><?=intval($model->package->adults_price)?></h2>
							</div>
							<div class="walkin circle">
								<p class="gray uppercase valB">walkin</p>
								<h2 class="price"><span>$</span><?=intval($model->package->adult_walkin)?></h2>
							</div>
                            					<div class="col quantity">
							<div class="centered">
								<div class="adultsCount">
			                        <div class="dec button disabled" data-attribute="adult"><span></span></div>
			                        <div class="adultsCountValWrapper">
			                            <input type="text" class="adultsCountVal" value="<?=$_SESSION['cart']->adults?>" pattern="[0-9]*"
			                                   name="reservationQuantity">
			                            <span class="key">Adults</span>
			                        </div>
			                        <div class="inc button" data-attribute="adult"><span></span><span></span></div>
			                    </div>
<!--								<h1 class="val"><span>$</span><//?=floatval($model->package->adults_price)?></h1>-->
							</div>
						</div>
						</div>

					</div>
					<div>
						<h6 class="gray uppercase valB">Kids</h6>
						<div class="row rowCentered">

							<div class="online_price circle">
								<p class="gray uppercase valB">online</p>
								<h2 class="price"><span>$</span><?=intval($model->package->kids_price)?></h2>
							</div>
							<div class="walkin circle">
								<p class="gray uppercase valB">walkin</p>
								<h2 class="price"><span>$</span><?=intval($model->package->kid_walkin)?></h2>
							</div>
                            						<div class="col quantity">
							<div class="centered">
								<div class="kidsCount">
			                        <div class="dec button disabled" data-attribute="kid"><span></span></div>
			                        <div class="kidsCountValWrapper">
			                            <input type="text" class="kidsCountVal" value="<?=$_SESSION['cart']->kids?>" pattern="[0-9]*"
			                                   name="reservationKidQuantity">
			                            <span class="key">kids</span>
			                        </div>
			                        <div class="inc button" data-attribute="kid"><span></span><span></span></div>
			                    </div>
<!--								<h1 class="val"><span>$</span><//?=floatval($model->package->kids_price)?></h1>-->
			                </div>
						</div>
						</div>
                        
					</div>
				</div>	
				<form class="row reservePackageForm" action="/reservation/resRental" method="post">
					<input type="hidden" name="reservationPackage" value="<?=$model->package->id?>"/>
					<?foreach($model->items as $item) { ?>
						<?php $datetime = $_SESSION['cart']->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $_SESSION['cart']->datetime)->format('M j, Y g:i a') : ''; ?>
						<input type="text" class="triggerDatepicker" hidden name="reservationDateTime[]" placeholder="When?" value="<?= strtotime($datetime) ?>">
						<input type="text" class="triggerDatepicker" hidden name="reservationPackageType[]" placeholder="When?" value="<? $n = new ReflectionClass($item);echo $n->getShortName() ?>">
						<input type="text" class="triggerDatepicker" hidden name="reservationPackageId[]" placeholder="When?" value="<?= $item->id ?>">
					<? } ?>


						<div class="col subtotal">
							<div class="centered">
								<h6 class="valB uppercase">Sub total</h6>
								<h1 class="val packageTotal">$<?=$model->total?></h1>
							</div>
						</div>
					<div class="continueBar row">
						<div class="resPackage">
							<div class="inputBtnWrapper">
								<input type="submit" value="Book Now" class="primaryBtn btn">
							</div>
						</div>
					</div>
				</form>		
				<div class="descriptionText">
                    <div class="mainDescriptionText">
					<p><?=$model->package->description?></p>
                        </div>
					<div class="packageAvailability" style="display:none;">
						<h3 class="val">Availability</h3>
						<div class="descriptionText">
							<p>
							<?foreach(json_decode($model->package->tour_id) as $value) {
								$tour = \Model\Tour::getItem($value);
								if ($tour->available_range && json_decode($tour->available_range, true)) {
									$occurrence = '';
									foreach (json_decode($tour->available_range, true) as $syntax => $value) {
										switch ($syntax) {
											case 'o':
												$occurrence = $value;
												break;
											case 'w':
												$str[] = implode(', ', array_map(function ($item) {
													return ucfirst($item);
												}, $value));
												break;
											case 'd':
												$str[] = implode(', ', array_map(function ($item) {
													return ucfirst($item);
												}, $value));
												break;
											case 'h':
												$str[] = implode(', ', array_map(function ($item) {
													if ($item < 12) {
														$str = fmod($item, 1) == 0 ? $item . 'AM' : floor($item) . ':30AM';
														return $str;
													} else if ($item == 12 || $item == 12.5) {
														$str = fmod($item, 1) == 0 ? $item . 'PM' : floor($item) . ':30PM';
														return $str;
													} else if ($item == 24 || $item == 24.5) {
														$str = fmod($item, 1) == 0 ? ($item - 12) . 'AM' : floor($item - 12) . ':30AM';
														return $str;
													} else {
														$str = fmod($item, 1) == 0 ? ($item - 12) . 'PM' : floor($item - 12) . ':30PM';
														return $str;
													}
												}, $value));
												break;
										}
									}
									$availability = $occurrence . ' (' . implode(' at ', $str) . ')';
								} else {
									$availability = 'Tour Specific';
								}
							}?>
							<?=$availability?>
							</p>
						</div>

					</div>
				</div>

				<div class="packageTours">

					<h3 class="val">Included Tours</h3>
					<?foreach(json_decode($model->package->tour_id,true) as $value){
						$tour = \Model\Tour::getItem($value);?>
					<div class="descriptionText">
						<div class="single_entity">
							<div class="tour_header_pack">
								<img class="open-sesame" src="<?=FRONT_IMG?>plus-symbol.png" >
								<img class="close-shut" src="<?=FRONT_IMG?>minus-symbol.png" style="display:none;">
								<a class='tour_open <? if($tour->main_route_id) echo "show_map' data-routeData='".\Model\Main_Route::getItem($tour->main_route_id)->route_data?>'>
									<h3 class="val"><?=$tour->name?><br>
										<i>(Duration Hours: <?=$tour->duration_hour?>)</i></h3>
								</a>
								<div class="media" style="background-image:url(<?=UPLOAD_URL.'tours/'.$tour->featured_image?>)"></div>
                                <div>
                                	<h5 class="val"><?=$tour->name?></h5>
                                    <h6><?=substr($tour->summary,0,100)?>...</h6>
                                    <div class="rating">
                                        <span>☆</span><span>★</span><span>★</span><span>★</span><span>★</span>
                                    </div>
                                    <h6 class="val"><?=$tour->description?></h6>
                                    <div class="pass_validity">
<!--                                         <img src="<//?= FRONT_IMG ?>valid_pass.png">
                                        <div class="text">
                                            <h6>Pass Validity</h6>
                                            <p><//?=$tour->name?> - Pass valid for 48 hours from the time of
                                                ticket redemption. Please find more information on how to redeem your tickets <span><a
                                                            href="">here</a></span></p>
                                        </div> -->
                                        <!-- <h3 class="valB">About this tour</h3> -->
                                        <?= $tour->details ?>
                                    </div>
                                    <div class="equipmentFreeSection row equipmentFreeSection_tour">
     	                               <?$routePath = \Model\Route_Path::getList(['where'=>"main_route_id = {$tour->main_route_id}"]);
									$googlePath = "[";
									   $route_num = 0;?>
										<!--                             <div id="slider">
																		  <a href="#" class="control_next">>></a>
																		  <a href="#" class="control_prev"><</a>
																		  <ul>
																			<li> -->
										<?foreach ($routePath as $route_path){
											$route = \Model\Route::getItem($route_path->route_id);
											$routeString = $route->points;
											$routeString = preg_replace("/[\[\]\"]/","",$routeString); //Format should now be -00.0000, +00.000
											$routeString = str_replace(", ",", lng: ",$routeString); //Format should now be -00.0000, lng: +00.000
											$routeString = "{lat: ".$routeString."},";
											$googlePath = $googlePath.$routeString; ?>
												<? if (json_decode($route_path->route_icon_ids,true)) {
													$route_num++;
												if($route_num == 4) {?>
													<div class = "col expand_routes">
														<div class="showHideMore">
															<h2 class="show-hide">Show More...</h2>
															<h2 class="show-hide" style="display:none">Show Less...</h2>
														</div>
													</div>
												<?} ?>
											<div class='col <?=($route_num >= 4) ? "expanded_routes' style='display:none" : "" ?>'>
												<!-- <img src="<//?= $route->fullFeaturedImagePath() ?>"> -->
												<div class="more_packages">
													<div class="title_bar open">
														<h2><?=$route->name?></h2>
														<span class="open_sesame"><a href=""><img
																		src="<?= FRONT_IMG ?>plus_open.png"></a></span>
													</div>
													<div class="more_content">
														<!-- <p><//?=$route->description?></p> -->
													<?foreach (json_decode($route_path->route_icon_ids,true) as $route_icon_id){
														$rii = \Model\Route_Icon::getItem($route_icon_id)?>

															<img src="<?=$rii->fullIconPath()?>"/>
															<!-- <p><//?=$rii->name?></p> -->

														<?}?>
													</div>
												</div>
											</div>
										<?}?>
									<?}?>
									<?$googlePath = rtrim($googlePath,",")."]"?>
										<!--                                     </li>
  								</ul>
                                            </div> --><!-- #slider -->
                                    </div>
                                    <?php /**<a class="btn btnLinkArrow invBtn arrowBtn" href="/tours/tour/<?=$tour->slug?>" target="_blank">Learn more</a>**/ ?>
                                </div>
                            </div>
						</div>
					</div>
					<?}?>
				</div>
				<div class = 'halfContainer' id="sticker">
					<div class ='google-map'>
						<iframe id='routeDisplay' src="https://www.google.com/maps/d/embed?mid=1FAuSP2TaxcLz5ZLd76pM6VsRl2E" width="100%" height="440"></iframe>
					</div>
				</div>
				</div>
				<!-- </div> -->

                <?if(isset($model->package->attractions)){?>
                    <div class="packageAttractions">
                        <h3 class="val">Attractions you'll visit</h3>
                        <div class="descriptionText">
                            <?foreach(json_decode($model->package->attractions,true) as $value){
                                $att = \Model\Attraction::getItem($value);
                                $trimmedDesc = strip_tags($att->description);
                                echo '<div><div class="media" style="background-image:url('.UPLOAD_URL.'attractions/'.$att->featured_image.')"></div><h5 class="val">'.$att->name.'</h5><p>'.substr($trimmedDesc,0,100).'...</p><a class="btn btnLinkArrow invBtn arrowBtn" href="/attractions/'.$att->slug.'" target="_blank">Learn more</p></a></div>';
                            }?>

                        </div>
                        <a class="btn btnLinkArrow invBtn arrowBtn" style="font-size:20px;text-transform:uppercase;"href="/attractions">View More Attractions<p></p></a>
                    </div>
                <?}?>
			</div>
		</div>

</section>
<section class="" style="display:none;">
<div class="column_packages">
    <!-- <h5>Arrive a tourist. Leave a local.</h5> -->
    <?if($model->featuredPackages){?>
        <?php foreach($model->featuredPackages as $package){ ?>
            <div class="solo_package">
                <a class="learnMore" href="<?='/packages/package/'.$package->slug?>">
                    <img src="<?=UPLOAD_URL.'packages/'.$package->featuredImage()?>"/>
                    <div class="package_name">
                        <h5>
                            <?=$package->name?>
                        </h5>
                    </div>
                </a>
                <div class="comparativePricingFormat row imageOverlay home-pricing">
                    <div class="starting_price">
                        <p>Adult starts at only <span class="big">$<?=number_format($package->adults_price,0)?></span></p>
                    </div>


                    <div class="act_now">
                        <a href="#" class="selectPackage" data-package_id="<?=$package->id?>">Buy Now</a>
                        <!-- <a class="learnMore" href="<//?='/packages/package/'.$package->slug?>">Learn More</a> -->
                    </div>
                </div>

            </div>
        <?}?>
    <?}?>
</div>
</section>


	<section class="bottomSection">
		<div class="row row_of_3 featuredLinks">
			<?foreach($model->tour_banner as $value){?>
				<div  class="col">
					<h4 class="valB medium"><?=$value->title?></h4>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'banners/'.$value->image?>')"></div>
					<p class="valB"><?=$value->description?></p>
					<a class="invBtn btn arrowBtn learnMore" href="<?=$value->url?>">
						<?=isset(json_decode($value->details,true)['url_text']) ? json_decode($value->details,true)['url_text']: 'Explore our tours here'?>
						<icon style="background-image:url('<?=FRONT_IMG?>btnLinkArrow.png')"></icon>
					</a>
				</div>
			<?}?>
		</div>
	</section>
</div>

<style>
    .fixedReserveRow {
        display: none !important;
    }
</style>


<script type="text/javascript">

$(document).ready(function () {
	$(".tour_header_pack div").hide();
	$("a.tour_open").click(function(){
		$(this).parents(".tour_header_pack").find("div").not('.expanded_routes').toggle();
		$(this).parents(".tour_header_pack").find("img.open-sesame").toggle();
		$(this).parents(".tour_header_pack").find("img.close-shut").toggle();
	});
	$(".expand_routes").click(function(){
		$(this).siblings('.expanded_routes').toggle();
		$(this).find('.show-hide').toggle();
	});
}); 
</script>
<!-- Google Map -->
<script type="text/javascript">
/*	function initMap() {
		var map = new google.maps.Map(document.getElementById("tourMap"),{zoom:12, center:	{lat:40.706336, lng: -73.982105}});

		var googlePath= <?=$googlePath?>;
		var tourPath = new google.maps.Polyline({
			path: googlePath,
			geodesic: true,
			strokeColor: '#FF0000',
			strokeOpacity: 1.0,
			strokeWeight: 2
		});

		tourPath.setMap(map);
	}
	$(document).ready(function(e){

		initMap();
	});*/
</script>
<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAo94coA_S_IuJUUMTBr9Tw3pwMUpkzqME&callback=initMap">
</script>-->
<script>
	$(document).ready(function(){
		var adultPrice = <?=$model->package->adults_price?>;
		var kidPrice = <?=$model->package->kids_price?>;
		$('.inc,.dec').on('click',function(){
			var adults = $('.adultsCountVal').val();
			var kids = $('.kidsCountVal').val();
			var adultTotal = adults * adultPrice;
			var kidTotal = kids * kidPrice;
			$('.packageTotal').html('$'+(adultTotal+kidTotal));
		});
		$('.reservePackageForm').on('submit',function(e){
            var adults = $('.adultsCountVal').val();
            var kids = $('.kidsCountVal').val();
            if(adults + kids == 0){
                e.preventDefault();
                $('.resPackage').append('<div class="resBar_error resBar_error_count"><p class="errorText">Please select how many adults or kids</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
            }
        });

		$('a.show_map').click(function(){
			console.log("SDasd");
			if($(this).data('routedata')){
				$('#routeDisplay').attr('src', $(this).data('routedata')).removeAttr('hidden');
			}
		});
	});
</script>
<script>
	$('a.bookPkge').click(function(){
		$(window).scrollTop($('.textualInfo').offset().top);
	});
</script>

<!-- Route Slider -->
<script type="text/javascript">
	jQuery(document).ready(function ($) {

  $('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  });
  
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.control_prev').click(function (e) {
    	e.preventDefault();
        moveLeft();
    });

    $('a.control_next').click(function (e) {
    	e.preventDefault();
        moveRight();
    });

});    

</script>

<!--Map fixed on scroll-->
<script>
  $(document).ready(function(){
    $("#sticker").sticky({topSpacing:50});
  });
</script>

<!--Prevent map from overlapping section below-->
<script type="text/javascript">
$(window).scroll(function () { 

// distance from top of footer to top of document
footertotop = ($('.featuredLinks').position().top);
// distance user has scrolled from top, adjusted to take in height of sidebar (570 pixels inc. padding)
scrolltop = $(document).scrollTop()+490;
// difference between the two
difference = scrolltop-footertotop;

// if user has scrolled further than footer,
// pull sidebar up using a negative margin

if (scrolltop > footertotop) {

$('#sticker').css('margin-top',  0-difference);
}

else  {
$('#sticker').css('margin-top', 0);
}


});
</script>

<script>
$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 800) {
    $('.bookNowHeader').fadeIn();
  } else {
    $('.bookNowHeader').fadeOut();
  }
});
</script>
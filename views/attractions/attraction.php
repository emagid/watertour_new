<div class="pageWrapper attractionPageWrapper">


	<section class="topSection">
		<div class="promoBar">
			<p class="valB">Discover a great package deal for tickets to <?=$model->attraction->name?> with Central Park Bike Tours</p>
		</div>	
		<div class="heroContainer">
			<div class="absTransCenter heroTextFixed heroText">
				<h1 class="biggest valB"><?=$model->attraction->name?></h1>
				<h3 class="valB valUpper"></h3>
				<?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
			</div>
			<div class="heroSlideshow heroSlideshowBikeTours">
				<?foreach($model->attraction_images as $image){?>
					<div class="slide">
						<div class="blurBar">
							<div class="mediaBlurredBar media" style="background-image:url('<?=UPLOAD_URL.'attractions/'.$image->image?>');"></div>
						</div>
						<div class="media" style="background-image:url('<?=UPLOAD_URL.'attractions/'.$image->image?>')"></div>
					</div>
				<?}?>
			</div>
		</div>
	</section>	
	<section class="middleSection">
		<div class="row attractionInfo">
			<div class="col right_float imagesCalls">
				<div class="verticalMedia media">
					<img src="<?=UPLOAD_URL.'attractions/'.$model->attraction->featured_image?>">
				</div>
				<div class="attractionDetailMap">
					<iframe src="https://www.google.com/maps/embed/v1/place?key=<?=GOOGLE_MAPS_API_KEY?>&q=<?=htmlentities($model->attraction->name)?> Central park" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="availablePackagesWrapper">
					<?foreach($model->related as $item){?>
						<div class="availablePackagesGrid">
						<div class="row packageGridItem">
							<div class="textualData">
								<h2 class="val"><?=$item->getName()?></h2>
								<h4 class="val gray"><?=date('l M d, Y')?><span class="middot"></span><i class="material-icons">timelapse</i><?=$item->getDurationFormatted()?></h4>
								<div class="lineSep"></div>
							</div>
							<div class="row row_of_2">
								<div class="pricing col">
									<h6 class="gray uppercase valB">Starting at</h6>
									<h2 class="price"><span>$</span><?=$item->getPrice()?></h2>
									<h6 class="gray uppercase valB">per person</h6>
								</div>
								<div class="bookBtnWrapper col">
									<a class="btn fullBtn primaryBtn">Book this Combo</a>
								</div>
							</div>
						</div>
					</div>
					<?}?>
					<div class="footer">
						<a class="btn primaryBtn" href="/packages?f=<?=$model->attraction->slug?>">View More Packages</a>
					</div>
				</div>
			</div>
			<div class="col left_float textualInfo">
				<div class="row">
					<div class="col left_float">
						<h3 class="val"><?=$model->attraction->name?><span class="middot"></span>The Highlights</h3>
					</div>
				</div>
				<div class="descriptionText">
					<?=$model->attraction->description?>
				</div>
<!-- 				<div class="learnMoreLinkWrapper">
					<p>Learn more at</p>
					<a class="invBtn btn arrowBtn storeVisit" href="<//?=$model->attraction->external_link?>">
						<//?=$model->attraction->external_link?>
						<icon style="background-image:url('<//?=FRONT_IMG?>btnLinkIcon.png')"></icon>
					</a>
				</div> -->
			</div>
			<div class="imagesCalls availablePackagesWrapper_mobile">
				<div class="availablePackagesWrapper">
					<div class="availablePackagesGrid">
						<div class="row packageGridItem">
							<div class="textualData">
								<h2 class="val">MoMA + Bike Rental Package</h2>
								<h4 class="val gray">Friday October 14, 2016 at 10:00 AM <span class="middot"></span><i class="material-icons">timelapse</i>1 Hour</h4>
								<div class="lineSep"></div>
							</div>
							<div class="row row_of_2">
								<div class="pricing col">
									<h6 class="gray uppercase valB">Starting at</h6>
									<h2 class="price"><span>$</span>65</h2>
									<h6 class="gray uppercase valB">per person</h6>
								</div>
								<div class="bookBtnWrapper col">
									<a class="btn fullBtn primaryBtn">Book this Combo</a>
								</div>
							</div>
						</div>
					</div>	
					<div class="availablePackagesGrid">
						<div class="row packageGridItem">
							<div class="textualData">
								<h2 class="val">MoMA + Bike Rental Package</h2>
								<h4 class="val gray">Friday October 15, 2016 at 10:00 AM <span class="middot"></span><i class="material-icons">timelapse</i>1 Hour</h4>
								<div class="lineSep"></div>
							</div>
							<div class="row row_of_2">
								<div class="pricing col">
									<h6 class="gray uppercase valB">Starting at</h6>
									<h2 class="price"><span>$</span>65</h2>
									<h6 class="gray uppercase valB">per person</h6>
								</div>
								<div class="bookBtnWrapper col">
									<a class="btn fullBtn primaryBtn">Book this Combo</a>
								</div>
							</div>
						</div>
					</div>					
					<div class="footer">
						<a class="btn primaryBtn">View More Packages</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="additionalAttractionsRow row">
			<div class="left_float col">
				<a href="/attractions/<?=$model->prev->slug?>">
					<icon class="leftChevron" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon>
					<p class="val"><?=$model->prev->name?></p>
					<div class="media" style="background-image:url('<?=UPLOAD_URL?>attractions/<?=$model->prev->featuredImage()?>')">
					</div>
				</a>
			</div>
			<div class="right_float col">
				<a href="/attractions/<?=$model->next->slug?>">
					<div class="media" style="background-image:url('<?=UPLOAD_URL?>attractions/<?=$model->next->featuredImage()?>')">
					</div>
					<p class="val"><?=$model->next->name?></p>
					<icon class="leftChevron" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon>
				</a>
			</div>
		</div>

	</section>
	<section class="bottomSection">
		<div class="row row_of_3 featuredLinks">
			<?foreach($model->tour_banner as $value){?>
				<div class="col">
					<h4 class="valB medium"><?=$value->title?></h4>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'banners/'.$value->image?>')"></div>
					<p class="valB"><?=$value->description?></p>
					<a class="invBtn btn arrowBtn learnMore" href="<?=$value->url?>">
						<?=isset(json_decode($value->details,true)['url_text']) ? json_decode($value->details,true)['url_text']: 'Explore our tours here'?>
						<icon style="background-image:url('<?=FRONT_IMG?>btnLinkArrow.png')"></icon>
					</a>
				</div>
			<?}?>
		</div>
	</section>	
</div>
<!--
        <div class="absHorizCenter reserveBarFixed">
            <//?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
        </div>
-->

<div class="pageWrapper attractionsPageWrapper">
    <div class="attractionsMapSearch row">
        <div class="col attractionsBrowseListings sidebar">
            <div class="header">
                <h1 class="work work_light">Explore Attractions and Things-to-do in NYC</h1>
            </div>
            <div class="filters collapsed">
                <div class="row row_of_3">
                    <div class="col">
                        <div class="typeahead_dropdown_ui">
            <span class="twitter_typeahead" id="names_typeahead">
            <input class="typeahead" type="text" autocomplete="off" spellcheck="false" dir="auto"
                   placeholder="All Attractions" data-placeholder-infocus="Search by Name"
                   data-placeholder-outfocus="All Attractions" id="att_search">
            </span>
                            <span class="chevron_down"></span>
                        </div>
                    </div>
                    <div class="col">
                        <div class="typeahead_dropdown_ui">
            <span class="twitter_typeahead" id="categories_typeahead">
            <input class="typeahead" type="text" autocomplete="off" spellcheck="false" dir="auto"
                   placeholder="All Categories" data-placeholder-infocus="Search Categories"
                   data-placeholder-outfocus="All Categories" id="cat_search">
            </span>
                            <span class="chevron_down"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebarHeader">
            </div>
            <div class="searchResults">
                <div id="listings">
                    <?$marker = 1;
                    foreach($model->attractions as $columns){?>
                        <div class="col">
                            <?foreach($columns as $attraction){?>
                                <div class="listing-item" data-marker-id="<?=$marker?>" data-marker-slug="<?=$attraction->slug?>" data-marker-lat="<?=$attraction->lat?>" data-marker-lng="<?=$attraction->lng?>" data-marker-exh="false" data-marker-letter="<?=strtoupper(substr($attraction->name,0,1))?>" data-marker-title="<?=$attraction->name?>" data-marker-color="<?=$attraction->marker_color ?'#'.$attraction->marker_color:''?>">
                                    <div class="attractionImagesWrapper">

                                        <div class="mainAttractionImage">
                                            <a href="<?= SITE_URL ?>attractions/<?=$attraction->slug?>" class="row">
                                                <img src="<?= UPLOAD_URL.'attractions/'.$attraction->featured_image ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="attraction_listing_textContent">
                                        <div class="row">
                                            <a href="<?= SITE_URL ?>attractions/<?=$attraction->slug?>" class="row">
                                                <h4 class="val gray col"><?=$attraction->name?></h4>
                                            </a>
                                        </div>
                                        <a href="<?= SITE_URL ?>attractions/<?=$attraction->slug?>" class="row">
                                            <?php $trimmedDesc = strip_tags($attraction->description);?>
                                            <h6 class="valB"><?=substr($trimmedDesc,0,140).'...'?></h6>
                                        </a>
                                        <?if ($attraction->hasPackage()) {?>
<!--                                            <a href="<?= SITE_URL ?>packages?f=<?=$attraction->slug?>" class="btn secondary_btn">Browse Packages</a>-->
                                        <a href="<?= SITE_URL ?>packages" class="btn secondary_btn">Browse Packages</a>
                                        <? } else { ?>
                                            <a href="<?= SITE_URL ?>attractions/<?=$attraction->slug?>" class="btn secondary_btn">Learn More</a>
                                        <? } ?>
                                    </div>
                                </div>
                            <?$marker++;}?>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
        <div class="col attractionsBrowseMap map">
            <div id="map-canvas"></div>
            <div class="loader-container">
                <div class="loader-inner">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div class="media blurredMap" style="background-image:url('<?=FRONT_IMG?>blurredCPMap.png')"></div>
        </div>
    </div>


    <div class="responsiveMapResultsToggler">
        <div class="togglerWrapper row">
            <a class="col">
                <p>List</p>
            </a>
            <a class="col active">
                <p>Map</p>
            </a>
        </div>
    </div>
</div>
<script
    src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.22&key=AIzaSyARncqmqm6295FwLX3VorpB1E8ln5WCE-Y">
</script>
<script type="text/javascript" src="<?= FRONT_LIBS ?>customGoogleMapMarker.js"></script>

<script>

    $(document).ready(function () {

        function query(term,cat) {
            $.post('/attraction/query', {term:term,cat:cat}, function (data) {
                var col = '';
                var marker = 1;
                $.each(data,function(i,e){
                    col += '<div class="col">';
                    var html = '';
                    $.each(e,function(a,u){
                        html += '<div class="listing-item" data-marker-id="'+marker+'" data-marker-slug="'+ u.slug+'" data-marker-lat="'+ u.lat+'" data-marker-lng="'+ u.lng+'" data-marker-exh="false" data-marker-letter="'+ u.name.charAt(0)+'" data-marker-title="'+ u.name+'" data-marker-color="'+ (u.marker_color || u.marker_color == 'FFFFFF'? '#'+ u.marker_color:'') +'">';
                        html += '<div class="attractionImagesWrapper">';
                        html += '<div class="mainAttractionImage">';
                        html += '<a href="/attractions/'+ u.slug+'" class="row">';
                        html += '<img src="/content/uploads/attractions/'+ u.featured_image+'"></a></div></div>';
                        html += '<div class="attraction_listing_textContent"><div class="row">';
                        html += '<a href="/attractions/'+ u.slug+'" class="row">';
                        html += '<h4 class="val gray col">'+ u.name+'</h4></a></div>';
                        html += '<a href="/attractions/'+ u.slug+'" class="row">';
                        html += '<h6 class="valB">'+ (u.description.length > 20 ? u.description.substr(0,100)+'...': u.description)+'</h6></a>';
                        if(u.hasPackage){
                            html += '<a href="/packages?f='+ u.slug+'" class="btn secondary_btn">Browse Packages</a>';
                        } else {
                            html += '<a href="/attractions/'+ u.slug+'" class="btn secondary_btn">Learn More</a>';
                        }
                        html += '</div></div>';
                        marker++;
                    });
                    col += html;
                    col += '</div>';
                });
                $('#listings').empty().append(col);

                /**
                 * After reload DOM, we need reload all marks
                 * */
                reloadMarkers();
            });
        }

        $(document).on("click", ".color_block", function () {
            window.location.replace('/attractions/'+$(this).data('slug'));
        });
        $(document).on("mouseover", ".attractionImageSlide > a", function () {
            $(".listing-item").removeClass("hoverActiveItem");
            $(this).closest(".listing-item").addClass("hoverActiveItem");
        });
        $(document).on("mouseover", ".attraction_listing_textContent a", function () {

            $(".listing-item").removeClass("hoverActiveItem");
            $(this).closest(".listing-item").addClass("hoverActiveItem");
        });
        $(document).on("mouseleave", ".attractionImageSlide > a", function () {
            $(".listing-item").removeClass("hoverActiveItem");
        });
        $(document).on("mouseleave", ".attraction_listing_textContent a", function () {
            $(".listing-item").removeClass("hoverActiveItem");
        });

        $(document).on("mouseover", "#map-canvas .htmlMarker", function () {
            $("this").addClass("markerActive");
        });        
        $(document).on("mouseleave", "#map-canvas .htmlMarker", function () {
            $("this").removeClass("markerActive");
        });

        // Make Plot Points From Results DOM Elements
        function makeMapPlotPoints() {

            // Set marker from results list and create empty plot point array
            var mapPlotPointDOM = $(".listing-item");
            var mapPlotPointArr = [];

            $(mapPlotPointDOM).each(function () {
                if ($(this).data("marker-lat") !== '') {
                    mapPlotPointArr.push([
                        $(this).data("marker-id"),
                        $(this).data("marker-lat"),
                        $(this).data("marker-lng"),
                        $(this).data("marker-exh"),
                        $(this).data("marker-color"),
                        $(this).data("marker-letter"),
                        $(this).data("marker-title"),
                        $(this).data("marker-slug")
                    ]);

                }
            });
            setMarkers(mapPlotPointArr);
        };

        var map;
        var markers = []; // Create a marker array to hold markers

        //create empty LatLngBounds object
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();
        var center = {
            lat: 0,
            lng: 0
        };

        var overlay;

        function setMarkers(locations) {
            for (var i = 0; i < locations.length; i++) {

                var mapMarkerItem = locations[i];
                var myLatLng = new google.maps.LatLng(mapMarkerItem[1], mapMarkerItem[2]);

                //to use it
                var htmlMarker = new HTMLMarker(mapMarkerItem[1], mapMarkerItem[2], mapMarkerItem[0], mapMarkerItem[3], mapMarkerItem[4], mapMarkerItem[5], mapMarkerItem[6], mapMarkerItem[7]);
                htmlMarker.setMap(map);


                // Set Map Bounds to Auto-center
                bounds.extend(myLatLng);
//                map.fitBounds(bounds);

                // Push marker to markers array
                //markers.push(marker);
                markers.push(htmlMarker);

                // Marker Info Window / Tooltip
                google.maps.event.addListener(htmlMarker, 'click', (function (htmlMarker, i) {
                    return function () {
                        infowindow.setContent(locations[i][5]);
                        infowindow.open(map, htmlMarker);
                    }
                })(htmlMarker, i));
                /*
                 google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                 return function() {
                 marker.setIcon(iconHover);
                 }
                 })(marker, i));

                 google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
                 return function() {
                 marker.setIcon(iconStandard);
                 }
                 })(marker, i));
                 */
            }
        }


        function reloadMarkers() {
            $('.htmlMarker').remove();
            // Loop through markers and set map to null for each
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }

            // Reset the markers array
            markers = [];

            // valBl set markers to re-add markers
            makeMapPlotPoints();
        }

        function HTMLMarker(lat, lng, text, exh, color, letter, title,slug) {
            this.lat = lat;
            this.lng = lng;
            this.pos = new google.maps.LatLng(lat, lng);
            this.text = text;
            this.exh = exh;
            this.color = color;
            this.letter = letter;
            this.title = title;
            this.slug = slug;
        }

        HTMLMarker.prototype = new google.maps.OverlayView();
        HTMLMarker.prototype.onRemove = function () {
        }

        //init your html element here


        HTMLMarker.prototype.onAdd = function () {
            console.log(this.color);
            var div = document.createElement('DIV');
            div.style.position = 'absolute';
            div.className = "htmlMarker";
            div.setAttribute("hoverConnect", this.text);
            div.setAttribute("backgroundColor", this.color);
            div.setAttribute("exhActive", this.exh);
            div.innerHTML = "<label>" + this.letter + "</label><span class='whiten_block'></span><span class='color_block' style='background-color:" + this.color + "' data-slug='"+this.slug+"'><span>"+ this.title +"</span></span><strong></strong><span class='color_block_triangle' style='background-color:" + this.color + "'></span>";
            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
            this.div = div;
            ensureColorsApplied();
        }

        HTMLMarker.prototype.draw = function () {
            var overlayProjection = this.getProjection();
            var position = overlayProjection.fromLatLngToDivPixel(this.pos);
            var panes = this.getPanes();
            this.div.style.left = position.x + 'px';
            this.div.style.top = position.y - 10 + 'px';
        }


        function initializeGalleriesMap() {

            var mapOptions = {
                zoom: 16,
                maxZoom: 18,
                minZoom: 2,
                center: new google.maps.LatLng(40.7743009,-73.9708426),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            }

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            makeMapPlotPoints();

        }

        function ensureColorsApplied() {
            $("#listings .listing-item").each(function () {
                var color = $(this).attr("data-marker-color");
                var id = $(this).attr("data-marker-id");
                var $correlatedMarker = $("#map-canvas .htmlMarker[hoverconnect='" + id + "']");
                $correlatedMarker.css({'background-color': color});
                $correlatedMarker.find('.color_block').css({'background-color': color});
                $correlatedMarker.find('.color_block_triangle').css({'border-top-color': color});
            });
        }

        applyColors();
        setTimeout(function () {
            initializeGalleriesMap();
            applyColors();
        }, 5000);
        function applyColors() {
            $(".mainAttractionImage > img").each(function () {
                var i = $(this);
                var image = i[0];
                var vibrant = new Vibrant(image, 64, 1);
                var swatches = vibrant.swatches();
                console.log(swatches);

                if (swatches['LightVibrant']) {
                    var hexVibrant = "#f18771";
                    $(this).closest(".listing-item").find("span.attractionIconColor").css("background-color", hexVibrant);
                    $(this).closest(".listing-item").attr("data-marker-color", hexVibrant);
                    $(this).closest('.attractionImagesWrapper').find('.color_block').css("background-color", hexVibrant);
                }
                initializeGalleriesMap();
                if (swatches['DarkMuted']) {
                    var hexDarkMuted = "#f18771";
                }
            });
        }

        var categories = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('category'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) {
                return obj.category;
            },
            local: <?=$model->categories?>
        });

        function categoryDefaults(q, sync) {
            if (q === '') {
                sync(categories.get('<?=$model->preload?>'));
            }

            else {
                categories.search(q, sync);
            }
        }


        $("body").on("click","#categories_typeahead .typeahead",function(event){

            if(!$(this).hasClass('typeaheadOpen')){
                $(this).addClass('typeaheadOpen');
                $('#names_typeahead .typeahead').removeClass('typeaheadOpen');
                $('#categories_typeahead .typeahead').typeahead('open');
            }else{
                $(this).removeClass('typeaheadOpen');
                $('#categories_typeahead .typeahead').typeahead('close');
            }
        });       

        $("body").on("click","#names_typeahead .typeahead",function(event){
            if(!$(this).hasClass('typeaheadOpen')){
                $(this).addClass('typeaheadOpen');
                $('#categories_typeahead .typeahead').removeClass('typeaheadOpen');
                $('#names_typeahead .typeahead').typeahead('open');                
            }else{
                $(this).removeClass('typeaheadOpen');
                $('#names_typeahead .typeahead').typeahead('close');
            }

        });

  


        $('#categories_typeahead .typeahead').typeahead({
                minLength: 0,
                highlight: true
            },
            {
                name: 'category-search',
                limit:40,                
                display: 'category',
                source: categoryDefaults
        });

        var attr = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('attr'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) {
                return obj.attr;
            },
            local: <?=$model->attr?>
        });

        function attrDefaults(q, sync) {
            if (q === '') {
                sync(attr.get('<?=$model->pPreload?>'));
            }

            else {
                attr.search(q, sync);
            }
        }

        $('#names_typeahead .typeahead').typeahead({
                minLength: 0,
                highlight: true
            },
            {
                name: 'attr-search',
                limit:40,
                display: 'attr',
                source: attrDefaults
        });

        var catSearch = $('#cat_search');
        var attSearch = $('#att_search');
        catSearch.bind('typeahead:selected',function(obj,datum,name){
            query(attSearch.val(),datum.category);
        });
        attSearch.bind('typeahead:selected',function(obj,datum,name){
            query(datum.attr,catSearch.val());
        });
        catSearch.add(attSearch).on('input',function(){
            if($(this).val() == ''){
                query(attSearch.val(),catSearch.val());
            }
        });

        $(".pageWrapper.attractionsPageWrapper").on("mouseover", ".attractionsBrowseListings #listings .listing-item", function () {
            $("#map-canvas .htmlMarker").removeClass("markerActive");
            var marker_id = $(this).attr("data-marker-id");
            $("#map-canvas .htmlMarker[hoverConnect='" + marker_id + "']").addClass("markerActive");
            var lat = $(this).attr("data-marker-lat");
            var lng = $(this).attr("data-marker-lng");
            if(map){
                map.panTo(new google.maps.LatLng($(this).attr("data-marker-lat"), $(this).attr("data-marker-lng")));
            }

        });
    });


</script>


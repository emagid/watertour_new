<style type="text/css">
    .map {
        text-align: center;
        width: 800px;
        height: 600px;
    }
    .google-maps {
        position: relative;
        padding-bottom: 120%;
        height: 0;
        overflow: hidden;
        text-align: center;
    }
    .accordion_row .hidden_portion {
        padding-left: 20px;
        padding-top: 5px;
        padding-bottom: 15px;
    }
    .route-stops {
        font-weight: 500;
        letter-spacing: .005em;
        line-height: 1.45;
        opacity: .7;
        font-size: 14.5px;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .route-stops-name {
        letter-spacing: .005em;
        line-height: 1.45;
        opacity: .7;
        font-size: 13px;
        margin-bottom: 10px;
    }
    @media only screen and (max-width: 900px){
        .halfContainer{
            width: 95%;
            display:block;
            float: none;
            margin: auto;

        }
    }
</style>

<div class="pageWrapper aboutPageWrapper cmsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php')?>
        </div>
    </div>
    <div class='mainContainer routeContainer'>
        <div class='container' >
            <div class="routes_map_title">
                <div class="routes_icon">
                    <img src="<?= FRONT_IMG ?>theatre-masks.png">
                </div>
                <div class="routes_icon">
                    <img src="<?= FRONT_IMG ?>cutlery.png">
                </div>
                <div class="routes_icon">
                    <img src="<?= FRONT_IMG ?>library.png">
                </div>
                <div class="routes_icon">
                    <img src="<?= FRONT_IMG ?>purse.png">
                </div>
            </div>

            <div class="routes_map_header">
                <div class="routes_map_btn">
                    <div class= 'visible_portion routeSelection active' id = 'fullRoute' data-routeID='0' data-routedata='https://www.google.com/maps/d/u/0/embed?mid=1NK_2qDq7Dc3gIPadCfKiWx8Otoo'>
                        <h4>Full Route</h4>
                    </div>
                </div>
                <? foreach($model->routes_data as $route) {?>
                    <div class="routes_map_btn">
                        <div class = 'visible_portion routeSelection active' data-routeID='<?=$route->id?>' data-routedata='<?=$route->route_data?>'>
                            <h4><?=$route->name?></h4>
                        </div>
                        <div class="" hidden>
                            <div class="features_row row row_4">
                                <? $routePath = \Model\Route_Path::getList(['where'=>"main_route_id = {$route->id}",'orderby'=>"display_order ASC"]);
                                $stopIndex = 1; ?>
                                <? foreach($routePath as $route_path){ ?>
                                    <? $stop = \Model\Route::getItem($route_path->route_id); ?>
                                    <? if (json_decode($route_path->route_icon_ids,true)) {?>
                                        <div class="stop" data-routeID='<?=$route->id?>' data-markerID="<?=$stopIndex?>">
                                            <? $stopIndex++; ?>
                                            <div class="route-stops"><b><?=$stop->name?></b></div>
                                            <div class="route-stops-name">
                                                <b>
                                                    <?foreach (json_decode($route_path->route_icon_ids,true) as $route_icon_id){
                                                        $rii = \Model\Route_Icon::getItem($route_icon_id)?>
                                                        <img src="<?=$rii->fullIconPath()?>"/>
                                                    <?}?>
                                                </b>
                                            </div>
                                        </div>
                                    <? } ?>
                                <?}?>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>


            <div class= 'mobile_portion' style="display:none;">
                <h4>Attractions</h4>
            </div>
            <div class='halfContainer' id="routes">
                <div class="routes_attractions_block">
                    <div class= 'visible_portion' id = 'attractions'>
                        <h4>Attractions</h4>
                    </div>
                    <div class= "attractions_list" style="height: 400px; overflow-y:auto">
                        <? $attractionCount = 0?>
                        <? foreach($model->attractions as $attraction) {?>
                            <? if($attractionCount >= 0) {?>
                                <div class="row row_4 attractions_row" data-index="<?=$attractionCount?>">
                                    <p><?=$attraction['name']?></p>
                                </div>
                            <?} ?>
                            <? $attractionCount++ ?>
                        <?} ?>
                    </div>
                </div>
            </div>

            <div class = 'halfContainer' id="maps">
                <div id ='tourMap' style='height:660px; width:100%'>
                </div>
            </div>
            <div style='clear:both'></div>
        </div>
    </div>
    <!--<div id="map"></div>-->
</div>


<script>
    $(document).ready(function(){
        $(".mobile_portion").click(function () {
            $(".attractions_list").toggle();
        });

    });
</script>


<script type="text/javascript">
    var tourPaths = {};
    var tourAttractions = {};
    var attractions = [];
    var matchAttractions = {};
    var attrInfo = {};
    var routeBounds = {};
    var googleMap;
    function initMap() {
        routeBounds.fullAttr = new google.maps.LatLngBounds();
        routeBounds.full = new google.maps.LatLngBounds();
        googleMap = new google.maps.Map(document.getElementById("tourMap"),{zoom:12, center:	{lat:40.706336, lng: -73.982105}});

        $.get('/nycguide/getRoutesData', function(r){
            var readit;

            for(let mainRoute in r.data){
                (function()
                {
                    routeBounds[mainRoute] = new google.maps.LatLngBounds();
                    readit = r.data[mainRoute].googlePath;
                    readit.forEach(function (coord) {
                        coord.lat = parseFloat(coord.lat);
                        coord.lng = parseFloat(coord.lng);
                        routeBounds.full.extend(coord);
                        routeBounds[mainRoute].extend(coord);
                    });
                    if (r.data[mainRoute].hasOwnProperty("attractions")) {
                        tourAttractions[mainRoute] = [];
                        attrInfo[mainRoute] = [];

                        for (let marker_id in r.data[mainRoute]["attractions"]) {
                            _marker = r.data[mainRoute]["attractions"][marker_id];
                            _marker.coordinates.lat = parseFloat(_marker.coordinates.lat);
                            _marker.coordinates.lng = parseFloat(_marker.coordinates.lng);

                            let icon_path = 'https://<?=$_SERVER['SERVER_NAME'].FRONT_ASSETS?>icon/'+r.data[mainRoute].color+'_icon.png';
                            let marker = new google.maps.Marker({
                                position: _marker.coordinates,
                                map: googleMap,
                                icon: icon_path ,
                                title: mainRoute
                            });
                            marker.set('color',r.data[mainRoute].color);
                            let infowindow = new google.maps.InfoWindow({
                                content: "<div>" +
                                "<h2>" + _marker.name + "</h2>" +
                                "</div>"
                            });

                            attrInfo[mainRoute][marker_id] = infowindow;
                            marker.addListener('click', function () {
                                attrInfo[mainRoute][marker_id].open(googleMap, tourAttractions[mainRoute][marker_id]);
                            });
                            tourAttractions[mainRoute].push(marker);
                            if(matchAttractions.hasOwnProperty(_marker.name)){
                                matchAttractions[_marker.name].push(marker);
                            } else {
                                matchAttractions[_marker.name] = [marker];
                            }
                        }
                    }
                    var tourPath = new google.maps.Polyline({
                        path: readit,
                        geodesic: true,
                        strokeColor: r.data[mainRoute].color,
                        strokeOpacity: 1.0,
                        strokeWeight: 5
                    });
                    tourPaths[mainRoute] = tourPath;
                    /* tourPath.addListener('click', function () {
                     showRoute(mainRoute);
                     });*/
                    tourPath.setMap(googleMap);
                }());
            }

            //googleMap.fitBounds(routeBounds.full);
            console.log(readit);
            $(".stop").click(function(){
                var tourLength = tourAttractions[$(this).data("routeid")].length;
                var marker  = tourAttractions[$(this).data("routeid")][tourLength-$(this).data("markerid")];
                google.maps.event.trigger(marker, 'click');
            });

            $.get('/nycguide/getAttractionsData', function(r){
                console.log('success');

                for(let i in r.data){
                    let _marker = r.data[i];
                    _marker.coordinates.lat = parseFloat(_marker.coordinates.lat);
                    _marker.coordinates.lng = parseFloat(_marker.coordinates.lng);
                    routeBounds.fullAttr.extend(_marker.coordinates);

                    let marker = new google.maps.Marker({
                        position:  _marker.coordinates,
                        map: null, //Change to googleMap to display by default
                        title: _marker.name
                    });
                    let info = new google.maps.InfoWindow({
                        content: "<div>" +
                        "<h2>" + _marker.name + "</h2>" +
                        "</div>"
                    });
                    marker.addListener('click', function () {
                        attractions[i][1].open(googleMap, marker);
                    });
                    attractions.push([marker, info]);
                    let icon_path = 'https://<?=$_SERVER['SERVER_NAME'].FRONT_ASSETS?>icon/';
                    if(matchAttractions.hasOwnProperty(_marker.name)) {
                        let color = '';
                        let routes = 0;
                        for(let m in matchAttractions[_marker.name]){
                            color = color+matchAttractions[_marker.name][m].color;
                            if(!color) color = "barney";
                            routes++;
                        }
                        icon_path = icon_path+color+'_icon.png';
                        if( routes > 1 ) icon_path = icon_path+'barney_icon.png';
                    } else {
                        icon_path = icon_path+"barney_icon.png";
                    }
                    marker.setIcon(icon_path);
                }
                googleMap.fitBounds(routeBounds.full);
                $('.attractions_row').click(function(){
                    console.log('huh');
                    let attraction = attractions[$(this).data('index')][0];
                    if(attraction.getMap()){
                        attraction.setMap(null);
                    } else {
                        attraction.setMap(googleMap);
                        let coord = attraction.getPosition();
                        googleMap.setCenter(coord);
                        google.maps.event.trigger(attraction, 'click');
                    }
                });
            });
        });
    }
    function toggleRoute(routeID){
        console.log("hooplah");
        if(routeID >0){
            let path = tourPaths[routeID];
            if(path.getMap()){
                path.setMap(null);
//            $(".routeSelection[data-routeid='" + routeID + "']").removeClass('active');
                if (tourAttractions.hasOwnProperty(routeID)){
                    for(var marker in tourAttractions[routeID]){
                        tourAttractions[routeID][marker].setMap(null);
                    }
                }
            } else {
                path.setMap(googleMap);
//            $(".routeSelection[data-routeid='" + routeID + "']").addClass('active');
                googleMap.fitBounds(routeBounds[routeID]);
                if (tourAttractions.hasOwnProperty(routeID)){
                    for(var marker in tourAttractions[routeID]){
                        tourAttractions[routeID][marker].setMap(googleMap);
                    }
                }
            }
            if($(".routeSelection[data-routeid]").not("[data-routeid='attractions'],[data-routeid=0]").length == $(".routeSelection.active[data-routeid]").not("[data-routeid='attractions'],[data-routeid=0]").length) {
                $(".routeSelection[data-routeid]").addClass('active');
            } else {
                $(".routeSelection[data-routeid=0]").removeClass('active');
            }
        } else if (routeID == 'attractions') {

        } else {
            $(".routeSelection[data-routeid]").addClass('active');
            for(var tour in tourPaths){
                tourPaths[tour].setMap(googleMap);
                if (tourAttractions.hasOwnProperty(tour)){
                    for(var marker in tourAttractions[tour]){
                        tourAttractions[tour][marker].setMap(googleMap);
                    }
                }
            }
            googleMap.fitBounds(routeBounds.fullAttr);
        }
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAo94coA_S_IuJUUMTBr9Tw3pwMUpkzqME&callback=initMap" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
<script>
    $(document).ready(function(){
        $('.routeSelection').click(function(){
            $('.routeSelection.active').addClass('active');
            $(this).toggleClass('active');
            toggleRoute($(this).data('routeid'));
            $('.visible_portion').siblings('.hidden_portion').hide();
            $(this).siblings('.hidden_portion').show();
        });
    });
</script>


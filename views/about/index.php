<div class="pageWrapper aboutPageWrapper cmsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
			<?include(__DIR__.'/../../templates/biketour/bookingTemplate.php');?>
		</div>
    </div>

    <div class="about_hero" style="background:url(<?= FRONT_IMG ?>downtown_liberty.jpg) no-repeat;">
        <h1 class="valB big">About Us</h1>
<!--        <p class="valB">With a fleet of over 200 bikes, we offer an all inclusive rental package that provides a map, helmet, chain lock and basket at no extra charge. Conquer the park on a mountain bike or enjoy a leisurely ride with a friend on a tandem bike. With many options, choose the perfect bike for your adventure!</p>-->
    </div>
	<div class="tiling row squareContents">
            <? $alternator = 'left';
            $fullSpanStorage = [];
            foreach ($model->about as $value) {
                if($alternator == 'left'){
                    echo '<div class="row row_of_2">';
                }?>
                <div class="col float_<?= $alternator ?>">
                    <div class="absTransCenterRevised">
                        <img src="<?= UPLOAD_URL.'about/'.$value->featured_image ?>" style="width: 100%">
                        <h4 class="uppercase valB gray"><?= $value->title ?></h4>
                        <p class="val"><?= $value->description ?></p>
                        <a class="btn secondary_btn" href="<?= $value->url ?>"><?= $value->url_text ?></a>
                    </div>
                </div>
                <? if($alternator == 'right'){
                    echo '</div>';
                }
                $alternator = $alternator == 'left' ? 'right' : 'left';} ?>
        </div>
	</div>    


</div>
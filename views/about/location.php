    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-oHy4vd-HgjAWJZ9x7E-O90wkcDih0wo&callback=initMap"
    async defer></script>    
<!--    <script src="http://matchingnotes.com/javascripts/leaflet-google.js"></script>-->
<style type="text/css">
          .leaflet-map-pane {
          z-index: 2 !important;
      }

      .leaflet-google-layer {
          z-index: 1 !important;
      }
</style>
<div class="pageWrapper cmsPageWrapper locationsPageWrapper">
    <div class="fixedReserveRow">
        <div class="left_float">
            <div class="logoCall">
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
                </a>
            </div>
        </div>
        <div class="absHorizCenter">
            <?include(__DIR__.'/../../templates/biketour/bookingTemplate.php');?>
        </div>
    </div>

    <h1 class="valB uppercase big">Location</h1>


    <h3 class="medium val">Visitor Center at 2 East 42nd ST (off of 5th Ave) New York NY 10017</h3>
     <h3 class="medium val">Departure Point at Battery Park: Slip 5 at the Battery Maritime Building (Corner of Whitehall ST & South ST)</h3>
     <h3 class="medium val">Departure Point at Midtown West: Pier 79 (West 39th St Hudson River Park)</h3>
    <div class="contactNumberWrapper">
    	<h4 class="valB gray">For more information, call <span><a href="tel:2126430080">(212) 643-0080</a></span></h4>
        <h2>Hours of operation: 8:00AM - 7:00PM (Mon - Sun)</h2>
    </div>
    <section class="locationImagesWrapper">
    	<div class="row row_of_2">
    		<div class="col">
    			<div class="media">

<div class="contact_form_container">
    <h3 class="valB gray medium">Contact Us</h3>
  <form action="/contacts/add" method="post" id="submit_form">
      <input hidden name="redirect" value="<?=$this->emagid->uri?>">
    <div class="input_contain">
        <label for="fname">Full Name</label>
        <input type="text" name="full_name" placeholder="John Smith" required>
    </div>

    <div class="input_contain">
        <label for="email">Email Address</label>
        <input type="email" name="email" placeholder="name@domain.com" required>
    </div>

    <div class="input_contain">
        <label for="subject">Subject</label>
        <input type="text" name="subject" placeholder="Topic" required>
    </div>

    <div class="input_contain">
        <label for="subject">Message</label>
        <textarea name="message" placeholder="Message" required></textarea>
    </div>

    <input type="submit" value="Submit">

  </form>
</div>
                </div>
    		</div>
    		<div class="col">

                <div style="width:500px; height:500px" id="map"></div>
            
    		</div>
    	</div>
    </section>
    <section class="lastSection">
        <div class="primaryContentWidth row row_of_3">
            <div class="col tripGuides">
                <icon style="background-image:url('<?=FRONT_IMG?>guidesIcon.png')"></icon>
                <h5 class="medium">NYC Guide</h5>
                <p>Find the best places to go and other insights from our team of native New Yorkers</p>
                <a class="btn primaryBtn btn_red" href="<?=SITE_URL?>attractions">View all Trip Guides</a>
            </div>
            <div class="col locations">
                <icon style="background-image:url('<?=FRONT_IMG?>routesIcon.png')"></icon>
                <h5 class="medium">Routes</h5>
                <p>Choose the tour that's right for your unique NYC adventure</p>
                <a href="<?=SITE_URL?>about/location">
                    <div class="media" style="background-image:url('<?=FRONT_IMG?>routes_map.png')")></div>
                </a>
            </div>
            <div class="col blog">
                <icon style="background-image:url('<?=FRONT_IMG?>subscribeIcon.png')"></icon>
                <h5 class="medium">Our Latest Post</h5>
<!--                <p><//?=$model->blog->name?></p>-->
                <p>Don't leave town until you've seen these essential NYC destinations</p>
                <a class="btn primaryBtn btn_red" href="<?=SITE_URL?>blog">Visit our Blog</a>
            </div>
        </div>
    </section>
</div>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAo94coA_S_IuJUUMTBr9Tw3pwMUpkzqME" type="text/javascript"></script>
<script type='text/javascript'>
    $(function(){
	var mapA = new L.Map('map', {center: new L.LatLng(40.7531577, -73.9807303), zoom: 12,scrollWheelZoom:false});
				var googleLayer = new L.Google('ROADMAP');
				mapA.addLayer(googleLayer);


				// var thisIcon = new L.Icon({
				//     iconUrl: '../../bikeTourLogo.png',
				//     iconAnchor: new L.Point(16, 16),
				//     iconSize: new L.Point(32, 32),
				//     size: new L.Point(32, 32)
				// });
				// var thisMarker = new L.Marker(new L.LatLng(40.7682729, -73.9873471), {icon: thisIcon});
				// map.addLayer(thisMarker);

				var LeafIcon = L.Icon.extend({
					options: {
						iconAnchor:   [19, 46],
						popupAnchor:  [0, -36]
					}
				});
				  var greenIcon = new LeafIcon({iconUrl: 'http://technobd.rvillage.com/application/modules/Rvillage/externals/images/all_members.png'});
				  L.marker([40.7531577, -73.9807303]).bindPopup("Topview Tours").addTo(mapA);
				  L.marker([40.76046, -74.0035], {}).bindPopup("Pier 79").addTo(mapA);
				  L.marker([40.70053, -74.01342], {}).bindPopup("Slip 5").addTo(mapA);


				  let shuttlePath = [[40.76214, -74.00135],
									 [40.75712, -74.00501],
									 [40.74846, -73.98457],
									 [40.7531577, -73.9807303],[40.76214, -74.00135]];
				  L.polyline(shuttlePath, {color: 'black'}).addTo(mapA);


	});
</script>
<script>
/*
 * L.TileLayer is used for standard xyz-numbered tile layers.
 */

L.Google = L.Class.extend({
	includes: L.Mixin.Events,

	options: {
		minZoom: 0,
		maxZoom: 18,
		tileSize: 256,
		subdomains: 'abc',
		errorTileUrl: '',
		attribution: '',
		opacity: 1,
		continuousWorld: false,
		noWrap: false,
	},

	// Possible types: SATELLITE, ROADMAP, HYBRID
	initialize: function(type, options) {
		L.Util.setOptions(this, options);

		this._type = google.maps.MapTypeId[type || 'SATELLITE'];
	},

	onAdd: function(map, insertAtTheBottom) {
		this._map = map;
		this._insertAtTheBottom = insertAtTheBottom;

		// create a container div for tiles
		this._initContainer();
		this._initMapObject();

		// set up events
		map.on('viewreset', this._resetCallback, this);

		this._limitedUpdate = L.Util.limitExecByInterval(this._update, 150, this);
		map.on('move', this._update, this);
		//map.on('moveend', this._update, this);

		this._reset();
		this._update();
	},

	onRemove: function(map) {
		this._map._container.removeChild(this._container);
		//this._container = null;

		this._map.off('viewreset', this._resetCallback, this);

		this._map.off('move', this._update, this);
		//this._map.off('moveend', this._update, this);
	},

	getAttribution: function() {
		return this.options.attribution;
	},

	setOpacity: function(opacity) {
		this.options.opacity = opacity;
		if (opacity < 1) {
			L.DomUtil.setOpacity(this._container, opacity);
		}
	},

	_initContainer: function() {
		var tilePane = this._map._container
			first = tilePane.firstChild;

		if (!this._container) {
			this._container = L.DomUtil.create('div', 'leaflet-google-layer leaflet-top leaflet-left');
			this._container.id = "_GMapContainer";
		}

		if (true) {
			tilePane.insertBefore(this._container, first);

			this.setOpacity(this.options.opacity);
			var size = this._map.getSize();
			this._container.style.width = size.x + 'px';
			this._container.style.height = size.y + 'px';
		}
	},

	_initMapObject: function() {
		this._google_center = new google.maps.LatLng(0, 0);
		var map = new google.maps.Map(this._container, {
		    center: this._google_center,
		    zoom: 0,
		    mapTypeId: this._type,
		    disableDefaultUI: true,
		    keyboardShortcuts: false,
		    draggable: false,
		    disableDoubleClickZoom: true,
		    scrollwheel: false,
		    streetViewControl: false
		});

		var _this = this;
		this._reposition = google.maps.event.addListenerOnce(map, "center_changed", 
			function() { _this.onReposition(); });
	
		map.backgroundColor = '#ff0000';
		this._google = map;
	},

	_resetCallback: function(e) {
		this._reset(e.hard);
	},

	_reset: function(clearOldContainer) {
		this._initContainer();
	},

	_update: function() {
		this._resize();

		var bounds = this._map.getBounds();
		var ne = bounds.getNorthEast();
		var sw = bounds.getSouthWest();
		var google_bounds = new google.maps.LatLngBounds(
			new google.maps.LatLng(sw.lat, sw.lng),
			new google.maps.LatLng(ne.lat, ne.lng)
		);
		var center = this._map.getCenter();
		var _center = new google.maps.LatLng(center.lat, center.lng);

		this._google.setCenter(_center);
		this._google.setZoom(this._map.getZoom());
		//this._google.fitBounds(google_bounds);
	},

	_resize: function() {
		var size = this._map.getSize();
		if (this._container.style.width == size.x &&
		    this._container.style.height == size.y)
			return;
		this._container.style.width = size.x + 'px';
		this._container.style.height = size.y + 'px';
		google.maps.event.trigger(this._google, "resize");
	},

	onReposition: function() {
		//google.maps.event.trigger(this._google, "resize");
	}
});
</script>

<div class="pageWrapper tourDetailPageWrapper">
	<section class="topSection">
		<div class="promoBar">
			<p class="valB">Book your bike tours online for up to 80% off our walk-in price!</p>
		</div>	
		<div class="heroContainer">
			<div class="heroSlideshow heroSlideshowBikeTours">
				<div class="slide">		
					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<?=FRONT_IMG?>toursIndexHero.jpg');"></div>
					</div>				
					<div class="heroText">
						<h1 class="biggest valB">Central Park Bike Rentals</h1>
						 <h3 class="valB valUpper">From the original Central Park Bike Tour Company</h3>
					</div>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>toursIndexHero.jpg')"></div>
				</div>
				<div class="slide">
					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<?=FRONT_IMG?>heroImg2.jpg');"></div>
					</div>				
					<div class="heroText">
						<h1 class="biggest valB">Central Park Bike Rentals</h1>
						 <h3 class="valB valUpper">From the original Central Park Bike Tour Company</h3>
					</div>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>heroImg2.jpg')"></div>
				</div>
			</div>
		</div>
		<div id="slideDownToBook">
			<icon style="background-image:url('<?=FRONT_IMG?>slideDownIcon.png')"></icon>
		</div>
	</section>
	<section class="bookingSection middleSection">
		<h5 class="valB">Make your Rental Reservation</h5>
		<div class="row dealsEquipmentCols">
			<div class="equipmentSelectionBox tabbedView">
				<div class="tabController">
					<a class="tab active sliderLinkedTab" data-tab_title="adult"><p>Adult</p></a>
					<a class="tab sliderLinkedTab" data-tab_title="kid"><p>Kid</p></a>
					<a class="tab sliderLinkedTab" data-tab_title="child"><p>Accessories</p></a>
					<a class="tab sliderLinkedTab" data-tab_title="tandem"><p>Other</p></a>
				</div>
				<div class="tabContents">
					<div class="tab_content tab_content_adult tab_content_kid tab_content_child tab_content_tandem tab_content_active">
						<div class="equipmentCustomizationSlider">
							<?foreach($model->equipment as $type=>$items){
								foreach($items as $item){
									$equip = isset($_SESSION[$model->sessionSelect]->equipment[$item->id]) ? $_SESSION[$model->sessionSelect]->equipment[$item->id]: null; ?>
									<div class="slide sliderLinkedTab_content_<?=$type?>">
										<div class="equipmentImage media" style="background-image:url('<?=UPLOAD_URL.'equipments/'.$item->featuredImage()?>')">
											<p class="whiteOut"><?=$item->name?></p>
										</div>

										<div class="comparativePricingFormat centeredCols" data-id="<?=$item->id?>">
	<!--												<div class="col">-->
	<!--													<h4 class="valB">Walk-In</h4>-->
	<!--													<div class="co_price">-->
	<!--														<span></span>-->
	<!--														<p>$--><?//=number_format($item->getPrice('',$_SESSION[$model->sessionSelect]->duration))?><!--</p>-->
	<!--													</div>-->
	<!--												</div>-->
											<?foreach($model->rentals as $rental){?>
												<div class="col">
													<h4 class="valB"><?=$rental->name?></h4>
													<div class="online_price">
														<p>$<?=number_format($item->getPrice('',$rental->id))?></p>
													</div>
												</div>
											<?}?>
										</div>
										<?if($item->category == 'Insurance'){?>
										<div class="addInsuranceBtn btn primaryBtn equipmentCustomizationActionBtn <?=$equip ? 'equipmentSelected':''?>">
											<a class="addEquipment checkboxUX insuranceButton">
												<input type="checkbox" class="checkInsurance" data-id="<?=$item->id?>" data-category="<?=$item->category?>" <?=$equip ? 'checked': ''?>>
												<p class="addText">
													<span class="plusSign"><span></span><span></span></span>Add Insurance 
													<?if($item->description){?>
														<div class="hoverInfo">
															<icon>why?</icon>
															<div class="infoBox">
																<p><?=$item->description?></p>
															</div>
														</div>
													<?}?>
												</p>
												<p class="addedText">
													<icon class="checkmark" style="background-image:url('<?=FRONT_IMG?>selectedCheckmarkIcon.png')"></icon>
													<span>Selected</span>
												</p>
											</a>
											

											
										</div>
										<?}else{?>
										<div class="btn primaryBtn equipmentCustomizationActionBtn <?=$equip ? 'equipmentSelected':''?>">
											<a class="addEquipment">
												<p class="addText">Add to Reservation</p>
												<p class="addedText">
													<icon class="checkmark" style="background-image:url('<?=FRONT_IMG?>selectedCheckmarkIcon.png')"></icon>
													<span>Selected</span>
												</p>
											</a>
											

											
											<div class="numericalToggler">
												<div class="numberCount">
													<div class="dec button <?=$equip ? '': 'disabled'?>" data-id="<?=$item->id?>" data-category="<?=$item->category?>"><span></span></div>
													<div class="numberCountValWrapper">
														<input type="text" class="numberCountVal" value="<?=$equip ? $equip['qty']: 0?>" pattern="[0-9]*" data-category="<?=$item->category?>" data-name="<?=$item->name?>">
													</div>
													<div class="inc button" data-id="<?=$item->id?>" data-category="<?=$item->category?>"><span></span><span></span></div>
												</div>
											</div>
											
										</div>	
										<?}?>										
									</div>
								<?}?>
							<?}?>
						</div>
					</div>
				</div>
			</div>		
			<div class="col rentalBookingFieldsCol">
				<div class="reservationBarWrapper reservationBarTandemIncluded tourDetailReservationBar">
		            <div class="container">
		                <form action="/reservation/resRental" method="post" id="book_form">
							<input id="unique_type" value="<?=$model->sessionSelect?>" type="hidden"/>
							<?if(($rent = \Model\Cart::getActiveRental($model->user ? $model->user->id: 0,session_id()))){?>
								<input id="rental_ref" value="<?=$rent->ref_num?>" type="hidden"/>
							<?}?>
		                    <div class="step countStep">
		                    	<label class="aboveLabel">Your Reservation</label>
								<div class="countDropdownTrigger_data">
									<?$str = [];
									if($_SESSION[$model->sessionSelect]->adults && $_SESSION[$model->sessionSelect]->kids){
										$adults = $_SESSION[$model->sessionSelect]->adults;
										$kids = $_SESSION[$model->sessionSelect]->kids;
										$str[] = '<b>'. ($adults+$kids) .' Bikes</b>';
									} else if($_SESSION[$model->sessionSelect]->adults){
										$adults = $_SESSION[$model->sessionSelect]->adults;
										$str[] = '<b>'. $adults .'</b> '. pluralize($adults,'<b>Adult</b> Bike');
									} else if($_SESSION[$model->sessionSelect]->kids){
										$kids = $_SESSION[$model->sessionSelect]->kids;
										$str[] = '<b>'. $kids .'</b> '. pluralize($kids,'<b>Kid</b> Bike');
									} 
									if($_SESSION[$model->sessionSelect]->tandem){
										$tandem = $_SESSION[$model->sessionSelect]->tandem;
										$str[] = '<b>'. $tandem .'</b> '. pluralize($tandem,'<b>Tandem</b>','<b>Tandems</b>');
									}
									foreach($_SESSION[$model->sessionSelect]->equipment as $id=>$value){
										if(($e = \Model\Equipment::getItem($id)) && in_array($e->category,['Accessories','Insurance','Children','Baby Seat','Trailer'])){
											$str[] = '<b>'.$value['qty'].'</b> '.pluralize($value['qty'],"<b>$e->name</b>","<b>{$e->name}s</b>");
										}
									}
									?>
									<div class="text <?=!implode(' & ',$str) ? 'placeholderActive': 'placeholderInactive'?>">
										<p class="placeholder val gray">Select rental equipment above</p>
										<p class="data">
											<?
												$last = array_pop($str);
												$string = count($str) ? implode(", ", $str) . " & " . $last : $last;
											?>
											<?=$string?>
										</p>
									</div>
								</div>
		                    </div> 					
		                    <div class="stepHours step">
		                    	<label class="aboveLabel">Please select a duration</label>
		                        <div class="timepickerInputWrapper">
		                            <select class="btSelect reservationTime" name="reservationTime">
										<option value="-1" disabled <?=$_SESSION[$model->sessionSelect]->duration == -1 ? 'selected': ''?>>Rental Duration</option>
										<?foreach($model->rentals as $rental){
											$adult = $_SESSION[$model->sessionSelect]->adults;
											$kid = $_SESSION[$model->sessionSelect]->kids;
											$tandem = $_SESSION[$model->sessionSelect]->tandem;
											$price = $rental->getPrice();
											$kidPrice = $rental->getPrice('kid');
											$tandemPrice = $rental->getPrice('tandem');
											$displayTotal = ' - $'.floatval($price);?>
											<option value="<?=$rental->id?>" <?=$rental->id == $_SESSION[$model->sessionSelect]->duration ? 'selected': ''?>><?=$rental->name?></option>
										<?}?>
		                            </select>
		                           	<span class="dropdownArrow">
		                                <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
		                            </span>
		                        </div>  
		                    </div>                          
		                         
		                    <div class="stepDate step">
		                    	<label class="aboveLabel">Please select a starting date &#x26; time</label>
		                        <div class="datepickerInputWrapper">

									<input type="text" class="triggerDatepicker" hidden name="reservationDateTime" placeholder="When?" value="<?=$_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : ''?>">
<!--									<div class="datepickerCellUI whenSelectorParent"></div>-->
									<div class="<?=$_SESSION[$model->sessionSelect]->datetime ? 'placeholderInactive ': 'placeholderActive '?>dateText">
										<p class="placeholder val gray">Book Online & Save</p>
										<p class="data whenSelectorResult">Book Online & Save</p>
									</div>
									<!--<span class="dropdownArrow">
		                                <icon style="background-image:url('<?/*= FRONT_IMG */?>dropdown_arrow.png')"></icon>
		                            </span>-->
		                        </div>
		                    </div>
		                    <div class="step finalStep">
		                        <a class="btn primaryBtn reserveBtn">
		                            <p><?=($total = \Model\Cart::getSessionTotal()) > 8? 'Book this Rental <span class="middot"></span> $'.number_format($total,0): 'Starting at $8'?></p>
		                        </a>
		                    </div>
		                </form>
		            </div>
		        </div>
			</div>		
			<div class="col rentalsFreeItemsSection">
				<div class="equipmentFreeSection row">
					<div class="text col">
						<h1 class="valB">Free</h1>
						<h4 class="val gray">with your<br> bus tour</h4>
					</div>
					<div class="col">
						<icon class="lockIcon" style="background-image:url('<?=FRONT_IMG?>poncho.png')"></icon>
						<h6 class="valB">Poncho</h6>
					</div>
					<div class="col">
						<icon class="helmetIcon" style="background-image:url('<?=FRONT_IMG?>earphones.png')"></icon>
						<h6 class="valB">Earphones</h6>
					</div>
				</div>
				<div class="getDiscountsSection row">
					<h1 class="valB">Get discounts</h1>
					<h4 class="val gray">on bus and boat tour tickets when<br>you reserve your bike rentals online<br>here with <span>Bike Rentals Central Park</span></h4>
				</div>
			</div>
		</div>

		<div class="rentalsOverviewText">
			<h5 class="uppercase valB gray">More Details on your Bike Rental</h5>
			<h4 class="valR">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h4>		
		</div>
		<div class="col hidden responsiveRentalDiscounts">
			<div class="wbox pricingBox">
				<div class='tabbedView'>

					<h3 class="valR gray">Rentals Pricing</h3>
					<div class="tabController row">
						<a class="tab active col" data-tab_title="adultsPricing">
							<p>adults</p>
						</a>
						<a class="tab col" data-tab_title="kidsPricing">
							<p>kids</p>
						</a>
						<a class="tab col" data-tab_title="tandemPricing">
							<p>tandem</p>
						</a>
					</div>
					<div class="tabContents">

						<div class="pricingTable tab_content tab_content_active tab_content_adultsPricing">
							<div class="row row_of_3">
								<div class="col durationCol">
									<div class="header">Duration</div>
									<?foreach($model->rentals as $rental){
										$allDay = $rental->duration == 24?>
										<div class="tableRow <?=$allDay ? 'allDayRow': ''?>">
											<?if($allDay){?>
												<p>All Day</p>
											<?}else{?>
												<span><?=$rental->duration?></span>
												<p><?=$rental->duration == 1 ? 'Hour': 'Hours';?></p>
											<?}?>
										</div>
									<?}?>
								</div>
								<div class="col">
									<div class="header">Walk-In</div>
									<?foreach($model->rentals as $rental){?>
										<div class="tableRow">
											<div class="co_price">
												<span></span>
												<p>$<?=floatval($rental->adult_walkin)?></p>
											</div>
										</div>
									<?}?>
								</div>
								<div class="col">
									<div class="header">Online</div>
									<?foreach($model->rentals as $rental){?>
										<div class="tableRow">
											<div class="online_price">
												<p>$<?=intval(number_format($rental->getPrice(),2))?></p>
											</div>
										</div>
									<?}?>
								</div>
							</div>
						</div>
						<div class="pricingTable tab_content tab_content_kidsPricing">
							<div class="row row_of_3">
								<div class="col durationCol">
									<div class="header">Duration</div>
									<?foreach($model->rentals as $rental){
										$allDay = $rental->duration == 24?>
										<div class="tableRow <?=$allDay ? 'allDayRow': ''?>">
											<?if($allDay){?>
												<p>All Day</p>
											<?}else{?>
												<span><?=$rental->duration?></span>
												<p><?=$rental->duration == 1 ? 'Hour': 'Hours';?></p>
											<?}?>
										</div>
									<?}?>
								</div>
								<div class="col">
									<div class="header">Walk-In</div>
									<?foreach($model->rentals as $rental){?>
										<div class="tableRow">
											<div class="co_price">
												<span></span>
												<p>$<?=floatval($rental->kid_walkin)?></p>
											</div>
										</div>
									<?}?>
								</div>
								<div class="col">
									<div class="header">Online</div>
									<?foreach($model->rentals as $rental){?>
										<div class="tableRow">
											<div class="online_price">
												<p>$<?=intval(number_format($rental->getPrice('kid'),2))?></p>
											</div>
										</div>
									<?}?>
								</div>
							</div>
						</div>
						<div class="pricingTable tab_content tab_content_tandemPricing">
							<div class="row row_of_3">
								<div class="col durationCol">
									<div class="header">Duration</div>
									<?foreach($model->rentals as $rental){
										$allDay = $rental->duration == 24?>
										<div class="tableRow <?=$allDay ? 'allDayRow': ''?>">
											<?if($allDay){?>
												<p>All Day</p>
											<?}else{?>
												<span><?=$rental->duration?></span>
												<p><?=$rental->duration == 1 ? 'Hour': 'Hours';?></p>
											<?}?>
										</div>
									<?}?>
								</div>
								<div class="col">
									<div class="header">Walk-In</div>
									<?foreach($model->rentals as $rental){?>
										<div class="tableRow">
											<div class="co_price">
												<span></span>
												<p>$<?=floatval($rental->tandem_walkin)?></p>
											</div>
										</div>
									<?}?>
								</div>
								<div class="col">
									<div class="header">Online</div>
									<?foreach($model->rentals as $rental){?>
										<div class="tableRow">
											<div class="online_price">
												<p>$<?=intval(number_format($rental->getPrice('tandem'),2))?></p>
											</div>
										</div>
									<?}?>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="freeFooter">
					<p>Poncho and earphones are <span>free</span></p>
				</div>
			</div>		
			<div class="equipmentFreeSection row">
				<div class="text col">
					<h1 class="valB">Free</h1>
					<h4 class="val gray">with your<br> bus tour</h4>
				</div>
				<div class="col">
					<icon class="lockIcon" style="background-image:url('<?=FRONT_IMG?>poncho.png')"></icon>
					<h6 class="valB">Poncho</h6>
				</div>
				<div class="col">
					<icon class="helmetIcon" style="background-image:url('<?=FRONT_IMG?>earphones.png')"></icon>
					<h6 class="valB">Earphones</h6>
				</div>
			</div>
			<div class="getDiscountsSection row">
				<h1 class="valB">Get discounts</h1>
				<h4 class="val gray">on bus and boat tour tickets when<br>you reserve your bike rentals online<br>here with <span>Bike Rentals Central Park</span></h4>
			</div>
		</div>
	</section>
	<section class="reviewsSection reviewsHorizontalScroller">
		<h2 class="big valB">Reviews<a class="writeReview">Write a Review</a></h2>
		<div class="reviewFormExpander">
			<span class="expanderArrow"></span>
			<div class="reviewFormWrapper">
				<form action="/reviews/add" method="post" id="submit_form">
					<input hidden name="redirect" value="<?=$this->emagid->uri?>">
					<input hidden name="review_type" value="1">
					<div class="inputRow">
						<label>Rental</label>
						<select name="review_id">
							<?foreach($model->rentals as $rental){?>
								<option value="<?=$rental->id?>"><?=$rental->name?></option>
							<?}?>
						</select>
					</div>
					<div class="inputRow">
						<label>Name</label>
						<input name="customer_name" id="reviewCustomerName">
					</div>
					<div class="inputRow">
						<label>Title</label>
						<input name="title">
					</div>
					<div class="inputRow">
						<label>Body</label>
						<textarea name="description"></textarea>
					</div>
					<div class="inputRow">
						<label>City</label>
						<input name="city">
					</div>
					<div class="inputRow">
						<label>State</label>
						<select name="state">
							<? foreach (get_states() as $short => $long) { ?>
								<option value="<?= $short ?>"><?= $long ?></option>
							<? } ?>
						</select>
					</div>
					<div class="inputRow">
						<label>Rating</label>
						<select name="rating">
							<?for($i = 5; $i >= 1; $i -= .5){?>
								<option value="<?=$i?>"><?=$i?></option>
							<?}?>
						</select>
					</div>
					<div class="inputRow submitRow">
						<input type="submit" value="Submit" class="btn primaryBtn submit_review">
					</div>
				</form>
			</div>
		</div>
		<div class="reviewScroller">
			<?foreach($model->reviews as $review){?>
				<div class="reviewItem">
					<div class="quoteMark"></div>
					<div class="quoteText">
						<h6 class="medium val"><?=$review->description?></h6>
						<div class="starRating">
							<?for($i = 1; $i < 6; $i++){
								$sub = $review->rating - $i;
								switch($sub){
									case $sub > .5:
									case $sub == 0:
										$rating = 'star_rate';
										break;
									case $sub == .5:
										$rating = 'star_half';
										break;
									case $sub < 0:
									default:
										$rating = 'star_border';
										break;
								}?>
								<i class="material-icons"><?=$rating?></i>
							<?}?>
						</div>
						<p><strong>- <?=$review->customer_name?></strong><span class="middot">&#xb7;</span><?=date('M jS, Y',strtotime($review->create_date))?></p>
					</div>
					<div class="reviewHelpful">
						<a>
							<h6 class="valB uppercase">Helpful</h6>
							<icon class="thumbsUp" style="background-image:url('<?=FRONT_IMG?>reviewThumbsUp.png')">
								<span><?=$review->helpful ? : 0?></span>
							</icon>
						</a>
					</div>
				</div>
			<?}?>
		</div>
	</section>
	<section class="bottomSection">
		<div class="row row_of_2">
			<div class="col bestViewsCol">
				<div class="media" style="background-image:url('<?=FRONT_IMG?>bestViews.jpg')"></div>
				<div class="topText">
					<h2 class="big">Adventure to the best Views of NYC</h2>
				</div>
				<div class="bottomText">
					<div class="pane">
						<p class="val">Only with Central Park Bike Rentals dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
						<div class="media mediaBlur" style="background-image:url('<?=FRONT_IMG?>bestViews.jpg')"></div>
					</div>
				</div>
			</div>
			<div class="col topEquipmentCol">
				<div class="media" style="background-image:url('<?=FRONT_IMG?>equipmentBikeShadow.png')"></div>
				<div class="topText">
					<h2 class="big">Top-Rated Equipment</h2>
					<p class="val">Comfort. Safety. Clean. Easy-to-use. Our equipment is guaranteed to make your biking experience 100% stree-free.</p>
				</div>
			</div>	
		</div>
		<div class="row row_of_3 featuredLinks">
			<?foreach($model->tour_banner as $value){?>
				<div class="col">
					<h4 class="valB medium"><?=$value->title?></h4>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'banners/'.$value->image?>')"></div>
					<p class="valB"><?=$value->description?></p>
					<a class="invBtn btn arrowBtn learnMore" href="<?=$value->url?>">
						<?=isset(json_decode($value->details,true)['url_text']) ? json_decode($value->details,true)['url_text']: 'Explore our tours here'?>
						<icon style="background-image:url('<?=FRONT_IMG?>btnLinkArrow.png')"></icon>
					</a>
				</div>
			<?}?>
		</div>
	</section>
</div>
<?php
	$relatedProducts = $model->product->relatedProducts(10) ? : null;
	$relatedProducts = $relatedProducts ? array_chunk($relatedProducts, 2) : null;
?>
<div class="pageWrapper productPageWrapper pageLeftNavSidebar">
	<div class="content">
		<div class="productHeader">
			<h1 class="as_r"><?=$model->product->name?></h1>
			<div class="right_float">
				<div class="scrollingHeaderSection">
					<a class="btn btn_blue addToBag" data-product_id="<?=$model->product?$model->product->id:0?>" data-user_id="<?=$model->user?$model->user->id:0?>">Add to Bag</a>
				</div>
			</div>
		</div>
		<div class="row productDetailSection">
			<div class="col productMedia quickSlideInactive">
				<?if($model->product->linked_list){?>
				<div class="linkedItemsWrapper">
						<?foreach($model->product->getLinkedProducts() as $item){?>
							<a class="linkedItemCircle" href="<?=SITE_URL.'products/'.$item->slug?>">
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$item->featuredImage();?>)"></div>
								</div>
								<div class="tooltip">
									<p class="as_r"><?=$item->linked_name ? : $item->name?></p>
								</div>
							</a>
						<?}?>
				</div>
				<?}?>
				<div class="media_thumbs">
					<?if($model->product->video_link){?>
					<a class="mediaThumb mediaThumbVideo" data-mediaThumb="1">
						<video id="video" autoplay="autoplay" loop="true">
							<source src="<?=$model->product->videoLink()?>" type="video/mp4">
						</video>
					</a>
					<?}?>
					<?$i = 2;
					$active = true;
					foreach($model->product->getAllProductImages() as $image){?>
					<a class="mediaThumb <?=$active ? 'mediaThumbActive' : ''?>" data-mediaThumb="<?=$i++?>">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$image?>)"></div>
						</div>
					</a>
					<?$active = false;}?>
				</div>
				<div class="main_media">
					<div class="media_enlarged media_enlarged_1 video_media_wrapper">
						<div class="mediaWrapper scrubber">


							<div class="dragToRotate ready">
								<div class="dragToRotateContainer">
									<div class="arrowLeftContainer">
										<icon class="arrowLeft">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
												<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
												L-321,348.1z"/>
											</svg>
										</icon>
									</div>
									<div class="dragToRotateText">Drag to rotate</div>
									<div class="arrowRightContainer">
										<icon class="arrowRight">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
												<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
												L-321,348.1z"/>
											</svg>
										</icon>
									</div>
								</div>
							</div>
							<video id="video" controls="true">
								<source src="<?=$model->product->videoLink()?>" type="video/mp4">
							</video>
							<div id="output"></div>

							<script>
								$(document).ready(function(){

									var scaleFactor = .9;
									var video = $("#video")[0];
									var output = document.getElementById('output');
									var snapshots = [];
									var i = 0	;

									function capture(video, scaleFactor) {
										if(scaleFactor == null){
											scaleFactor = 1;
										}
										var w = video.videoWidth * scaleFactor;
										var h = video.videoHeight * scaleFactor;
										var canvas = document.createElement('canvas');
										canvas.width  = w;
										canvas.height = h;
										var ctx = canvas.getContext('2d');
										ctx.drawImage(video, 0, 0, w, h);
										return canvas;
									}

									video.addEventListener('loadeddata', function() {

										video.currentTime = i;

									}, false);

									video.addEventListener('seeked', function() {

										// now video has seeked and current frames will show
										// at the time as we expect
										var canvas = capture(video, scaleFactor);
										document.getElementById('output').appendChild(canvas);



										// when frame is captured, increase
										i += .45;

										// if we are not passed end, seek to next interval
										if (i <= (video.duration - .5)) {
											// this will trigger another seeked event
											video.currentTime = i;

										} else {
											// DONE!, next action
										}

									}, false);



								});
							</script>
						</div>

					</div>
					<?$j = 2;
					$active = true;
					foreach($model->product->getAllProductImages() as $image){?>
					<div data-scale="1.4" data-image="<?=UPLOAD_URL.'products/'.$image?>" class="media_enlarged media_enlarged_image media_enlarged_<?=$j++?> <?=$active ? 'media_enlarged_active' : ''?>">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$image?>)"></div>
						</div>
					</div>
					<?$active = false;}?>
				</div>
				<?if($image = $model->product->getLegShot()){?>
				<div class="legShotsLifestyle">
					<span class="trigger_text">
						<span class="moreText">More Photos</span>
						<span class="lessText">Less Photos</span>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 2464 332 536" enable-background="new 0 2464 332 536" xml:space="preserve">
								<g>
									
										<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="166.1" y1="2496.3" x2="166.1" y2="2980.6"/>
									
										<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="166.1" y1="2980.6" x2="21.4" y2="2834.4"/>
									
										<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="166.1" y1="2980.6" x2="310.8" y2="2834.4"/>
								</g>
							</svg>
						</icon>
					</span>
					<!--section contains structured images of the product, from leg shots, to lifestyle, to social -->
					<div class="productMediaMasonry grid">
						<div class="gradientOverlay"></div>
						<!-- if image is leg shot -->
						<?if($image = $model->product->getLegShot()){?>
						<div class="grid-item">
							<div class="mediaWrapper media_wrapper_legshot">
								<div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$image?>)"></div>
							</div>
						</div>
						<!-- leg shot flipped image for both angles (bookmatch)-->
						<div class="grid-item">
							<div class="mediaWrapper media_wrapper_legshot media_wrapper_legshot_flipped">
								<div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$image?>)"></div>
							</div>
						</div>
						<?}?>

<!-- 						if image is lifestyle

						<div class="grid-item grid-item_lifestyle">
							<div class="mediaWrapper media_wrapper_lifestyle">
								<img class="media" src="<?=FRONT_IMG?>lifestyle.jpg">
							</div>
						</div>

						<div class="grid-item grid-item_lifestyle">
							<div class="mediaWrapper media_wrapper_lifestyle">
								<img class="media" src="<?=FRONT_IMG?>lifestyle2.jpg">
							</div>
						</div> -->



					</div>
				</div>
				<?}?>				
			</div>
			<div class="col productDataActions">
				<span class="fabric_img fabric_left"></span>
				<div class="header">
					<h4 class="as_r full_price" style="text-align: left;text-decoration: line-through">
					<?php $defaultPrice = $model->product->basePrice();
					if($model->product->getPrice()!=$defaultPrice){?>
					$<?=number_format($model->product->basePrice(),2)?>
					<?}?>
					</h4>
					<h3 class="as_r price_text"><span>$</span><?=number_format($model->product->getPrice(),2)?></h3>
				</div>


				<? if ($model->product->color) { 
					$colorid = $model->product->default_color_id;
					if($colorid){
						$defaultcolor = \Model\Color::getItem($colorid);
					} else {
						$defaultcolor = new stdClass();
						$defaultcolor->name = '';
					}
				?>
				<div class="customizationOption customizationOptionColor accordionUI">
					<a class="toggleCustomizationOptions visible"><label><span id="color-name">Color: <span><?=$defaultcolor->name?></span></span><icon></icon></label></a>
					<div class="hidden customizationOptionsModule color_selection_module selection_module radioOptionsModule">
						<div class="row" id="color-content">
							<? if ($model->product->color) {
								foreach (json_decode($model->product->color, true) as $item) {
									if ($color = \Model\Color::getItem($item)) {?>
										<div class="col optionBox optionBoxRadio <?=$color->id == $model->product->default_color_id ? 'selected': ''?>">
											<a href="#">
												<div class="media colorSwatch"
													 style="background-image:url(<?=$color->swatch()?>)"></div>
												<p><?= $color->name ?></p>
												<input type="checkbox" value="<?= $color->id ?>"
													   data-color="<?= $color->name ?>" <?=$color->id == $model->product->default_color_id ? 'checked="checked"': ''?>>
											</a>
										</div>
									<? } ?>
								<? } ?>
							<? } else { ?>
								<div class="col optionBox optionBoxRadio">
									<a href="#">
										<div class="media colorSwatch"
											 style="background-image:url(<?= FRONT_IMG ?>color_swatch_.png)"></div>
										<p>Default</p>
										<input type="checkbox" value="0"
											   data-color="0">
									</a>
								</div>
							<? } ?>
						</div>
					</div>
				</div>
				<? } ?>
				<?if($model->product->size != ''){?>
				<div class="customizationOption customizationOptionSize accordionUI">
					<a class="toggleCustomizationOptions visible"><label><span id="size-name">Select a Size</span><icon></icon></label></a>
					<div class="hidden customizationOptionsModule size_selection_module radioOptionsModule">
						<!-- <a><p>Size Guide</p></a> -->
						<div class="row row_of_3" id="size-content">
							<?foreach($model->product->getSizes() as $item){
								if($item != ''){?>
								<div class="col optionBox optionBoxRadio">
<!--									<h3 class="as_r">--><?//='US:'.floatval($item->us_size).'|EU:'.floatval($item->eur_size)?><!--</h3>-->
									<h3 class="as_r"><span>EUR <?=floatval($item->eur_size)?></span><span>US <?=floatval($item->us_size)?></span></h3>
									<input type="checkbox" value="<?=$item->id?>">
								</div>
							<?}?>
							<?}?>
						</div>
					</div>
				</div>
				<?}?>
				<?$defColor = $model->product->default_color_id ? \Model\Product_Attributes::getItem(null,['where'=>['product_id'=>$model->product->id, 'color_id'=>$model->product->default_color_id, 'name'=>'mto']]) : null?>
				<a class="btn btn_black addToBag firstAddBagBtn" id="add" data-product_id="<?=$model->product?$model->product->id:0?>" data-user_id="<?=$model->user?$model->user->id:0?>">Add to Bag</a>				
				<div id="madeToOrder" <?=$defColor && $defColor->value? 'style="display: block"': 'style="display: none"';?>>
					<h6 class="cortado">made to order</h6>
					<h5 class="cal_r">Please allow for 15-20 days for this product to ship.</h5>
				</div>
				<div class="add_to_bag_wrapper">
					<a class="favorite_btn btn btn_gray <?=$model->favorite ? 'favored': ''?>" data-product_id="<?=$model->product?$model->product->id:0?>" data-user_id="<?=$model->user?$model->user->id:0?>">
						<icon>
	                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                             viewBox="0 0 241.3 224.7" enable-background="new 0 0 241.3 224.7" xml:space="preserve">
	                            <path d="M120.6,34.6c0,0,0.5-0.5,0.8-0.8c9.8-11.2,30.1-30,54.2-30c33.6,0,61.9,28.4,61.9,62c0,22-14.5,50.1-43.3,83.7
	                            c-10.8,12.6-23.5,25.8-37.6,39.3c-10,9.5-18.7,17.1-24.2,21.9c-3.4,3-11.9,10.5-11.9,10.5s-8.3-7.5-11.6-10.4
	                            C77.2,183.6,3.6,115.4,3.6,65.9c0-33.6,28.3-62,61.9-62c24.2,0,44.4,18.7,54.3,29.9L120.6,34.6z"/>
	                        </svg>
	                    </icon>
	                    <p><?=$model->favorite ? 'Saved': 'Save for Later'?></p>
					</a>				
					<div class="productShareRow">
						<h6 class="as_r">Share</h6>
						<ul>
							<li>
			                    <div>
			                        <a href="https://www.facebook.com/watertour" class="facebook footer_social_icon" style="background-image:url(<?=FRONT_IMG?>facebookCircleBtn.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://twitter.com/watertour" class="twitter footer_social_icon" style="background-image:url(<?=FRONT_IMG?>twitterCircleBtn.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://www.instagram.com/watertour/" class="instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>instagramCircleBtn.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://www.instagram.com/watertour/" class="pinterest instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>pinterestCircleBtn.png)"></a>
			                    </div>
			                </li>
						</ul>
					</div>				
				</div>

				<div class="accordionUI accordionProductData accordionUI_active" id="productDescriptionContainer">
					<a class="visible">
						<h6 class="as_r">Description</h6>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</icon>
					</a>
					<a class="hidden">
						<p class="as_l"><?=$model->product->description?></p>
					</a>
				</div>
				<div class="accordionUI accordionProductData" id="productDescriptionContainer">
					<a class="visible">
						<h6 class="as_r">Details</h6>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</icon>
					</a>
					<a class="hidden">
						<span class="as_l">
							<?$details = explode('|',$model->product->details);
							foreach($details as $detail){?>
								<p><?=$detail?></p>
							<?}?>
						</span>
					</a>
				</div>
				<div class="accordionUI accordionProductData accordionUI_active" id="productDescriptionContainer">
					<a class="visible">
						<h6 class="as_r">Delivery</h6>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</icon>
					</a>
					<a class="hidden">
<!-- 						<div class="icon_title_text row">
							<icon class="col truck_icon" style="background-image:url(<?=FRONT_IMG?>delivery_truck.png)"></icon>
							<span class="col as_l">
								<b>Next day delivery</b>
								<p>Place your order today and receive it tomorrow</p>
							</span>
						</div> -->
						<div class="icon_title_text row">
							<icon class="col truck_icon" style="background-image:url(<?=FRONT_IMG?>delivery_truck.png)"></icon>
							<span class="col as_l">
								<b>Free standard shipping in the Continental United States</b>
								<p>Place your order today and receive it within 3-5 business days after your items have been made</p>
							</span>
						</div>
<!-- 						<div class="icon_title_text row">
							<icon style="background-image:url(<?=FRONT_IMG?>shopping_bag_large.png)"></icon>
							<span class="col as_l">
								<b>Collect-in-Store</b>
								<p>Order online today and pick up your items in our NYC factory store as early as tomorrow</p>
							</span>
						</div> -->
						<div class="icon_title_text row">
							<icon style="background-image:url(<?=FRONT_IMG?>returns.png)"></icon>
							<span class="col as_l">
								<b>Complimentary Returns</b>
								<p>Enjoy free returns on your order</p>
							</span>
						</div>
					</a>
				</div>
				<div class="help_options_wrapper">
					<p class="as_l">For style or product advice, please contact our Shoe Design Advisors at <a class="tel black_thin_btn" href="tel:2127771851">212 777 1851</a> or start a <a id="triggerZopimChat">live chat</a> with a member of our design team right away</p>
				</div>
				<?if($model->product->related_products){?>
				<div class="relatedProducts">
					<h5 class="as_m">You May also Like</h5>
					<div class="relatedProductsSlider">
						<? if ($relatedProducts) { ?>
							<?php foreach ($relatedProducts as $products) { ?>
								<div class="sliderItem">
									<? if ($products) { ?>
										<?php foreach ($products as $product) { ?>
											<a href="<?='/products/'.$product->slug?>">
												<div class="mediaWrapper">
													<div class="media"
														 style="background-image:url(<?= UPLOAD_URL . 'products/' . $product->featuredImage() ?>)">
													</div>
												</div>
												<div class="copyWrapper">
													<h6 class="as_m"><?= $product->name ?></h6>
												</div>
											</a>
										<?php } ?>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<? } ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#color-content, #sole-content').on('click','.optionBox',function(){
			var product_id = <?=$model->product->id?>;
			var color_id = $('#color-content').find(':checked').val() > 0 ? $('#color-content').find(':checked').val() : 0;
			var data = {product_id:product_id,color_id:color_id};
			var thumb = $('.media_thumbs');
			var large = $('.main_media');
			var leg = $('.productMediaMasonry');
			var madeToOrder = $('#madeToOrder');
			var priceText = $('.price_text');
			var defaultPrice = $('.full_price');
			$.post('/product/buildImages',data,function(item){
				thumb.children().not('.mediaThumbVideo').remove();
				large.children().not('.video_media_wrapper').remove();
				leg.empty();
				thumb.append(item.thumb);
				large.append(item.large);
				leg.html(item.legshot);
				if(item.madeToOrder){madeToOrder.show()}else{madeToOrder.hide()}
				if(item.sale){
					defaultPrice.html("$"+ "<?=number_format($model->product->price,2)?>");
					defaultPrice.show();
				}else{
					defaultPrice.hide();
				}
				priceText.html("$"+item.price);
			})
		});
	})
</script>

<div class="pageWrapper productPageWrapper pageLeftNavSidebar">
	<div class="content">
		<div class="productHeader">
			<h1 class="as_r">The White Snake Chloe</h1>
			<div class="right_float">
				<div class="scrollingHeaderSection">
					<div class="toggleNextPrevProducts">
						<a class="btn btn-prev">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</a>
						<a class="btn btn-next">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</a>
					</div>
					<a class="btn btn_blue">Add to Bag</a>
				</div>
			</div>
		</div>
		<div class="row productDetailSection">
			<div class="col productMedia quickSlideInactive">
				<div class="media_thumbs">
					<a class="mediaThumb mediaThumbVideo" data-mediaThumb="1">
						<p>360&#xb0;</p>
						<circle>
							<span class="playTriangle"></span>
						</circle>
					</a>
					<a class="mediaThumb mediaThumbActive" data-mediaThumb="2">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_1.jpg)"></div>
						</div>
					</a>
					<a class="mediaThumb" data-mediaThumb="3">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_2.jpg)"></div>
						</div>
					</a>
					<a class="mediaThumb"  data-mediaThumb="4">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_3.jpg)"></div>
						</div>
					</a>
				</div>
				<div class="main_media">
					<div class="media_enlarged media_enlarged_1 video_media_wrapper">
						<div class="mediaWrapper scrubber">


								<div class="dragToRotate ready">
									<div class="dragToRotateContainer">
										<div class="arrowLeftContainer">
											<icon class="arrowLeft">
											</icon>
										</div>
										<div class="dragToRotateText">Drag to rotate</div>
										<div class="arrowRightContainer">
											<icon class="arrowRight">
											</icon>
										</div>
									</div>
								</div>
							    <video id="video" controls="true">
							        <source src="<?= FRONT_ASSETS ?>video/flower_handler.mp4"><!-- WebKit -->
							    </video>
							    <div id="output"></div>
								
						    <script>
						    	$(document).ready(function(){

						    		var scaleFactor = .9;
									var video = $("#video")[0];
									var output = document.getElementById('output');
									var snapshots = [];
									var i = 0	;

									function capture(video, scaleFactor) {
										if(scaleFactor == null){
											scaleFactor = 1;
										}
										var w = video.videoWidth * scaleFactor;
										var h = video.videoHeight * scaleFactor;
										var canvas = document.createElement('canvas');
											canvas.width  = w;
										    canvas.height = h;
										var ctx = canvas.getContext('2d');
											ctx.drawImage(video, 0, 0, w, h);
									    return canvas;
									} 	

									video.addEventListener('loadeddata', function() {

									    video.currentTime = i;

									}, false);

									video.addEventListener('seeked', function() {

									    // now video has seeked and current frames will show
									    // at the time as we expect
									    var canvas = capture(video, scaleFactor);
									       document.getElementById('output').appendChild(canvas);



									    // when frame is captured, increase
									    i += .15;

									    // if we are not passed end, seek to next interval
									    if (i <= (video.duration - .5)) {
									        // this will trigger another seeked event
									        video.currentTime = i;

									    } else {
									        // DONE!, next action
									    }

									}, false);									



								});
						    </script>							
						</div>
						
					</div>
					<div class="media_enlarged media_enlarged_2 media_enlarged_active">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_1.jpg)"></div>
						</div>
					</div>
					<div class="media_enlarged media_enlarged_3">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_2.jpg)"></div>
						</div>
					</div>
					<div class="media_enlarged media_enlarged_4">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_3.jpg)"></div>
						</div>
					</div>
				</div>
				<div class="legShotsLifestyle">
					<span class="trigger_text">
						<span class="moreText">More Photos</span>
						<span class="lessText">Less Photos</span>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 2464 332 536" enable-background="new 0 2464 332 536" xml:space="preserve">
								<g>
									
										<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="166.1" y1="2496.3" x2="166.1" y2="2980.6"/>
									
										<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="166.1" y1="2980.6" x2="21.4" y2="2834.4"/>
									
										<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="166.1" y1="2980.6" x2="310.8" y2="2834.4"/>
								</g>
							</svg>
						</icon>
					</span>
					<!--section contains structured images of the product, from leg shots, to lifestyle, to social -->
					<div class="productMediaMasonry grid">
						<!-- if image is leg shot -->
						<div class="grid-item">
							<div class="mediaWrapper media_wrapper_legshot">
								<div class="media" style="background-image:url(<?=FRONT_IMG?>legShot.jpg)"></div>
							</div>
						</div>
						<!-- leg shot flipped image for both angles (bookmatch)-->
						<div class="grid-item">
							<div class="mediaWrapper media_wrapper_legshot media_wrapper_legshot_flipped">
								<div class="media" style="background-image:url(<?=FRONT_IMG?>legShot.jpg)"></div>
							</div>
						</div>			

<!-- 						if image is lifestyle

						<div class="grid-item grid-item_lifestyle">
							<div class="mediaWrapper media_wrapper_lifestyle">
								<img class="media" src="<?=FRONT_IMG?>lifestyle.jpg">
							</div>
						</div>

						<div class="grid-item grid-item_lifestyle">
							<div class="mediaWrapper media_wrapper_lifestyle">
								<img class="media" src="<?=FRONT_IMG?>lifestyle2.jpg">
							</div>
						</div> -->



					</div>
				</div>
			</div>
			<div class="col productDataActions">
				<span class="fabric_img fabric_left"></span>
				<div class="header">
					<h3 class="as_r">$388</h3>
				</div>
				<div class="customizationOption customizationOptionSize accordionUI">
					<a class="visible toggleCustomizationOptions"><label><span>Select a Size</span><icon></icon></label></a>
					<div class="hidden customizationOptionsModule size_selection_module radioOptionsModule">
						<a><p>Size Guide</p></a>
						<div class="row">
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 39</span><span>US 6</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 40</span><span>US 7</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 40.5</span><span>US 7.5</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 41</span><span>US 8</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 41.5</span><span>US 8.5</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 42</span><span>US 9</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 42.5</span><span>US 9.5</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 43</span><span>US 10</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 43.5</span><span>US 10.5</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 44</span><span>US 11</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 44.5</span><span>US 11.5</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 45</span><span>US 12</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 45.5</span><span>US 12.5</span></h3>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<h3 class="as_r"><span>EUR 46</span><span>US 13</span></h3>
								<input type="checkbox">
							</div>
						</div>
					</div>
				</div>
				<div class="customizationOption customizationOptionColor accordionUI">
					<a class="visible toggleCustomizationOptions"><label><span>Color: White Snake</span><icon></icon></label></a>
					<div class="hidden customizationOptionsModule color_selection_module radioOptionsModule">
						<div class="row row_of_2">
							<div class="col optionBox optionBoxRadio">
								<div class="media colorSwatch" style="background-image:url(<?=FRONT_IMG?>color_swatch_white_snake.png)"></div>
								<p>White Snake</p>
								<input type="checkbox">
							</div>
							<div class="col optionBox optionBoxRadio">
								<div class="media colorSwatch" style="background-image:url(<?=FRONT_IMG?>color_swatch_black_snake.png)"></div>
								<p>Black Snake</p>
								<input type="checkbox">
							</div>
						</div>
					</div>
				</div>
				<div class="add_to_bag_wrapper">
					<a class="btn btn_black">Add to Bag</a>
					<a class="favorite_btn btn btn_gray">
						<icon>
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 241.3 224.7" enable-background="new 0 0 241.3 224.7" xml:space="preserve">
                                <path d="M120.6,34.6c0,0,0.5-0.5,0.8-0.8c9.8-11.2,30.1-30,54.2-30c33.6,0,61.9,28.4,61.9,62c0,22-14.5,50.1-43.3,83.7
                                c-10.8,12.6-23.5,25.8-37.6,39.3c-10,9.5-18.7,17.1-24.2,21.9c-3.4,3-11.9,10.5-11.9,10.5s-8.3-7.5-11.6-10.4
                                C77.2,183.6,3.6,115.4,3.6,65.9c0-33.6,28.3-62,61.9-62c24.2,0,44.4,18.7,54.3,29.9L120.6,34.6z"/>
                            </svg>
                        </icon>
                        <p>Add to Favorites</p>
					</a>
					<div class="productShareRow">
						<ul>
							<li>
			                    <div>
			                        <a href="https://www.facebook.com/watertour" class="facebook footer_social_icon" style="background-image:url(<?=FRONT_IMG?>facebook_icon.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://twitter.com/watertour" class="twitter footer_social_icon" style="background-image:url(<?=FRONT_IMG?>twitter_icon.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://www.instagram.com/watertour/" class="instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>instagram_icon.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://www.instagram.com/watertour/" class="pinterest instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>pinterest_icon.png)"></a>
			                    </div>
			                </li>
						</ul>
					</div>
				</div>
				<div class="accordionUI accordionProductData accordionUI_active" id="productDescriptionContainer">
					<a class="visible">
						<h6 class="as_r">Description</h6>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</icon>
					</a>
					<a class="hidden">
						<p class="as_l">Matte effect lambskin sneakers with tone-on-tone laces and rubber sole. Italian-made leather sandals studded with a metal eyelet trim and the design fastens with a lace-up closure.</p>
					</a>
				</div>
				<div class="accordionUI accordionProductData" id="productDescriptionContainer">
					<a class="visible">
						<h6 class="as_r">Details</h6>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</icon>
					</a>
					<a class="hidden">
						<span class="as_l">
							<p>100% calf leather with metal trim</p>
							<p>Lining: 75% goat leather, 20% calf leather, 5% polyamide</p>
							<p>Sole: 100% calf leather</p>
							<p>Cross-over straps</p>
							<p>Made in NYC</p>
							<p>5 mm Arch</p>
						</span>
					</a>
				</div>
				<div class="accordionUI accordionProductData accordionUI_active" id="productDescriptionContainer">
					<a class="visible">
						<h6 class="as_r">Delivery</h6>
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"/>
							</svg>
						</icon>
					</a>
					<a class="hidden">
						<div class="icon_title_text row">
							<icon class="col truck_icon" style="background-image:url(<?=FRONT_IMG?>delivery_truck.png)"></icon>
							<span class="col as_l">
								<b>Next day delivery</b>
								<p>Place your order today and receive it tomorrow</p>
							</span>
						</div>
						<div class="icon_title_text row">
							<icon class="col truck_icon" style="background-image:url(<?=FRONT_IMG?>delivery_truck.png)"></icon>
							<span class="col as_l">
								<b>Complimentary standard shipping</b>
								<p>Place your order today and receive it with 3-5 business days</p>
							</span>
						</div>
						<div class="icon_title_text row">
							<icon style="background-image:url(<?=FRONT_IMG?>shopping_bag_large.png)"></icon>
							<span class="col as_l">
								<b>Collect-in-Store</b>
								<p>Order online today and pick up your items in our NYC factory store as early as tomorrow</p>
							</span>
						</div>
						<div class="icon_title_text row">
							<icon style="background-image:url(<?=FRONT_IMG?>returns.png)"></icon>
							<span class="col as_l">
								<b>Complimentary Returns</b>
								<p>Enjoy free returns on your order</p>
							</span>
						</div>	
					</a>
				</div>
				<div class="help_options_wrapper">
					<p class="as_l">For style or product advice, please contact our Shoe Design Advisors at <a class="tel black_thin_btn" href="tel:2127771851">212 777 1851</a> or start a <a>live chat</a> with a member of our design team right away</p>
				</div>
				<div class="relatedProducts">
					<h5 class="as_m">You May also Like</h5>
					<div class="relatedProductsSlider">
						<div class="sliderItem">
							<a>
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=FRONT_IMG?>relatedHandler.jpg)">
									</div>
								</div>
								<div class="copyWrapper">
									<h6 class="as_m">Floral Handler with Wood Sole</h6>
								</div>
							</a>
							<a>
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=FRONT_IMG?>relatedDiana.jpg)">
									</div>
								</div>
								<div class="copyWrapper">
									<h6 class="as_m">Diana White Snake</h6>
								</div>
							</a>
						</div>
						<div class="sliderItem">
							<a>
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=FRONT_IMG?>relatedJett.jpg)">
									</div>
								</div>
								<div class="copyWrapper">
									<h6 class="as_m">Jett Clasic: Embossed Snake</h6>
								</div>
							</a>
							<a>
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=FRONT_IMG?>relatedHandler.jpg)">
									</div>
								</div>
								<div class="copyWrapper">
									<h6 class="as_m">Floral Handler with Wood Sole</h6>
								</div>
							</a>
						</div>
						<div class="sliderItem">	
							<a>
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=FRONT_IMG?>relatedDiana.jpg)">
									</div>
								</div>
								<div class="copyWrapper">
									<h6 class="as_m">Diana White Snake</h6>
								</div>
							</a>
							<a>
								<div class="mediaWrapper">
									<div class="media" style="background-image:url(<?=FRONT_IMG?>relatedJett.jpg)">
									</div>
								</div>
								<div class="copyWrapper">
									<h6 class="as_m">Jett Clasic: Embossed Snake</h6>
								</div>
							</a>
						</div>
					</div>
				</div>	
<!-- 				<div class="reviews">
					<div class="header">
						<h5 class="as_m">Reviews</h5>
						<a class="show_mdl" data-mdl_name="writeReview">Write a Review</a>
					</div>
					<div class="ratingSnapshot">
						<h6 class="as_m">Rating Snapshot</h6>
						
					</div>
				</div>	 -->		
			</div>
		</div>
	</div>
</div>
<div class="pageWrapper confirmationPageWrapper">
    <div class="promoBar">
        <p class="valB">Book your Reservation online now to save <span>50%</span> compared to our walk-in price.</p>
    </div>
    <section class="topSection">
        <h1 class="big uppercase valB">Your Reservation</h1>
        <p class="valB">Continue to checkout and purchase your reservation(s) to receive e-tickets immediately</p>
        <div class="confirmationContinueBar">
            <div class="step emailStep">
                <label for="emailAddress" class="valB">Email Address</label>
                <input type="email" placeholder="name@domain.com" id="emailAddress" name="email" data-ErrorText="Please enter your email address">
            </div>
            <div class="step nameStep">
                <label for="fullName" class="valB">Full Name</label>
                <input type="text" placeholder="John Smith" id="fullName" name="full_name" data-ErrorText="Please enter your full name">
            </div>
            <div class="step telStep">
                <label for="phoneNumber" class="valB">Phone Number <span>(optional)</span></label>
                <input type="tel" placeholder="(XXX) - XXX - XXXX" id="phoneNumber" name="phone">
            </div>
            <div class="step continueStep">
                <a class="btn primaryBtn" id="submitCheckout"><p>Proceed to Checkout</p><i class="material-icons">chevron_right</i></a>
            </div>
        </div>
    </section>
    <section class="reservationDetailsSection middleSection">
        <div class="thinGrayBar">
            <div class="checkoutContentWidth">
                <div class="left_float">
                    <p class="valB">Reservation Information</p>
                </div>
                <div class="right_float">
                    <p class="valB">Sub-Total:<span id="subtotal">$<?=number_format($model->total,2)?></span></p>
                </div>
            </div>
        </div>
        <div class="checkoutContentWidth row_of_2 row reservationDetailsContent">
        <?$increment = 1;
        foreach($model->cart as $cart){
        $re = $cart->duration ? : 0;?>
        
            <div class="row row_of_2 reservationItemDetails">
                <!--IF MORE THAN 1 RESERVATION -->
                <p>Reservation #<?=$increment?></p>
                <div class="col">
                    <div class="labelInputWrapper">
                        <label><!--IF just one activity:"activity", multiple:"activities"-->Which activity?</label>

                        <!--IF RENTAL:-->
                        <!--
                        <div class="selectedActivity">
                            <h5 class="valB">1 Hour Rental</h5>
                            <div class="labelInputWrapper">
                                <label for="triggerDatepicker">When?</label>
                                <input type="text" class="triggerDatepicker" id="triggerDatepicker" value="Sep 26, 2016 at 12:30 pm">
                                <span class="dropdownArrow"></span>
                            </div>
                        </div>
                        -->


                        <!--IF TOUR-->
                        <?if($cart->tour_id){
                            $item = \Model\Tour::getItem($cart->tour_id)?>
                            <div class="selectedActivity">
                                <div class="tourSelection">
<!--
                                     <div class="selectedTourImage media" style="background-image:url('<//?=UPLOAD_URL.'tours/'.$item->featuredImage()?>')">
                                        <icon class="checkmark" style="background-image:url('<//?=FRONT_IMG?>checkmarkWhite.png')"></icon>
                                    </div>
-->
                                    <input name="tourName" type="hidden" value="<?=$item->id?>">
                                    <h5 class="valB"><?=$item->name?></h5>
                                </div>
                            </div>
                        <?}?>
                        <?if($cart->package_id){
                            $item = \Model\Package::getItem($cart->package_id);
                            $details = $cart->package_details ? json_decode($cart->package_details,true): [];?>
                            <div class="selectedActivity">
                                <div class="tourSelection">
<!--
                                     <div class="selectedTourImage media" style="background-image:url('<//?=UPLOAD_URL.'packages/'.$item->banner?>')">
                                        <icon class="checkmark" style="background-image:url('<//?=FRONT_IMG?>checkmarkWhite.png')"></icon>
                                    </div>
-->
                                    <input name="tourName" type="hidden" value="<?=$item->id?>">
                                    <h5 class="valB"><?=$item->name?></h5>
                                </div>
                            </div>
                            <div class="packageContents">
                            <? foreach ($details as $name => $detail) {?>
                                <?foreach ($detail as $items) {
                                    $m = "\\Model\\" . $name;
                                    $obj = $m::getItem($items['id']);
                                    $img = $name == 'Tour' ? $obj->featuredImage() : '';?>
                                    <div class="selectedActivity" style="margin-left: 15px;">
                                        <div class="tourSelection">
                                             <div class="selectedTourImage media">
                                                <icon class="checkmark" style="background-image:url('<?=FRONT_IMG?>checkmarkWhite.png')"></icon>
                                            </div>
                                            <input name="tourName" type="hidden" value="<?= $obj->id ?>">
                                            <h5 class="valB"><?= $obj->getName() ?></h5>
                                        </div>
                                    </div>
                                <? } ?>
                            <? } ?>
                            </div>
                        <?}?>
                        <?if($cart->duration){ //todo replace duration with rental id
                            $item = \Model\Rental::getItem($cart->duration)?>
                            <div class="selectedActivity">
                                <div class="tourSelection">
                                    <div class="selectedTourImage media">
                                        <icon class="checkmark" style="background-image:url('<?=FRONT_IMG?>checkmarkWhite.png')"></icon>
                                    </div>
                                    <h5 class="valB"><?=$item->name?></h5>
                                </div>
                            </div>
                        <?}?>
                    </div>
                    <div class="labelInputWrapper" id="peopleCount">
                        <label>Who's going?</label>
                        <div class="adultsCount">
                            <div class="dec button <?=$cart->adult_count ? '': 'disabled';?>" data-ref="<?=$cart->ref_num?>" data-name="adult_count"><span></span></div>
                            <div class="adultsCountValWrapper">
                                <input type="text" class="adultsCountVal" value="<?=$cart->adult_count ? : 0?>" pattern="[0-9]*"
                                       name="reservationQuantity">
                                <span class="key"><?=pluralize($cart->adult_count,'Adult')?></span>
                            </div>
                            <div class="inc button" data-ref="<?=$cart->ref_num?>" data-name="adult_count"><span></span><span></span></div>
                        </div>
                        <span class="separator">and</span>
                        <div class="kidsCount">
                            <div class="dec button <?=$cart->kid_count ? '': 'disabled';?>" data-ref="<?=$cart->ref_num?>" data-name="kid_count"><span></span></div>
                            <div class="kidsCountValWrapper">
                                <input type="text" class="kidsCountVal" data-name="kid_count" value="<?=$cart->kid_count ? : 0?>" pattern="[0-9]*"
                                       name="reservationKidQuantity">
                                <span class="key"><?=pluralize($cart->kid_count,'Kid')?></span>
                            </div>
                            <div class="inc button" data-ref="<?=$cart->ref_num?>" data-name="kid_count"><span></span><span></span></div>
                        </div>
<!--                        --><?//if($cart->duration){?>
<!--                            <span class="separator">and</span>-->
<!--                            <div class="tandemsCount">-->
<!--                                <div class="dec button --><?//=$cart->tandem_count ? '': 'disabled';?><!--" data-ref="--><?//=$cart->ref_num?><!--" data-name="tandem_count"><span></span></div>-->
<!--                                <div class="tandemsCountValWrapper">-->
<!--                                    <input type="text" class="tandemCountVal" data-name="tandem_count" value="--><?//=$cart->tandem_count ? : 0?><!--" pattern="[0-9]*"-->
<!--                                           name="reservationTandemQuantity">-->
<!--                                    <span class="key">Tandem</span>-->
<!--                                </div>-->
<!--                                <div class="inc button" data-ref="--><?//=$cart->ref_num?><!--" data-name="tandem_count"><span></span><span></span></div>-->
<!--                            </div>-->
<!--                        --><?//}?>
                    </div>

                   <!-- <?if(!$cart->package_id){?>
                        <div class="labelInputWrapper whenLabelInputWrapper">
                            <label>When?</label>
                            <div class="datepickerInputWrapper">

                                <input type="text" class="triggerDatepicker" hidden name="reservationDateTime" data-ref="<?=$cart->ref_num?>" data-name="reserve_date" placeholder="When?" value="<?=\Carbon\Carbon::createFromTimestamp(strtotime($cart->reserve_date))->format('M d, Y g:i a')?>">
                                <div class="datepickerCellUIConfirm whenSelectorParent" data-ref="<?=$cart->ref_num?>" <?=$cart->tour_id && ($tour = \Model\Tour::getItem($cart->tour_id))? 'data-hours="'.implode(',', $tour->getHours()).'" data-week="'.implode(',',$tour->getWeek()).'" data-day="'.implode(',',$tour->getDay()).'"': ''?>></div>
                                <div class="placeholderInactive dateText">
                                    <p class="placeholder val gray">When?</p>
                                    <p class="data whenSelectorResult"><?=\Carbon\Carbon::createFromTimestamp(strtotime($cart->reserve_date))->format('M d, Y g:i a')?></p>
                                    <span class="dropdownArrow">
                                        <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
                                    </span>
                                </div>
                            </div>
                        </div>-->
                   <!-- <? } else {
                        foreach (json_decode($cart->package_details, true) as $name => $details) {
                            foreach ($details as $detail) {
                                $m = "\\Model\\" . $name;
                                $obj = $m::getItem($detail['id']);
                                $dataDay = $dataWeek = $dataHour = '';
                                if ($name == 'Tour') {
                                    $dataDay = implode(',', $obj->getDay());
                                    $dataWeek = implode(',', $obj->getWeek());
                                    $dataHour = implode(',', $obj->getHours());
                                } ?>
                                <div class="labelInputWrapper whenLabelInputWrapper">
                                    <label><?= $obj->getName() ?></label>
                                    <div class="datepickerInputWrapper">
                                        <input type="text" class="triggerDatepicker" hidden name="reservationDateTime" data-ref="<?= $cart->ref_num ?>" data-name="reserve_date" placeholder="When?" value="<?= \Carbon\Carbon::createFromTimestamp(strtotime($detail['datetime']))->format('M d, Y g:i a') ?>">
<!--                                        <div class="datepickerCellUIConfirm whenSelectorParent" data-category="--><?//= $name ?><!--" data-cat_id="--><?//= $obj->id ?><!--" data-ref="--><?//= $cart->ref_num ?><!--" data-hours="--><?//= $dataHour ?><!--" data-week="--><?//= $dataWeek ?><!--" data-day="--><?//= $dataDay ?><!--"></div>-->
                                        <!--<div class="placeholderInactive dateText">
                                            <p class="placeholder val gray">When?</p>
                                            <p class="data whenSelectorResult"><?= \Carbon\Carbon::createFromTimestamp(strtotime($detail['datetime']))->format('M d, Y g:i a') ?></p>
                                            <span class="dropdownArrow">
                                                <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>
                        <? } ?>
                    <? } ?>-->

                    <h4 class="val gray">
                        <!--							<span class="time"><i class="material-icons">access_time</i>11:00 AM</span>-->
                        <!--							<span class="duration"><i class="material-icons">timelapse</i>--><?//=$package->getDurationFormatted()?><!--</span>-->
                        <span class="date"><i class="material-icons">date_range</i>Use within 6 months of purchase</span>
                    </h4>

                    <div class="labelInputWrapper">
                        <label>Reservation Price</label>
                        <h5 class="valB reservationPrice">$<?=number_format($cart->getTotal(),2)?></h5>
                    </div>
                    <div class="labelInputWrapper deleteLabelInputWrapper">
                        <form action="/reservation/delete/<?=$cart->id?>" method="post">
                            <input type="submit" value="Delete Reservation"/>
                        </form>
                    </div>
                </div>
               
            </div>
        <? $increment++;}?>
        </div>
    </section>
    <section class="topSection finalConfirmSection">
        <div class="confirmationContinueBar">
            <div class="step emailStep">
                <label for="emailAddress" class="valB">Email Address</label>
                <input type="email" placeholder="name@domain.com" id="final_emailAddress" name="email" data-ErrorText="Please enter your email address">
            </div>
            <div class="step nameStep">
                <label for="fullName" class="valB">Full Name</label>
                <input type="text" placeholder="John Smith" id="final_fullName" name="full_name" data-ErrorText="Please enter your full name">
            </div>
            <div class="step telStep">
                <label for="phoneNumber" class="valB">Phone Number <span>(optional)</span></label>
                <input type="tel" placeholder="(XXX) - XXX - XXXX" id="final_phoneNumber" name="phone">
            </div>
            <div class="step continueStep">
                <a class="btn primaryBtn" id="finalSubmitCheckout"><p>Proceed to Checkout</p><i class="material-icons">chevron_right</i></a>
            </div>
        </div>
        <p class="valB">Continue to checkout and purchase your reservation(s) to receive e-tickets immediately</p>
    </section>
    <section class="deals confirmationDealsRow row">
        <div class="equipmentFreeSection row col">
            <div class="text col">
                <h1 class="valB">Free</h1>
                <h4 class="val gray">with your bus tour <br>reservation tickets</h4>
            </div>
            <div class="col">
                <icon class="lockIcon" style="background-image:url('<?=FRONT_IMG?>poncho.png')"></icon>
                <h6 class="valB">Poncho</h6>
            </div>
            <div class="col">
                <icon class="helmetIcon" style="background-image:url('<?=FRONT_IMG?>earphones.png')"></icon>
                <h6 class="valB">Earphones</h6>
            </div>
        </div>
        <div class="discountOfferSection col row">
            <div class="col">
                <h1 class="valB">Get Discounts</h1>
                <h5 class="val gray">on bus and boat tour tickets<br>when you reseve your bike<br>tour online here with <span>Bike Rentals Central Park</span></h5>
            </div>
            <?foreach($model->featured as $item){?>
                <div class="col discountOption">
                <h5 class="valB">Bike Tour</h5>
                <span class="discountGroupSize"><span><?=$model->cart[0]->adult_count.' '.pluralize($model->cart[0]->adult_count,'Adult')?></span><span><?=$model->cart[0]->kid_count.' '.pluralize($model->cart[0]->kid_count,'Kid')?></span></span>
                <a>
                    <div class="media" style="background-image:url('<?=UPLOAD_URL.'packages/'.$item->banner?>')"></div>
                    <span class="addUI absTransCenter">
                        <span class="circle">
                            <span class="plussign"></span>
                            <span class="plussign"></span>
                        </span>
                        <p class="valB">Add</p>
                    </span>
                </a>
                <div class="discountValue">
                    <span class="val">Save</span>
                    <span class="online_price">
                        <p>$18</p>
                    </span>
                </div>
            </div>
            <?}?>
        </div>
    </section>
    <section class="reservationQuestionsSection" style="display:none;pointer-events: none;opacity:0;visibility: hidden;">
        <h1 class="big valB">Questions</h1>
        <div class="questionsBox">
            <div class="row row_of_3">
                <div class="col">
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="questionUI">
                        <h4 class="futura futura_m">When will I get my tickets?</h4>
                        <p class="futura navy">Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        $(document).on('click', '.inc, .dec', function (e) {
            var name = $(this).data('name');
            var id = $(this).parent().find('input').data('id');
            var val = $(this).parent().find('input').val();
            var ref_num = $(this).data('ref');
            var upgrade = typeof $(this).data('upgrade') === 'undefined' ? false : $(this).data('upgrade');
            var add_ons = typeof $(this).data('add_ons') === 'undefined' ? false : $(this).data('add_ons');

            var reservationElement = $(this).closest('.reservationItemDetails').find('.reservationPrice');
            var equipmentElement = $(this).closest('.reservationItemDetails').find('.equipmentCustomizationReservationInfo');
            var dropdownEquivalent = $(this).closest('.reservationItemDetails').find('.cartDropdown[data-id=' + id + ']');
            dropdownEquivalent.val(val);
            if (val == 0) {
                dropdownEquivalent.parent().siblings('.dec').addClass('disabled');
                dropdownEquivalent.closest('.equipmentCustomizationActionBtn').removeClass('equipmentSelected');
            }

            var data = {qty: val, ref_num: ref_num, upgrade: upgrade,add_ons:add_ons};
            data[name] = typeof id === 'undefined' ? val : id;
            $.post('/reservation/updateCart', data, function (returnData) {
                var json = $.parseJSON(returnData);
                if (json.status == 'success') {
                    $('#subtotal').html('$' + json.newTotal.toFixed(2));
                    reservationElement.html('$' + json.lineItemTotal.toFixed(2));
//                    if (json.equipment.length) {
//                        var html = '<div class="selectedEquipmentList row">';
//                        $.each(json.equipment, function (i, e) {
//                            html +=
//                                '<div class="col selectedI">' +
//                                '<div class="equipmentImage media" style="background-image:url(/content/uploads/equipments/' + e.featuredImage + '>)">' +
//                                '<p class="whiteOut">' + e.name + '</p></div><div class="comparativePricingFormat centeredCols"><div class="col"><h4 class="valB">Walk-In</h4><div class="co_price"><span></span>' +
//                                '<p>$' + e.walk_in + '</p></div></div><div class="col"><h4 class="valB">Online</h4><div class="online_price">' +
//                                '<p>$' + e.online_price + '</p></div></div><div class="col numberTogglerCol"><div class="numericalToggler"><div class="numberCount">' +
//                                '<div class="dec button" data-ref="' + e.ref + '" data-name="equipment" data-category="' + e.category + '"><span></span></div><div class="numberCountValWrapper">' +
//                                '<input name="reserveRental[]" type="text" class="numberCountVal" value="' + e.qty + '" pattern="[0-9]*" data-id="' + e.id + '"></div>' +
//                                '<div class="inc button" data-ref="' + e.ref + '" data-name="equipment" data-category="' + e.qty + '"><span></span><span></span></div></div></div></div></div></div>';
//                        });
//                        html += '</div>';
//                    } else {
//                        var html = '<div class="noAddOnsBox">' +
//                            '<div class="absTransCenter">' +
//                            '<h8 class="valB uppercase gray">no additional equipment has been chosen for this reservation</h8>' +
//                            '</div> ' +
//                            '</div>';
//                    }
//                    equipmentElement.find('.selectedEquipmentList').remove();
//                    equipmentElement.find('.noAddOnsBox').remove();
//                    equipmentElement.append(html);
                }
            });
        });
        $('#submitCheckout').on('click', function (e) {
            e.preventDefault();
            var email = $('#emailAddress');
            var fullName = $('#fullName');
            var phone = $('#phoneNumber');
            if(formValidator([email,fullName])) {
                $.post('/reservation/addSession', {
                    email: email.val(),
                    fullName: fullName.val(),
                    phone: phone.val()
                }, function (data) {
                    var json = $.parseJSON(data);
                    if (json.status == 'success') {
                        window.location.replace('/checkout/payment');
                    }
                });
            }
        });
        $('#finalSubmitCheckout').on('click', function (e) {
            e.preventDefault();
            var email = $('#final_emailAddress');
            var fullName = $('#final_fullName');
            var phone = $('#final-phoneNumber');
            if(formValidator([email,fullName])) {
                $.post('/reservation/addSession', {
                    email: email.val(),
                    fullName: fullName.val(),
                    phone: phone.val()
                }, function (data) {
                    var json = $.parseJSON(data);
                    if (json.status == 'success') {
                        window.location.replace('/checkout/payment');
                    }
                });
            }
        });
        var datepickerCellUI = document.getElementsByClassName('datepickerCellUIConfirm');
        for (var i = 0; i < datepickerCellUI.length; i++) {
            var date = moment(Math.ceil((Date.now() / 1000) / (30 * 60)) * (30 * 60) * 1000);
            (function(e){
                rome(datepickerCellUI[i], options = {
                    "appendTo": document.body,
                    "autoClose": true,
                    "autoHideOnBlur": true,
                    "autoHideOnClick": true,
                    "date": true,
                    "dateValidator": function (d) {
//                    var index = typeof datepickerCellUI[1] === 'undefined' ? 0 : 1;
                        var index = e;
                        var availableWeeks = datepickerCellUI[index].dataset.week ? datepickerCellUI[index].dataset.week : '';
                        var m = moment(d);
                        if (availableWeeks) {
                            var weekArray = availableWeeks.split(',');
                            var weekMap = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
                            var indexMap = weekArray.map(function (obj) {
                                if (weekMap.indexOf(obj.valueOf()) != -1) {
                                    return weekMap.indexOf(obj.valueOf());
                                }
                            });
                            return indexMap.indexOf(m.day()) !== -1;
                        } else {
                            return true;
                        }
                    },
                    "dayFormat": "D",
                    "initialValue": $(datepickerCellUI[i]).siblings('[name=reservationDateTime]').val(),
                    "inputFormat": "MMM D, YYYY h:mm a",
                    "invalidate": true,
                    "max": null,
                    "min": date.format('MMM DD, YYYY h:mm a'),
                    "monthFormat": "MMMM 'YY",
                    "monthsInCalendar": 1,
                    "required": false,
                    "strictParse": false,
                    "styles": {
                        "back": "rd-back",
                        "container": "rd-container",
                        "date": "rd-date",
                        "dayBody": "rd-days-body",
                        "dayBodyElem": "rd-day-body",
                        "dayConcealed": "rd-day-concealed",
                        "dayDisabled": "rd-day-disabled",
                        "dayHead": "rd-days-head",
                        "dayHeadElem": "rd-day-head",
                        "dayRow": "rd-days-row",
                        "dayTable": "rd-days",
                        "month": "rd-month",
                        "next": "rd-next",
                        "positioned": "rd-container-attachment",
                        "selectedDay": "rd-day-selected",
                        "selectedTime": "rd-time-selected",
                        "time": "rd-time",
                        "timeList": "rd-time-list",
                        "timeOption": "rd-time-option"
                    },
                    "time": true,
                    "timeFormat": "h:mm a",
                    "timeInterval": 1800,
                    timeValidator: function (d) {
//                    var index = typeof datepickerCellUI[1] === 'undefined' ? 0 : 1;
                        var index = e;
                        var availableHours = datepickerCellUI[index].dataset.hours ? datepickerCellUI[index].dataset.hours : '';
                        var m = moment(d);
                        if (availableHours) {
                            var availableHoursArray = availableHours.toString().split(',');
                            var hoursStringArray = [];
                            for (var i = 0; i < availableHoursArray.length; i++) {
                                var thisMins = parseFloat(availableHoursArray[i]) * 60;
                                var h = m.clone().startOf('day').minute(thisMins).second(0);
                                hoursStringArray.push(h.toString());
                            }
                            var comparedM = m.second(0);
                            return hoursStringArray.indexOf(comparedM.toString()) != -1;
                        } else {
                            var start = m.clone().hour(7).minute(59).second(59);
                            var end = m.clone().hour(20).minute(0).second(1);
                            return m.isAfter(start) && m.isBefore(end);
                        }
                    },
                    "weekdayFormat": "min",
                    "weekStart": moment().weekday(0).day()

                }).on('data', function (value) {
                    if (window.location.pathname == '/reservation/confirm') {
                        var ref_num = this.associated.dataset.ref;
                        var data = {ref_num: ref_num, reserve_date: value, upgrade: false};
                        if(this.associated.dataset.category && this.associated.dataset.cat_id){
                            data.category = this.associated.dataset.category;
                            data.cat_id = this.associated.dataset.cat_id;
                        }
                        $.post('/reservation/updateCart', data);
                    }
                    var whenSelectorResult = $(this.associated).siblings('.dateText').find('.whenSelectorResult');
                    var validateTime = moment.duration(moment(value).format('HH:mm')).asHours();
                    var availableHours = datepickerCellUI[0].dataset.hours ? datepickerCellUI[0].dataset.hours : '';
                    var availableHoursArray = [];
                    if (availableHours) {
                        availableHoursArray = availableHours.toString().split(',');
                    } else {
                        for (var i = 8; i < 20.5; i += .5) {
                            availableHoursArray.push(i + "");
                        }
                    }
                    if (availableHoursArray.indexOf(validateTime.toString()) != -1) {
                        //time is good
                    } else {
                        //time is bad - set to nearest available
                        var newTime = moment(value);
                        if (newTime.isSame(moment(), 'day')) {
                            var self = this;
                            $.each(availableHoursArray, function (i, e) {
                                if (e > validateTime) {
                                    var h = newTime.startOf('day').minute(parseFloat(e) * 60).second(0);
                                    self.setValue(h);
                                    value = h.format('MMM D, YYYY h:mm a');
                                    return false;
                                }
                            })
                        } else {
                            var h = newTime.startOf('day').minute(availableHoursArray[0] * 60).second(0);
                            this.setValue(h);
                            value = h.format('MMM D, YYYY h:mm a');
                        }
                    }

                    whenSelectorResult.html(value);
                    $('[name=reservationDateTime]').val(value);
                    if ($('.rd-time-selected').hasClass("timeChosenbyUser")) {
                        $(".dateTimeConfirmButton").removeClass("disabled");
                    }
                }).on('time', function (time) {
                    if ($(".rd-time-list").is(":visible")) {
                        $('.rd-time-selected').addClass('timeChosenbyUser');
                        $(".dateTimeConfirmButton").removeClass("disabled");
                    }
                    $(".rd-time").toggleClass('activeTimeDropdown');
                }).on('ready', function () {
                    if ($('[name=reservationDateTime]').val().length > 0) {
                        $('.rd-time-selected').addClass('timeChosenbyUser');
                    }
                });
            })(i);
        }
    })
</script>
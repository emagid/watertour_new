<?php
$states = get_states();
unset($states['GU']);unset($states['PR']);unset($states['VI']);
$countries = get_countries();
$months = range(1,12);
$curr_year = (int)date("Y",time());
$years = range($curr_year,$curr_year+8);
?>
<div class="banner cms myaccount_banner">
	<div class="container">
		<div class="inner">
			<h1>My Account</h1>
		</div>
	</div>
</div>

<div class="container container_myaccount">
	<div class="myaccount_table nav_table">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/account_menu.php'); ?>
		<div class="myaccount_fields">
			<form method="post" action="<?php echo SITE_URL."user/pm_update";?>">
				<input type="hidden" name="id" value="<?php echo $model->payment_profile->id;?>" />
				<div class="row">
					<div class="col-md-12">
					  <div class="box">
					    <h4>Billing Information</h4>
					    <div class="form-group">
					      <label>Label</label>
					      <?php echo $model->form->editorFor("label");?>
					    </div>
					    <div class="form-group">
					      <label>First Name</label>
					      <?php echo $model->form->editorFor("first_name");?>
					    </div>
					     <div class="form-group">
					      <label>Last Name</label>
					      <?php echo $model->form->editorFor("last_name");?>
					    </div>
					     <div class="form-group">
					      <label>Phone Number</label>
					      <?php echo $model->form->editorFor("phone");?>    
					    </div>
					     <div class="form-group">
					      <label>Address</label>
					      <?php echo $model->form->editorFor("address");?>
					    </div>
					     <div class="form-group">
					      <label>Address 2</label>
					      <?php echo $model->form->editorFor("address2");?>
					    </div>
					    <div class="form-group">
					      <label>City</label>
					      <?php echo $model->form->editorFor("city");?>
					    </div>
					     <div class="form-group">
					      <label>State</label>
					      <?php echo $model->form->dropDownListFor("state",$states);?>
					    </div>
					     <div class="form-group">
					      <label>Zip Code</label>
					      <?php echo $model->form->editorFor("zip");?>
					    </div>
					     <div class="form-group">
					      <label>Country</label>
					      <?php echo $model->form->dropDownListFor("country",$countries);?>
					    </div>
					  </div>
					</div>

					<div class="col-md-12">
						<div class="box">
							<h4>Credit Card Information</h4>
							<div class="form-group">
								<label>Credit Card Number</label>
								<?php echo $model->form->editorFor("cc_number");?>	
							</div>
							<div class="form-group">
							    <label>Exp Month</label>
							    <?php echo $model->form->dropDownListFor("cc_expiration_month",$months);?>
							</div>
							<div class="form-group">
								<label>Exp Year</label>
								<?php echo $model->form->dropDownListFor("cc_expiration_year",$years);?>
							</div>
							<div class="form-group">
							  	<label>Credit Card Code</label>
							  	<?php echo $model->form->editorFor("cc_ccv");?>
							</div>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success btn-save">Save</button>
			</form>
    	</div>
	</div>
</div>
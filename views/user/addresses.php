<div class="banner cms myaccount_banner">
	<div class="container">
		<div class="inner">
			<h1>My Account</h1>
		</div>
	</div>
</div>

<div class="container container_myaccount">
	<div class="myaccount_table nav_table">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/account_menu.php'); ?>
		<h4>Shipping Information</h4>
		<div class="account_info">
			<div class="form-group myaccount_input add_new">
				<a href="<?=SITE_URL?>user/ad_update" class="btn btn-lg btn-default">&#43; Add New Address</a>
	    	</div>
			<?php foreach($model->addresses as $addr) { ?>
			  <div class="col-md-6">
	            <div class="box">
	              <div class="form-group">
	                <label>Label: </label>
	                <?php echo $addr->label;?>
	              </div>
	              <div class="form-group">
	                <label>First Name: </label>
	                <?php echo $addr->first_name;?>
	              </div>
	              
	              <div class="form-group">
	                <label>Last Name: </label>
	                <?php echo $addr->last_name;?>
	              </div>
	              <?php if ($addr->phone!="") { ?>
	              <div class="form-group">
	                <label>Phone Number: </label>
	                <?php echo $addr->phone;?>
	              </div>
	              <?php } ?>
	              <div class="form-group">
	                <label>Address: </label>
	                <?php echo $addr->address;?>
	              </div>
	              
	              <div class="form-group">
	                <label>Address 2: </label>
	                <?php echo $addr->address2;?>
	              </div>
	              
	              <div class="form-group">
	                <label>City: </label>
	                <?php echo $addr->city;?>
	              </div>
	              
	              <div class="form-group">
	                <label>State: </label>
	                
	                <?php echo get_states()[$addr->state];?>
	              </div>
	              
	              <div class="form-group">
	                <label>Zip Code: </label>
	                <?php echo $addr->zip;?>
	              </div>
	              
	              <div class="form-group">
	                <label>Country: </label>
	                <?php echo $addr->country;?>
	              </div>
	              
	              <div class="form-group">
	                 <a class="btn-actions" href="<?php echo SITE_URL; ?>user/ad_update/<?php echo $addr->id; ?>">
			           <i class="icon-pencil">Edit</i> 
			         </a>
			         <br />
	                 <a class="btn-actions" href="<?php echo SITE_URL; ?>user/ad_delete/<?php echo $addr->id; ?>" onClick="return confirm('Are You Sure?');">
			            <i class="icon-cancel-circled">Delete</i> 
			         </a>
	              </div>
	            </div>
	          </div>
			<?php }?>
	    </div><!-- .account_info -->
	</div><!-- .table-responsive -->
</div><!-- .container -->





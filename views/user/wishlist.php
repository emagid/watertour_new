<div class="banner cms myaccount_banner">
	<div class="container">
		<div class="inner">
			<h1>My Account</h1>
		</div>
	</div>
</div>

<div class="container container_myaccount">
	<div class="myaccount_table nav_table">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/account_menu.php'); ?>
		<h4>Wishlist</h4>
		<?php if (isset($model->products)) { ?>
		<!-- Items -->
			<div class="brand_items">
				<div class="category">
					<div class="row">
						<?php foreach($model->products as $product) { ?>
						<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
							<div class="product brand_product">
								<?php 
						            $img_path = ADMIN_IMG.'watertour_shoe.png'; //TODO st-dev default product image
						            if(!is_null($product->featured_image) && $product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$product->featured_image)){
						             	$img_path = UPLOAD_URL . 'products/' . $product->featured_image;
						            }
						        ?>
							        <div class="product-img-holder">
							        	<a href="<?=SITE_URL.'product/'.$product->slug?>"><img src="<?php echo $img_path; ?>" /></a>
							    	</div>
								<div class="inner">
									<h2><a href="<?=SITE_URL.'product/'.$product->slug?>"><?=$product->name?></a></h2>	
									<span itemprop="model" class="model"><?=$product->mpn?></span>
									<p itemprop="offers" itemscope itemtype="http://schema.org/Offer">
										<span itemprop="price">$<?=$product->price?></span>
									</p>
								</div>
							</div>
							<a href="<?=SITE_URL?>user/wishlist_remove/<?=$product->id?>">Remove</a>
						</div>
						<?php } ?>
					</div>	
				</div>	
			</div>
		<?php } ?>
	</div><!-- .table-responsive -->
</div><!-- .container -->
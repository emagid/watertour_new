<div class="pageWrapper categoryPageWrapper">
	<div class="categoryIntro row">
		<div class="col categoryData">
			<div class="header">
				<h1 class="as_l">Women</h1>
				<a class="btn_oreo btn btn_transparent">View All</a>
			</div>
			<div class="subHeader">
				<a class="show_mdl" data-mdl_name="newItems"><h2 class="as_l">New Arrivals</h2><h2 class="count_ui">22<span>items</span></h2></a>
			</div>
			<div class="indentedSubCategories row row_of_2">
				<div class="col">
					<div>
						<a class="btn inv_btn no_border">Classic</a>
					</div>
					<div>
						<a class="btn inv_btn no_border">Exotic</a>
					</div>
					<div>
						<a class="btn inv_btn no_border">Jetts</a>
					</div>
					<div>
						<a class="btn inv_btn no_border">Donnas</a>
					</div>
				</div>
				<div class="col">
					<div>
						<a class="btn inv_btn no_border">Clearance</a>
					</div>
					<div>
						<a class="btn inv_btn no_border">Gold &#x26; Silver</a>
					</div>
					<div>
						<a class="btn inv_btn no_border">Snakeskin</a>
					</div>
					<div>
						<a class="btn inv_btn no_border">Shop by Size</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col categoryFeaturedVids">
			<div class="productVideoSlider">
				<div>
					<a>
						<div class="image_mask">
							<video autoplay="true" loop="true" muted="" poster="<?= FRONT_ASSETS ?>img/video_poster.jpg)" style="height:100%;width:auto;" class="categoryVideo">
    							<source src="<?= FRONT_ASSETS ?>video/flower_handler.mp4" type="video/webm">
    							<!--NEED TO ADD WEBM VIDEO SOURCE TOO-->
							</video>
						</div>
					</a>
					<div class='quickBuyBox'>
						<h2 class="as_l">Floral Handler<span>$480</span></h2>
						<div class="row optionsModule radioOptionsModule">
							<label class="col"><h4 class="as_l">Size:</h4></label>
							<div class="optionBox col optionBoxRadio">
								<p class="as_l dg">6</p>
								<input type="checkbox">
							</div>
							<div class="optionBox col optionBoxRadio">
								<p class="as_l dg">7</p>
								<input type="checkbox">
							</div>
							<div class="optionBox col optionBoxRadio">
								<p class="as_l dg">8</p>
								<input type="checkbox">
							</div>
							<div class="optionBox col optionBoxRadio">
								<p class="as_l dg">9</p>
								<input type="checkbox">
							</div>
							<div class="optionBox col optionBoxRadio">
								<p class="as_l dg">10</p>
								<input type="checkbox">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<? require_once('templates/watertour/filterBar.php'); ?>
	<div class="noGutter_content_width product_inventory_gridui">
		<div class="category">
			<div class="cushion_h3">
				<h3 class="cal_r">Jetts</h3>
			</div>
			<div class="row row_of_4 product_grid_ui">
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_1.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price"><span class="full_price"><span class="currency">$</span>388.00</span><span class="markdown">-50%</span><span class="value">$199.00</span></h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_2.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_3.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_4.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
			</div>
			<div>
				<a class="inv_btn dg">
					<p>View More</p>
				</a>
			</div>
		</div>
		<div class="category">
			<div class="cushion_h3">
				<h3 class="cal_r">Handlers</h3>
			</div>
			<div class="row row_of_4 product_grid_ui">
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_5.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_6.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_7.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">

				</div>
			</div>
			<div>
				<a class="inv_btn dg">
					<p>View More</p>
				</a>
			</div>
		</div>
	</div>
	<div class="noGutter_content_width categoryMarketingHero">
		<div class="mediaWrapper">
			<div class="media" style="background-image:url(<?=FRONT_IMG?>factory_hero.jpg)"></div>
		</div>
		<div class="abs_trans_center">
			<h4 class="as_l">Women's Shoes</h4>
			<p class="lyon_r">Our full collection of women's boots includes styles for both casual and formal occasions. Elegantleather evening sandals, pumps and wedges feature in a seasonal colour palette with cut-out ankle boots from the runway. Ankle and knee-high boots feature alongside practical rubber weatherboots with check detailing and trainers.</p>
		</div>
	</div>
	<div class="noGutter_content_width product_inventory_gridui">
		<div class="category">
			<div class="cushion_h3">
				<h3 class="cal_r">Jetts</h3>
			</div>
			<div class="row row_of_4 product_grid_ui">
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_1.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_2.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_3.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_4.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
			</div>
			<div>
				<a class="inv_btn dg">
					<p>View More</p>
				</a>
			</div>
		</div>
		<div class="category">
			<div class="cushion_h3">
				<h3 class="cal_r">Handlers</h3>
			</div>
			<div class="row row_of_4 product_grid_ui">
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_5.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_6.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">
					<a>
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=FRONT_IMG?>item_7.jpg)"></div>
						</div>
						<div class="dataWrapper">
							<h4 class="product_name">Suede Calf Jett</h4>
							<h4 class="product_price">$388</h4>
						</div>
					</a>
				</div>
				<div class="col productGridItem">

				</div>
			</div>
			<div>
				<a class="inv_btn dg">
					<p>View More</p>
				</a>
			</div>
		</div>
	</div>	
</div>
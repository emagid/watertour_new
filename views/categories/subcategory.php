<div class="pageWrapper subcategoryPageWrapper pageLeftNavSidebar">
	<div class="subCategoryHero">
		<a class="show_mdl" data-mdl_name="newItems">
			<div class="image_mask">
				<img class="media" src="<?=FRONT_IMG?>home_hero.jpg">
			</div>
			<div class="hero_description_wrapper">
				<div class="abs_vert_center">
					<h1 class="as_l">New <span class="subcategory_name">Handlers</span></h1>
					<div class="btn btn_white btn_large ghost_btn">Shop Now</div>
					<style>
						.subCategoryHero:hover div.btn{
							background:url(<?=FRONT_IMG?>home_hero.jpg) -212px -316px/639% no-repeat;
						    -webkit-background-clip: text;
						    -webkit-text-fill-color: transparent;
						    background-color: #FFFFFF!important;							
						}
					

					</style>
				</div>
			</div>
		</a>
	</div>
	<? require_once('templates/watertour/filterBar.php'); ?>
	<div class="noGutter_content_width product_inventory_gridui">
		<div class="row row_of_4 product_grid_ui">
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_1.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price"><span class="full_price"><span class="currency">$</span>388.00</span><span class="markdown">-50%</span><span class="value">$199.00</span></h4>
					</div>
				</a>
			</div>
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_2.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price">$388</h4>
					</div>
				</a>
			</div>
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_3.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price">$388</h4>
					</div>
				</a>
			</div>
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_4.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price">$388</h4>
					</div>
				</a>
			</div>
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_5.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price">$388</h4>
					</div>
				</a>
			</div>
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_6.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price">$388</h4>
					</div>
				</a>
			</div>
			<div class="col productGridItem">
				<a>
					<div class="mediaWrapper">
						<div class="media" style="background-image:url(<?=FRONT_IMG?>item_7.jpg)"></div>
					</div>
					<div class="dataWrapper">
						<h4 class="product_name">Suede Calf Jett</h4>
						<h4 class="product_price">$388</h4>
					</div>
				</a>
			</div>			
		</div>	
	</div>
</div>


 <? if($model->category->slug == 'men'){?>
    <div class="pageWrapper comingSoonPageWrapper categoryIndexPageWrapper">
        <div class="abs_trans_center content_width_1000">
            <h1 class="as_m"><?=$model->category->name?>'s Collection</h2>
            <h2 class="as_m">Coming Soon</h1>
        </div>
    </div>
<?}else{?>
	<div class="pageWrapper categoryPageWrapper <?=$model->category->slug;?>">
		<?
		$prod = $model->category->getFeaturedProduct();
		if(!$prod->video_link){		
		$banner_text_color = $model->category != 'all' && $model->category->banner_text_color ? 'black': 'white';
		}
		?>
		<div class="categoryIntro row <?=$banner_text_color?>">
			<div class="absImg mediaWrapper">
				<div class="media" style="background-image:url('<?=SITE_URL?>content/uploads/categories/<?=$model->category->banner;?>');"></div>
			</div>
			<div class="col categoryData">
				<div class="header">
					<h1 class="as_l"><?=$model->category->name;?></h1>
					<a class="btn_oreo btn btn_transparent" href="<?=SITE_URL?>collections/<?=$model->category->slug;?>/all">View All</a>
				</div>
				<?if(isset($model->featured_category) && $model->featured_category){
					$itemCount = count($model->featured_category->getProducts());?>
					<div class="subHeader">
						<a href="/collections/<?=$model->category->slug?>/<?=$model->featured_category->slug?>"><h2 class="as_l"><?=$model->featured_category->name?></h2><h2 class="count_ui"><?=$itemCount?><span><?=$itemCount > 1 ? 'items': 'item'?></span></h2></a>
					</div>
				<?}?>
				<div class="indentedSubCategories row row_of_2">
					<?php $sql = "select * from subnav where active = 1 and
									category_id = {$model->category->id} and
									sub_category_id in
									(select id from category where (featured_category != 1 or featured_category is NULL))
									 order by column_num ASC, display_order ASC;";
					$subNav = \Model\Subnav::getList(['sql'=>"$sql"]); $list = [];
					$subnavSql = "select * from subnav where active = 1 and
									category_id = {$model->category->id} and
									sub_category_id in
									(select id from category where slug not like '%shop-by%')
									order by column_num, display_order";
					$fullSubNav = \Model\Subnav::getList(['sql'=>$subnavSql]); $subCat = [];
					foreach($fullSubNav as $fsn){$subCat[] = \Model\Category::getItem($fsn->sub_category_id);}
	//				$subCat = \Model\Category::getList(['where'=>"parent_category = {$model->category->id}"]);
					?>

					<?php
					foreach($subNav as $sub){ $sub = \Model\Category::getItem($sub->sub_category_id); $list[] = $sub;} $newList = array_chunk($list, floor(count($list)/2));
					?>
					<?php foreach($newList as $list){?>
					<div class="col">
						<?php foreach($list as $sub){ ?>
						<div>
							<a class="btn inv_btn no_border" href="/collections/<?=$model->category->slug?>/<?=$sub->slug?>"><?=$sub->name?></a>
						</div>
						<?}?>
					</div>
					<?}?>

				</div>
			</div>
			<? $prod = $model->category->getFeaturedProduct();
			if($prod->video_link){?>		
			<div class="col categoryFeaturedVids">
			<?}else{?>
			<div class="col categoryFeaturedVids imageContained">
			<?}?>
				<div class="productVideoSlider">	
					<div>
						<? if($prod = $model->category->getFeaturedProduct()) { ?>
							<a href="<?=SITE_URL.'products/'.$prod->slug?>">
								<? if($prod->video_link){?>
								<div class="image_mask actualVideo">
								
									<video autoplay="true" loop="true" muted=""
										   poster="<?= UPLOAD_URL . 'products/' . $prod->featuredImage() ?>"
										   style="height:100%;width:auto;" class="categoryVideo">
										<source src="<?= UPLOAD_URL . 'products/' . $prod->video_link ?>"
												type="video/webm">
										<!--NEED TO ADD WEBM VIDEO SOURCE TOO-->
									</video>
								<?} else{?>
								<div class="image_mask actualImage">
									<div class="media fullWidthCatIndexBanner" style="background-image:url('<?=UPLOAD_URL.'categories/'.$model->category->banner?>')"></div>
								<?}?>
								</div>
								<div class="btn btn_white abs_trans_center">
									<p><?=$prod->name?></p>
								</div>
							</a>
						<? } ?>
						<!-- 					<div class='quickBuyBox'>
	                                            <h2 class="as_l">Floral Handler<span>$480</span></h2>
	                                            <div class="row optionsModule radioOptionsModule">
	                                                <label class="col"><h4 class="as_l">Size:</h4></label>
	                                                <div class="optionBox col optionBoxRadio">
	                                                    <p class="as_l dg">6</p>
	                                                    <input type="checkbox">
	                                                </div>
	                                                <div class="optionBox col optionBoxRadio">
	                                                    <p class="as_l dg">7</p>
	                                                    <input type="checkbox">
	                                                </div>
	                                                <div class="optionBox col optionBoxRadio">
	                                                    <p class="as_l dg">8</p>
	                                                    <input type="checkbox">
	                                                </div>
	                                                <div class="optionBox col optionBoxRadio">
	                                                    <p class="as_l dg">9</p>
	                                                    <input type="checkbox">
	                                                </div>
	                                                <div class="optionBox col optionBoxRadio">
	                                                    <p class="as_l dg">10</p>
	                                                    <input type="checkbox">
	                                                </div>
	                                            </div>
	                                        </div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="noGutter_content_width product_inventory_gridui">
			<? $info_banners = json_decode($model->category->info_banners,true);
			$info_count = count($info_banners);
			$maximum = count($subCat);
			$sCatChunk = array_chunk($subCat,$maximum/($info_count+1));
			$counter = 0;
			foreach ($sCatChunk as $subCat) {
				foreach ($subCat as $subC) { ?>
					<div class="category">
						<div class="cushion_h3">
							<a href="<?= $model->category->slug . '/' . $subC->slug ?>"><h3 class="cal_r"><?= $subC->name ?></h3></a>
						</div>
						<div class="row row_of_4 product_grid_ui">
							<?
							$subItem = [];
							if ($subC->product_preview_ids) {
								foreach (json_decode($subC->product_preview_ids, true) as $item) {
									$subItem[] = \Model\Product::getItem($item);
								}
							}
							//				$subItem = $subCat[$i]->getProducts();
							foreach ($subItem as $subI) { ?>
								<div class="col productGridItem">
									<div id="showQuickView" class="show_mdl" data-id="<?=$subI->id?>" data-mdl_name="quickView">
					                    <h5 class="as_m"><span><i></i><i></i></span>Quick View</h5>
					                </div>
									<? $prodAttr = \Model\Product_Attributes::getColorList($subI->id,0);
									if(count($prodAttr)>=5){?>
					                <div class="productGridItemSwatches productGridItemSwatchesSlider">
					                <?}else{?>
					                <div class="productGridItemSwatches">
					                <?}?>
					                    <?foreach($prodAttr as $pa){?>
					                        <div class="productGridItemSwatchWrapper">
					                            <img src="<?=$pa->swatch()?>" class="productGridItemSwatch" data-color_id="<?=$pa->id?>" data-pro_id="<?=$subI->id?>"/>
					                        </div>
					                    <?}?>
					                </div>				                
									<a href="/products/<?= $subI->slug ?>">
										<div class="saleInfoWrapper">
											<?=$subI->isDiscounted() ? '<small class="saleIndicator">On Sale</small>': ''?>
											<?if(\Model\Event::eventSale($subI->id,$subI->default_color_id)){
												$eventAttr = \Model\Product_Attributes::getEventById($subI->id,$subI->default_color_id);
												$event = \Model\Event::getItem($eventAttr->value)?>
												<div class="saleEventIcon icon" style="background-image:url('<?=$event->getIcon()?>');"></div>
											<?}?>
										</div>
										<div class="mediaWrapper">
											<div class="media"
												 style="background-image:url(<?= UPLOAD_URL . 'products/' . $subI->featuredImage($subC->id) ?>)"></div>
										</div>
										<div class="dataWrapper">
											<h4 class="product_name"><?= $subI->name ?></h4>
											<? if ($subI->msrp > 0.0) {?>
												<?$percent = round(($subI->basePrice() - $subI->msrp()) * 100 / $subI->basePrice()) ?>
												<h4 class="product_price">
													<span class="full_price">
														<span class="currency">$</span><?= number_format($subI->basePrice(),2) ?>
													</span>
													<!-- <span class="markdown">-<?= $percent ?>%</span> -->
													<span class="value">$<?= number_format($subI->msrp(),2) ?></span>
												</h4>
											<? } else if($subI->isDiscounted()){?>
												<?$percent = round(($subI->basePrice() - $subI->price()) * 100 / $subI->basePrice()) ?>
												<h4 class="product_price">
													<span class="full_price">
														<span class="currency">$</span><?= number_format($subI->basePrice(),2) ?>
													</span>
													<!-- <span class="markdown">-<?= $percent ?>%</span> -->
													<span class="value">$<?= number_format($subI->price(),2) ?></span>
												</h4>
											<? } else if(($subI->price)>$subI->basePrice()){?>
												<?$percent = round(($subI->price - $subI->price()) * 100 / $subI->price) ?>
												<h4 class="product_price">
													<span class="full_price">
														<span class="currency">$</span><?= number_format($subI->price,2) ?>
													</span>
													<!-- <span class="markdown">-<?= $percent ?>%</span> -->
													<span class="value">$<?= number_format($subI->price(),2) ?></span>
												</h4>
											<?} else { ?>
												<h4 class="product_price">$<?= number_format($subI->price(),2) ?></h4>
											<? } ?>
										</div>
									</a>
								</div>
							<? } ?>
						</div>
						<div>
							<a class="inv_btn dg" href="<?= $model->category->slug . '/' . $subC->slug ?>">
								<p>View More</p>
							</a>
						</div>
					</div>
				<? } ?>
				<?if(isset($info_banners[$counter])){?>
					<div class="noGutter_content_width categoryMarketingHero">
						<a href="<?= $info_banners[$counter]['info-link'] ?>">
						<div class="mediaWrapper">
								<div class="media" style="background-image:url(<?= UPLOAD_URL . 'categories/' . $info_banners[$counter]['info-banner'] ?>)"></div>
						</div>
						<? if($info_banners[$counter]['info-desc']){?>
						<div class="abs_trans_center" style="background-color:#<?= $info_banners[$counter]['info-color']?>;">
							<h4 class="as_l" style="color:#<?= $info_banners[$counter]['info-fontcolor']?>;"><?= $info_banners[$counter]['info-header'] ?></h4>

							<p class="lyon_r" style="color:#<?= $info_banners[$counter]['info-fontcolor']?>;"><?= $info_banners[$counter]['info-desc'] ?></p>
						</div>
						<?}else{?>
							<h4 class="as_l absCenterNoBG" style="color:#<?= $info_banners[$counter]['info-color']?>;"><?= $info_banners[$counter]['info-header'] ?></h4>
						<?}?>
						</a>
					</div>
				<?}?>
			<? $counter++;} ?>
		</div>
		
	</div>
	<script>
		$(document).ready(function(){
			$(document).on('mouseover', '.productGridItemSwatch', function (e) {
				var product_id = $(this).attr('data-pro_id');
				var color_id = $(this).attr('data-color_id');
				var self = $(this);
				$.post('/category/buildVariantImage', {product_id: product_id, color_id: color_id}, function (data) {
					if (data.status == 'success') {
						self.closest('.productGridItem').find('.media').css('background-image', 'url(' + data.image + ')');
						if(data.isDiscounted){
							var html = '<span class="full_price"> <span class="currency">$</span>'+data.basePrice+'</span><span class="value">$'+data.price+'</span>';
							if(self.closest('.productGridItem').find('small').length){
								self.closest('.productGridItem').find('small').show();
							} else {
								self.closest('.productGridItem').find('.product_name').after('<small>On Sale</small>');
							}
							if(self.closest('.productGridItem').find('.product_price').length){
								self.closest('.productGridItem').find('.product_price').show();
							} else {
								self.closest('.productGridItem').find('small').after('<img class="event-icon" style="width: 30px;" src="'+data.event+'">');
							}
							self.closest('.productGridItem').find('.event-icon').show();
							self.closest('.productGridItem').find('.product_price').html(html);
						}else{
							self.closest('.productGridItem').find('small').hide();
							self.closest('.productGridItem').find('.event-icon').hide();
							self.closest('.productGridItem').find('.product_price').html('$' + data.price);
						}
					}
				});
			});
		})
	</script>
<?}?>
 <? if($model->parent == 'men'){?>
    <div class="pageWrapper comingSoonPageWrapper pageLeftNavSidebar">
        <div class="abs_trans_center content_width_1000">
            <h1 class="as_m"><?=$model->parent?>'s Collection</h2>
            <h2 class="as_m">Coming Soon</h1>
            <div class="comingSoonEmailCapture">
                <h1 class="bigger cortado">Let me know when it's Live</h1>
                <h1 class="as_m">Get on our mailing list now and you'll receive 10% off any order of your choosing. We'll also let you know when the Bike Tour <?=$model->parent?>'s collection is up for sale.</h1>
                <div class="row row_360">
                    <div class="col">
                        <form class="mailingListEmailCapture" method="post" action="<?=SITE_URL?>newsletter/add">
                            <div class="labelInputWrapper">
                                <input type="email" id="mailingListEmail" name="email" placeholder="Email address">
                                <input type="submit" value="Notify Me" class="btn btn_pink">
                            </div>
                        </form>
                    </div>
                </div>

                <script>
                    var frm = $('.mailingListEmailCapture');
                    frm.submit(function (e) {
                        $.post(frm.attr('action'),frm.serialize(),function(data){
                            if(data.status == 'success'){
                                alert(data.message);
                            } else{
                                alert(data.message);
                            }
                        })
                        e.preventDefault();
                    })
                </script>            
            </div>
        </div>
    </div>
<?}else{?>
    <div class="pageWrapper subcategoryPageWrapper pageLeftNavSidebar">

        <?$banner_text_color = $model->category != 'all' && $model->category->banner_text_color ? 'black': 'white'?>
        <div class="subCategoryHero <?=$banner_text_color?>">

            <? if(count($model->new_arrivals) > 0){?>
            <a class="show_mdl" data-mdl_name="newItems">
            <?}else{?>
            <div>
            <?}?>
                <div class="image_mask">
                    <?if($model->category == 'all'){?>
    <!--                    <img class="media" src="--><?//=FRONT_IMG.'banner3.jpg'?><!--">-->
                        <img class="media" src="<?=UPLOAD_URL.'categories/'.$model->mCategory->banner?>">
                    <?} else {?>
                        <img class="media" src="<?=UPLOAD_URL.'categories/'.$model->category->banner?>">
                    <?}?>
                </div>
                
                <div class="hero_description_wrapper <?=$banner_text_color?>">
                    <div class="abs_vert_center">
                        <? if(count($model->new_arrivals) > 0){?>
                        <h1 class="as_l"><span class="subcategory_name cortado"><?=$model->category == 'all'?'':$model->category->name?></span></h1>
                        <div class="btn btn_white btn_large ghost_btn">Shop New Arrivals</div>
                        <?}else{?>
                        <h1 class="as_l"><span class="subcategory_name cortado"><?=$model->category == 'all'?$model->category:$model->category->name?></span></h1>
                        <?}?>
                        <style>
                            <?$backgroundUrl = $model->category == 'all'? FRONT_IMG.'banner3.jpg': UPLOAD_URL.'categories/'.$model->category->banner?>
                            .subCategoryHero:hover div.btn{
                                background:url(<?=$backgroundUrl?>) -212px -316px/639% no-repeat;
                                -webkit-background-clip: text;
                                -webkit-text-fill-color: transparent;
                                background-color: #FFFFFF!important;
                            }


                        </style>
                    </div>
                </div>
            <? if(count($model->new_arrivals) > 0){?>
            </a>
            <?}else{?>
            </div>
            <?}?>
        </div>
        <? require_once('templates/watertour/filterBar.php'); ?>
        <div class="noGutter_content_width product_inventory_gridui mainPageInventoryUI">
            <div class="row row_of_4 product_grid_ui">
                <?foreach($model->products as $product){
                    $prodAttr = \Model\Product_Attributes::getColorList($product->id,0)?>
                <div class="col productGridItem">
                    <div id="showQuickView" class="show_mdl" data-id="<?=$product->id?>" data-mdl_name="quickView">
                        <h5 class="as_m"><span><i></i><i></i></span>Quick View</h5>
                    </div>

                    <? if(count($prodAttr)>=5){?>
                    <div class="productGridItemSwatches productGridItemSwatchesSlider">
                    <?}else{?>
                    <div class="productGridItemSwatches">
                    <?}?>
                        <?foreach($prodAttr as $pa){?>
                            <!--<div class="media colorSwatch" style="background-image: url(<?/*=$pa->swatch()*/?>)"></div>-->
                            <div class="productGridItemSwatchWrapper">
                                <img src="<?=$pa->swatch()?>" class="productGridItemSwatch" data-color_id="<?=$pa->id?>" data-pro_id="<?=$product->id?>"/>
                            </div>
                        <?}?>
                    </div>
                    <a href="<?=SITE_URL."products/$product->slug"?>">
                        <div class="saleInfoWrapper">
                            <?=($product->isDiscounted()||($product->price)>$product->basePrice()) ? '<small class="saleIndicator">On Sale</small>': ''?>
                            <?if(\Model\Event::eventSale($product->id,$product->default_color_id)){
                                $eventAttr = \Model\Product_Attributes::getEventById($product->id,$product->default_color_id);
                                $event = \Model\Event::getItem($eventAttr->value)?>
                                <div class="saleEventIcon event-icon icon" style="background-image:url('<?=$event->getIcon()?>')"></div>
                            <?}?>
                        </div>
                        <div class="mediaWrapper">
                            <?$featuredImageParam = $model->category == 'all' ? null: $model->category->id?>
                            <div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$product->featuredImage($featuredImageParam)?>)"></div>
                        </div>
                        <div class="dataWrapper">
                            <h4 class="product_name"><?=$product->name?></h4>

                            <?if($product->msrp > 0.0){
                                $percent = round(($product->basePrice() - $product->msrp()) * 100 / $product->basePrice())?>
                                <h4 class="product_price">
                                    <span class="full_price">
                                        <span class="currency">$</span><?=number_format($product->basePrice(),2)?>
                                    </span>
                                    <span class="markdown">-<?=$percent?>%</span>
                                    <span class="value">$<?=number_format($product->msrp(),2)?></span>
                                </h4>
                            <? } else if($product->isDiscounted()){?>
                                <?$percent = round(($product->basePrice() - $product->price()) * 100 / $product->basePrice()) ?>
                                <h4 class="product_price">
                                    <span class="full_price">
                                        <span class="currency">$</span><?= number_format($product->basePrice(),2) ?>
                                    </span>
                                    <!-- <span class="markdown">-<?= $percent ?>%</span> -->
                                    <span class="value">$<?= number_format($product->price(),2) ?></span>
                                </h4>
                            <? } else if(($product->price)>$product->basePrice()){?>
                                <?$percent = round(($product->price - $product->price()) * 100 / $product->price) ?>
                                <h4 class="product_price">
                                    <span class="full_price">
                                        <span class="currency">$</span><?= number_format($product->price,2) ?>
                                    </span>
                                    <!-- <span class="markdown">-<?= $percent ?>%</span> -->
                                    <span class="value">$<?= number_format($product->price(),2) ?></span>
                                </h4>
                            <?} else {?>
                                <h4 class="product_price"><span class="value">$<?=number_format($product->price(),2)?></span></h4>
                            <?}?>
                        </div>
                    </a>
                </div>
                <?}?>
            </div>

            <div id="loading-spinner">
                <div class="hanger"></div>
                <div class="discoball">
                    <img src="<?=FRONT_IMG?>disco_ball.png" alt="">
                </div>
            </div>
        </div>
        
    </div>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>subcategory.css">
    <script>
        $(document).ready(function(){
            /*Let event handler attach before triggering click*/
            <?if(isset($model->filterActive) && $model->filterActive){?>
                $('div[data-filter_optiontype=<?=$model->filterActive?>]').trigger('click');
            <?}?>
            $(document).on('mouseover', '.productGridItemSwatch', function (e) {
                var product_id = $(this).attr('data-pro_id');
                var color_id = $(this).attr('data-color_id');
                var self = $(this);
                $.post('/category/buildVariantImage', {product_id: product_id, color_id: color_id}, function (data) {
                    if (data.status == 'success') {
                        self.closest('.productGridItem').find('.media').css('background-image', 'url(' + data.image + ')');
                        if(data.isDiscounted){
                            var html = '<span class="full_price"> <span class="currency">$</span>'+data.basePrice+'</span><span class="value">$'+data.price+'</span>';
                            if(self.closest('.productGridItem').find('small').length){
                                self.closest('.productGridItem').find('small').show();
                            } else {
                                self.closest('.productGridItem').find('.saleInfoWrapper').append('<small class="saleIndicator">On Sale</small>');
                            }
                            if(self.closest('.productGridItem').find('.product_price').length){
                                self.closest('.productGridItem').find('.product_price').show();
                            } else {
                                self.closest('.productGridItem').find('small').after('<img class="event-icon" style="width: 30px;" src="'+data.event+'">');
                            }
                            self.closest('.productGridItem').find('.event-icon').css('background-image','url('+data.event+')').show();
                            self.closest('.productGridItem').find('.product_price').html(html);
                        }else{
                            self.closest('.productGridItem').find('small').hide();
                            self.closest('.productGridItem').find('.event-icon').hide();
                            self.closest('.productGridItem').find('.product_price').html('$' + data.price);
                        }
                    }
                });
            });
            var limit = <?=$model->limit?>;
            var offset = <?=$model->offset + $model->limit?>;
            var totalProductCount = <?=$model->productsCount?>;


            var offsetScroll = true;
            $(window).scroll(function() {


                if (($(window).scrollTop() >= ($(document).height() - $(window).height() - 300)) && (offsetScroll == true)) {
                    offsetScroll = false;
                    console.log('activated1');
                    if (offset < totalProductCount) {
                        console.log(offsetScroll);

                        var mCategory = '<?=$model->mCategory ? $model->mCategory->id : ''?>';
                        var category_id = '<?=$model->category != 'all' ? $model->category->id : 'all'?>';
                        $.post('/category/buildCategoryItems<?=isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '' ? '?' . $_SERVER['QUERY_STRING'] : ''?>', {
                            mCategory: mCategory,
                            category_id: category_id,
                            limit: limit,
                            offset: offset
                        }, function (data) {
                            $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').addClass("loadingProcessActive");
                            $("#loading-spinner").addClass("loading_spinner_active").delay(800).queue(function (appendData) {
                                $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').removeClass("loadingProcessActive");
                                $("#loading-spinner").removeClass("loading_spinner_active");
                                $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').append(data.itemHtml);
                                offset += limit;
                                appendData();

                                if (data.status === "success") {
                                    offsetScroll = true;
                                    $('.productGridItemSwatchesSlider').flickity({
                                        // options
                                        cellAlign: 'left',
                                        adaptiveHeight: true,
                                        prevNextButtons: true,
                                        pageDots: false,
                                        groupCells: 3,
                                        contain: true,
                                        arrowShape: {
                                            x0: 10,
                                            x1: 65, y1: 50,
                                            x2: 70, y2: 45,
                                            x3: 20
                                        }
                                    });
                                }
                            });

                        });
                    }
                }
            })
        })
    </script>

<? } ?>

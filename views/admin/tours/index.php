<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
    $(document).ready(function () {

        $('body').on('change', '.change', function () {


            var br = $(this).val();

            $("#ex").attr("href", "/admin/tours/export?brand=" + br);

            window.location.href = "/admin/tours?status_show=" + br;

        });


        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            <?if (isset($_GET['status_show'])){?>
            <?$br = $_GET['status_show'];?>
            window.location.href = '/admin/tours?status_show=<?=$br?>&how_many=' + how_many;
            <?}else{?>
            window.location.href = '/admin/tours?how_many=' + how_many;
            <?}?>
        });

    });
</script>

<div class="row">
    <div class="col-md-16">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
<!--        <div class="box">-->
<!--            <form action="--><?//= ADMIN_URL ?><!--tours/multi_update/" method="get">-->
<!--                <button type="submit" class="btn btn-success">Multi edit</button>-->
<!--                <a href="--><?//= ADMIN_URL ?><!--tours/google_product_feed" class="btn btn-warning">Google Feed</a>-->
<!--            </form>-->
<!--        </div>-->
    </div>
    <div class="col-md-8">
        <div class="box">
            Show on page:
            <select class="how_many" name="how_many" style="cursor:pointer">

                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 10) {
                        echo "selected";
                    }
                } ?> selected value="10">10
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 50) {
                        echo "selected";
                    }
                } ?> value="50">50
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 100) {
                        echo "selected";
                    }
                } ?> value="100">100
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 500) {
                        echo "selected";
                    }
                } ?> value="500">500
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 1000) {
                        echo "selected";
                    }
                } ?> value="1000">1000
                </option>
            </select>
        </div>
    </div>
    <div class="col-md-16">
        <div class="box">
            Featured
            <select class="multiselect" multiple>
                <?foreach($model->tours as $tour){?>
                    <option value="<?=$tour->id?>" <?=$tour->featured ? 'selected': ''?>><?=$tour->name?></option>
                <?}?>
            </select>
        </div>
    </div>
</div>
<?php if (count($model->tours) > 0): ?>
    <div class="rox">
        <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                <tr>
                    <th width="5%">Image</th>
                    <th width="5%">Name</th>
<!--                    <th width="21%">Alias</th>-->
                    <th width="5%">Valid Range</th>
                    <th width="5%">MSRP | Price | Walkin</th>
                    <th width="5%">Availability</th>
                    <th width="1%" class="text-center">Edit</th>
                    <th width="1%" class="text-center">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;
                foreach ($model->tours as $obj) { ?>
                    <tr class="originalTours">

                        <td>
                            <?$img_path = UPLOAD_URL.'tours/'.$obj->featuredImage()?>
                            <a href="<?php echo ADMIN_URL; ?>tours/update/<?php echo $obj->id; ?>"><img
                                    src="<?php echo $img_path; ?>" width="50"/></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>tours/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>tours/update/<?php echo $obj->id; ?>"><?php echo date('M d, Y',strtotime($obj->valid_start)).' - '.date('M d, Y',strtotime($obj->valid_end)); ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>tours/update/<?php echo $obj->id; ?>">$<?= number_format($obj->msrp, 2) .' | $'.number_format($obj->price, 2).' | $'.number_format($obj->walkin_price, 2)?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>tours/update/<?php echo $obj->id; ?>"><?= $obj->getAvailability() ?></a>
                        </td>
                        <td class="text-center">
                            <a class="btn-actions" href="
                                <?php echo ADMIN_URL; ?>tours/update/<?php echo $obj->id; ?>">
                                <i class="icon-pencil"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn-actions" href="
                                <?php echo ADMIN_URL; ?>tours/delete/<?php echo $obj->id; ?>?token_id=
                                <?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                <i class="icon-cancel-circled"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
                <div class='paginationContent'></div>
            </div>
        </div>
    </div>
<?php endif; ?>

</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '/admin/tours<?if (isset($_GET['status_show'])){echo "status_show=";echo $_GET['status_show'] ;echo"&";} if (isset($_GET['how_many'])){echo "how_many=";echo $_GET['how_many'] ;echo"&";}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>
<script type="text/javascript">
    $(function () {
        $("select.multiselect").each(function (i, e) {
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%',
                onChange:function(option,checked){
                    var selectedOptions = $('.multiselect option:selected');

//                    if (selectedOptions.length >= 2) {
                        // Disable all other checkboxes.
//                        var selectedOptions = $('.multiselect option').filter(function() {
//                            return $(this).is(':selected');
//                        });
                        var arr = selectedOptions.map(function(i,e){
                            return e.value;
                        });
                        $.post('/admin/tours/updateFeatured',{featured: JSON.stringify(arr.get())});
//                        var nonSelectedOptions = $('.multiselect option').filter(function() {
//                            return !$(this).is(':selected');
//                        });
//
//                        nonSelectedOptions.each(function() {
//                            var input = $('input[value="' + $(this).val() + '"]');
//                            input.prop('disabled', true);
//                            input.parent('li').addClass('disabled');
//                        });
                    }
//                    else {
//                        // Enable all checkboxes.
//                        $('.multiselect option').each(function() {
//                            var input = $('input[value="' + $(this).val() + '"]');
//                            input.prop('disabled', false);
//                            input.parent('li').addClass('disabled');
//                        });
//                    }
//                },
//                onDropdownShow:function(option,checked){
//                    var selectedOptions = $('.multiselect option:selected');
//
//                    if (selectedOptions.length >= 2) {
//                        // Disable all other checkboxes.
//                        var nonSelectedOptions = $('.multiselect option').filter(function() {
//                            return !$(this).is(':selected');
//                        });
//
//                        nonSelectedOptions.each(function() {
//                            var input = $('input[value="' + $(this).val() + '"]');
//                            input.prop('disabled', true);
//                            input.parent('li').addClass('disabled');
//                        });
//                    }
//                    else {
//                        // Enable all checkboxes.
//                        $('.multiselect option').each(function() {
//                            var input = $('input[value="' + $(this).val() + '"]');
//                            input.prop('disabled', false);
//                            input.parent('li').addClass('disabled');
//                        });
//                    }
//                }
            });
        });
        $('input[name="edit-all"]').change(function () {
            if ($(this).prop('checked')) {
                $('#data-list tr').each(function () {
                    if ($(this).css('display') == 'table-row') {
                        $('input[type="checkbox"]', $(this)).prop('checked', 'checked');
                    }
                })
            } else {
                $('#data-list tr').each(function () {
                    $('input[type="checkbox"]', $(this)).prop('checked', false);
                })
            }
        })

        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>tours/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalTours').remove();
                    $('.paginationContent').hide();
                    $('.originalTours').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<input type="checkbox" style="cursor:pointer;" name="edit' + list[key].id + '" value="' + list[key].id + '" />'));
                        if (list[key].featured_image == '') {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=ADMIN_IMG?>watertour_shoe.png");/*TODO st-dev default product image*/
                        } else {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=UPLOAD_URL . 'tours/'?>" + list[key].featured_image);
                        }
                        $('<td />').appendTo(tr).html(img);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>tours/update/' + list[key].id).html(list[key].name));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//tours/update/' + list[key].id).html(list[key].alias));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>tours/update/' + list[key].id).html(list[key].msrp));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>tours/update/' + list[key].id).html(list[key].price));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>tours/update/' + list[key].id).html(list[key].availability));

//                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//tours/update/' + list[key].id);
//                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
//                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//tours/delete/' + list[key].id);
//                        deleteLink.click(function () {
//                            return confirm('Are You Sure?');
//                        });
//                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalTours').remove();
                $('.paginationContent').show();
                $('.originalTours').show();
            }
        });
    })
</script>

































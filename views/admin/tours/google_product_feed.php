<div class="row">
    <div class="col-md-24">
		<div class="box">
			<div class="input-group">
			    <form action="<?= $this->emagid->uri; ?>" method="post" >
					<input type="submit" class="btn btn-warning" value="Generate Feed" /> 
				</form>
			</div>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-24">
		<div class="box">
			<?php if(isset($model->generated) && $model->generated){ ?>
				<p>Feed has been generated.</p>
				<form method="post" action="<?=ADMIN_URL?>products/download/google_product_feed.txt">
					<button class="btn btn-success" type="submit">Download</button>
				</form>
			<?php } else { ?>
				<span style="color:red;">Generating the feed may take several minutes.<br />Please be patient while it processes.</span>
			<?php } ?>
		</div>
    </div>
</div>

<? footer() ?>
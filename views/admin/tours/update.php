<?php
$duration_hours = [];
for($i = 0; $i < 21; $i++){
    $duration_hours[] = $i;
}
$availabilityHours = [];
for($i = 1; $i < 25; $i+=.5){
    if($i < 12){
        $str = fmod($i,1)==0 ? $i.'AM': floor($i).':30AM';
        $availabilityHours["$i"] = $str;
    } else if($i == 12 || $i == 12.5){
        $str = fmod($i,1)==0 ? $i.'PM': floor($i).':30PM';
        $availabilityHours["$i"] = $str;
    } else if($i == 24 || $i == 24.5){
        $str = fmod($i,1)==0 ? ($i-12).'AM': floor($i-12).':30AM';
        $availabilityHours["$i"] = $str;
    } else {
        $str = fmod($i,1)==0 ? ($i-12).'PM': floor($i-12).':30PM';
        $availabilityHours["$i"] = $str;
    }
}
// for start_date and end_date
function dateFormatter($value,$format = 'm/d/Y h:iA'){
    return date($format,$value ? strtotime($value) : time());
}
?>
<style type="text/css">
    .legshot {
        border:1px black solid;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->tour->id; ?>"/>
    <input type="hidden" name="featured" value="<?php echo $model->tour->featured; ?>"/>
<!--    <input type="hidden" name="redirectTo" value="--><?//= $_SERVER['HTTP_REFERER'] ?><!--"/>-->
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <li role="presentation"><a href="#add_on-tab" aria-controls="add_on" role="tab" data-toggle="tab">Add Ons</a></li>
            <?php if ($model->tour->id > 0) { ?>
            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
            <?}?>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Name</label>

                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label>Alias</label>-->
<!--                                --><?php //echo $model->form->editorFor("alias"); ?>
<!--                            </div>-->

                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small style="color:rgba(239, 45, 45, 0.8)"><b>(ideal featured image dimensions are 800 x 800, zoomed out with white space surrounding tour)</b></small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->tour->featured_image != "" && file_exists(UPLOAD_PATH . 'tours' . DS . $model->tour->featured_image)) {
                                        $img_path = UPLOAD_URL . 'tours/' . $model->tour->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'tours/delete_image/' . $model->tour->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->tour->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Featured Video</label>
                                <p>
                                    <small>(ideal featured video is less then 10M)</small>
                                </p>
                                <p><input type="file" name="video_link" class='video'/></p>
                                <?php if($model->tour->video_link) { ?>
                                    <div style="display:inline-block">
                                        <video controls width="400" height="300">
                                            <source src="<?=$model->tour->videoLink()?>" type="video/mp4">
                                        </video>
                                        <div class='preview-container'><?=$model->tour->video_link?></div>
                                        <input type="hidden" name="video_link" class='video' value="<?=$model->tour->video_link?>"/>
                                    </div>
                                <? } ?>
                            </div>

    <!--                    <div class="form-group">-->
    <!--                        <a href="#" id="add-to-hottest">Add this tour to hottest deals</a>-->
    <!--                    </div>-->
                            <div class="form-group">
                                <label>Summary</label>
                                <?php echo $model->form->editorFor("summary"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description",['class'=>'ckeditor']); ?>
                            </div>
                            <div class="form-group">
                                <label>Details</label>
                                <?php echo $model->form->textAreaFor("details",['class'=>'ckeditor']); ?>
                            </div>
                            <div class="form-group">
                                <label>Active</label>
                                <select name="availability" class="form-control">
                                    <?$selected_stock = ($model->tour->availability)?:2; ?>
                                    <option value="2" <?php if ($selected_stock == 2) {echo "selected";} ?>>Active</option>
                                    <option value="0" <?php if ($selected_stock == 0) {echo "selected";} ?>>Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Validity</label>
                                <select name="validity" class="form-control">
                                    <option value="-1">Select</option>
                                    <?foreach(\Model\Tour::$validity as $key=>$item){?>
                                        <option value="<?=$key?>" <?=$key == $model->tour->validity ? 'selected': ''?>><?=$item?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Days</label>
                                <select name="days" class="form-control">
                                    <option value="1" <?=$model->tour->days == 1 ? 'selected': ''?>>1</option>
                                    <option value="2" <?=$model->tour->days == 2 ? 'selected': ''?>>2</option>
                                    <option value="3" <?=$model->tour->days == 3 ? 'selected': ''?>>3</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Duration - Hour</label>
                                        <?=$model->form->dropDownListFor('duration_hour',$duration_hours,'',['class'=>'form-control'])?>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Duration - Minute</label>
                                        <?=$model->form->dropDownListFor('duration_minute',['00','15','30','45'], '',['class'=>'form-control'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Validity Range</label>
                                <input name="validity_range" class="daterange" type="text" value="<?=$model->tour->id > 0 ? dateFormatter($model->tour->valid_start) .' - '.dateFormatter($model->tour->valid_end) : ''?>"/>
                            </div>
                            <div class="form-group">
                                <label>Neighborhood</label>
                                <?=$model->form->dropDownListFor('neighborhood',\Model\Tour::$neighborhood,'',['class'=>'form-control'])?>
                            </div>
                            <div class="form-group">
                                <label>Departure Point</label>
                                <?=$model->form->textBoxFor('departure_point')?>
                            </div>
                            <div class="form-group">
                                <label>Languages</label>
                                <?=$model->form->dropDownListFor('languages[]',array_map(function($item){return $item->name;},$model->languages),'',['class'=>'multiselect', 'multiple'=>'multiple'])?>
                            </div>
                            <div class="form-group">
                                <label>Route</label>
                                <?=$model->form->dropDownListFor('main_route_id',array_map(function($item){return $item->name;},$model->main_routes),'',['class'=>'multiselect'])?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>External Ids</h4>

                            <div class="form-group">
                                <label>TourCode <small>(Server Side)</small></label>
                                <?php echo $model->form->editorFor("tour_code"); ?>
                            </div>
                            <div class="form-group">
                                <label>PackageId <small>(Server Side)</small></label>
                                <?php echo $model->form->editorFor("external_package_id"); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Availability</h4>

                            <div class="form-group">
                                <label>Occurrences</label>
                                <?=$model->form->dropDownListFor('o', ['None','Daily','Weekly','Monthly'],'',['class'=>'multiselect'])?>
                            </div>
<!--                            <div class="form-group monthly" style="display: none;">-->
<!--                                <label>Month</label>-->
<!--                                --><?//=$model->form->dropDownListFor('m[]', [1,2,3,4,5,6,7,8,9,10,11,12],'',['class'=>'multiselect','id'=>'mon','multiple'=>'multiple', 'disabled'=>'disabled'])?>
<!--                            </div>-->
                            <div class="form-group weekly" style="display: none;">
                                <label>Week</label>
                                <?=$model->form->dropDownListFor('w[]', ['mon'=>'Monday','tue'=>'Tuesday','wed'=>'Wednesday','thu'=>'Thursday','fri'=>'Friday','sat'=>'Saturday','sun'=>'Sunday'],'',['class'=>'multiselect','id'=>'week','multiple'=>'multiple', 'disabled'=>'disabled'])?>
                            </div>
                            <div class="form-group daily" style="display: none;">
                                <label>Day</label>
                                <?=$model->form->dropDownListFor('d[]', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],'',['class'=>'multiselect','id'=>'daily','multiple'=>'multiple', 'disabled'=>'disabled'])?>
                            </div>
                            <div class="form-group hourly" style="display: none;">
                                <label>Hour</label>
                                <?=$model->form->dropDownListFor('h[]', $availabilityHours,'',['class'=>'multiselect','id'=>'hourly','multiple'=>'multiple', 'disabled'=>'disabled'])?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>

                            <div class="form-group">
                                <label>Discounted Price</label><p><small>0 for no discount</small></p>
                                <?php echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group">
                                <label>Sell Price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>
                            <div class="form-group">
                                <label>Walkin Price</label>
                                <?php echo $model->form->editorFor("walkin_price"); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Related</h4>

                            <div class="form-group">
                                <label>Related Tours</label>
                                <select name="related[]" class="multiselect" multiple>
                                    <?foreach ($model->tours as $tour){?>
                                        <option value="<?=$tour->id?>"><?=$tour->name?></option>
                                    <?}?>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php echo $model->form->editorFor("tags"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="add_on-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <?$addOns = $model->tour->add_ons ? json_decode($model->tour->add_ons,true) : []?>
                            <h4>Services</h4>

                            <div class="form-group">
                                <label>Rentals</label>
                                <select name="rentals[]" class="multiselect" multiple>
                                    <?foreach(\Model\Rental::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['rentals']) && in_array($item->id,$addOns['rentals']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tours</label>
                                <select name="tours[]" class="multiselect" multiple>
                                    <?foreach(\Model\Tour::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['tours']) && in_array($item->id,$addOns['tours']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Packages</label>
                                <select name="packages[]" class="multiselect" multiple>
                                    <?foreach(\Model\Package::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['packages']) && in_array($item->id,$addOns['packages']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Attractions</label>
                                <select name="attractions[]" class="multiselect" multiple>
                                    <?foreach(\Model\Attraction::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['attractions']) && in_array($item->id,$addOns['attractions']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Products</h4>

                            <div class="form-group">
                                <label>Equipment</label>
                                <select name="equipment[]" class="multiselect" multiple>
                                    <?foreach(\Model\Equipment::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['equipment']) && in_array($item->id,$addOns['equipment']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Products</label>
                                <select name="products[]" class="multiselect" multiple>
                                    <?foreach(\Model\Product::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['products']) && in_array($item->id,$addOns['products']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($model->tour->id > 0) { ?>
                <div role="tabpanel" class="tab-pane" id="images-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="dropzone" id="dropzoneForm"
                                     action="<?php echo ADMIN_URL . 'tours/upload_images/' . $model->tour->id; ?>">

                                </div>
                                <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                                <br/>
                                <p>
                                    <small style="color:#A81927"><b>(ideal tour angle image dimensions are 1024px wide x 683px height)</b></small>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <table id="image-container" class="table table-sortable-container">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>File Name</th>
                                        <th>Display Order</th>
<!--                                        <th>Leg Shot</th>-->
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($prImg = \Model\Tour_Image::getList(['where' => 'tour_id=' . $model->tour->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {?>
                                            <tr data-image_id="<?php echo $pimg->id; ?>">
                                                <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100" height="100"/></td>
                                                <td><?php echo $pimg->image; ?></td>
                                                <td class="display-order-td"><?=$pimg->display_order?></td>
<!--                                                <td class="leg_shot"><input id="leg_shot" name="leg_shot" type="radio" value="--><?//=$pimg->id?><!--" --><?//=$pimg->leg_shot == 'TRUE' ? 'checked': ''?><!--/></td>-->
                                                <td class="text-center">
                                                    <a class="btn-actions delete-tour-image"
                                                       href="<?php echo ADMIN_URL; ?>tours/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                        <i class="icon-cancel-circled"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <button type="submit" class="btn btn-save" style="display:none">Save</button>
    <div id="target" class="btn btn-save">Save</div>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url =<?php echo json_encode(ADMIN_URL.'tours/'); ?>;
    $(document).ready(function () {
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        $(function() {
            $('.daterange').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->tour->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",strtotime($model->tour->valid_start));?>",
                endDate: "<?php echo date("m/d/Y g:iA",strtotime($model->tour->valid_end));?>",
                <?php } ?>

            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('[name=o]').on('change',function(){
            var occurrence = $(this).val();
//            var monthly = $('.monthly');
            var weekly = $('.weekly');
            var daily = $('.daily');
            var hourly = $('.hourly');
//            var mon = $('#mon');
            var week = $('#week');
            var day = $('#daily');
            var hour = $('#hourly');
            switch(occurrence){
                case 'Daily':
//                    monthly.hide();
                    weekly.hide();
                    daily.hide();
                    hourly.show();
//                    mon.multiselect('disable');
                    week.multiselect('disable');
                    day.multiselect('disable');
                    hour.multiselect('enable');
                    break;
                case 'Weekly':
//                    monthly.hide();
                    weekly.show();
                    daily.hide();
                    hourly.show();
//                    mon.multiselect('disable');
                    week.multiselect('enable');
                    day.multiselect('disable');
                    hour.multiselect('enable');
                    break;
                case 'Monthly':
//                    monthly.hide();
                    weekly.hide();
                    daily.show();
                    hourly.show();
//                    mon.multiselect('disable');
                    week.multiselect('disable');
                    day.multiselect('enable');
                    hour.multiselect('enable');
                    break;
                default:
//                    monthly.show();
                    weekly.hide();
                    daily.hide();
                    hourly.hide();
//                    mon.multiselect('enable');
                    week.multiselect('disable');
                    day.multiselect('disable');
                    hour.multiselect('disable');
                    break;
            }
        });

        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });

        $("#target").click(function () {
//            var price = $("input[name='price']").val();
            var name = $("input[name='name']").val();
            var slug = $("input[name='slug']").val();
            var id = <?=$model->tour->id?>;
            var errors = new Array();
//            if ($.trim(price) > 0) {
//                $("input[name='price']").css({
//                    "border-color": ""
//                });
//            } else {
//                $("input[name='price']").css({
//                    "border-color": "red"
//                });
//                errors.push("Incorrect price");
//            }
            if ($.trim(name) == "") {
                $("input[name='name']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect name");
            } else {
                $("input[name='name']").css({
                    "border-color": ""
                });
            }
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/tours/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {

                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name']").on('keyup', function (e) {
            var val = $.trim($(this).val());
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select.multiselect").each(function (i, e) {
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        <?if($model->tour->available_range){
            foreach(json_decode($model->tour->available_range,true) as $key=>$items){
                if($key == 'o'){?>
                    $('select[name=<?=$key?>').val('<?=$items?>').trigger('change');
                <?} else {?>
                    $('select[name="<?=$key?>[]"').val(<?=json_encode($items)?>);
                <?}}}?>
        $('select[name="languages[]"').val(<?=$model->tour->languages?>);
        $('select[name="related[]"').val(<?=$model->tour->related?>);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-tour-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        var adjustment;
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden; width: 100px; height: 100px;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-tour-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $(".numeric").numericInput();
    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'tours/upload_images/'.$model->tour->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>

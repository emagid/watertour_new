<style>
    #map{
        height:500px;
        width:500px;
    }
    #floating-panel {
        position: absolute;
        top: 343px;
        left:15%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->route->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Street</label>
                    <?php echo $model->form->editorFor("street"); ?>
                </div>
                <div class="form-group">
                    <label>Featured Image</label>
                    <p><input type="file" name="featured_image" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->route->featured_image != "" && file_exists(UPLOAD_PATH . 'routes' . DS . $model->route->featured_image)) {
                            $img_path = UPLOAD_URL . 'routes/' . $model->route->featured_image;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'routes/delete_image/' . $model->route->id; ?>?featured_image=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="featured_image"
                                       value="<?= $model->route->featured_image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description",['rows'=>6]); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
        <div class="box">
            <div id="floating-panel">
                <input id="address" type="text" value="New York, NY"/>
                <input id="submit" type="button" value="Find"/>
                <input onclick="mapMarkers()" type="button" value="Generate"/>
                <input onclick="deleteMarkers()" type="button" value="Delete"/>
            </div>
            <div id="map"></div>
        </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
            <div class="form-group">
                <div class="markerContainer">
                    <?if($model->route->points) {
                        foreach (json_decode($model->route->points, true) as $index => $item) {?>
                            <div class="marker_<?= $index ?>"><input type="text" name="markers[]" value="<?= $item ?>"/></div>
                        <? } ?>
                    <? } ?>
                </div>
            </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'route/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input.image").change(function () {
            readURL(this);
        });

    });
</script>
<script>
    var map;
    var poly;
    var path;
    var markers = [];
//    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: {lat: 40.7127837, lng: -74.00594130000002}
        });
        var geocoder = new google.maps.Geocoder();

        map.addListener('click',function(e){
            addMarker(e.latLng);
        });

        poly = new google.maps.Polyline({
            strokeColor: '#000000',
            strokeOpacity:1,
            strokeWeight: 2
        });
        poly.setMap(map);

        <?if($model->route->points){
        foreach(json_decode($model->route->points,true) as $index=>$item){
        $ex = explode(',',$item); $lat = $ex[0]; $lng = $ex[1]?>
        addMarker(new google.maps.LatLng(<?=$lat?>, <?=$lng?>));
        <?}?>
        <?}?>

        google.maps.event.addDomListener( map, 'drag', function(e) {
            google.maps.event.trigger(map,'resize');
            map.setZoom(map.getZoom());
        });

        document.getElementById('submit').addEventListener('click', function() {
            geocodeAddress(geocoder, map);
        });
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);

                path = poly.getPath();
                deleteMarkers();

                // Because path is an MVCArray, we can simply append a new coordinate
                // and it will automatically appear.
                path.push(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
//                    label: labels[markers.length % labels.length],
                    position: results[0].geometry.location
                });
                markers.push(marker);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function addMarker(location){
        path = poly.getPath();
        deleteMarkers();

        // Because path is an MVCArray, we can simply append a new coordinate
        // and it will automatically appear.
        path.push(location);
        var marker = new google.maps.Marker({
            position:location,
//            label: labels[markers.length % labels.length],
            map:map
        });
        markers.push(marker);
        mapMarkers();
    }

    function deleteMarkers(){
        clearMarkers();
        path.clear();
        markers = [];
        $('.markerContainer').empty();
    }

    function clearMarkers(){
        setMapOnAll(null);
    }

    function setMapOnAll(map){
        for(var i = 0; i < markers.length; i++){
            markers[i].setMap(map);
        }
    }

    function mapMarkers(){
        var markerHtml = '';
        $('.markerContainer').empty();
        for(var i = 0; i < markers.length; i++){
            markerHtml += '<div class="marker_'+i+'"><input class="form-control" name="markers[]" value="'+markers[i].getPosition().lat()+', '+markers[i].getPosition().lng()+'"/></div>';
        }
        $('.markerContainer').append(markerHtml);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAPS_API_KEY?>&callback=initMap"
        type="text/javascript"></script>
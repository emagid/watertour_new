<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
    $(document).ready(function () {

        $('body').on('change', '.change', function () {


            var br = $(this).val();

            $("#ex").attr("href", "/admin/bus_tours/export?brand=" + br);

            window.location.href = "/admin/bus_tours?status_show=" + br;

        });


        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            <?if (isset($_GET['status_show'])){?>
            <?$br = $_GET['status_show'];?>
            window.location.href = '/admin/bus_tours?status_show='+<?=$br?>+'&how_many=' + how_many;
            <?}else{?>
            window.location.href = '/admin/bus_tours?how_many=' + how_many;
            <?}?>
        });

    });
</script>

<div class="row">
    <div class="col-md-16">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
<!--        <div class="box">-->
<!--            <form action="--><?//= ADMIN_URL ?><!--bus_tours/multi_update/" method="get">-->
<!--                <button type="submit" class="btn btn-success">Multi edit</button>-->
<!--                <a href="--><?//= ADMIN_URL ?><!--bus_tours/google_product_feed" class="btn btn-warning">Google Feed</a>-->
<!--            </form>-->
<!--        </div>-->
    </div>
    <div class="col-md-8">
        <div class="box">
            Show on page:
            <select class="how_many" name="how_many" style="cursor:pointer">

                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 10) {
                        echo "selected";
                    }
                } ?> selected value="10">10
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 50) {
                        echo "selected";
                    }
                } ?> value="50">50
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 100) {
                        echo "selected";
                    }
                } ?> value="100">100
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 500) {
                        echo "selected";
                    }
                } ?> value="500">500
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 1000) {
                        echo "selected";
                    }
                } ?> value="1000">1000
                </option>
            </select>
        </div>
    </div>
</div>
<?php if (count($model->bus_tours) > 0): ?>
    <div class="rox">
        <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                <tr>
                    <th width="2%"><input style="cursor:pointer;" name="edit-all" type="checkbox"/></th>
                    <th width="5%">Image</th>
                    <th width="21%">Name</th>
<!--                    <th width="21%">Alias</th>-->
                    <th width="7%">MSRP</th>
                    <th width="8%">Price</th>
                    <th width="8%">Availability</th>
                    <th width="1%" class="text-center">Edit</th>
                    <th width="1%" class="text-center">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;
                foreach ($model->bus_tours as $obj) { ?>
                    <tr class="originalTours">

                        <td><input style="cursor:pointer;" name="edit<?php echo $i++ ?>" type="checkbox"
                                   value="<?php echo $obj->id ?>"/></td>
                        <td>
                            <?$img_path = UPLOAD_URL.'bus_tours/'.$obj->featuredImage()?>
                            <a href="<?php echo ADMIN_URL; ?>bus_tours/update/<?php echo $obj->id; ?>"><img
                                    src="<?php echo $img_path; ?>" width="50"/></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>bus_tours/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                        </td>
<!--                        <td>-->
<!--                            <a href="--><?php //echo ADMIN_URL; ?><!--bus_tours/update/--><?php //echo $obj->id; ?><!--">--><?php //echo $obj->alias; ?><!--</a>-->
<!--                        </td>-->
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>bus_tours/update/<?php echo $obj->id; ?>"><?= number_format($obj->msrp, 2) ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>bus_tours/update/<?php echo $obj->id; ?>"><?= number_format($obj->price, 2) ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>bus_tours/update/<?php echo $obj->id; ?>"><?= $obj->getAvailability() ?></a>
                        </td>
                        <td class="text-center">
                            <a class="btn-actions" href="
                                <?php echo ADMIN_URL; ?>bus_tours/update/<?php echo $obj->id; ?>">
                                <i class="icon-pencil"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn-actions" href="
                                <?php echo ADMIN_URL; ?>bus_tours/delete/<?php echo $obj->id; ?>?token_id=
                                <?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                <i class="icon-cancel-circled"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
                <div class='paginationContent'></div>
            </div>
        </div>
    </div>
<?php endif; ?>

</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '/admin/bus_tours?<?if (isset($_GET['status_show'])){echo "status_show=";echo $_GET['status_show'] ;echo"&";} if (isset($_GET['how_many'])){echo "how_many=";echo $_GET['how_many'] ;echo"&";}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>
<script type="text/javascript">
    $(function () {

        $('input[name="edit-all"]').change(function () {
            if ($(this).prop('checked')) {
                $('#data-list tr').each(function () {
                    if ($(this).css('display') == 'table-row') {
                        $('input[type="checkbox"]', $(this)).prop('checked', 'checked');
                    }
                })
            } else {
                $('#data-list tr').each(function () {
                    $('input[type="checkbox"]', $(this)).prop('checked', false);
                })
            }
        })

        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>bus_tours/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalTours').remove();
                    $('.paginationContent').hide();
                    $('.originalTours').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<input type="checkbox" style="cursor:pointer;" name="edit' + list[key].id + '" value="' + list[key].id + '" />'));
                        if (list[key].featured_image == '') {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=ADMIN_IMG?>watertour_shoe.png");/*TODO st-dev default product image*/
                        } else {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=UPLOAD_URL . 'bus_tours/'?>" + list[key].featured_image);
                        }
                        $('<td />').appendTo(tr).html(img);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>bus_tours/update/' + list[key].id).html(list[key].name));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//bus_tours/update/' + list[key].id).html(list[key].alias));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>bus_tours/update/' + list[key].id).html(list[key].msrp));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>bus_tours/update/' + list[key].id).html(list[key].price));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>bus_tours/update/' + list[key].id).html(list[key].availability));

//                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//bus_tours/update/' + list[key].id);
//                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
//                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//bus_tours/delete/' + list[key].id);
//                        deleteLink.click(function () {
//                            return confirm('Are You Sure?');
//                        });
//                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalTours').remove();
                $('.paginationContent').show();
                $('.originalTours').show();
            }
        });
    })
</script>

































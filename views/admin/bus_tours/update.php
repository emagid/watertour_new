<?php
$duration_hours = [];
for($i = 0; $i < 21; $i++){
    $duration_hours[] = $i;
}
// for start_date and end_date
function dateFormatter($value,$format = 'm/d/Y h:iA'){
    return date($format,strtotime($value));
}
?>
<style type="text/css">
    .legshot {
        border:1px black solid;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->bus_tour->id; ?>"/>
<!--    <input type="hidden" name="redirectTo" value="--><?//= $_SERVER['HTTP_REFERER'] ?><!--"/>-->
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <?php if ($model->bus_tour->id > 0) { ?>
            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
            <?}?>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>tours/delete/<?php echo $model->bus_tour->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>

                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label>Alias</label>-->
<!--                                --><?php //echo $model->form->editorFor("alias"); ?>
<!--                            </div>-->

                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small style="color:#A81927"><b>(ideal featured image dimensions are 800 x 800, zoomed out with white space surrounding bus_tour)</b></small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->bus_tour->featured_image != "" && file_exists(UPLOAD_PATH . 'tours' . DS . $model->bus_tour->featured_image)) {
                                        $img_path = UPLOAD_URL . 'tours/' . $model->bus_tour->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'tours/delete_image/' . $model->bus_tour->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->bus_tour->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Featured Video</label>
                                <p>
                                    <small>(ideal featured video is less then 10M)</small>
                                </p>
                                <p><input type="file" name="video_link" class='video'/></p>
                                <?php if($model->bus_tour->video_link) { ?>
                                    <div style="display:inline-block">
                                        <video controls width="400" height="300">
                                            <source src="<?=$model->bus_tour->videoLink()?>" type="video/mp4">
                                        </video>
                                        <div class='preview-container'><?=$model->bus_tour->video_link?></div>
                                        <input type="hidden" name="video_link" class='video' value="<?=$model->bus_tour->video_link?>"/>
                                    </div>
                                <? } ?>
                            </div>

    <!--                    <div class="form-group">-->
    <!--                        <a href="#" id="add-to-hottest">Add this bus_tour to hottest deals</a>-->
    <!--                    </div>-->
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description"); ?>
                            </div>
                            <div class="form-group">
                                <label>Details</label><p><small>Separate details using | symbol</small></p>
                                <?php echo $model->form->textAreaFor("details"); ?>
                            </div>
                            <div class="form-group">
                                <label>Availability</label>
                                <select name="availability" class="form-control">
                                    <?$selected_stock = ($model->bus_tour->availability)?:2; ?>
                                    <option value="2" <?php if ($selected_stock == 2) {echo "selected";} ?>>Active</option>
                                    <option value="1" <?php if ($selected_stock == 1) {echo "selected";} ?>>In stock</option>
                                    <option value="0" <?php if ($selected_stock == 0) {echo "selected";} ?>>Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Validity</label>
                                <select name="validity" class="form-control">
                                    <option value="-1">Select</option>
                                    <?foreach(\Model\Tour::$validity as $key=>$item){?>
                                        <option value="<?=$key?>"><?=$item?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Duration - Hour</label>
                                        <?=$model->form->dropDownListFor('duration_hour',$duration_hours,'',['class'=>'form-control'])?>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Duration - Minute</label>
                                        <?=$model->form->dropDownListFor('duration_minute',['00','10','20','30','40','50'], '',['class'=>'form-control'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Availability Range</label>
                                <input name="availability_range" class="daterange" type="text" value="<?=$model->bus_tour->id > 0 ? dateFormatter($model->bus_tour->start_date) .' - '.dateFormatter($model->bus_tour->end_date): ''?>"/>
                            </div>
                            <div class="form-group">
                                <label>Validity Range</label>
                                <input name="validity_range" class="daterange" type="text" value="<?=$model->bus_tour->id > 0 ? dateFormatter($model->bus_tour->valid_start) .' - '.dateFormatter($model->bus_tour->valid_end) : ''?>"/>
                            </div>
                            <div class="form-group">
                                <label>Departure Point</label>
                                <?=$model->form->textBoxFor('departure_point')?>
                            </div>
                            <div class="form-group">
                                <label>Languages</label>
                                <?=$model->form->dropDownListFor('languages[]',array_map(function($item){return $item->name;},$model->languages),'',['class'=>'multiselect', 'multiple'=>'multiple'])?>
                            </div>
                            <div class="form-group">
                                <label>Route</label>
                                <?=$model->form->textAreaFor('route',['rows'=>5])?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>

                            <div class="form-group">
                                <label>Discounted Price</label><p><small>0 for no discount</small></p>
                                <?php echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group">
                                <label>Sell price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php echo $model->form->editorFor("tags"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="categories-tab">
                <select name="tour_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
                    <?php foreach ($model->categories as $cat) { ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php if ($model->bus_tour->id > 0) { ?>
                <div role="tabpanel" class="tab-pane" id="images-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="dropzone" id="dropzoneForm"
                                     action="<?php echo ADMIN_URL . 'tours/upload_images/' . $model->bus_tour->id; ?>">

                                </div>
                                <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                                <br/>
                                <p>
                                    <small style="color:#A81927"><b>(ideal bus_tour angle image dimensions are 1024px wide x 683px height)</b></small>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <table id="image-container" class="table table-sortable-container">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>File Name</th>
                                        <th>Display Order</th>
<!--                                        <th>Leg Shot</th>-->
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($prImg = \Model\Tour_Image::getList(['where' => 'tour_id=' . $model->bus_tour->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {?>
                                            <tr data-image_id="<?php echo $pimg->id; ?>">
                                                <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100" height="100"/></td>
                                                <td><?php echo $pimg->image; ?></td>
                                                <td class="display-order-td"><?=$pimg->display_order?></td>
<!--                                                <td class="leg_shot"><input id="leg_shot" name="leg_shot" type="radio" value="--><?//=$pimg->id?><!--" --><?//=$pimg->leg_shot == 'TRUE' ? 'checked': ''?><!--/></td>-->
                                                <td class="text-center">
                                                    <a class="btn-actions delete-bus_tour-image"
                                                       href="<?php echo ADMIN_URL; ?>tours/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                        <i class="icon-cancel-circled"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <button type="submit" class="btn btn-save" style="display:none">Save</button>
    <div id="target" class="btn btn-save">Save</div>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url =<?php echo json_encode(ADMIN_URL.'tours/'); ?>;
    $(document).ready(function () {
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        $(function() {
            $('.daterange').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->bus_tour->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",strtotime($model->bus_tour->start_date));?>",
                endDate: "<?php echo date("m/d/Y g:iA",strtotime($model->bus_tour->end_date));?>",
                <?php } ?>

            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });

        $("#target").click(function () {
            var price = $("input[name='price']").val();
            var name = $("input[name='name']").val();
            var slug = $("input[name='slug']").val();
            var id = <?=$model->bus_tour->id?>;
            var errors = new Array();
            if ($.trim(price) > 0) {
                $("input[name='price']").css({
                    "border-color": ""
                });
            } else {
                $("input[name='price']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect price");
            }
            if ($.trim(name) == "") {
                $("input[name='name']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect name");
            } else {
                $("input[name='name']").css({
                    "border-color": ""
                });
            }
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/tours/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {

                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name']").on('keyup', function (e) {
            var val = $.trim($(this).val());
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $('select[name="languages[]"').val(<?=$model->bus_tour->languages?>);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-bus_tour-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        var adjustment;
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden; width: 100px; height: 100px;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-bus_tour-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $(".numeric").numericInput();
    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'tours/upload_images/'.$model->bus_tour->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>

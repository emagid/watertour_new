<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->about->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Featured Image</label>
                    <p><input type="file" name="featured_image" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->about->featured_image != "" && file_exists(UPLOAD_PATH . 'about' . DS . $model->about->featured_image)) {
                            $img_path = UPLOAD_URL . 'about/' . $model->about->featured_image;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'about/delete_image/' . $model->about->id; ?>?featured_image=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="featured_image"
                                       value="<?= $model->about->featured_image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreafor("description",['class'=>'ckeditor']); ?>
                </div>
                <div class="form-group">
                    <label>Url <small>(www link)</small></label>
                    <?php echo $model->form->editorFor("url"); ?>
                </div>
                <div class="form-group">
                    <label>Url Text</label>
                    <?php echo $model->form->editorFor("url_text"); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <h4>Display</h4>

                <div class="form-group">
                    <label>Order</label>
                    <?php echo $model->form->textBoxFor("display_order",['type'=>'number','class'=>'form-control']); ?>
                </div>
<!--                <div class="form-group">-->
<!--                    <label>Full Span <small>(Full Span will fill an entire row rather than split into 2)</small></label>-->
<!--                    --><?php //echo $model->form->checkBoxFor("display_position",1,['class'=>'form-control']); ?>
<!--                </div>-->
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'about/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>
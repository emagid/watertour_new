<?php if (count($model->about) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="2%"></th>
                <th width="20%">Title</th>
                <th width="20%">Description</th>
                <th width="10%">Url</th>
                <th width="10%">Display Order</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->about as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about/update/<?php echo $obj->id; ?>"><img width="50" src="<?php echo UPLOAD_URL.'about'.DS.$obj->featured_image; ?>"></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about/update/<?php echo $obj->id; ?>"><?php echo strReplace($obj->description,30,'...'); ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about/update/<?php echo $obj->id; ?>"><?=$obj->url?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about/update/<?php echo $obj->id; ?>"><?=$obj->display_order?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>about/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>about/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'about';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>


<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->nav->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Preset Urls</label>
                            <select id="preset" class="form-control">
                                <? foreach ($model->urls as $label => $url) { ?>
                                    <optgroup label="<?= $label ?>">
                                        <? foreach ($url as $name=>$link) { ?>
                                            <option value="<?=$link?>"><?=$name?></option>
                                        <? } ?>
                                    </optgroup>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label>Url</label>
                            <?php echo $model->form->editorFor("url") ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Display Order</label>
                    <?php echo $model->form->editorFor("display_order") ?>
                </div>
            </div>
            <button type="submit" class="btn btn-save">Save</button>
        </div>
    </div>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'nav/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });

        $('#preset').on('change',function(){
            $('[name=url]').val($(this).val());
        });
    });
</script>
<?
// for start_date and end_date
function dateFormatter($value,$format = 'm/d/Y h:iA'){
    return date($format,strtotime($value));
}
?>
<style>
    .featured{
        border:1px solid black;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <?if($model->bus_package->id > 0){?>
                <li role="presentation"><a href="#images" aria-controls="general" role="tab" data-toggle="tab">Images</a></li>
            <?}?>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->bus_package->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tours</label>
                                <select name="bus_tour_id[]" class="multiselect" multiple>
                                    <?foreach($model->tours as $tour){?>
                                        <option value="<?=$tour->id?>"><?=$tour->name?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["rows" => 6]); ?>
                            </div>
                            <div class="form-group">
                                <label>Upgrade Option</label>
                                <?php echo $model->form->textBoxFor("upgrade_options"); ?>
                            </div>
                            <div class="form-group">
                                <label>Adult Price</label>
                                <?php echo $model->form->textBoxFor("adults_price"); ?>
                            </div>
                            <div class="form-group">
                                <label>Kids Price</label>
                                <?php echo $model->form->textBoxFor("kids_price"); ?>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <?php echo $model->form->dropDownListFor("status", \Model\Package::$status,'',['class'=>"form-control"]); ?>
                            </div>
                            <div class="form-group">
                                <label>Package Range</label>
                                <input name="package_range" class="daterange" type="text" value="<?=$model->bus_package->id > 0 ? dateFormatter($model->bus_package->start_date) .' - '.dateFormatter($model->bus_package->end_date): ''?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor('slug'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor('meta_title'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor('meta_keywords'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->textAreaFor('meta_description'); ?>
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-group">
                                <h4>Banner</h4>

                                <p>
                                    <small>(ideal image size is 1200 x 344)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->bus_package->banner != "") {
                                    $img_path = UPLOAD_URL . 'bus_packages/' . $model->bus_package->banner;
                                }
                                ?>
                                <p><input type="file" name="banner" class='image'/></p>
                                <?php if ($model->bus_package->banner != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <div id='image-preview'>
                                            <img src="<?php echo $img_path; ?>" width="600" height="172"/>
                                            <br/>
                                            <a href=<?= ADMIN_URL . 'packages/delete_image/' . $model->bus_package->id . '?banner=1'; ?> class="btn
                                               btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="banner" value="<?= $model->bus_package->banner ?>"/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div id='preview-container'></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($model->bus_package->id > 0) { ?>
                <div role="tabpanel" class="tab-pane" id="images">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="dropzone" id="dropzoneForm"
                                     action="<?php echo ADMIN_URL . 'packages/upload_images/' . $model->bus_package->id; ?>">

                                </div>
                                <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                                <br/>
                                <p>
                                    <small style="color:#A81927"><b>(ideal angle image dimensions are 1024px wide x 683px height)</b></small>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <table id="image-container" class="table table-sortable-container">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>File Name</th>
                                        <th>Display Order</th>
                                        <!--                                        <th>Leg Shot</th>-->
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($prImg = \Model\Package_Image::getList(['where' => 'package_id=' . $model->bus_package->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {?>
                                        <tr data-image_id="<?php echo $pimg->id; ?>">
                                            <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100" height="100"/></td>
                                            <td><?php echo $pimg->image; ?></td>
                                            <td class="display-order-td"><?=$pimg->display_order?></td>
                                            <!--                                                <td class="leg_shot"><input id="leg_shot" name="leg_shot" type="radio" value="--><?//=$pimg->id?><!--" --><?//=$pimg->leg_shot == 'TRUE' ? 'checked': ''?><!--/></td>-->
                                            <td class="text-center">
                                                <a class="btn-actions delete-bus_package-image"
                                                   href="<?php echo ADMIN_URL; ?>packages/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        var new_arrivals = <?php echo ($model->bus_package->new_arrivals?:0); ?>;
        var product_preview_ids = <?php echo ($model->bus_package->product_preview_ids?:0); ?>;
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '600');
                    img.attr('height', '172');
                    $("#preview-container").html(img);
                };

                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(function() {
            $('.daterange').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->bus_package->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",strtotime($model->bus_package->start_date));?>",
                endDate: "<?php echo date("m/d/Y g:iA",strtotime($model->bus_package->end_date));?>",
                <?php } ?>

            });
        });
        $('.changeImg').on('click',function(){
            var product_id = $(this).attr('data-product_id');
            var category_id = <?=$model->bus_package->id?>;
            var image = $(this).attr('data-image');
            var self = $(this);
            $.post('/admin/categories/updateCategoryProductImage', {product_id:product_id,category_id:category_id,image:image},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    self.parent().find('.featured').css('border','0px').removeClass('featured');
                    self.addClass('featured');
                    self.css('border','1px solid black');
                }
            });
        });

        $('#add-info-banner').on('click',function(){
            var html =
                '<div class="info-banner-group">' +
                    '<div class="form-group">' +
                        '<label>Banner Image</label>' +
                        '<input type="file" name="info-banner[]">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Header</label>' +
                        '<input type="text" name="info-header[]">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Description</label>' +
                        '<textarea name="info-desc[]" rows="5"></textarea>' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Link</label>' +
                        '<input type="text" name="info-link[]">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Color</label>' +
                        '<input type="text" name="info-color[]" class="jscolor">' +
                    '</div>' +
                    '<div class="delete-banner">Delete</div><hr/>' +
                '</div>';
            $('.info-banner-container').append(html);
            jscolor.installByClassName('jscolor');
        });

        $('.info-banner-container').on('click','.delete-banner',function(){
            if(confirm("Delete this banner?")){
                $(this).parent().remove();
            } else {
                return false;
            }
        });

        $('.product_preview_ids').multiselect({
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.product_preview_ids option:selected');

                if (selectedOptions.length > 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.product_preview_ids option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    var dropdown = $('.product_preview_ids').siblings('.multiselect-container');
                    nonSelectedOptions.each(function() {
                        var input = $(this).parent().parent().find('input[value="' + $(this).val() + '"]');
//                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    var dropdown = $('.product_preview_ids').siblings('.multiselect-container');
                    $('.product_preview_ids option').each(function() {
                        var input = $(this).parent().parent().find('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='bus_tour_id[]']").val(<?=$model->bus_package->bus_tour_id?>);
        $("select.multiselect").multiselect("rebuild");

        $("input[name='name']").on('keyup', function (e) {
            var val = $.trim($(this).val());
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }
        var adjustment;
        $('#selector').sortable({
            containerSelector: 'div',
            itemSelector: '.item',
            placeholder: '<div class="item"><img width="100px" height="100px" src=""/></div>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("prodCat_id"));

                });
                var array = [];
                $.each($('.item'),function(key,item){
                    array.push(item.getAttribute('data-id'));
                });
                $.post('/admin/categories/orderPreview',{array:array,id:<?=$model->bus_package->id?>});
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("prodcat_id"));

                });
                $.post('<?=ADMIN_URL.'categories/orderProduct'?>', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'packages/upload_images/'.$model->bus_package->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>

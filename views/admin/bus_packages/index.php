<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>

<?php
 if(count($model->bus_packages)>0): ?>
 <?php //dd($model)?>
  <div class="box box-table">
    <table id="sortables" class="table">
      <thead>
        <tr>
          <th width="15%">Name</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody class="sort">
       <?php foreach($model->bus_packages as $obj){ ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>bus_packages/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>bus_packages/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i>
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>bus_packages/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i>
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'bus_packages';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        var adjustment;
        /*$('#sortables').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden">.</td></tr>',
            onDrop: function ($item, container, _super) {
//                var $clonedItem = $('<tr/>').css({height: 0});
//                $item.before($clonedItem);
//                $clonedItem.animate({'height': $item.height()});
//
//                $item.animate($clonedItem.position(), function  () {
//                    $clonedItem.detach();
//                    _super($item, container);
//                });
//                if ($(event.target).hasClass('delete-product-image')) {
//                    $(event.target).trigger('click');
//                }
//
//                var ids = [];
//                var tr_containers = $("#image-container > tbody > tr");
//                tr_containers.each(function (i, e) {
//                    ids.push($(e).data("image_id"));
//
//                });
//                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
//                    sort_number_display();
//                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });*/
    })
</script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-24 dashboardnotify">
            <div class="top_panels">


                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">
                                    <div class="huge">$<?= number_format($model->monthly_sum, 2) ?></div>
                                    <div class="panel_text">
                                        <p>
                                            Monthly Sales
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-orange">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-line-chart fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge">$<?php echo number_format($model->weekly_sum, 0); ?></div>
                                    <div class="panel_text">
                                        <p>Weekly Sales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge">N/A</div>
                                    <div class="panel_text">
                                        <p>New Questions</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>questions">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-blue">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge"><?= (count($model->new_orders) > 0) ? $model->new_orders : 0; ?>
                                    </div>
                                    <div class="panel_text">
                                        <p>New Orders</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- top_panels -->


    <div class="col-md-8 chartsales">
        <div class="panel panel-default" style="min-height: 390px;">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Monthly Sales</h3>
            </div>

            <!-- html executes chart javascript -->
            <div id="company-sales" style="width: 100%">
                <canvas id="A" height="400"></canvas>
            </div>

        </div>
    </div>


    <div class="col-md-8 chartgross">
        <div class="panel panel-default" style="min-height: 390px;">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-area-chart"></i> Weekly Sales</h3>
            </div>

            <div id="gross-margin" style="width: 100%">
                <canvas id="B" height="400"></canvas>
            </div>

        </div>
    </div>

    <div class="col-md-8 chartorders">
        <div class="panel panel-default" style="min-height: 390px;">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-truck"></i> Monthly Orders</h3>
            </div>

            <!-- html executes chart javascript -->
            <div id="monthly-orders" style="width: 100%">
                <canvas id="C" height="400"></canvas>
            </div>


        </div>
    </div>


    <div class="row">


        <div class=" charts-list">
            <div class="col-md-12">
                <div class="panel panel-default" style="min-height: 290px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money"></i> Recent Transactions</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesorter">
                                <thead>
                                <tr>
                                    <th class="header">Order # <i class="fa fa-sort"></i></th>
                                    <th class="header">Order Date <i class="fa fa-sort"></i></th>
                                    <th class="header">Order Time <i class="fa fa-sort"></i></th>
                                    <th class="header">Amount <i class="fa fa-sort"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->recent_orders as $order): ?>
                                    <tr>
                                        <td>
                                            <a href="<?= ADMIN_URL ?>orders/update/<?= $order->id ?>"><?php echo $order->id; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?= ADMIN_URL ?>orders/update/<?= $order->id ?>"><?php echo date("m/d/Y", strtotime($order->insert_time)); ?></a>
                                        </td>
                                        <td>
                                            <a href="<?= ADMIN_URL ?>orders/update/<?= $order->id ?>"><?php echo date("g:i A", strtotime($order->insert_time)); ?></a>
                                        </td>
                                        <td>
                                            <a href="<?= ADMIN_URL ?>orders/update/<?= $order->id ?>">$<?= number_format($order->total, 2); ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo ADMIN_URL; ?>orders">View All Transactions <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-users"></i> Recent Clients</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped tablesorter">
                            <thead>
                            <tr>
                                <th class="header">Full Name <i class="fa fa-sort"></i></th>
                                <th class="header">Email <i class="fa fa-sort"></i></th>
                                <th class="header">Date Joined <i class="fa fa-sort"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($model->recent_clients as $client): ?>
                                <tr>
                                    <td>
                                        <a href="<?= ADMIN_URL ?>users/update/<?= $client->id ?>"><?php echo $client->first_name; ?><?php echo $client->last_name; ?></a>
                                    </td>
                                    <td>
                                        <a href="<?= ADMIN_URL ?>users/update/<?= $client->id ?>"><?php echo $client->email; ?></a>
                                    </td>
                                    <td>
                                        <a href="<?= ADMIN_URL ?>users/update/<?= $client->id ?>"><?php echo $client->insert_time; ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="<?php echo ADMIN_URL; ?>users">View All Clients <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var randomScalingFactor = function () {
        return Math.round(Math.random() * 100)
    };
    var dataSet = <?=json_encode($model->graphsData)?>;
    var weeklySet = <?=json_encode($model->weeklySet)?>;

    var months = [];
    for (key in dataSet.previous) {
        var month = new Date(key.split('-')[0], key.split('-')[1] - 1);
        months.push(monthNames[month.getMonth()]);
    }

    var weeks = ['7 Weeks', '6 Weeks', '5 Weeks', '4 Weeks', '3 Weeks', '2 Weeks', 'Last Week'];

    var set = [[], []];
    for (key in dataSet.previous) {
        set[0].push(dataSet.previous[key].monthly_sales);
    }
    for (key in dataSet.current) {
        set[1].push(dataSet.current[key].monthly_sales);
    }
    var barChartData1 = {
        labels: months,
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: set[0]
            },
            {
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: set[1]
            }
        ]

    }

    var set = [[], []];
    for (key in weeklySet.previous) {
        set[0].push(weeklySet.previous[key].weekly_sales);
    }
    for (key in weeklySet.current) {
        set[1].push(weeklySet.current[key].weekly_sales);
    }

    var lineChartData = {
        labels: weeks,
        datasets: [
            {
                label: "Previous",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                data: set[0]
            },
            {
                label: "Current",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                data: set[1]
            }
        ]

    }

    set = [[], []];
    for (key in dataSet.previous) {
        set[0].push(dataSet.previous[key].monthly_orders);
    }
    for (key in dataSet.current) {
        set[1].push(dataSet.current[key].monthly_orders);
    }
    var barChartData2 = {
        labels: months,
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: set[0]
            },
            {
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: set[1]
            }
        ]

    }

    window.onload = function () {
        var ctxB = document.getElementById("B").getContext("2d");
        window.myLine = new Chart(ctxB).Line(lineChartData, {
            responsive: true
        });
        var ctxA = document.getElementById("A").getContext("2d");
        window.myBar = new Chart(ctxA).Bar(barChartData1, {
            responsive: true
        });
        var ctxC = document.getElementById("C").getContext("2d");
        window.myBar = new Chart(ctxC).Bar(barChartData2, {
            responsive: true
        });
    }

</script>

<?php echo footer(); ?>
<style>
    #map {
        height: 500px;
        width: 500px;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->attraction->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#image-tab" aria-controls="image" role="tab" data-toggle="tab">Images</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Categories</label>
                                <select class="multiselect" name="attraction_categories[]" multiple>
                                    <?foreach($model->categories as $index=>$value){?>
                                        <option value="<?=$index?>" <?=\Model\Attraction_Categories::getCount(["where"=>"attraction_id = {$model->attraction->id} AND category_id = {$index}"]) > 0 ? 'selected' : "" ?>><?=$value?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Featured Image</label>
                                <p><input type="file" name="featured_image" class='image'/></p>
                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->attraction->featured_image != "" && file_exists(UPLOAD_PATH . 'attractions' . DS . $model->attraction->featured_image)) {
                                        $img_path = UPLOAD_URL . 'attractions/' . $model->attraction->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'attractions/delete_image/' . $model->attraction->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->attraction->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description",['class'=>'ckeditor ']); ?>
                            </div>
                            <div class="form-group">
                                <label>Marker Color</label>
                                <?php echo $model->form->textBoxFor("marker_color", ['class'=>"jscolor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>External Link</label>
                                <?php echo $model->form->editorFor("external_link"); ?>
                            </div>
                            <div class="form-group">
                                <label>Latitude</label>
                                <?php echo $model->form->editorFor("lat"); ?>
                            </div>
                            <div class="form-group">
                                <label>Longitude</label>
                                <?php echo $model->form->editorFor("lng"); ?>
                            </div>
                            <div class="form-group">
                                <div id="floating-panel">
                                    <input id="pac-input" type="text" value=""/>
                                    <input type="radio" name="type" id="changetype-all" checked="checked" style="display: none;">
            <!--                        <input onclick="mapMarkers()" type="button" value="Generate"/>-->
            <!--                        <input onclick="deleteMarkers()" type="button" value="Delete"/>-->
                                </div>
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="image-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <div class="dropzone" id="dropzoneForm"
                                 action="<?php echo ADMIN_URL . 'attractions/upload_images/' . $model->attraction->id; ?>">

                            </div>
                            <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                            <br/>
                            <p>
                                <small style="color:#A81927"><b>(ideal angle image dimensions are 1024px wide x 683px height)</b></small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table id="image-container" class="table table-sortable-container">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>File Name</th>
                                    <th>Display Order</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($prImg = \Model\Attraction_Images::getList(['where' => 'attraction_id =' . $model->attraction->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {?>
                                    <tr data-image_id="<?php echo $pimg->id; ?>">
                                        <td><img src="<?php echo UPLOAD_URL.'attractions/'.$pimg->image ?>" width="100" height="100"/></td>
                                        <td><?php echo $pimg->image; ?></td>
                                        <td class="display-order-td"><?=$pimg->display_order?></td>
                                        <td class="text-center">
                                            <a class="btn-actions delete-attraction-image"
                                               href="<?php echo ADMIN_URL; ?>attractions/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                <i class="icon-cancel-circled"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'attraction/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input.image").change(function () {
            readURL(this);
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='attraction_categories[]']").val(<?=json_encode(\Model\Attraction_Categories::getCategories($model->attraction->id))?>);
        $("select.multiselect").multiselect("rebuild");
    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'attractions/upload_images/'.$model->attraction->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>
<script>
    <?
    $lat = $model->attraction->lat ? : 40.7127837;
    $lng = $model->attraction->lng ? : -74.00594130000002;
    ?>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?=$lat?>, lng: <?=$lng?>},
            zoom: 13
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setIcon(/** @type {google.maps.Icon} */({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            $('[name=lat]').val(place.geometry.location.lat());
            $('[name=lng]').val(place.geometry.location.lng());
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);
        });
        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
            });
        }

        setupClickListener('changetype-all', []);
    }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAPS_API_KEY?>&libraries=places&callback=initMap"></script>

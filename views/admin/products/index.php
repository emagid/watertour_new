<?php if (count($model->products) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th></th>
                <th width="20%">Name</th>
                <th width="20%">Price</th>
                <th width="20%">Discount</th>
                <th width="10%" class="text-center">Edit</th>
                <th width="10%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->products as $obj) { ?>
                <tr>
                    <td><img src="<?=UPLOAD_URL.'products/'.$obj->featured_image?>" style="width: 50px"/></td>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->price; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->discount; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>products/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'products';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>


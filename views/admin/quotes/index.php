<?php if (count($model->quotes) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='15%'>Full Name</th>
                <th width='15%'>Phone</th>
                <th width='20%'>Email</th>
                <th width='20%'>Company</th>
                <th width='20%'>Comment</th>
                <th width='15%'>Pick-up Date</th>
                <th width='15%'>Drop-off Date</th>
                <th width='10%'>Guests</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->quotes as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->first_name; ?> <?php echo $obj->last_name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->phone; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->company; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->comment; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->pickup_date; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->dropoff_date; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>"><?php echo $obj->guest; ?></a></td>



                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>quotes/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'quotes';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->quote->id; ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>First Name</label>
                    <div><?=$model->quote->first_name?></div>
                </div>

                <div class="form-group">
                    <label>Last Name</label>
                    <div><?=$model->quote->last_name?></div>
                </div>

                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" disabled="disabled" value="<?=$model->quote->phone?>" />
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" disabled="disabled" value="<?=$model->quote->email?>" />
                </div>

                <div class="form-group">
                    <label>Company</label>
                    <textarea disabled="disabled"><?=$model->quote->company?></textarea>
                </div>

                <div class="form-group">
                    <label>Comment</label>
                    <textarea disabled="disabled"><?=$model->quote->comment?></textarea>
                </div>

                <div class="form-group">
                    <label>Route</label>
                    <textarea disabled="disabled"><?=$model->quote->route?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <h4>Details</h4>
                <div class="form-group">
                    <label>Pick-up Location</label>
                    <div><?=$model->quote->pickup_location?></div>
                </div>
                <div class="form-group">
                    <label>Pick-up</label>
                    <div>Date: <?=$model->quote->pickup_date?> Time: <?=$model->quote->pickup_time?></div>
                </div>
                <div class="form-group">
                    <label>Drop-off Location</label>
                    <div><?=$model->quote->dropoff_location?></div>
                </div>
                <div class="form-group">
                    <label>Drop-off</label>
                    <div>Date: <?=$model->quote->dropoff_date?> Time: <?=$model->quote->dropoff_time?></div>
                </div>
                <div class="form-group">
                    <label># of Guests</label>
                    <div><?=$model->quote->guest?></div>
                </div>
                <div class="form-group">
                    <label>Language Request</label>
                    <div><?=$model->quote->language_request?></div>
                </div>
                <div class="form-group">
                    <label>Special Request</label>
                    <div><?=$model->quote->special_request?></div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'quote/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>
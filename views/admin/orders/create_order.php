<style type="text/css">


    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
</style>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value=""/>
    <input type="hidden" name="old_status" value="New"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <div class="tab-content">
            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-10">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Customer Email*</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        Guest
                                    </div>
                                    <input type="text" name="email" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input name="phone" id="phone-us" type="text" value="">
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Shipping Method</label>-->
<!--                                --><?//= $model->form->dropDownListFor('shipping_cost', [0 => 'Standard - $0', 80 => 'International - $80'], '', ['class' => 'form-control']); ?>
<!--                            </div>-->
                            <div class="form-group">
                                <label>In Store</label>
                                <?= $model->form->checkBoxFor('in_store', 1,['checked'=>'checked']); ?>
                            </div>
                            <div class="form-group">
                                <label>Cardholder</label>
                                <input name="card_name" type="text" value="" class="form-group">
                            </div>
                            <div class="row cc-info">
                                <div class="col-xs-7 cc_number">
                                    <label>Credit Card #*</label>
                                    <div class="input-group">
                                        <?php echo $model->form->textBoxFor('cc_number',['required'=>'required','maxlength'=>16]); ?>
                                    </div>
                                </div>
                                <input name="cc_type" id="cc_type" style="display: none;">
                                <div class="col-xs-13">
                                    <div class="col-xs-24">
                                        <label>Expiration*</label>
                                    </div>
                                    <div>
                                        <div class="col-xs-12">
                                            <?= $model->form->dropDownListFor('cc_expiration_month', get_month(),'',['class'=>'form-control','required'=>'required']); ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <?= $model->form->dropDownListFor('cc_expiration_year', range(date('Y') - 0, date('Y') + 9),'',['class'=>'form-control','required'=>'required']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <label>CCV*</label>
                                    <?php echo $model->form->textBoxFor('cc_ccv',['required'=>'required','maxlength'=>4]); ?>
                                </div>
                            </div>
                            <? if (!is_null($model->order->coupon_code)) { ?>
                                <div class="form-group">
                                    <label>Coupon</label>

                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $model->coupon->id; ?>">
                                                <button type="button" class="btn"><i class="icon-eye"></i></button>
                                            </a>
                                        </div>
                                        <input type="text" disabled="disabled" value="<?= $model->coupon->code ?>"/>
                                    </div>
                                </div>
                            <? } ?>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <td>Subtotal</td>
                                    <td id="sub">$<?php echo number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Tax</td>
                                    <td id="tax">$<?php echo number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Shipping</td>
                                    <td id="shipping">$<?php echo number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    <td id="discount">$<?= number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td id="total">$<?= number_format(0, 2) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?
                    $shp = array($model->order->ship_first_name, $model->order->ship_last_name, $model->order->ship_address, $model->order->ship_address2,
                        $model->order->ship_country, $model->order->ship_city, $model->order->ship_state, $model->order->ship_zip);


                    $blng = array($model->order->bill_first_name, $model->order->bill_last_name, $model->order->bill_address, $model->order->bill_address2,
                        $model->order->bill_country, $model->order->bill_city, $model->order->bill_state, $model->order->bill_zip);

                    ?>
                    <div class="col-md-6">
                        <div class="box">
                            <h4>Shipping*</h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('ship_first_name',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('ship_last_name',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('ship_address',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('ship_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('ship_city',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?= $model->form->dropDownListFor('ship_state', get_states(), 'Select', ['class' => 'form-control','required'=>'required']); ?>
                                <?= $model->form->textBoxFor('ship_state', ['required'=>'required','disabled'=>'disabled','style'=>'display:none']); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('ship_zip',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('ship_country', get_countries(), '', ['class'=>'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <h4>Billing*</h4>
                            <div class="form-group">
                                <label>Same as Shipping</label>
                                <input id="same-as-shipping" type="checkbox">
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('bill_first_name',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('bill_last_name',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('bill_address',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('bill_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('bill_city',['required'=>'required']); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?= $model->form->dropDownListFor('bill_state', get_states(), 'Select', ['class' => 'form-control','required'=>'required']); ?>
                                <?= $model->form->textBoxFor('bill_state', ['required'=>'required','disabled'=>'disabled','style'=>'display:none']); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('bill_zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('bill_country', get_countries(), '', ['class' => 'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box product_parent">
                            <h4>Products*</h4>
                            <span style="float: right">Tax Free <input name="tax-free" type="checkbox"></span>
                            <div class="product_container">
                                <label>Product</label>
                                <select name="product_id[]" class="form-control" required>
                                        <option value="-1">Select a product</option>
                                        <option value="0">* Miscellaneous Charge</option>
                                    <?foreach(\Model\Product::getList(['orderBy'=>'name']) as $item){?>
                                        <option value="<?=$item->id?>"><?=$item->name?></option>
                                    <?}?>
                                </select>
                                <div class="product_select">
                                    <label>Color Variant</label>
                                    <select name="color[]" class="form-control"></select>
                                    <label>Size <small>(US)</small></label>
                                    <select name="size[]" class="form-control"></select>
                                </div>
                                <div class="misc_select" style="display: none">
                                    <label>Name</label>
                                    <input type="text" name="misc_name[]" class="form-control" required/>
                                </div>
                                <label>Custom Price <small>(overrides defined price)</small></label>
                                <input name="custom_price[]" type="number" class="form-control">
                                <hr/>
                            </div>
                        </div>
                        <div class="box">
                            <a class="add-another">Add another product</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Notes</h4>
                            <div class="form-group">
                                <label>Note</label>
                                <?php echo $model->form->textAreaFor("note", ['rows' => '10']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
    <button type="submit" class="btn btn-save">Save</button>
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>
<script src="<?=FRONT_JS.'cc_type.js'?>"></script>
<script>


    $(function () {

        var gsubtotal = [];
        var gtax = 0;
        var gshipping = 0;
        var gdiscount = 0;
        var gtotal = 0;

        function updateTotal(){
            var parseSub = gsubtotal.map(function(value){
                return parseFloat(value);
            });
            var subtotal = parseSub.reduce(function(a,b){return a + b;},0);
            gtax = parseFloat(gtax);
            gshipping = parseFloat(gshipping);
            gdiscount = parseFloat(gdiscount);
            gtotal = parseFloat(gtotal);

            if(!$('input[name=tax-free]').is(':checked')) {
                gtax = subtotal * .08875;
            } else {
                gtax = 0;
            }
            gtotal = subtotal + gtax + gshipping - gdiscount;
            var sub = $('#sub');
            var tax = $('#tax');
            var shipping = $('#shipping');
            var discount = $('#discount');
            var total = $('#total');

            sub.html('$'+subtotal.toFixed(2));
            tax.html('$'+gtax.toFixed(2));
            shipping.html('$'+gshipping.toFixed(2));
            discount.html('$'+gdiscount.toFixed(2));
            total.html('$'+gtotal.toFixed(2));
        }

        $($('input[name=tax-free]')).on('change',function(){
            updateTotal();
        });

        $('input[name=cc_number]').on('input',function(){
//			var creditCardType = require('<?//=FRONT_JS?>//cc_type');
            $('#cc_type').val(creditCardType($(this).val())[0].type);
//			console.log(creditCardType($(this).val())[0].niceType);
        });

        $(document).on('change',"select[name='product_id[]']",function(){
            var self = $(this);
            var index = self.index("select[name='product_id[]']");
            var product_id = self.find(':selected').val();
            var color = self.parent().find($('select[name="color[]"]'));
            var size = self.parent().find($('select[name="size[]"]'));
            var name = self.parent().find($('input[name="misc_name[]"]'));
            var custom_price = self.parent().find($('input[name="custom_price[]"]'));
            var product_select = self.parent().find($('.product_select'));
            var misc_select = self.parent().find($('.misc_select'));
            if(product_id > 0) {
                product_select.show(); misc_select.hide(); color.prop('readonly',false); size.prop('readonly',false); name.prop('readonly',true);
                $.getJSON('/admin/orders/getVariations', {product_id: product_id}, function (data) {
                    if (data.status == 'success') {
                        gsubtotal[index] = data.subtotal;
                        color.html(data.colorHtml);
                        size.html(data.sizeHtml);
                        updateTotal();
                    }
                });
            } else if(product_id == 0){
                product_select.hide(); misc_select.show(); color.prop('readonly',true); size.prop('readonly',true); name.prop('readonly',false);
                gsubtotal[index] = 0;
                updateTotal();
            } else {
                product_select.show(); misc_select.hide(); color.prop('readonly',false); size.prop('readonly',false); name.prop('readonly',true);
                gsubtotal[index] = 0;
                updateTotal();
                color.html("");
                size.html("");
            }
        });
        $(document).on('change',"select[name='color[]']",function(){
            var self = $(this);
            var index = self.index("select[name='color[]']");
            var product_id = self.parent().parent().find('select[name="product_id[]"]').find(':selected').val();
            var color_id = self.find(':selected').val();
            $.getJSON('/admin/orders/setColorVariant',{product_id:product_id,color_id:color_id},function(data){
                if(data.status == 'success'){
                    gsubtotal[index] = data.price;
                    updateTotal();
                }
            });
        });

        $(document).on('keyup','input[name="custom_price[]"]',function(){
            var value;
            var index = $(this).index('input[name="custom_price[]"]');
            if($(this).val() > 0){
                value = $(this).val();
            } else {
                value = 0;
            }
            gsubtotal[index] = value;
            updateTotal();
        });

        $('select[name=ship_country]').on('change',function(){
            var value = $(this).val();
            if(value == 'United States'){
                gshipping = 0;
                updateTotal();
            } else {
                $.getJSON('/admin/orders/getShippingCost',{country:value},function(data){
                    gshipping = parseFloat(data.shipping);
                    updateTotal();
                })
            }
        });

        $('select[name=ship_country],select[name=bill_country]').on('change',function(){
            var value = $(this).val();
            var state = $(this).attr('name').replace('country','state');
            if(value == 'United States'){
                $('select[name='+state+']').prop('disabled',false).show();
                $('input[name='+state+']').prop('disabled',true).hide();
            } else {
                $('select[name='+state+']').prop('disabled',true).hide();
                $('input[name='+state+']').prop('disabled',false).show();
            }
        });

        $('.add-another').on('click',function(){
            var container = '<div class="product_container"> ' +
                '<label>Product</label> ' +
                '<select name="product_id[]" class="form-control" required> ' +
                '<option value="-1">Select a product</option> ' +
                '<option value="0">* Miscellaneous Charge</option>';
            <? foreach (\Model\Product::getList(['orderBy' => 'name']) as $item) { ?>
            container += '<option value="<?= $item->id ?>"><?= $item->name ?></option>';
            <? } ?>
            container += '</select> ' +
                '<div class="product_select"> ' +
                '<label>Color Variant</label> ' +
                '<select name="color[]" class="form-control"><option value="0">None</option></select> ' +
                '<label>Size <small>(US)</small> ' +
                '</label> ' +
                '<select name="size[]" class="form-control"><option value="0">None</option></select> ' +
                '</div> ' +
                '<div class="misc_select" style="display: none"> ' +
                '<label>Name</label> ' +
                '<input type="text" name="misc_name[]" class="form-control" required/> ' +
                '</div> ' +
                '<label>Custom Price <small>(overrides defined price)</small> ' +
                '</label> ' +
                '<input name="custom_price[]" type="number" class="form-control"> ' +
                '<hr/> ' +
                '</div>';
            $('.product_parent').append(container);
        });

        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });
        $('.cc_number button').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '<?=ADMIN_URL?>orders/getCcNumber',
                    'data': 'password=' + pwd + '&order_id=<?=$model->order->id?>',
                    'success': function (data) {
                        data = JSON.parse(data);
                        if (data == "false") {
                            alert('Incorrect code.');
                        } else {
                            $('.cc_number input').val(data);
                        }
                    }
                })
            }
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("orders/");

        });

        $('#save').mouseover(function () {

            var id = $("input[name=id]").val();
            $("#act_for_click").val("orders/update/" + id);

        });


    });
    $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>


<script>

    $(document).ready(function () {

        $('#same-as-shipping').on('change',function(){
            var array = {bill_first_name:'ship_first_name',bill_last_name:'ship_last_name',bill_address:'ship_address',bill_address2:'ship_address2',bill_city:'ship_city',bill_state:'ship_state',bill_zip:'ship_zip',bill_country:'ship_country'};
            if($(this).is(':checked')){
                $.each(array,function(index,value){
                    $('[name='+index+']').val($('[name='+value+']:enabled').val());
                    if(index == 'bill_country'){
                        $('[name='+index+']').trigger('change');
                    }
                });
            } else {
                $.each(array,function(index){
                    $('[name='+index+']').val('');
                });
            }
        });

        $('body').on('change', '.change', function () {
            var status = $(this).val();
            if (status == "Processed") {
                $("#mail").html('<p>Hello  <?=$model->order->ship_first_name?>,<p>Thank you for your recent order.</p><p>This is to confirm that your orders is in stock and your credit card has now been processed.</p><p>You will receive an email with the tracking number as soon as it has shipped.</p><p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@watertour.com</p><p>Thank you again for your business,</p><p>&nbsp;</p><p>The Sales Team at watertour.com</p><p>&ldquo;If it tells time, we sell it&rdquo;</p><p>(877) 752-6919</p>');
            } else if (status == "Shipped") {
                $("#mail").html('<p>Hello &nbsp;<?=$model->order->ship_first_name?>,</p><p>Your watertour order has shipped and the tracking number is _________.</p><p>If you have any questions please call or email us.</p><p>Thank you again for your business,</p><p>&nbsp;</p><p>The Sales Team at watertour.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
            } else if (status == "Delivered") {
                $("#mail").html('<p>Hello &nbsp;<?=$model->order->ship_first_name?>,</p><p>We have been informed by the manufacturer that the item you ordered from watertour has been discontinued.</p><p>And we checked with our other suppliers and they too do not have any in stock.</p><p>Would you like us to provide you with alternatives to that item&gt;&gt;SHOW ME OTHERS&lt;&lt; &nbsp;or would you prefer to just have your order canceled &gt;&gt;Cancel&lt;&lt;.?</p><p>&nbsp;</p><p>The Sales Team at watertour.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
            } else if (status == "Discontinued") {
                $("#mail").html('<p>Hello &nbsp;<?=$model->order->ship_first_name?>,</p><p>We have been informed by the manufacturer that the item you ordered from watertour has been discontinued.</p><p>And we checked with our other suppliers and they too do not have any in stock.</p><p>Would you like us to provide you with alternatives to that item&gt;&gt;SHOW ME OTHERS&lt;&lt; &nbsp;or would you prefer to just have your order canceled &gt;&gt;Cancel&lt;&lt;.?</p><p>&nbsp;</p><p>The Sales Team at watertour.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
            } else if (status == "Billing_Shipping") {
                $("#mail").html('<p>Hello &nbsp;(&lsquo;name&rsquo;),</p><p>In our efforts to protect all our customers we do credit card billing and shipping address verification on all orders.</p><p>When we contacted your credit card issuing bank to confirm the shipping address that you provided they were unable to do so.</p><p>Please just call the toll free number on the back of your credit card and ask them to add this as an &quot;Alternate Shipping Address&quot;.&nbsp;</p><p>Then just let us know that this has been done &gt;&gt;SHIPPING ADDRESS ADDED&lt;&lt; and we will be able to ship the watch to you at your requested address.</p><p>Thank you for your business,</p><p>&nbsp;</p><p>The Sales Team at watertour.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
            } else if (status == "Canceled") {
                $("#mail").html('Dear <b><?=$model->order->ship_first_name?></b>, We have canceled your order as requested.<br> Thank you for giving us the opportunity to try to help you and we hope that you will come back to watertour.com in the future for any watch needs- Remember, "If it tells time we sell it".<br><br> Please note that your credit card was never charged for this purchase - only an authorization was received to confirm that it was not a fraudulent charge.<br><br> The Sales Team at watertour.com <br>(877) 752-6919<br> www.watertour.com');
            } else {
            }

        });


    });
</script>
<?php if (count($model->languages) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->languages as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>languages/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>languages/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>languages/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'languages';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>


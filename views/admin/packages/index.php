<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>

<?php
if (count($model->packages) > 0): ?>
    <div class="box box-table">
        <table id="sortables" class="table">
            <thead>
            <tr>
                <th width="15%">ID</th>
                <th width="15%">Name</th>
                <th width="15%">Price (adult | child)</th>
                <th width="10%">Status</th>
                <th width="10%">Tours</th>
                <th width="10%">Display Order</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
                <th width="1%" class="text-center">Featured</th>
            </tr>
            </thead>
            <tbody class="sort">
            <?php foreach ($model->packages as $obj) { ?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>packages/update/<?= $obj->id ?>"><?php echo $obj->package_id; ?></a>
                    <td><a href="<?php echo ADMIN_URL; ?>packages/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>packages/update/<?= $obj->id ?>"><?php echo '$' . number_format($obj->adults_price, 2) . ' | $' . number_format($obj->kids_price, 2) ?></a>
                    </td>
                    <td>
                        <span style="display: none"><?=$obj->status == 1?'active':'inactive'?></span>
                        .<select class="package-status" data-package="<?=$obj->id?>">
                            <option value="1" <?=$obj->status == 1?'selected':null?>>Active</option>
                            <option value="0" <?=$obj->status == 1?null:'selected'?>>Inactive</option>
                        </select>
<!--                        <a href="--><?php //echo ADMIN_URL; ?><!--packages/update/--><?//= $obj->id ?><!--">--><?php //echo $obj->status == 1 ? 'Active' : 'Inactive'; ?><!--</a>-->
                    </td>
                    <td><? if ($obj->tour_id) {
                            foreach (json_decode($obj->tour_id, true) as $value) { ?><a
                                href="<?php echo ADMIN_URL; ?>tours/update/<?= $value ?>">
                                -<?= \Model\Tour::getItem($value)->name ?><br></a><? }
                        } ?></td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>packages/update/<?php echo $obj->id; ?>"><?=$obj->display_order?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>packages/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>packages/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions change-featured" data-id="<?= $obj->id ?>"
                           data-featured="<?= $obj->featured ?>">
                            <i class="<?= $obj->featured ? 'icon-ok' : 'icon-minus' ?>"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
<!--        <div class="box-footer clearfix">-->
<!--            <div class='paginationContent'></div>-->
<!--        </div>-->
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL . 'packages';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function () {
        $('.package-status').on('change', function(e){
            e.preventDefault();
            $.post('/admin/packages/update_status', {id: $(this).data('package'), value: $(this).val()}, function(e){
                if(e.status) alert('Updated');
                else alert('Update failed');
            })
        });

       /* $(document).on('change','.package-status', function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var status = $(this).data('status');
            var self = $(this);
            $.post('/admin/packages/update_status', {id: id, status: status}, function(e){
                if(e.status) alert('Updated');
                else alert('Update failed');
                var json = $.parseJSON(data);
                self.find('i').attr('class', json.iClass);
                self.data('status', json.status);
            })
        });*/

        $('#sortables').DataTable({
            "order": [[ 0, "desc" ]],
            "pageLength": 100,
        });

        $(document).on('click', '.change-featured', function(){
            var id = $(this).data('id');
            var featured = $(this).data('featured');
            var self = $(this);
            $.post('/admin/packages/updateFeatured', {id: id, featured: featured}, function (data) {
                var json = $.parseJSON(data);
                self.find('i').attr('class', json.iClass);
                self.data('featured', json.featured);
            })
        });
    })
</script>
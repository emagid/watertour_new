<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->main_route->id; ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#route-tab" aria-controls="route" role="tab" data-toggle="tab">Route</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Route Data</label>
                                <?php echo $model->form->editorFor("route_data"); ?>
                            </div>
                            <div class="form-group">
                                <label>Route Type</label>
                                <?php echo $model->form->dropDownListFor("type",\Model\Main_Route::$TYPE); ?>
                            </div>
                            <div class="form-group">
                                <label>Color</label>
                                <?php $colorArray = ['yellow','orange','green','purple','red','blue']; ?>
                                <?php echo $model->form->dropDownListFor("color",$colorArray); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="route-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Add Routes</h4>

                            <div class="form-group">
                                <label>Routes</label>
                                <select class="multiselect routes" multiple>
                                    <?foreach ($model->routes as $item){?>
                                        <option value="<?=$item->id?>"><?=$item->name?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group routeItems">
                                <?if($model->route_path){
                                    foreach ($model->route_path as $index=>$route){?>
                                        <div class="row" data-route_id="<?=$route->id?>">
                                            <input type="hidden" name="route_path[<?=$index?>][route_id]" value="<?=$route->id?>"/>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Route ID</label>
                                                    <p><?=$route->id?></p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Route Name</label>
                                                    <p><?=$route->name?></p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Display Order</label>
                                                    <input name="route_path[<?=$index?>][display_order]" type="number" class="form-control" value="<?=$route->display_order?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Route Icons</label>
                                                    <select name="route_path[<?=$index?>][route_icon_id][]" class="multiselect" multiple>
                                                        <?foreach (\Model\Route_Icon::getList() as $item){?>
                                                            <option value="<?=$item->id?>"><?=$item->name?></option>
                                                        <?}?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?}
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-save">Save</button>
    </div>
</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL . 'main_route/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $("select[name='color'] option").each(function(op){
            $(this).css("background-color",$(this).val());
        });
        $('.routes').multiselect({
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            onChange: function (option, checked) {
                var routeItems = $('.routeItems');
                var routes = $('.routes option:selected');
//                routeItems.empty();

                var item = $('[data-route_id=' + option.val() + ']');
                if (item.length && checked) {
                    item.show();
                    item.find('input, select').prop('disabled', false);
                } else if (item.length && !checked) {
                    item.hide();
                    item.find('input, select').prop('disabled', true);
                } else if (!item.length && checked) {
                    var route_id = option.val();
                    var count = $('[data-route_id]').length;
                    var routePathId = 'route_path[' + count + '][route_id]';
                    var routePathOrder = 'route_path[' + count + '][display_order]';
                    var routePathIcons = 'route_path[' + count + '][route_icon_id][]';
                    var
                        html = '<div class="row" data-route_id="' + route_id + '">';
                    html += '<input type="hidden" name="' + routePathId + '" value="' + route_id + '"/>';
                    html += '<div class="col-md-6">';
                    html += '<div class="form-group">';
                    html += '<label>Route ID</label>';
                    html += '<p>' + route_id + '</p>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-md-6">';
                    html += '<div class="form-group">';
                    html += '<label>Route Name</label>';
                    html += '<p>' + option.text() + '</p>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-md-6">';
                    html += '<div class="form-group">';
                    html += '<label>Display Order</label>';
                    html += '<input name="' + routePathOrder + '" type="number" class="form-control" value="0"/>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-md-6">';
                    html += '<div class="form-group">';
                    html += '<label>Route Icons</label>';
                    html += '<select name="' + routePathIcons + '" class="multiselect" multiple>';
                    <?foreach (\Model\Route_Icon::getList() as $item){?>
                    html += '<option value="<?=$item->id?>"><?=$item->name?></option>';
                    <?}?>
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    routeItems.append(html);
                    $(document).find('[name="' + routePathIcons + '"').multiselect({
                        maxHeight: 415,
                        checkboxName: '',
                        enableCaseInsensitiveFiltering: true,
                        buttonWidth: '100%'
                    });
                }
            }
        });
        $("select.multiselect").each(function (i, e) {
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        <?if($model->route_path){
        $routes = [];
        foreach ($model->route_path as $index=>$path){
        $routes[] = $path->id;?>
        $('select[name="route_path[<?=$index?>][route_icon_id][]"]').val(<?=$path->route_icon_ids?>);
        <?}?>
        $('.routes').val(<?=json_encode($routes)?>);
        <?}?>
        $("select.multiselect").multiselect("rebuild");
    });
</script>
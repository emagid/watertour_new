<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->rental->id ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#add_on-tab" aria-controls="add_on" role="tab" data-toggle="tab">Add On</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Duration</label>
                                <?php echo $model->form->textBoxFor("duration", ['class' => 'numeric']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <h4>External Server Ids</h4>

                            <div class="form-group">
                                <label>TourCode <small>(Server Side)</small></label>
                                <?php echo $model->form->editorFor("tour_code"); ?>
                            </div>
                            <div class="form-group">
                                <label>PackageId <small>(Server Side)</small></label>
                                <?php echo $model->form->editorFor("external_package_id"); ?>
                            </div>
                            <h4>Tandem Ids</h4>
                            <div class="form-group">
                                <label>TourCode <small>(Server Side)</small></label>
                                <input type="text" name="tandem_tour" value="<?=$model->rental->options ? json_decode($model->rental->options,true)['tandem_tour']:''?>"/>
                            </div>
                            <div class="form-group">
                                <label>PackageId <small>(Server Side)</small></label>
                                <input type="text" name="tandem_package" value="<?=$model->rental->options ? json_decode($model->rental->options,true)['tandem_package']:''?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box">
                            <h4>Price</h4>

                            <div class="form-group">
                                <label>Pricing formula derived from Equipment Price x Duration</label>
                            </div>
                            <? foreach (\Model\Equipment::getList() as $item) {
                                $rp = $item->rental_price ? json_decode($item->rental_price, true) : []; ?>
                                <div class="form-group">
                                    <label><?= $item->name ?></label>
                                    <h4><?= array_key_exists($model->rental->id, $rp) ? '$' . number_format($rp[$model->rental->id], 2) : '$' . number_format(0, 2) ?></h4>
                                </div>
                            <? } ?>
                        </div>
                    </div>

                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="add_on-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <?$addOns = $model->rental->add_ons ? json_decode($model->rental->add_ons,true) : []?>
                            <h4>Services</h4>

                            <div class="form-group">
                                <label>Rentals</label>
                                <select name="rentals[]" class="multiselect" multiple>
                                    <?foreach(\Model\Rental::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['rentals']) && in_array($item->id,$addOns['rentals']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tours</label>
                                <select name="tours[]" class="multiselect" multiple>
                                    <?foreach(\Model\Tour::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['tours']) && in_array($item->id,$addOns['tours']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Packages</label>
                                <select name="packages[]" class="multiselect" multiple>
                                    <?foreach(\Model\Package::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['packages']) && in_array($item->id,$addOns['packages']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Attractions</label>
                                <select name="attractions[]" class="multiselect" multiple>
                                    <?foreach(\Model\Attraction::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['attractions']) && in_array($item->id,$addOns['attractions']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Products</h4>

                            <div class="form-group">
                                <label>Equipment</label>
                                <select name="equipment[]" class="multiselect" multiple>
                                    <?foreach(\Model\Equipment::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['equipment']) && in_array($item->id,$addOns['equipment']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Products</label>
                                <select name="products[]" class="multiselect" multiple>
                                    <?foreach(\Model\Product::getList() as $item){?>
                                        <option value="<?=$item->id?>" <?=isset($addOns['products']) && in_array($item->id,$addOns['products']) ? 'selected': ''?>><?=$item->getName()?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <button type="submit" class="btn btn-lg btn-save">Save</button>
            </div>
        </div>
    </div>

</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $('.numeric').numericInput();
        $("select.multiselect").each(function (i, e) {
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
    });

</script>
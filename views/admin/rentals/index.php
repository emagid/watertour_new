<?php if (count($model->rentals) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%">Name</th>
                <th width="10%">Duration</th>
                <th width="10%">Price (Adults)</th>
                <th width="10%">Price (Kids)</th>
                <th width="10%">Price (Tandem)</th>
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->rentals as $rental):
                $adultPriceStr = '$'.number_format($rental->adult_price,2);
                $kidPriceStr = '$'.number_format($rental->kid_price,2);
                $tandemPriceStr = '$'.number_format($rental->tandem_price,2);
                if($rental->discount_amount > 0){
                    $adultPriceStr .= ' ($'.number_format($rental->getPrice('adult'),2).')';
                    $kidPriceStr .= ' ($'.number_format($rental->getPrice('kid'),2).')';
                    $tandemPriceStr .= ' ($'.number_format($rental->getPrice('tandem'),2).')';
                }
                ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>rentals/update/<?php echo $rental->id; ?>"><?php echo $rental->name; ?></a>
                    </td>
                    <td><a href="<?php echo ADMIN_URL; ?>rentals/update/<?php echo $rental->id; ?>"><?php echo $rental->duration.' Hour'; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>rentals/update/<?php echo $rental->id; ?>"><?php echo $adultPriceStr;?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>rentals/update/<?php echo $rental->id; ?>"><?php echo $kidPriceStr; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>rentals/update/<?php echo $rental->id; ?>"><?php echo $tandemPriceStr; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>rentals/update/<?php echo $rental->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>rentals/delete/<?php echo $rental->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'rentals/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>


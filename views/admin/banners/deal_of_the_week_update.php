<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Title</label>
					<input type="text" value="Deal of the Week" disabled="disabled" />
				</div>
				<div class="form-group">
					<label>Link</label>
					<input type="text" value="<?=$model->banner->link?>" name="link" />
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<p><small>(ideal "deal of the week" image size is 1200 x 240)</small></p>
					<?php 
						$img_path = '';
						if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.jpg')){
							$img_path = UPLOAD_URL.'banners/deal_of_the_week.jpg';
						} else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.jpeg')){
							$img_path = UPLOAD_URL.'banners/deal_of_the_week.jpeg';
						} else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.png')){
							$img_path = UPLOAD_URL.'banners/deal_of_the_week.png';
						} else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.gif')){
							$img_path = UPLOAD_URL.'banners/deal_of_the_week.gif';
						}
					?>
					<p><input type="file" name="image" class='image' /></p>
					<?php if($img_path != ''){ ?>
					<div class="well well-sm pull-left">
						<div id='image-preview'>
							<img src="<?php echo $img_path; ?>" width="1200" height="240" />
							<br />
							<a href=<?= ADMIN_URL.'banners/deal_of_the_week_delete'?> class="btn btn-default btn-xs">Delete</a>
						</div>
					</div>
					<?php } ?>
					<div id='preview-container'></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>


<div class="box" style="margin-top: 15px;">
	<h4>Home Map</h4>
	<div class="home-map">
		<div class="top"><div class="container">TOP</div></div>
		<div class="header"><div class="container">HEADER</div></div>
		<div class="main-banner"><div class="container">MAIN BANNER</div></div>
		<div class="featured-banner">
			<div class="container">
				<div class="col-sm-8"><div id="f-1">FEATURED #1</div><div id="f-2">FEATURED #2</div></div>
				<div class="col-sm-8"><div id="f-3">FEATURED #3</div><div id="f-4">FEATURED #4</div></div>
				<div class="col-sm-8"><div id="f-5">FEATURED #5</div><div id="f-6">FEATURED #6</div></div>
			</div>
		</div>
		<div class="deal-of-the-week"><div class="container" style="border-style:solid; background: #ccc; font-weight: bold;">DEAL OF THE WEEK</div></div>
		<div class="news"><div class="container">RECENT NEWS</div></div>
		<div class="footer"><div class="container">FOOTER</div></div>
	</div>
</div>

<?= footer(); ?>

<script type='text/javascript'>
$(document).ready(function() {
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				var img = $("<img />");
				img.attr('src',e.target.result);
				img.attr('alt','Uploaded Image');
				img.attr("width",'1200');
				img.attr('height','240');
				$("#preview-container").html(img);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("input.image").change(function(){
		readURL(this);
		$('#previewupload').show();
	});	
});
</script>
<?php if(count($model->banners)>0): ?>
	<div class="box box-table">
		<table class="table">
			<thead>
			<tr>
				<th width="70%">Banner</th>
				<th width="15%" class="text-center">Edit</th>
				<th width="15%" class="text-center">Delete</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($model->banners as $banner): ?>
				<tr>
					<td><a href="<?php echo ADMIN_URL; ?>banners/featured_update/<?php echo $banner->id; ?>"><?php echo $banner->title; ?></a></td>
					<td class="text-center">
						<a class="btn-actions" href="<?php echo ADMIN_URL; ?>banners/featured_update/<?php echo $banner->id; ?>">
							<i class="icon-pencil"></i>
						</a>
					</td>
					<td class="text-center">
						<a class="btn-actions" href="<?php echo ADMIN_URL; ?>banners/featured_delete/<?php echo $banner->id; ?>" onClick="return confirm('Are You Sure?');">
							<i class="icon-cancel-circled"></i>
						</a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<div class="box-footer clearfix">
			<div class='paginationContent'></div>
		</div>
	</div>
<?php endif; ?>
<div class="box">
	<h4>Home Map</h4>
	<div class="home-map">
		<div class="top"><div class="container">TOP</div></div>
		<div class="header"><div class="container">HEADER</div></div>
		<div class="main-banner"><div class="container">MAIN BANNER</div></div>
		<div class="featured-banner">
			<div class="container" style="border-style:solid; background: #ccc; font-weight: bold;">
				<div class="col-sm-8"><div id="f-1">FEATURED #1</div><div id="f-2">FEATURED #2</div></div>
				<div class="col-sm-8"><div id="f-3">FEATURED #3</div><div id="f-4">FEATURED #4</div></div>
				<div class="col-sm-8"><div id="f-5">FEATURED #5</div><div id="f-6">FEATURED #6</div></div>
			</div>
		</div>
		<div class="deal-of-the-week"><div class="container">SELECTION</div></div>
		<div class="deal-of-the-week"><div class="container">MEDIA</div></div>
		<div class="news"><div class="container">SPECIALITY</div></div>
		<div class="footer"><div class="container">FOOTER</div></div>
	</div>
</div>

<?php echo footer(); ?>
<?php if(count($model->banners)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="70%">Banner</th>	
          <th width="15%" class="text-center">Edit</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->banners as $banner): ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>banners/description_update/<?php echo $banner->id; ?>"><?php echo $banner->title; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>banners/description_update/<?php echo $banner->id; ?>">
             <i class="icon-pencil"></i> 
           </a>
         </td>
       </tr>
     <?php endforeach; ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'banners/description';?>';
  	var total_pages = <?= $model->pagination->total_pages;?>;
  	var page = <?= $model->pagination->current_page_index;?>;
</script>


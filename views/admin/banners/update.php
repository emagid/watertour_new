<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <input type="hidden" name="id" value="<?php echo $model->banner->id;?>" />
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Title</label>
					<?php echo $model->form->editorFor("title"); ?>
				</div>
				<div class="form-group">
					<label>Link</label>
					<?php echo $model->form->editorFor("link"); ?>
				</div>
				<!--div class="form-group checkbox">
					<label>
						<?php echo $model->form->checkBoxFor("status", 1); ?> Active?
					</label>
				</div-->
			</div>
		</div>
		<div class="col-md-8">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<p><small>(ideal featured image size is 1920 x 300)</small></p>
					<?php 
					$img_path = "";
					if($model->banner->image != ""){
						$img_path = UPLOAD_URL . 'banners/' . $model->banner->image;
					}
					?>
					<p><input type="file" name="image" class='image' /></p>
					<?php if($model->banner->image != ""){ ?>
					<div class="well well-sm pull-left">
						<div id='image-preview'>
							<img src="<?php echo $img_path; ?>" width="100" height="100" />
							<br />
							<a href=<?= ADMIN_URL.'banners/delete_image/'.$model->banner->id;?> class="btn btn-default btn-xs">Delete</a>
						</div>
					</div>
					<?php } ?>
					<div id='preview-container'></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>

<script type='text/javascript'>
	$(document).ready(function() {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container").html(img);


			//$('#previewupload').attr('src', e.target.result);
			//$("a.passport-img").attr("href", e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

$("input.image").change(function(){
	readURL(this);
	$('#previewupload').show();
});	

});

</script>
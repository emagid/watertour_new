<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->banner->id;?>" />
  <input type="hidden" name="banner_type" value="1" />
	<div class="row">
		<div class="col-md-8">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Title</label>
					<?php echo $model->form->editorFor("title"); ?>
				</div>
				<div class="form-group">
					<label>Description</label>
					<?php echo $model->form->editorFor("description"); ?>
				</div>
<!--				<div class="form-group">-->
<!--					<label>URL</label>-->
<!--					--><?php //echo $model->form->editorFor("url"); ?>
<!--				</div>-->
<!--				<div class="form-group">-->
<!--					<label>Multi-Button Input</label><br>-->
<!--					<a id="add_desc" href="#">Add New Button</a>-->
<!--					<div id="content_box">-->
<!--						--><?//if(($content = json_decode($model->banner->options,true)) != null){
//							foreach($content['buttons'] as $value){?>
<!--								<div class="form-group">-->
<!--									<input type="text" name="button_title[]" placeholder="Button Title" value="--><?//=$value['title']?><!--">-->
<!--									<input type="text" name="button_url[]" placeholder="Button URL" value="--><?//=$value['url']?><!--"/>-->
<!--									<input type="number" name="button_order[]" placeholder="Enter display order" value="--><?//=$value['button_order']?><!--" ><br>-->
<!--									<a href="#" class="delete_content">Delete</a>-->
<!--								</div>-->
<!--							--><?//}?>
<!--						--><?//}?>
<!--					</div>-->
<!--				</div>-->
			</div>
		</div>
		<div class="col-md-16">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<p><small>(ideal "main banner" image size is 1200 x 540)</small></p>
					<?php 
					$img_path = "";
					if($model->banner->image != ""){
						$img_path = UPLOAD_URL . 'banners/' . $model->banner->image;
					}
					?>
					<p><input type="file" name="image" class='image' /></p>
					<?php if($model->banner->image != ""){ ?>
					<div class="well well-sm pull-left">
						<div id='image-preview'>
							<img src="<?php echo $img_path; ?>" width="600" height="270" />
							<br />
							<a href=<?= ADMIN_URL.'banners/main_delete_image/'.$model->banner->id;?> class="btn btn-default btn-xs">Delete</a>
							<input type="hidden" name="image" value="<?=$model->banner->image?>" />
						</div>
					</div>
					<?php } ?>
					<div id='preview-container'></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>

<script type='text/javascript'>
$(document).ready(function() {
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				var img = $("<img />");
				img.attr('src',e.target.result);
				img.attr('alt','Uploaded Image');
				img.attr("width",'600');
				img.attr('height','270');
				$("#preview-container").html(img);
			};

			$(input).parent().parent().find('input[type="hidden"]').remove();

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("input.image").change(function(){
		readURL(this);
		$('#previewupload').show();
	});
	$('#add_desc').on('click', function(){
		var content = $(
				'<div class="form-group"><input data-title-id="" type="text" name="button_title[]" placeholder="Button Title" value=""><input data-desc-id="" name="button_url[]" placeholder="Button URL" type="text"/><input type="number" name="content_order[]" value="" placeholder="Enter display order"><br><a href="#" class="delete_content">Delete</a></div>'
		);
		$('#content_box').append(content);
	});
	$(document).on('click','.delete_content', function(){
		$(this).parent().remove();
	});
});
</script>
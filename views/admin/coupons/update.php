<?
function dateFormatter($value,$format = 'm/d/Y h:iA'){
    return date($format,$value ? strtotime($value) : time());
}
?>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->coupon->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <input name="coupon_range" class="daterange" type="text" value="<?=$model->coupon->id > 0 ? dateFormatter($model->coupon->start_date) .' - '.dateFormatter($model->coupon->end_date) : ''?>"/>
                </div>
                <div class="form-group">
                    <label>Coupon Code</label>
                    <?php echo $model->form->editorFor("coupon_code"); ?>
                </div>
                <div class="form-group">
                    <label>Coupon Amount</label>
                    <?php echo $model->form->textBoxFor("coupon_amount",['type'=>'number','class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Coupon Type</label>
                    <?php echo $model->form->dropDownListFor("coupon_type",\Model\Coupon::$coupon_type,'',['class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Maximum Use</label>
                    <?php echo $model->form->editorFor("maximum_use"); ?>
                </div>
                <div class="form-group">
                    <label>Minimum Amount</label>
                    <?php echo $model->form->editorFor("minimum_amount"); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'coupon/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $(function() {
            $('.daterange').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->coupon->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",strtotime($model->coupon->start_date));?>",
                endDate: "<?php echo date("m/d/Y g:iA",strtotime($model->coupon->end_date));?>",
                <?php } ?>

            });
        });
    });
</script>
<?php if (count($model->coupons) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Name</th>
                <th width='20%'>Start Date</th>
                <th width='20%'>End Date</th>
                <th width='20%'>Coupon Code</th>
                <th width='20%'>Coupon Amount</th>
                <th width='20%'>Coupon Type</th>
                <th width='20%'>Maximum Use</th>
                <th width='20%'>Minimum Amount</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->coupons as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->start_date; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->end_date; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->coupon_code; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->coupon_amount; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo \Model\Coupon::$coupon_type[$obj->coupon_type]; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->maximum_use; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>"><?php echo $obj->minimum_amount; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>coupons/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'coupons';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
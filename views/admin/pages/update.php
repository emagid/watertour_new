<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->page->id?>"/>
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Title</label>
					<?php echo $model->form->editorFor("title"); ?>
				</div>
				<div class="form-group">
					<label>Content</label>
					<?php echo $model->form->textAreaFor("description", ["class"=>"ckeditor"]); ?>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="box">
				<h4>Meta Information</h4>
				<div class="form-group">
					<label>Slug</label>
					<?php echo $model->form->editorFor("slug"); ?>
				</div>
				<div class="form-group">
					<label>Meta title</label>
					<?php echo $model->form->editorFor("meta_title"); ?>
				</div>
				<div class="form-group">
					<label>Meta keywords</label>
					<?php echo $model->form->textAreaFor("meta_keywords"); ?>
				</div>
				<div class="form-group">
					<label>Meta description</label>
					<?php echo $model->form->textAreaFor("meta_description", ["rows"=>"3"]); ?>
				</div>
			</div>
			<div class="box">
				<div class="form-group">
					<label>Banner image</label>
					<p><small>(ideal banner image size is 1920 x 300)</small></p>
					<?php 
					$img_path = "";
					if($model->page->featured_image != ""){
						$img_path = UPLOAD_URL . 'pages/' . $model->page->featured_image;
					}
					?>
					<p><input type="file" name="featured_image" class='image' /></p>
					<?php if($model->page->featured_image != ""){ ?>
					<div class="well well-sm pull-left">
						<img src="<?php echo $img_path; ?>" width="100" />
						<br />
						<a href="<?= ADMIN_URL.'pages/delete_image/'.$model->page->id.'/?featured_image=1';?>" class="btn btn-default btn-xs">Delete</a>
					</div>
					<?php } ?>
					<div id='preview-container'></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="col-lg-24">
			<button type="submit" class="btn btn-lg btn-save">Save</button>
		</div>
	</div>
</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	$(document).ready(function() {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container").html(img);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
			$('#previewupload').show();
		});	

		$("input[name='title']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-')
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});

	});

</script>
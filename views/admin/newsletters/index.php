<?php if (count($model->newsletters) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Email</th>
                <th width='20%'>Coupon Code</th>
                <th width='20%'>Coupon Redeemed</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->newsletters as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>newsletters/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>newsletters/update/<?php echo $obj->id; ?>"><?php echo $obj->coupon_code; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>newsletters/update/<?php echo $obj->id; ?>"><?php echo $obj->coupon_redeemed; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>newsletters/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>newsletters/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'newsletters';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
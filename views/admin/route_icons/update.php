<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->route_icon->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Icon</label>
                    <p><input type="file" name="icon" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->route_icon->icon != "" && file_exists(UPLOAD_PATH . 'route_icons' . DS . $model->route_icon->icon)) {
                            $img_path = UPLOAD_URL . 'route_icons/' . $model->route_icon->icon;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'route_icons/delete_image/' . $model->route_icon->id; ?>?icon=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="icon"
                                       value="<?= $model->route_icon->icon ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'route_icon/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>
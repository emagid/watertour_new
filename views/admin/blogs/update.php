<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->blog->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Slug</label>
                    <?php echo $model->form->editorFor("slug"); ?>
                </div>
                <div class="form-group">
                    <label>Featured Image</label>
                    <p><input type="file" name="featured_image" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->blog->featured_image != "" && file_exists(UPLOAD_PATH . 'blogs' . DS . $model->blog->featured_image)) {
                            $img_path = UPLOAD_URL . 'blogs/' . $model->blog->featured_image;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'blogs/delete_image/' . $model->blog->id; ?>?featured_image=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="featured_image"
                                       value="<?= $model->blog->featured_image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Summary</label>
                    <?php echo $model->form->editorFor("summary"); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreafor("description",['class'=>'ckeditor']); ?>
                </div>
                <div class="form-group">
                    <label>Author</label>
                    <?php echo $model->form->editorFor("author"); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php echo $model->form->dropDownListFor("status",\Model\Blog::$activity,'',['class'=>'form-control']); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <h4>Meta</h4>

                <div class="form-group">
                    <label>Meta Title</label>
                    <?php echo $model->form->editorFor("meta_title"); ?>
                </div>
                <div class="form-group">
                    <label>Meta Keywords</label>
                    <?php echo $model->form->editorFor("meta_keywords"); ?>
                </div>
                <div class="form-group">
                    <label>Meta Description</label>
                    <?php echo $model->form->editorFor("meta_description"); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'blog/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>
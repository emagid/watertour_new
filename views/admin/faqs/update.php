<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->faq->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Question</label>
                    <?php echo $model->form->textAreaFor("question",['rows'=>2]); ?>
                </div>
                <div class="form-group">
                    <label>Answer</label>
                    <?php echo $model->form->textAreaFor("answer",['rows'=>5]); ?>
                </div>
                <div class="form-group">
                    <label>Faq Category</label>
                    <select name="faq_category_id" class="form-control">
                        <?foreach(\Model\Faq_Category::getList() as $item){?>
                            <option value="<?=$item->id?>" <?=$item->id == $model->faq->faq_category_id ? 'selected': ''?>><?=$item->name?></option>
                        <?}?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Display Order</label>
                    <ol class="sortable">
                        <?foreach(\Model\Faq::getList(['orderBy'=>'display_order']) as $item){?>
                            <li data-id="<?=$item->id?>"><?=$item->question?></li>
                        <?}?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'faq/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input.image").change(function () {
            readURL(this);
        });
        $('[name=review_type]').on('change',function(){
            var type = $(this).val();
            $.post('/admin/reviews/gri',{type:type},function(data){
                var json = $.parseJSON(data);
                var options = '';
                $.each(json,function(i,e){
                    var selected = '';
                    if(i == <?=$model->faq->review_id ? : 0?>){
                        selected = 'selected';
                    }
                    options += "<option value='"+i+"' "+selected+">"+e+"</option>";
                });
                $('[name=review_id]').empty().append(options);
            })
        }).trigger('change');
        $(".numeric").numericInput();
    });
</script>
<script>
    $(document).ready(function(){
        var adjustment;
        var serial = $('.sortable').sortable({
            group:'sortable',
            onDrop: function ($item, container, _super) {
                var data = serial.sortable("serialize").get();
                var jsonData = JSON.stringify(data,null,'');

//                $('.serialize-result').text(jsonData);
                $.post('/admin/faqs/updateOrder',{data:jsonData},function(){
                    console.log('done');
                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
    })
</script>
<?php if (count($model->faqs) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="1%">Question</th>
                <th width="1%">Answer</th>
                <th width="1%">Category</th>
                <th width="1%">Display Order</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->faqs as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>"><?php echo $obj->question; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>"><?php echo strReplace($obj->answer,20,'...'); ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>"><?php echo $obj->faq_category_id ? \Model\Faq_Category::getItem($obj->faq_category_id)->name : ''; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>"><?php echo $obj->display_order; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>faqs/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'faqs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>


<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->review->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <input type=hidden name="old_status" value="<?php echo $model->review->status; ?>"/>
    <?if($model->review->id){?>
        <input type=hidden name="create_date" value="<?=$model->review->create_date?>"/>
    <?}?>
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description"); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php echo $model->form->dropDownListFor("status",\Model\Review::$status,'',['class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Customer Name</label>
                    <?php echo $model->form->editorFor("customer_name"); ?>
                </div>
                <div class="form-group">
                    <label>City</label>
                    <?php echo $model->form->editorFor("city"); ?>
                </div>
                <div class="form-group">
                    <label>State</label>
                    <?php echo $model->form->dropDownListFor("state",get_states(),'',['class'=>'form-control']); ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <h4>Ratings</h4>

                <div class="form-group">
                    <label>Review Rating</label>
                    <input type="number" name="rating" class="form-control" value="<?=$model->review->rating ? : 3?>" min="1" max="5" step=".5" required/>
                </div>
                <div class="form-group">
                    <label>Helpful</label>
                    <input type="number" name="rating" class="form-control" value="<?=$model->review->helpful ? : 3?>" min="1" max="5" step=".5"/>
                    <!--<?php echo $model->form->editorFor("helpful"); ?>-->
                </div>
                <div class="form-group">
                    <label>Unhelpful</label>
                    <input type="number" name="rating" class="form-control" value="<?=$model->review->unhelpful ? : 3?>" min="1" max="5" step=".5"/>
                    <!--<?php echo $model->form->editorFor("unhelpful"); ?>-->
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <h4>Meta</h4>

                <div class="form-group">
                    <label>Review Type</label>
                    <?php echo $model->form->dropDownListFor("review_type",\Model\Review::$review_type,'',['class'=>'form-control','required'=>'required']); ?>
                </div>
                <div class="form-group">
                    <label>Review Item</label>
                    <?php echo $model->form->dropDownListFor("review_id",[],'',['class'=>'form-control','required'=>'required']); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'review/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("input.image").change(function () {
            readURL(this);
        });
        $('[name=review_type]').on('change',function(){
            var type = $(this).val();
            $.post('/admin/reviews/gri',{type:type},function(data){
                var json = $.parseJSON(data);
                var options = '';
                $.each(json,function(i,e){
                    var selected = '';
                    if(i == <?=$model->review->review_id ? : 0?>){
                        selected = 'selected';
                    }
                    options += "<option value='"+i+"' "+selected+">"+e+"</option>";
                });
                $('[name=review_id]').empty().append(options);
            })
        }).trigger('change');
        $(".numeric").numericInput();
    });
</script>
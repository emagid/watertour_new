<?php if (count($model->reviews) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="1%">Title</th>
                <th width="1%">Customer</th>
                <th width="1%">Review Type</th>
                <th width="1%">Review Item</th>
                <th width="1%">Rating</th>
                <th width="1%">Status</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->reviews as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->customer_name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo \Model\Review::$review_type[$obj->review_type]; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->getReviewItem()->name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->rating; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->getStatus(); ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>reviews/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'reviews';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>


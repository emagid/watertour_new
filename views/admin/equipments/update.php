<?$equip = \Model\Equipment::getList(['where'=>"id != {$model->equipment->id} and (category = 'Baby Seat' or category = 'Trailer')"]);
$categories = \Model\Equipment::$category;
foreach($equip as $e){
    unset($categories[array_search($e->category,$categories)]);
}
?>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->equipment->id ?>"/>
    <input type="hidden" name="old_category" value="<?php echo $model->equipment->category ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description", ['rows' => 6]); ?>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" class="form-control">
                        <?foreach($categories as $category){?>
                            <option value="<?=$category?>" <?=$category == $model->equipment->category ? 'selected':''?>><?=$category?></option>
                        <?}?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Display in</label>
                    <?php echo $model->form->dropDownListFor("displayable", \Model\Equipment::$displayable,'',['class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Featured Image</label>

                    <p>
                        <small>(ideal banner image size is 1920 x 300)</small>
                    </p>
                    <?php
                    $img_path = "";
                    if ($model->equipment->featured_image != "") {
                        $img_path = UPLOAD_URL . 'equipments/' . $model->equipment->featured_image;
                    }
                    ?>
                    <p><input type="file" name="featured_image" class='image'/></p>
                    <?php if ($model->equipment->featured_image != "") { ?>
                        <div class="well well-sm pull-left">
                            <img src="<?php echo $img_path; ?>" width="100"/>
                            <br/>
                            <a href="<?= ADMIN_URL . 'equipments/delete_image/' . $model->equipment->id . '/?featured_image=1'; ?>"
                               class="btn btn-default btn-xs">Delete</a>
                            <input type="hidden" name="featured_image"
                                   value="<?= $model->equipment->featured_image ?>"/>
                        </div>
                    <?php } ?>
                    <div id='preview-container'></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box">
                <h4>Price</h4>

                <? $rp = [];
                if($model->equipment->rental_price){
                    $rp = json_decode($model->equipment->rental_price,true);
                }
                foreach (\Model\Rental::getList(['orderBy' => 'duration']) as $item) {
                    $val = array_key_exists($item->id,$rp) ? $rp[$item->id]: ''?>
                    <div class="form-group">
                        <label><?= $item->getName() ?></label>
                        <?php echo $model->form->textBoxFor("rental_id$item->id", ['class' => 'numeric','value'=>$val]); ?>
                    </div>
                <? } ?>
<!--                        <div class="col-xs-24">-->
<!--                            <label>Price</label>-->
<!--                            --><?php //echo $model->form->textBoxFor("adult_price",['class'=>'numeric']); ?>
<!--                        </div>-->
<!--                        <div class="col-xs-12">-->
<!--                            <label>Kids Price</label>-->
<!--                            --><?php //echo $model->form->textBoxFor("kid_price",['class'=>'numeric']); ?>
<!--                        </div>-->
<!--                <div class="form-group">-->
<!--                    <div class="row">-->
<!--                        <div class="col-xs-19">-->
<!--                            <label>Discount Amount (if applicable)</label>-->
<!--                            --><?php //echo $model->form->textBoxFor("discount_amount",['class'=>'numeric']); ?>
<!--                        </div>-->
<!--                        <div class="col-xs-5">-->
<!--                            <label>Discount Type</label>-->
<!--                            --><?php //echo $model->form->dropDownListFor("discount_type", \Model\Equipment::$discount_type,'',['class'=>'form-control']); ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>

        <div class="col-md-12">
            <div class="box">
                <h4>Meta Information</h4>

                <div class="form-group">
                    <label>Slug</label>
                    <?php echo $model->form->editorFor("slug"); ?>
                </div>
                <div class="form-group">
                    <label>Meta title</label>
                    <?php echo $model->form->editorFor("meta_title"); ?>
                </div>
                <div class="form-group">
                    <label>Meta keywords</label>
                    <?php echo $model->form->textAreaFor("meta_keywords"); ?>
                </div>
                <div class="form-group">
                    <label>Meta description</label>
                    <?php echo $model->form->textAreaFor("meta_description", ["rows" => "3"]); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <button type="submit" class="btn btn-lg btn-save">Save</button>
        </div>
    </div>

</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $('.numeric').numericInput();
    });

</script>
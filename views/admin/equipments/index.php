<?php if (count($model->equipments) > 0): ?>
    <div class="col-md-8">
        <div class="box">
            <label>Adult Default</label>
            <select id="adult_default" class="form-control" data-category="Adult">
                <?$aDefault = \Model\Equipment::getItem(null,['where'=>"category='Adult' and default_rental = 1"]); $selected = $aDefault ? $aDefault->id: null?>
                <option disabled value="-1" <?=$selected == null ? 'selected': ''?>>Select a Default</option>
                <?foreach(\Model\Equipment::getList(['where'=>"category='Adult'"]) as $item){?>
                    <option value="<?=$item->id?>" <?=$selected == $item->id ? 'selected': ''?>><?=$item->name?> ($<?=number_format($item->getPrice(),2)?>)</option>
                <?}?>
            </select>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box">
            <label>Kid Default</label>
            <select id="kid_default" class="form-control" data-category="Kid">
                <?$kDefault = \Model\Equipment::getItem(null,['where'=>"category='Kid' and default_rental = 1"]); $selected = $kDefault ? $kDefault->id: null?>
                <option disabled value="-1" <?=$selected == null ? 'selected': ''?>>Select a Default</option>
                <?foreach(\Model\Equipment::getList(['where'=>"category='Kid'"]) as $item){?>
                    <option value="<?=$item->id?>" <?=$selected == $item->id ? 'selected': ''?>><?=$item->name?> ($<?=number_format($item->getPrice(),2)?>)</option>
                <?}?>
            </select>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box">
            <label>Tandem Default</label>
            <select id="tandem_default" class="form-control" data-category="Tandem">
                <?$tDefault = \Model\Equipment::getItem(null,['where'=>"category='Tandem' and default_rental = 1"]); $selected = $tDefault ? $tDefault->id: null?>
                <option disabled value="-1" <?=$selected == null ? 'selected': ''?>>Select a Default</option>
                <?foreach(\Model\Equipment::getList(['where'=>"category='Tandem'"]) as $item){?>
                    <option value="<?=$item->id?>" <?=$selected == $item->id ? 'selected': ''?>><?=$item->name?> ($<?=number_format($item->getPrice(),2)?>)</option>
                <?}?>
            </select>
        </div>
    </div>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%"></th>
                <th width="10%">Name</th>
                <th width="10%">Category</th>
                <th width="10%">Slug</th>
                <th width="10%">Price</th>
<!--                <th width="10%">Price (Kids)</th>-->
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->equipments as $equipment):
                $adultPriceStr = '$'.number_format($equipment->adult_price,2);
                $kidPriceStr = '$'.number_format($equipment->kid_price,2);
                if($equipment->discount_amount > 0){
                    $adultPriceStr .= ' ($'.number_format($equipment->getPrice('adult'),2).')';
                    $kidPriceStr .= ' ($'.number_format($equipment->getPrice('kid'),2).')';
                }
                ?>
                <tr>
                    <td>
                        <img src="<?=UPLOAD_URL.'equipments/'.$equipment->featured_image?>" style="width: 100px;"/>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>equipments/update/<?php echo $equipment->id; ?>"><?php echo $equipment->name; ?></a>
                    </td>
                    <td><?php echo $equipment->category; ?></td>
                    <td><?php echo $equipment->slug; ?></td>
                    <td><?php echo $adultPriceStr;?></td>
<!--                    <td>--><?php //echo $kidPriceStr; ?><!--</td>-->
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>equipments/update/<?php echo $equipment->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>equipments/delete/<?php echo $equipment->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    $('#adult_default, #kid_default, #tandem_default').on('change',function(){
        var category = $(this).data('category');
        var id = $(this).val();
        var data = {category:category,id:id};
        $.post('<?=ADMIN_URL.'equipments/updateDefault'?>',data);
    });

    var site_url = '<?= ADMIN_URL.'equipments/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>


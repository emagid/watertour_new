<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->contacts->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Full Name</label>
                    <?php echo $model->form->editorFor("full_name"); ?>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <?php echo $model->form->editorFor("email"); ?>
                </div>

                <div class="form-group">
                    <label>Subject</label>
                    <?php echo $model->form->editorFor("subject"); ?>
                </div>

                <div class="form-group">
                    <label>Message</label>
                    <?php echo $model->form->editorFor("message"); ?>
                </div>

            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'contacts/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>
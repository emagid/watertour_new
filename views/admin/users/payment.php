<?php
$states = get_states();
unset($states['GU']);unset($states['PR']);unset($states['VI']);
$months = range(1,12);
$curr_year = (int)date("Y",time());
$years = range($curr_year,$curr_year+8);
?>
<form method="post" action="<?php echo ADMIN_URL."users/payment";?>" id="payment-form">
  <input type="hidden" name="id" value="<?php echo $model->payment_profile->id;?>" />
  <input type="hidden" name="user_id" value="<?php echo $model->user->id;?>" />
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <h4>Billing Information</h4>
        <div class="form-group">
          <label>Label</label>
          <?php echo $model->form->editorFor("label");?>
        </div>
        <div class="form-group">
          <label>First Name</label>
          <?php echo $model->form->editorFor("first_name");?>
        </div>
         <div class="form-group">
          <label>Last Name</label>
          <?php echo $model->form->editorFor("last_name");?>
        </div>
         <div class="form-group">
          <label>Phone Number</label>
          <?php echo $model->form->editorFor("phone");?>    
        </div>
         <div class="form-group">
          <label>Address</label>
          <?php echo $model->form->editorFor("address");?>
        </div>
         <div class="form-group">
          <label>Address 2</label>
          <?php echo $model->form->editorFor("address2");?>
        </div>
        <div class="form-group">
          <label>City</label>
          <?php echo $model->form->editorFor("city");?>
        </div>
         <div class="form-group">
          <label>State</label>
          <?php echo $model->form->dropDownListFor("state",$states);?>
        </div>
         <div class="form-group">
          <label>Zip Code</label>
          <?php echo $model->form->editorFor("zip");?>
        </div>
         <div class="form-group">
          <label>Country</label>
          <?php echo $model->form->dropDownListFor("country", get_countries());?>
        </div>
      </div>
    </div>
    
	<div class="col-md-12">
		<div class="box">
			<h4>Credit Card Information</h4>
			<div class="form-group">
				<label>Credit Card Number</label>
				<?php echo $model->form->editorFor("cc_number");?>	
			</div>
			<div class="form-group">
			    <label>Exp Month</label>
			    <?php echo $model->form->dropDownListFor("cc_expiration_month",$months);?>
			</div>
			<div class="form-group">
				<label>Exp Year</label>
				<?php echo $model->form->dropDownListFor("cc_expiration_year",$years);?>
			</div>
			<div class="form-group">
			  	<label>Credit Card Code</label>
			  	<?php echo $model->form->editorFor("cc_ccv");?>
			</div>
		</div>
	</div>
  </div>

  <button type="submit" class="btn btn-save">Save</button>
</form>
<?php footer();?>
<script type="text/javascript">
  
    jQuery.validator.addMethod("validate_phone",function(value,element) {
    var patt = new RegExp(/^\(\d{3}\)\d{3}-\d{4}$/);
    if(value!=="" && !patt.test(value)) {
      return false;
    } else {return true;}
  },"invalid phone number format.");
  
  $("#payment-form").validate({
  onkeyup: false,
  onfocusout: false,
  onclick: false,
  focusInvalid :false,
  rules: {
    address: {required:true},
    zip: {required:true},
    state: {required:true},
    phone: {validate_phone:true},
    country: {required:true},
    city: {required:true}
    // cc_number: {required:true,maxlength:16,minlength:13},
    // cc_code: {required:true},
    // cc_expiration_month: {required:true},
    // cc_expiration_year: {required:true},
  },
});

$("input[name='phone']").inputmask("(999)999-9999");
</script>

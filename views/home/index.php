<div class="pageWrapper homePageWrapper">
    <section class="" style="display:none;">
        <div class="heroContainer">
            <div class="heroText">  
                <h1 class="valB">Stunning New York Views From The Water</h1>
            </div>
            <div class="heroSlideshow">
                <!--<//?foreach($model->banners as $banner){?>-->

            </
            /?}//?>
        </div>
</div>
</section>
<div class="homepage-hero-module">
    <div class="video-container" style="background-image:url(<?= FRONT_IMG ?>watertour_bg.jpg);">
        <div class="filter"></div>
<!--
        <video autoplay loop class="fillWidth">
            <source src="<?= FRONT_IMG ?>gonyloop.mp4" type="video/mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="<?= FRONT_IMG ?>gonyloop.webm" type="video/webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
-->
                            <div class="heroText">  
                <h1 class="valB">Stunning New York Views From The Water</h1>
                <? include(__DIR__ . '/../../templates/biketour/bookingTemplate.php'); ?>
            </div>
        <div class="poster hidden">
            <img src="<?= FRONT_IMG ?>sol.jpg" alt="">
        </div>
    </div>
</div>
<section class="midContainer row homepageTourInfo">
    <div class="home_columns">
        <div class="column_reviews">
            <div class="choose_us_left">
                <h5>You Can't Miss This Exciting Ride</h5>
                <p>Experience magical views of New York City’s Statue of Liberty, Brooklyn Bridge, One World Trade Center, and Empire State Building, all aboard our top-rated water tours.</p>
            </div>
        </div>
    </div>
</section>

<section class="main_packages">
    <div class="main_packages_wrapper">
        <? for($i = 0; $i <= 1; $i++) { ?>
            <? $package = $model->featuredPackages[$i];?>
        <div class="main_packages_child">
            <div class="main_packages_child_top" style="background-image:url(<?= FRONT_IMG.$package->featuredImage()?>)">
                <h3><?=$package->name?></h3>
                <a class="btn primaryBtn" href="/packages/package/<?=$package->slug?>"><p>See the Schedule</p></a>
                
                <div class="main_packages_pricing">
                    <div class="main_package_adult_pricing">
                        <h6>Adults</h6>
                        <div class="adult_pricing" id="walkins">
                            <p>Walk-in <br> <span>$<?=(integer)$package->adult_walkin?></span></p>
                        </div>
                        <div class="adult_pricing" id="online">
                            <p>Online <br> <span>$<?=(integer)$package->adults_price?></span></p>
                        </div>
                    </div>
                    <div class="main_package_kid_pricing">
                        <h6>Kids</h6>
                        <div class="kid_pricing" id="walkins">
                            <p>Walk-in <br> <span>$<?=(integer)$package->kid_walkin?></span></p>
                        </div>
                        <div class="kid_pricing" id="online">
                            <p>Online  <br> <span>$<?=(integer)$package->kids_price?></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? } ?>
    </div>
</section>

<section class="homepageTours" style="display:none;">

    <div class="offer_row">
        <h3>Special Offers in <?=\Carbon\Carbon::now()->format('F')?></h3>
        <!-- #region Jssor Slider Begin -->
        <script type="text/javascript">
            jssor_1_slider_init = function () {

                var jssor_1_options = {
                    $AutoPlay: 1,
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $ThumbnailNavigatorOptions: {
                        $Class: $JssorThumbnailNavigator$,
                        $Cols: 4,
                        $SpacingX: 4,
                        $SpacingY: 4,
                        $Orientation: 2,
                        $Align: 0
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*responsive code begin*/
                /*remove responsive code if you don't want the slider scales while window resizing*/
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 1280);
                        jssor_1_slider.$ScaleWidth(refSize);
                    }
                    else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }

                ScaleSlider();
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*responsive code end*/
            };
        </script>
        <div id="jssor_1"
             style="position:relative;margin:0 auto;top:0px;left:0px;width:810px;height:300px;overflow:hidden;visibility:hidden;background-color:#000000;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides"
                 style="cursor:default;position:relative;top:0px;left:0px;width:600px;height:300px;overflow:hidden;">
                <?foreach ($model->featuredTours as $tour){?>
                    <div>
                        <a href="/tours/tour/<?=$tour->slug?>">
                            <img data-u="image" src="<?= UPLOAD_URL .'tours/'.$tour->featuredImage()?>"/>
                        </a>
                        <div data-u="thumb">
                            <img class="i" src="<?= UPLOAD_URL .'tours/'.$tour->featuredImage()?>"/>
                            <div class="c"
                                 style="text-align:center;top:20px;left:0;right:0;margin-left:auto;margin-right:0;"><?=$tour->name?>
                            </div>
                        </div>
                    </div>
                <?}?>
            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort11"
                 style="position:absolute;right:5px;top:0px;font-family:Arial, Helvetica, sans-serif;-moz-user-select:none;-webkit-user-select:none;-ms-user-select:none;user-select:none;width:200px;height:300px;"
                 data-autocenter="2">
                <!-- Thumbnail Item Skin Begin -->
                <div data-u="slides" style="cursor: default;">
                    <div data-u="prototype" class="p">
                        <div data-u="thumbnailtemplate" class="tp"></div>
                    </div>
                </div>
                <!-- Thumbnail Item Skin End -->
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora02l" style="top:0px;left:8px;width:55px;height:55px;"
                  data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora02r" style="top:0px;right:218px;width:55px;height:55px;"
                  data-autocenter="2"></span>
        </div>
        <script type="text/javascript">jssor_1_slider_init();</script>
        <!-- #endregion Jssor Slider End -->
    </div>
<!--     <div class="row_of_perks">
        <div class="perk_row">
            <ul>
                <li><p>Exciting perks<br> we’re pleased to<br> offer you</p></li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_tix.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_tix.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_tix.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_tix.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_poncho.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_poncho.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_poncho.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_poncho.png">
                </li>
                <li>
                    <img src="<//?= FRONT_IMG ?>icon_poncho.png">
                </li>



            </ul>
        </div>
    </div> -->

    <?if ($model->featuredTours){?>
        <div class="row_of_attractions" >
            <h5>Free attraction passes come with every tour</h5>
            <h4>Get Exclusive Prices on some of NY's Top Attractions</h4>
            <div class="featuredTours">
                <?foreach ($model->featuredTours as $featuredTour){?>
                    <div class="featuredTour">
                        <div class="imgContainer">
                            <img src="<?=$featuredTour->featuredImage() ? UPLOAD_URL.'tours/'.$featuredTour->featuredImage(): FRONT_ASSETS.'img/topview_tour.jpg'?>"/>
                            <div class="ft-name"><?=$featuredTour->name?></div>
                        </div>
                        <div class="infoContainer">
                            <div class="featuredDesc">
                                <div class="ft-desc"><?=$featuredTour->description?></div>
                            </div>


                            <div class="comparativePricingFormat row imageOverlay home-pricing">
                            <div class="col">
                                <div class="circle_pretext">
                                    <p>Regular Price</p>
                                </div>
                                <div class="circle walkin">
                                    <h5 class="val">$<?=number_format($featuredTour->walkin_price,0)?></h5>
                                </div>
                            </div>
                            <div class="col">
                                <div class="circle_pretext">
                                    <p>Online Special</p>
                                </div>                          
                                <div class="circle online">
                                    <h5 class="val">$<?=number_format($featuredTour->price,0)?></h5>
                                </div>
                            </div>
                        </div>


                        </div>
                    </div>
                <?}?>
            </div>
        </div>
    <?}?>

</section>

<section class="promo_home" style="background-image:url('<?= FRONT_IMG ?>cruise_selfie.jpg')">
    <div class="promo_home_text">
        <h5 class="valB">Special Promo</h5>
        <h1 class="valB">Reserve your NYC water tour online to save up to 50%</h1>
        <a class="btn primaryBtn" href="/packages"><p>Book Now</p></a>
    </div>
</section>

<section class="lastSection" style="background:#f5f5f5;">
    <div class="primaryContentWidth row row_of_3">
        <div class="col">
            <img src="<?= FRONT_IMG ?>guidesIcon.png">
            <h5><a href="/blog">NYC Guide</a></h5>
            <p>Find the best places to go and other insights from our team of native New Yorkers</p>
        </div>
        <div class="col">
            <img src="<?= FRONT_IMG ?>routesIcon.png">
            <h5><a href="/nycguide">Routes</a></h5>
            <p>Choose the tour that's right for your unique NYC adventure</p>
        </div>
        <div class="col">
            <img src="<?= FRONT_IMG ?>subscribeIcon.png">
            <h5><a href="/attractions">Things To Do In NYC</a></h5>
            <p>Don't leave town until you've seen these essential NYC destinations</p>
        </div>

    </div>
</section>

</div>



<script>
    $(document).ready(function(){
        $('.nsSub').on('submit',function(e){
            e.preventDefault();
            var email = $('[name=mail]');
            $.post($(this).attr('action'),{email:email.val()},function(data){
                alert(data.msg);
            })
        });
        $('.selectPackage').on('click',function(){
            $($('.reservationTime')[0]).val($(this).data('package_id')).trigger('change');

        })
    })
</script>

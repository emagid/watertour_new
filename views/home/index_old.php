<div class="pageWrapper homePageWrapper">
    <? display_notification() ?>
    <div class="minorGutter_content_width">
        <div id="fullView_slider">
            <?foreach($model->banners as $banner){?>
            <div class="fullView_slide">
                <a href="<?=$banner->url?>">
                    <div class="media" style="background-image:url(<?=UPLOAD_URL.'banners/'.$banner->image?>)">
                    </div>
                </a>
                <div class="abs_trans_center messaging">
                    <h1 class="ctado"><?=$banner->title?></h1>
                    <h1 class="as_m"><?=$banner->description?></h1>
                    <?if(json_decode($banner->options,true) != null){?>
                        <div class="row">
                        <?foreach(json_decode($banner->options,true)['buttons'] as $selectionBanner){?>
                            <a class="btn btn_oreo" href="<?=$selectionBanner['url']?>"><?=$selectionBanner['title']?></a>
                        <?}?>
                        </div>
                    <?}?>
                </div>
            </div>
            <?}?>
        </div>
    </div>
    <div class="content_width_1000 tabbedView">
        <?$arr = ['mens','womens','men','women']?>
        <div class="tabController line_tabs row row_of_2" style="display:none!important;visibility: hidden!important;opacity:0!important;pointer-events: none!important;">
            <?$active = true;
            foreach($model->mainCategories as $category){
                if(in_array(strtolower($category->name),$arr)){?>
                <a class="tab line_tab col <?=$active?'active':''?>"  data-tab_title="<?=strtolower($category->name)?>">
                    <p class="as_l"><?=$category->name?></p>
                </a>
            <?$active = false;}}?>
        </div>

        <div class="tab_contents">
            <?$active = true;
            foreach($model->mainCategories as $category){
                if(in_array(strtolower($category->name),$arr)){?>
            <div class="tab_content tab_content_<?=strtolower($category->name)?> <?=$active?'tab_content_active':''?>" <?=!$active?'style="display:none"':''?>>
                <div class="row row_of_2">
                    <?foreach($model->categoryBanners as $categoryBanner){
                        if($category->id == json_decode($categoryBanner->options,true)['category']){?>
                        <div class="col">
                            <a href="<?=$categoryBanner->url?>">
                                <div class="image_mask">
                                    <img src="<?=UPLOAD_URL.'banners/'.$categoryBanner->image?>">
                                </div>
                                <div class="btn btn_white abs_trans_center btn_border_texture">
                                    <p><?=$categoryBanner->title?></p>
                                </div>
                            </a>
                        </div>
                        <?}?>
                    <?}?>
                </div>
            </div>
            <?$active=false;}}?>
        </div>
    </div>
    <div class="noGutter_content_width l_bend_ui row row_of_2">
        <?$alternator = true;
        foreach($model->selectionBanners as $selectionBanner){
            $selectCategory = \Model\Category::getItem(json_decode($selectionBanner->options,true)['category']);
            $count = count($selectCategory->getProductCategory());
            $newCount = count(json_decode($selectCategory->new_arrivals,2));
            $alternator ? $class = 'upCopy': $class = 'downCopy'?>
            <div class="<?=$class?> col">
                <a href="<?=$selectionBanner->url?>">
                    <div class="mediaWrapper">
                        <div class="media" style="background-image:url(<?=UPLOAD_URL.'banners/'.$selectionBanner->image?>)"></div>
                        <div class="btn btn_white">
                            <p><?=$selectionBanner->title?></p>
                        </div>
                    </div>
                    <div class="copyWrapper">
                        <div class="abs_trans_center">
                            <?if($alternator){?>
                                <p class="lyon_r"><?=$selectionBanner->description?></p>
                                <h2 class="as_r count_ui"><?=$count?> 
                                    <span>
                                        <? 
                                            if($count > 1 || $count == 0){ 
                                                $itemString = 'items';
                                            }else{
                                                $itemString = 'item';
                                            } 
                                            echo($itemString);
                                        ?>
                                    </span>
                                </h2>
                            <?} else {?>
                                <p class="lyon_r"><?=$selectionBanner->description?></p>
                                <h2 class="as_r count_ui"><?=$newCount?> 
                                    <span>
                                        <i>New</i>
                                        <? 
                                            if($newCount > 1 || $newCount == 0){ 
                                                $itemString = 'items';
                                            }else{
                                                $itemString = 'item';
                                            } 
                                            echo($itemString);
                                        ?>
                                    </span>
                                    
                                </h2>
                            <?}?>
                        </div>
                    </div>
                </a>
            </div>
            <!--<div class="downCopy col">
            <a>
                <div class="mediaWrapper">
                    <div class="media" style="background-image:url(<?/*=FRONT_IMG*/?>collection_image_5.jpg)"></div>
                    <div class="btn btn_border_texture btn_white">
                        <p>Shop Snakes</p>
                    </div>
                </div>
                <div class="copyWrapper">
                    <div class="abs_trans_center">
                         <p class="lyon_r">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                        <h2 class="as_r count_ui">15 <span>items</span></h2>
                    </div>
                       
                        
                </div>
            </a>
        </div>-->
        <?$alternator ? $alternator = false: $alternator = true;}?>
    </div>
<!--     <div class="noGutter_content_width row row_of_3 home_trio">
        <div class="col most_loved">
            <div class="abs_content">
                <h3 class="as_r color_underline_header">Most Loved</h3>
                <div class="mostLovedSlider">
                    <div>
                        <a>
                            <div class="mediaWrapper image_mask">
                                <img src="<?=FRONT_IMG?>shoe_1.jpg">
                            </div>
                            <div class="data">
                                <p class="favoritesCount"><icon class="heart"></icon>140</p>
                                <h4 class="as_r product_name">Jett Classic Suede</h4>
                                <h4 class="as_r product_price">$288.00</h4>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a>
                            <div class="mediaWrapper image_mask">
                                <img src="<?=FRONT_IMG?>shoe_2.jpg">
                            </div>
                            <div class="data">
                                <p class="favoritesCount"><icon class="heart"></icon>118</p>
                                <h4 class="as_r product_name">Jett Open Air Aztec</h4>
                                <h4 class="as_r product_price">$440.00</h4>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a>
                            <div class="mediaWrapper image_mask">
                                <img src="<?=FRONT_IMG?>shoe_3.jpg">
                            </div>
                            <div class="data">
                                <p class="favoritesCount"><icon class="heart"></icon>102</p>
                                <h4 class="as_r product_name">Chloe White Snake</h4>
                                <h4 class="as_r product_price">$440.00</h4>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a>
                            <div class="mediaWrapper image_mask">
                                <img src="<?=FRONT_IMG?>shoe_exotic_womens.jpg">
                            </div>
                            <div class="data">
                                <p class="favoritesCount"><icon class="heart"></icon>77</p>
                                <h4 class="as_r product_name">Jett Metallic Dots</h4>
                                <h4 class="as_r product_price">$550.00</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col instagram_look">
            <div class="abs_content">
                <div class="dark_header">
                    <icon style="background-image:url(<?=FRONT_IMG?>instagram_icon.png)"></icon>
                    <p>shop <span>watertour</span> instagram</p>
                </div>
                <div class="insta_slider">
                    <div>
                        <a>
                            <div class="mediaWrapper">
                                <div class="media" style="background-image:url(<?=FRONT_IMG?>instagram_photo_1.jpg)"></div>
                            </div>
                            <div class="btn btn_oreo">Shop the Look</div>
                        </a>
                    </div>
                    <div>
                        <a>
                            <div class="mediaWrapper">
                                <div class="media" style="background-image:url(<?=FRONT_IMG?>instagram_photo_2.jpg)"></div>
                            </div>
                            <div class="btn btn_oreo">Shop the Look</div>
                        </a>
                    </div>
                    <div>
                        <a>
                            <div class="mediaWrapper">
                                <div class="media" style="background-image:url(<?=FRONT_IMG?>instagram_photo_3.jpg)"></div>
                            </div>
                            <div class="btn btn_oreo">Shop the Look</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col clearance_code">
            <div class="abs_content">
                <a>
                    <p class="lyon_r">take an additional</p>
                    <div class="big_number_ui row row_of_2">
                        <div class="col">30</div>
                        <div class="col">
                            <h2>%</h2>
                            <h4>off</h4>
                        </div>
                    </div>
                    <p class="lyon_r">all clearance styles<br>with code <span>spring</span> at checkout</p>
                    <div class="inv_btn">
                        <p>Shop Clearance</p>
                    </div>
                </a>
            </div>
        </div>
    </div> -->
    <div class="noGutter_content_width row row_of_2 footerCollections">
        <h3 class="cal_r">Featured <span>Collaborations</span></h3>
        <?foreach($model->specialityBanners as $specialityBanner){?>
        <div class="col">
            <a href="<?=$specialityBanner->url?>">
                <div class="media" style="background-image:url(<?=UPLOAD_URL.'banners/'.$specialityBanner->image?>)"></div>
                <div class="btn btn_white abs_trans_center btn_border_texture">
                    <p><?=$specialityBanner->title?></p>
                </div>
            </a>
        </div>
        <?}?>
    </div>
</div>
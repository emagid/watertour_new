<div class="pageWrapper toursIndexPageWrapper">

	<section class="topSection">

		        <div class="heroContainer">
            <div class="loader-container">
                <div class="loader-inner">
                </div>
            </div>
            <div class="heroText">
                <h1 class="valB">Your NYC Adventure starts Here</h1>


                <? include(__DIR__ . '/../../templates/biketour/bookingTemplate.php'); ?>
            </div>
            <div class="heroSlideshow">
                <!--<//?foreach($model->banners as $banner){?>-->
                <div class="slide">
                    <!-- 						<div class="media" style="background-image:url('<//?= UPLOAD_URL.'banners/'.$banner->image ?>');"></div> -->
                    <!-- static insert -->
                    <div class="media" style="background-image:url('<?= FRONT_IMG ?>skyline.jpg');"></div>
                </div>
            </
            /?}//?>
        		</div>
			</div>
		</div>
	</section>

	<section class="tour_explore">
		<div class="tour_container">
			<div class="tour_header">
				<h3 class="valB valUpper">Neighborhoods</h3>
				<h1 class="valB">Explore</h1>
			</div>

            <?$n = isset($_GET['n']) ? $_GET['n']: null?>
			<div class="tour_locals">
				<div>
                    <?$_GET['n'] = 1?>
                    <a href="/tours?<?=http_build_query($_GET)?>">
                        <img src="<?= FRONT_IMG ?>tour_midtown.jpg">
                    </a>
                </div>
                <div>
                    <?$_GET['n'] = 2?>
                    <a href="/tours?<?=http_build_query($_GET)?>">
                        <img src="<?= FRONT_IMG ?>tour_downtown.jpg">
                    </a>
                </div>
                <div>
                    <?$_GET['n'] = 3?>
                    <a href="/tours?<?=http_build_query($_GET)?>">
                        <img src="<?= FRONT_IMG ?>tour_brooklyn.jpg">
                    </a>
                </div>
			</div>
            <?if(!$n){ unset($_GET['n']); } else {$_GET['n'] = $n;}?>

			<div class="tour_subscribe">
				<p class="valB">Join our mailing list to get updates on new tours and offers from Topview Tours</p>
				<h3 class="valB valUpper">AND GET A 10% DISCOUNT CODE TO YOU SENT NOW.</h3>
				<div class="subscribe_form">
		            <form action="/newsletter/subscribe" method="post" enctype="text/plain" class="nsSub">
		                <input type="text" name="mail" placeholder="Email address" required>
		                <input type="submit" value="Let's Go!">
		            </form>
		        </div>
			</div>
		</div>
	</section>
	<section class="middleSection toursGrid row row_of_2 toursGridSimplified">

        <?if(count($model->featured)){?>
            <div class="tours_featured">
                <div class="row_of_attractions">
                    <h5>Spots are limited - Book Now</h5>
                    <h4><?=\Carbon\Carbon::now()->format('F Y')?>'s Featured Tours</h4>

                    <div class="featuredTours">
                        <?foreach ($model->featured as $tour){?>
                            <a href="/tours/tour/<?=$tour->slug?>">
                            <div class="featuredTour">
                                <div class="imgContainer">
                                    <img src="<?=$tour->featuredImage() ? UPLOAD_URL.'tours/'.$tour->featuredImage() : FRONT_ASSETS.'img/topview_tour.jpg'?>"/>
                                    <div class="ft-name"><?=$tour->name?></div>
                                </div>
                                <div class="infoContainer">
                                    <div class="featuredDesc">
                                        <div class="ft-desc"><?=$tour->description?></div>
                                    </div>

                            <div class="comparativePricingFormat row imageOverlay home-pricing">
                            <div class="col">
                                <div class="circle_pretext">
                                    <p>Regular Price</p>
                                </div>
                                <div class="circle walkin">
                                    <h5 class="val">$<?=number_format($tour->walkin_price,0)?></h5>
                                </div>
                            </div>
                            <div class="col">
                                <div class="circle_pretext">
                                    <p>Online Special</p>
                                </div>                          
                                <div class="circle online">
                                    <h5 class="val">$<?=number_format($tour->price,0)?></h5>
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                            </a>
                        <?}?>
                    </div>
                    <img src="<?= FRONT_IMG ?>">
<!--                    <img src="--><?//= FRONT_IMG ?><!--attraction_example.png">-->
                </div>
            </div>
        <?}?>

		<div class="filter_bar">
			<div class="filter_left">
				<h3>All Tours</h3>
			</div>
            <div class="filter_right">
                <?$d = isset($_GET['d']) ? $_GET['d']: null?>
                <?$_GET['d'] = 1?>
                <a href="/tours?<?=http_build_query($_GET)?>"><button>1 Day</button></a>
                <?$_GET['d'] = 2?>
                <a href="/tours?<?=http_build_query($_GET)?>"><button>2 Days</button></a>
                <?$_GET['d'] = 3?>
                <a href="/tours?<?=http_build_query($_GET)?>"><button>3 Days</button></a>
                <?if(!$d){ unset($_GET['d']); } else {$_GET['d'] = $d;}?>
            
	
						<div class="">
			<?if(isset($_GET['o']) && $_GET['o'] == 'htl'){
				$_GET['o'] = 'lth'?>
				<a class="faux_dropdown highToLow" href="/tours?<?=http_build_query($_GET)?>"><p>Price: <span>High to Low</span></p><icon class="dropdownArrow"></icon></a>
			<?}else{
				$_GET['o'] = 'htl'?>
				<a class="faux_dropdown lowToHigh" href="/tours?<?=http_build_query($_GET)?>"><p>Price: <span>Low to High</span></p><icon class="dropdownArrow"></icon></a>
			<?}?>
        </div>
			</div>
		</div>
		<? if ($model->tours) {
			foreach ($model->tours as $tour) {?>
				<div class="col">
					<img src="<?= UPLOAD_URL.'tours/'.$tour->featuredImage() ?>">
					<a href="<?='/tours/tour/'.$tour->slug?>">
						<h4 class="valB"><?=$tour->name?></h4>

						<!-- <p class="val"><//?=$tour->summary && (strlen($tour->summary) > 200) ? substr($tour->summary,0,220).'...' : $tour->summary;?></p> -->
					</a>
					<div class="comparativePricingFormat row imageOverlay">
							<div class="col">
								<div class="circle_pretext">
									<p>Regular Price</p>
								</div>
								<div class="circle walkin">
									<h5 class="val">$<?=intval(number_format($tour->getWalkin(),2))?></h5>
								</div>
							</div>
							<div class="col">
								<div class="circle_pretext">
									<p>Online Special</p>
								</div>							
								<div class="circle online">
									<h5 class="val">$<?=intval(number_format($tour->getPrice(),2))?></h5>
								</div>
							</div>
						</div>


<!-- 					<div class="tourMediaSlider_container">
						<a href="<//?='/tours/tour/'.$tour->slug?>">
						<div class="tourMediaSlider">
							<div class="slide">
								<div class="media" style="background-image:url('<//?= UPLOAD_URL.'tours/'.$tour->featured_image ?>')"></div>
							</div>
						</div>
						</a>
						<div class="comparativePricingFormat row imageOverlay">
							<div class="col">
								<h8 class="valB">Adults</h8>
							</div>
							<div class="col">
								<div class="circle walkin">
									<p>Walk-In</p>
									<h5 class="val">$<//?=intval(number_format($tour->getWalkin(),2))?></h5>
								</div>
							</div>
							<div class="col">
								<div class="circle online">
									<p>Online</p>
									<h5 class="val">$<//?=intval(number_format($tour->getPrice(),2))?></h5>
								</div>
							</div>
						</div>
					</div> -->
<!-- 					<div class="btn_footer">
						<a class="btn primaryBtn" href="<//?='/tours/reservetour/'.$tour->slug?>">Book Now</a>
						<a class="btn secondary_btn" href="<//?='/tours/tour/'.$tour->slug?>">Learn More</a>
					</div> -->
				</div>
				<? } ?>
		<? } ?>

	</section>

	<section class="lastSection" style="background:#f1f3f5;">
    <div class="primaryContentWidth row row_of_3">
        <div class="col">
            <h5>Trip Guides</h5>
            <p>Follow our trip guides to find the best places to bike in NYC</p>
        </div>
        <div class="col">
            <h5>Routes</h5>
            <p>Follow our trip guides to find the best places to bike in NYC</p>
        </div>
        <div class="col">
            <h5>Subscribe</h5>
            <p>Follow our trip guides to find the best places to bike in NYC</p>
        </div>

    </div>
</section>

</div>
<script>
    $(document).ready(function(){
        $('.nsSub').on('submit',function(e){
            e.preventDefault();
            var email = $('[name=mail]');
            $.post($(this).attr('action'),{email:email.val()},function(data){
                alert(data.msg);
            })
        });
    })
</script>
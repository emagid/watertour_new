<div class="pageWrapper reservePackagePageWrapper">

	<section class="topSection">
		<div class="promoBar">
			<div class="float_left">
				<a href="<?=SITE_URL?>tours" class="promoBarLeftBtn"><icon class="whiteArrowIcon" style="background-image:url('<?=FRONT_IMG?>whiteArrowIcon.png');"></icon>Browse Tours</a>
			</div>		
			<p class="valB">Don't stress over planning the perfect itinerary. We've got your covered with a great tour deal!</p>
            <div class="right_float_hidden hidden show770">
                <a class="menuBtn globalSideMenuTrigger">
                    <icon>
                        <div class="menu-container">
                            <div class="menu-circle">
                                <div class="line-wrapper">
                                    <span class="line"></span>
                                    <span class="line"></span>
                                    <span class="line"></span>
                                </div>
                            </div>
                        </div>
                    </icon>
                    <span class="text">
                        <span class="open">Menu</span>
                        <span class="close">Close</span>
                    </span>
                </a>
            </div>			
		</div>	
		<div class="heroContainer shortHeroContainer">
			<div class="heroSlideshow heroSlideshowBikeTours">
				<div class="slide">		
					<div class="blurBar">
						<div class="mediaBlurredBar media" style="background-image:url('<?=FRONT_IMG?>empiresb.jpg');"></div>
					</div>				
					<div class="heroText row">
						<div class="col logoBlock">
			                <a class="homeLink" href="/">
			                    <img src="<?= FRONT_IMG ?>bikeTourLogo.png">
			                </a>
			            </div>
			            <div class="col">
							<h1 class="biggest valB"><?=$model->tour->name?></h1>
							<h3 class="valB valUpper">1 Hour</h3>
						</div>
					</div>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'tours/'.$model->tour->featured_image?>')"></div>
				</div>
			</div>
		</div>
	</section>	
	<section class="middleSection selectTickets">
		<form id="submit-form" class="row" action="/reservation/resRental" method="post">
			<input type="hidden" name="reservationTour" value="<?=$model->tour->id?>"/>
<!--			<input type="hidden" name="reservationDateTime" value="--><?//=strtotime($_SESSION['cart']->datetime)?><!--"/>-->
			<div class="row row_of_3">
				<div class="datepickerInputWrapper" style="z-index: 9999;">
					<?php $datetime = $_SESSION['cart']->datetime? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION['cart']->datetime)->format('M j, Y g:i a'):\Carbon\Carbon::now()->minute(0)->second(0)->format('M j, Y g:i a');?>
					<input type="text" class="triggerDatepicker" hidden name="reservationDateTime" placeholder="When?" value="<?=strtotime($datetime)?>">
					<div class="datepickerCellUI whenSelectorParent" data-hours="<?=implode(',', $model->tour->getHours())?>" data-week="<?=implode(',',$model->tour->getWeek())?>" data-day="<?=implode(',',$model->tour->getDay())?>"></div>
					<div class="<?=$datetime ? 'placeholderInactive ': 'placeholderActive '?>dateText">
						<label class="valB gray uppercase">Tour Starting Time:</label>
						<p class="placeholder val gray">When?</p>
						<p class="data whenSelectorResult"><?=$datetime?></p>
					</div>
					<span class="dropdownArrow">
						<icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
					</span>
				</div>
				<div class="col quantity">
					<div class="centered">
						<h6 class="uppercase valB">How many <span>Adults</span>?</h6>
						<div class="adultsCount">
	                        <div class="dec button disabled" data-attribute="adult"><span></span></div>
	                        <div class="adultsCountValWrapper">
	                            <input type="text" class="adultsCountVal" value="<?=$_SESSION['cart']->adults?>" pattern="[0-9]*"
	                                   name="reservationQuantity">
	                            <span class="key">Adults</span>
	                        </div>
	                        <div class="inc button" data-attribute="adult"><span></span><span></span></div>
	                    </div>
						<h1 class="val"><span>$</span><?=floatval($model->tour->getPrice())?></h1>
					</div>
				</div>
				<div class="col quantity">
					<div class="centered">
						<h6 class="uppercase valB">How many <span>Kids</span>?</h6>
						<div class="kidsCount">
	                        <div class="dec button disabled" data-attribute="kid"><span></span></div>
	                        <div class="kidsCountValWrapper">
	                            <input type="text" class="kidsCountVal" value="<?=$_SESSION['cart']->kids?>" pattern="[0-9]*"
	                                   name="reservationKidQuantity">
	                            <span class="key">kids</span>
	                        </div>
	                        <div class="inc button" data-attribute="kid"><span></span><span></span></div>
	                    </div>
						<h1 class="val"><span>$</span><?=floatval($model->tour->getPrice())?></h1>
	                </div>
				</div>
				<div class="col subtotal">
					<div class="absTransCenter">
						<h6 class="valB uppercase">Sub total</h6>
						<h1 class="val packageTotal">$<?=$model->total?></h1>
					</div>
				</div>

				<div class="resBar_error resBar_error_count" style="display:none"><p class="errorText">Please select how many adults or kids you want to book for this tour</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>
			</div>
			<div class="continueBar row">
				<div class="left_float">
<!-- 					<label for="discountCode">Do you have a discount code?</label>
					<div class="inputBtnRow">
						<input type="text" id="discountCode">
						<input type="submit" value="Apply" class="btn secondary_btn">
					</div> -->
				</div>
				<div class="right_float">
					<div class="inputBtnWrapper">
						<input id="submit" type="submit" value="Continue" class="primaryBtn btn"><icon class="rightChev" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon>
					</div>
				</div>
			</div>
		</form>
	</section>
</div>
<script>
	$(document).ready(function(){
		var adultPrice = <?=$model->tour->price?>;
		var kidPrice = <?=$model->tour->price?>;
		$('.inc,.dec').on('click',function(){
			var adults = $('.adultsCountVal').val();
			var kids = $('.kidsCountVal').val();
			var adultTotal = adults * adultPrice;
			var kidTotal = kids * kidPrice;
			$('.packageTotal').html('$'+(adultTotal+kidTotal));
		});

		$('#submit-form').submit(function(e){
			if(parseFloat($('.adultsCountVal').val()) <= 0 && parseFloat($('.kidsCountVal').val()) <= 0){
				$('.resBar_error').show();
				e.preventDefault();
			}
		});
	})
</script>
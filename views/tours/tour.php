<?
$bookingFlow = isset($_GET["book"]) && $_GET["book"] ? $_GET['book'] : false;
if ($bookingFlow == true) {
    ?>
    <script>
        $(document).ready(function () {
            $('html, body').animate({
                scrollTop: $("section.bookingSection.middleSection").offset().top + 24
            }, 400);
            $("#slideDownToBook").fadeOut(400);
            $("section.bookingSection.middleSection .reservationBarWrapper .container").addClass("activeBookingContainer");
        });
    </script>
<?
} else {

}
?>

<div class="pageWrapper tourDetailPageWrapper">
    <section class="topSection">
        <div class="heroContainer">
            <div class="tourOptionsSlider heroSlideshowBikeTours">
                <? $max = count($model->tours);
                for ($i = 0; $i < $max; $i++) {
                    $prev = array_key_exists($i - 1, $model->tours) ? $model->tours[$i - 1] : $model->tours[$max - 1];
                    $next = array_key_exists($i + 1, $model->tours) ? $model->tours[$i + 1] : $model->tours[0];
                    $package = \Model\Package::getItem(null,['where'=>"package_id = ".$model->tours[$i]->external_package_id])?>
                    <div class="slide" data-slug="<?= $model->tours[$i]->slug ?>">
                        <div class="blurBar">
                            <div class="mediaBlurredBar media"
                                 style="background-image:url('<?= UPLOAD_URL . 'tours/' . $model->tours[$i]->featuredImage() ?>');"></div>
                        </div>
                        <div class="heroText">
                            <h1 class="biggest valB"><?= $model->tours[$i]->name ?><br><span class="book_now"><a href="<?=$model->tours[$i]->external_package_id && $package ? '/packages/package/'.$package->slug: ''?>">Book Now and Save 20%</a></span>
                            </h1>

                        </div>
                        <div class="more_info_button">
                            <p>More Info</p>
                            <a href=""><img src="<?= FRONT_IMG ?>scroll_down.png"></a>
                        </div>
                        <div class="media"
                             style="background-image:url('<?= UPLOAD_URL . 'tours/' . $model->tours[$i]->featuredImage() ?>')"></div>
                        <div class="tourAdjacentName tourPrev">
                            <p class="valB"><?= $prev->name ?></p>
                        </div>
                        <div class="tourAdjacentName tourNext">
                            <p class="valB"><?= $next->name ?></p>
                        </div>
                    </div>
                <? } ?>

            </div>
            <section class="bookingSection middleSection">
                <div class="reservationBarWrapper tourDetailReservationBar" style="display:none;">
                    <div class="container">
                        <!--<form action="/reservation/resRental" method="post" id="book_form">
							<div class="stepHours step">
								<div class="timepickerInputWrapper">
									<div class="loaderBox">

										<div class='uil-default-css' style='transform:scale(0.2);'>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
											<div style='top:80px;left:93px;width:14px;height:40px;background:#ffffff;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>

											</div>
										</div>
									</div>
									<div class="selectedTourImage media" style="background-image:url('<? /*=UPLOAD_URL.'tours/'.$model->tour->featuredImage()*/ ?>')">
										<icon class="checkmark" style="background-image:url('<? /*=FRONT_IMG*/ ?>checkmarkWhite.png')"></icon>

									</div>
									<div class="tourName" style="text-overflow: initial;">
										<select class="valB" id="tour-list" style="font-size: 18px;font-weight: 500;margin-top: -50px;">
											<?php /*foreach($model->tours as $tour){ */ ?>
												<option value="<? /*=$tour->id*/ ?>" <? /*=$tour->name == $model->tour->name?'selected':null*/ ?>><? /*=$tour->name*/ ?></option>
											<?php /*}*/ ?>
										</select>
										<span class="dropdownArrow tourSelectorDropdownArrow">
											<icon style="background-image:url('<? /*= FRONT_IMG */ ?>dropdown_arrow.png')"></icon>
										</span>
									</div>
									<input type="hidden" name="reservationTour" value="<? /*=$model->tour->id*/ ?>"/>

								</div>
							</div>
							<div class="step countStep">
								<div class="adultsCount">
									<div class="dec button <? /*=$_SESSION['cart']->adults > 0 ? '': 'disabled'*/ ?>"><span></span></div>
									<div class="adultsCountValWrapper">
										<input type="text" class="adultsCountVal" value="<? /*=$_SESSION['cart']->adults*/ ?>" pattern="[0-9]*"
											   name="reservationQuantity">
										<span class="key"><? /*if(($_SESSION['cart']->adults)==1){*/ ?>Adult<? /*}else{*/ ?>Adults<? /*}*/ ?></span>
									</div>
									<div class="inc button"><span></span><span></span></div>
								</div>
								<div class="kidsCount">
									<div class="dec button <? /*=$_SESSION['cart']->kids > 0 ? '': 'disabled'*/ ?>"><span></span></div>
									<div class="kidsCountValWrapper">
										<input type="text" class="kidsCountVal" value="<? /*=$_SESSION['cart']->kids*/ ?>" pattern="[0-9]*"
											   name="reservationKidQuantity">
										<span class="key"><? /*if(($_SESSION['cart']->kids)==1){*/ ?>Kid<? /*}else{*/ ?>Kids<? /*}*/ ?></span>
									</div>
									<div class="inc button"><span></span><span></span></div>
								</div>
							</div>
							<div class="stepDate step">
								<div class="datepickerInputWrapper">

									<input type="text" class="triggerDatepicker" hidden name="reservationDateTime" placeholder="When?" value="<? /*=$_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : ''*/ ?>">
									<div class="datepickerCellUI whenSelectorParent" data-hours="<? /*=implode(',', $model->tour->getHours())*/ ?>" data-week="<? /*=implode(',',$model->tour->getWeek())*/ ?>" data-day="<? /*=implode(',',$model->tour->getDay())*/ ?>"></div>
									<div class="<? /*=$_SESSION[$model->sessionSelect]->datetime ? 'placeholderInactive ': 'placeholderActive '*/ ?>dateText">
										<p class="placeholder val gray">When?</p>
										<p class="data whenSelectorResult"><? /*=$_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : ''*/ ?></p>
									</div>
									<span class="dropdownArrow">
										<icon style="background-image:url('<? /*= FRONT_IMG */ ?>dropdown_arrow.png')"></icon>
									</span>
								</div>
							</div>
							<div class="step finalStep">
								<a class="btn primaryBtn reserveBtn">
									<p><? /*=($total = $model->tour->getSessionTotal()) ? 'Book this Tour &middot; $'.number_format($total,0): 'Starting at $'.floatval($model->tour->getPrice())*/ ?></p>
								</a>
							</div>
						</form>-->
                    </div>
                </div>
            </section>
        </div>
        <!-- 		<div id="slideDownToBook">
			<icon style="background-image:url('<?= FRONT_IMG ?>slideDownIcon.png')"></icon>
		</div> -->
    </section>
    <section class="bookingSection middleSection">
        <div class="row dealsEquipmentCols">
            <div class="col">
                <h5 class="valB gray" style="display:none;visibility: hidden;opacity:0;pointer-events: none;">Select
                    Tour Add-Ons</h5>
                <div class="equipmentSelectionBox tabbedView equipmentSelectionBoxTours"
                     style="display:none;visibility: hidden;opacity:0;pointer-events: none;">
                    <div class="tabController">
                        <a class="tab active sliderLinkedTab" data-tab_title="adult"><p>General Upgrades</p></a>
                    </div>
                    <div class="tabContents">
                        <div class="tab_content tab_content_adult tab_content_kid tab_content_child tab_content_tandem tab_content_active">
                            <div class="equipmentCustomizationSlider">
                                <? foreach ($model->equipment as $type => $items) {
                                    foreach ($items as $item) {
                                        $equip = isset($_SESSION['cart']->equipment[$item->id]) ? $_SESSION['cart']->equipment[$item->id] : null; ?>
                                        <div class="slide sliderLinkedTab_content_<?= $type ?>">
                                            <div class="equipmentImage media"
                                                 style="background-image:url('<?= UPLOAD_URL . 'equipments/' . $item->featuredImage() ?>')">
                                                <p class="whiteOut"><?= $item->name ?></p>
                                            </div>

                                            <div class="comparativePricingFormat centeredCols">
                                                <div class="col">
                                                    <h4 class="valB">Walk-In</h4>
                                                    <div class="co_price">
                                                        <span></span>
                                                        <p>$<?= number_format($item->getPrice(), 2) ?></p>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <h4 class="valB">Online</h4>
                                                    <div class="online_price">
                                                        <p>$<?= number_format($item->getPrice(), 2) ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn primaryBtn equipmentCustomizationActionBtn <?= $equip ? 'equipmentSelected' : '' ?>">
                                                <a class="addEquipment">
                                                    <p class="addText">Add to Reservation</p>
                                                    <p class="addedText">
                                                        <icon class="checkmark"
                                                              style="background-image:url('<?= FRONT_IMG ?>checkmarkWhite.png')"></icon>
                                                        <span>Selected</span>
                                                    </p>
                                                </a>
                                                <div class="numericalToggler">
                                                    <div class="numberCount">
                                                        <div class="dec button <?= $equip ? '' : 'disabled' ?>"
                                                             data-id="<?= $item->id ?>"><span></span></div>
                                                        <div class="numberCountValWrapper">
                                                            <input type="text" class="numberCountVal"
                                                                   value="<?= $equip ? $equip['qty'] : 0 ?>"
                                                                   pattern="[0-9]*">
                                                        </div>
                                                        <div class="inc button" data-id="<?= $item->id ?>"><span></span><span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? } ?>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="bookingForm">
                    <div class="bookingFormWrapper">
                        <form action="/reservation/resRental" method="post" id="submit_form">
                            <div class="inputRow">
                                <label>Select a tour</label>
                                <select name="reservationTour" id="tour-list">
                                    <?php foreach ($model->tours as $tour) { ?>
                                        <option value="<?= $tour->id ?>" <?= $tour->name == $model->tour->name ? 'selected' : null ?>><?= $tour->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="step countStep">
                                <div class="adultsCount">
                                    <div class="dec button <?= $_SESSION['cart']->adults > 0 ? '' : 'disabled' ?>">
                                        <span></span></div>
                                    <div class="adultsCountValWrapper">
                                        <input type="text" class="adultsCountVal"
                                               value="<?= $_SESSION['cart']->adults ?>" pattern="[0-9]*"
                                               name="reservationQuantity">
                                        <span class="key"><? if (($_SESSION['cart']->adults) == 1) { ?>Adult<? } else { ?>Adults<? } ?></span>
                                    </div>
                                    <div class="inc button"><span></span><span></span></div>
                                </div>
                                <div class="kidsCount">
                                    <div class="dec button <?= $_SESSION['cart']->kids > 0 ? '' : 'disabled' ?>">
                                        <span></span></div>
                                    <div class="kidsCountValWrapper">
                                        <input type="text" class="kidsCountVal" value="<?= $_SESSION['cart']->kids ?>"
                                               pattern="[0-9]*"
                                               name="reservationKidQuantity">
                                        <span class="key"><? if (($_SESSION['cart']->kids) == 1) { ?>Kid<? } else { ?>Kids<? } ?></span>
                                    </div>
                                    <div class="inc button"><span></span><span></span></div>
                                </div>
                            </div>
                            <div class="inputRow">
                                <label>Starting date and time</label>
                                <div class="datepickerInputWrapper">

                                    <input type="text" class="triggerDatepicker" hidden name="reservationDateTime"
                                           placeholder="When?"
                                           value="<?= $_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : '' ?>">
                                    <div class="datepickerCellUI whenSelectorParent"
                                         data-hours="<?= implode(',', $model->tour->getHours()) ?>"
                                         data-week="<?= implode(',', $model->tour->getWeek()) ?>"
                                         data-day="<?= implode(',', $model->tour->getDay()) ?>"></div>
                                    <div class="<?= $_SESSION[$model->sessionSelect]->datetime ? 'placeholderInactive ' : 'placeholderActive ' ?>dateText">
                                        <p class="placeholder val gray">When?</p>
                                        <p class="data whenSelectorResult"><?= $_SESSION[$model->sessionSelect]->datetime ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $_SESSION[$model->sessionSelect]->datetime)->format('M j, Y g:i a') : '' ?></p>
                                    </div>
                                    <span class="dropdownArrow">
                                <icon style="background-image:url('<?= FRONT_IMG ?>dropdown_arrow.png')"></icon>
                            </span>
                                </div>
                            </div>

<!--                            <div class="inputRow submitRow">-->
<!--                                <a class="btn primaryBtn reserveBtn">-->
<!--                                    <p>--><?//= ($total = \Model\Cart::getSessionTotal()) > 8 ? 'Book it <span class="middot"></span> $' . number_format($total, 0) : 'Starting at $8' ?><!--</p>-->
<!--                                </a>-->
<!--                                <p><strong>Continue for 20% off &#8226;</strong> 3 travelers have this pass in their-->
<!--                                    cart</p>-->
<!--                            </div>-->
                        </form>
                    </div>
                </section>
                <div class="textualInfo">
                    <div class="tourName" id="tourHighlight">
                        <h5 class="valB"><?= $model->tour->name ?></h5>
                        <p class="inline">Explore the legendary landmark of Manhattan</p>
                        <div class="starRating">
                            <? $average = array_sum(array_map(function($item){return $item->rating;},$model->reviews))/count($model->reviews);
                            $average = round($average*2,0) / 2;
                            for ($i = 0; $i < 5; $i++) {
                                $sub = $average - $i;
                                switch ($sub) {
                                    case $sub > .5:
                                    case $sub == 0:
                                        $rating = 'star_rate';
                                        break;
                                    case $sub == .5:
                                        $rating = 'star_half';
                                        break;
                                    case $sub < 0:
                                    default:
                                        $rating = 'star_border';
                                        break;
                                } ?>
                                <i class="material-icons"><?= $rating ?></i>
                            <? } ?>
                        </div>
<!--                        <div class="rating">-->
<!--                            <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>-->
<!--                        </div>-->
                        <p class="inline"><?=count($model->reviews).' '.pluralize(count($model->reviews),'review')?></p>
                    </div>

                    <div class="pass_validity">
                        <img src="<?= FRONT_IMG ?>valid_pass.png">
                        <div class="text">
                            <h6>Pass Validity</h6>
                            <p><?=$model->tour->name?> - Pass valid for 48 hours from the time of
                                ticket redemption. Please find more information on how to redeem your tickets <span><a
                                            href="">here</a></span></p>
                        </div>
                    </div>
                    <div class="descriptionText">
                        <h3 class="valB">About this tour</h3>
                        <?= $model->tour->details ?>
                        <p>Have questions? Want some expert suggestions? <span><a href="">Chat with us</a></span</p>
                    </div>
                </div>
            </div>
            <div class="col textual" style="width: 50%">
                <!--<div class="equipmentFreeSection row equipmentFreeSection_tour">
                    <div class="text col">
                        <h1 class="valB">Included in this package</h1>
                    </div>

                    <div class="col">
                        <img src="<?/*= FRONT_IMG */?>bustour_icon.png">
                        <div class="more_packages">
                            <div class="title_bar open">
                                <h2>Downtown Bus Tour</h2>
                                <span class="open_sesame"><a href=""><img
                                                src="<?/*= FRONT_IMG */?>plus_open.png"></a></span>
                            </div>
                            <div class="more_content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged. It was popularised in the 1960s with
                                    the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                                    with desktop publishing software like Aldus PageMaker including versions of Lorem
                                    Ipsum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <img src="<?/*= FRONT_IMG */?>watertour_icon.png">
                        <div class="more_packages">
                            <div class="title_bar open">
                                <h2>Liberty Cruise</h2>
                                <span class="open_sesame"><a href=""><img
                                                src="<?/*= FRONT_IMG */?>plus_open.png"></a></span>
                            </div>
                            <div class="more_content">
                                <div class="tourDetailSecondaryMedia_wrapper">
                                    <div class="tourDetailSecondaryMedia_slider">
                                        <?/* foreach ($model->tour_images as $tour_image) { */?>
                                            <div class="slide">
                                                <div class="media"
                                                     style="background-image:url('<?/*= UPLOAD_URL . 'tours/' . $tour_image */?>');"></div>
                                            </div>
                                        <?/* } */?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <img src="<?/*= FRONT_IMG */?>biketour_icon.png">
                        <div class="more_packages">
                            <div class="title_bar">
                                <h2>Brooklyn Bike Tour</h2>
                                <span class="open_sesame"><a href=""><img
                                                src="<?/*= FRONT_IMG */?>plus_open.png"></a></span>
                            </div>
                        </div>
                    </div>
                </div>-->

                <!-- 				<div class="tourDetails_languages">
					<h8 class="uppercase valB gray">Languages &#x2014; <span id="language"><?= $model->tour->languages == 'null' || !$model->tour->languages ? '' : implode(',', json_decode($model->tour->languages, true)) ?></span></h8>
				</div>		
				<div class="tourDetails_validity">
					<h8 class="uppercase valB gray">Duration &#x2014; <span id="duration"><?= round($model->tour->duration_hour) ?> hours <? if ($model->tour->duration_minute != 0) { ?>and <?= round($model->tour->duration_minute) ?> minutes<? } ?></span></h8>
				</div>
				<div class="tourDetails_validity">
					<? if ($model->tour->available_range && json_decode($model->tour->available_range, true)) {
                    $occurrence = '';
                    foreach (json_decode($model->tour->available_range, true) as $syntax => $value) {
                        switch ($syntax) {
                            case 'o':
                                $occurrence = $value;
                                break;
                            case 'w':
                                $str[] = implode(', ', array_map(function ($item) {
                                    return ucfirst($item);
                                }, $value));
                                break;
                            case 'd':
                                $str[] = implode(', ', array_map(function ($item) {
                                    return ucfirst($item);
                                }, $value));
                                break;
                            case 'h':
                                $str[] = implode(', ', array_map(function ($item) {
                                    if ($item < 12) {
                                        $str = fmod($item, 1) == 0 ? $item . 'AM' : floor($item) . ':30AM';
                                        return $str;
                                    } else if ($item == 12 || $item == 12.5) {
                                        $str = fmod($item, 1) == 0 ? $item . 'PM' : floor($item) . ':30PM';
                                        return $str;
                                    } else if ($item == 24 || $item == 24.5) {
                                        $str = fmod($item, 1) == 0 ? ($item - 12) . 'AM' : floor($item - 12) . ':30AM';
                                        return $str;
                                    } else {
                                        $str = fmod($item, 1) == 0 ? ($item - 12) . 'PM' : floor($item - 12) . ':30PM';
                                        return $str;
                                    }
                                }, $value));
                                break;
                        }
                    }
                    $availability = $occurrence . ' (' . implode(' at ', $str) . ')';
                } else {
                    $availability = 'Tour Specific';
                } ?>
					<h8 class="uppercase valB gray">Reservation availability &#x2014; <span id="availability"><?= $availability ?></span></h8>
				</div>	 -->
                <!-- 				<div class="getDiscountsSection row">
                                    <h1 class="valB">Get discounts</h1>
                                    <h4 class="val gray">on bus and boat tour tickets when<br>you reserve your bike rentals online<br>here with <span>Bike Rentals Central Park</span></h4>
                                </div> -->
            </div>
            <div class="col textual">
                <?if($model->route_path){?>
                    <div class="equipmentFreeSection row equipmentFreeSection_tour">
                        <div class="text col">
                            <h1 class="valB">Route</h1>
                        </div>

                        <?foreach ($model->route_path as $route_path){
                            $route = \Model\Route::getItem($route_path->route_id);?>
                        <div class="col">
                            <img src="<?= $route->fullFeaturedImagePath() ?>">
                            <div class="more_packages">
                                <div class="title_bar open">
                                    <h2><?=$route->name?></h2>
                                    <span class="open_sesame"><a href=""><img
                                                    src="<?= FRONT_IMG ?>plus_open.png"></a></span>
                                </div>
                                <div class="more_content">
                                    <p><?=$route->description?></p>
                                </div>
                                <?foreach (json_decode($route_path->route_icon_ids,true) as $route_icon_id){
                                    $rii = \Model\Route_Icon::getItem($route_icon_id)?>
                                <div class="more_content">
                                    <img src="<?=$rii->fullIconPath()?>"/>
                                    <p><?=$rii->name?></p>
                                </div>
                                <?}?>
                            </div>
                        </div>
                        <?}?>
                    </div>
                <?}?>
                <section class="reviewsSection reviewsHorizontalScroller">
                    <div class="inPageCurtain"></div>
                    <h2 class="big valB">Reviews<a class="writeReview">Write a Review</a></h2>
                    <div class="reviewFormExpander">
                        <span class="expanderArrow"></span>
                        <div class="reviewFormWrapper">
                            <form action="/reviews/add" method="post" id="submit_form">
                                <input hidden name="redirect" value="<?= $this->emagid->uri ?>">
                                <input hidden name="review_type" value="2">
                                <input hidden name="review_id" value="<?= $model->tour->id ?>">
                                <div class="inputRow">
                                    <label>Name</label>
                                    <input name="customer_name" id="reviewCustomerName">
                                </div>
                                <div class="inputRow">
                                    <label>Title</label>
                                    <input name="title">
                                </div>
                                <div class="inputRow">
                                    <label>Body</label>
                                    <textarea name="description"></textarea>
                                </div>
                                <div class="inputRow">
                                    <label>City</label>
                                    <input name="city">
                                </div>
                                <div class="inputRow">
                                    <label>State</label>
                                    <select name="state">
                                        <? foreach (get_states() as $short => $long) { ?>
                                            <option value="<?= $short ?>"><?= $long ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="inputRow">
                                    <label>Rating</label>
                                    <select name="rating">
                                        <? for ($i = 5; $i >= 1; $i -= .5) { ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="inputRow submitRow">
                                    <input type="submit" value="Submit" class="btn primaryBtn submit_review">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="reviewScroller">
                        <? foreach ($model->reviews as $review) { ?>
                            <div class="reviewItem">
                                <div class="quoteMark"></div>
                                <div class="quoteText">
                                    <h6 class="medium val"><?= $review->description ?></h6>
                                    <div class="starRating">
                                        <? for ($i = 0; $i < 5; $i++) {
                                            $sub = ($review->rating - 1) - $i;
                                            switch ($sub) {
                                                case $sub > .5:
                                                case $sub == 0:
                                                    $rating = 'star_rate';
                                                    break;
                                                case $sub == .5:
                                                    $rating = 'star_half';
                                                    break;
                                                case $sub < 0:
                                                default:
                                                    $rating = 'star_border';
                                                    break;
                                            } ?>
                                            <i class="material-icons"><?= $rating ?></i>
                                        <? } ?>
                                    </div>
                                    <p><strong>- <?= $review->customer_name ?></strong><span
                                                class="middot">&#xb7;</span><?= date('M jS, Y', strtotime($review->create_date)) ?>
                                    </p>
                                </div>
                                <div class="reviewHelpful">
                                    <a>
                                        <h6 class="valB uppercase">Helpful</h6>
                                        <icon class="thumbsUp"
                                              style="background-image:url('<?= FRONT_IMG ?>reviewThumbsUp.png')">
                                            <span><?= $review->helpful ?: 0 ?></span>
                                        </icon>
                                    </a>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                </section>

                <!-- 				<div class="tourDetails_languages">
					<h8 class="uppercase valB gray">Languages &#x2014; <span id="language"><?= $model->tour->languages == 'null' || !$model->tour->languages ? '' : implode(',', json_decode($model->tour->languages, true)) ?></span></h8>
				</div>
				<div class="tourDetails_validity">
					<h8 class="uppercase valB gray">Duration &#x2014; <span id="duration"><?= round($model->tour->duration_hour) ?> hours <? if ($model->tour->duration_minute != 0) { ?>and <?= round($model->tour->duration_minute) ?> minutes<? } ?></span></h8>
				</div>
				<div class="tourDetails_validity">
					<? if ($model->tour->available_range && json_decode($model->tour->available_range, true)) {
                    $occurrence = '';
                    foreach (json_decode($model->tour->available_range, true) as $syntax => $value) {
                        switch ($syntax) {
                            case 'o':
                                $occurrence = $value;
                                break;
                            case 'w':
                                $str[] = implode(', ', array_map(function ($item) {
                                    return ucfirst($item);
                                }, $value));
                                break;
                            case 'd':
                                $str[] = implode(', ', array_map(function ($item) {
                                    return ucfirst($item);
                                }, $value));
                                break;
                            case 'h':
                                $str[] = implode(', ', array_map(function ($item) {
                                    if ($item < 12) {
                                        $str = fmod($item, 1) == 0 ? $item . 'AM' : floor($item) . ':30AM';
                                        return $str;
                                    } else if ($item == 12 || $item == 12.5) {
                                        $str = fmod($item, 1) == 0 ? $item . 'PM' : floor($item) . ':30PM';
                                        return $str;
                                    } else if ($item == 24 || $item == 24.5) {
                                        $str = fmod($item, 1) == 0 ? ($item - 12) . 'AM' : floor($item - 12) . ':30AM';
                                        return $str;
                                    } else {
                                        $str = fmod($item, 1) == 0 ? ($item - 12) . 'PM' : floor($item - 12) . ':30PM';
                                        return $str;
                                    }
                                }, $value));
                                break;
                        }
                    }
                    $availability = $occurrence . ' (' . implode(' at ', $str) . ')';
                } else {
                    $availability = 'Tour Specific';
                } ?>
					<h8 class="uppercase valB gray">Reservation availability &#x2014; <span id="availability"><?= $availability ?></span></h8>
				</div>	 -->
                <!-- 				<div class="getDiscountsSection row">
                                    <h1 class="valB">Get discounts</h1>
                                    <h4 class="val gray">on bus and boat tour tickets when<br>you reserve your bike rentals online<br>here with <span>Bike Rentals Central Park</span></h4>
                                </div> -->
            </div>
        </div>
        <!-- 		<div class="toursOverviewText toursOverviewMedia tourDetailsOverviewDataWrapper">
                    <h5><//?=$model->tour->name?><span class="middot"></span>More Photos from the Tour</h5>
                    <div class="tourDetailSecondaryMedia_wrapper">
                        <div class="tourDetailSecondaryMedia_slider">
                            <//?foreach($model->tour_images as $tour_image){?>
                                <div class="slide">
                                    <div class="media" style="background-image:url('<//?=UPLOAD_URL.'tours/'.$tour_image?>');"></div>
                                </div>
                            <//?}?>
                        </div>
                    </div>
                </div>	 -->
    </section>

    <? if ($model->tour->related) { ?>
        <section class="attractionDetailMap tourDetails_departure">
            <!-- 					<div class="tourDetails_departure">
                                    <h8 class="uppercase valB gray">Departure Point &#x2014; <span id="departure"><''?=$model->tour->departure_point?></span></h8>
                                </div> -->
<!--            <div id="map"></div>-->

            <div class="tours_similar">
                <div class="row_of_attractions">
                    <h5>NYC Main Attractions</h5>
                    <h4>Similar Tours</h4>
                    <div class="featuredTours">
                        <? foreach (json_decode($model->tour->related) as $id) {
                            $tour = \Model\Tour::getItem($id); ?>
                            <div class="featuredTour">
                                <div class="imgContainer">
                                    <img src="<?= $tour->featuredImage() ? UPLOAD_URL . 'tours/' . $tour->featuredImage() : FRONT_ASSETS . 'img/topview_tour.jpg' ?>"/>
                                    <div class="ft-name"><?= $tour->name ?></div>
                                </div>
                                <div class="infoContainer">
                                    <div class="featuredDesc">
                                        <div class="ft-desc"><?= $tour->description ?></div>
                                    </div>
                                    <div class="featuredPrice">
                                        <div class="ft-walk">Walkin: <?= $tour->walkin_price ?></div>
                                        <div class="ft-price">Price: <?= $tour->price ?></div>
                                    </div>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                    <!--					        <img src="--><? //= FRONT_IMG ?><!--attraction_example.png">-->
                </div>
            </div>
        </section>
    <? } ?>
</div>
<script>
    $(document).ready(function () {
        $('.tourOptionsSlider').slick({
            speed: 800,
            autoplay: false,
            draggable: false,
            pauseOnHover: false,
            fade: true,
            cssEase: 'ease-in-out',
            initialSlide:<?=$model->tour_index?>
        }).on('beforeChange', function (event, slick, currentSlide) {
            $(".tourDetailReservationBar .timepickerInputWrapper").addClass("animationTransitionActive");
        }).on('afterChange', function (event, slick, currentSlide) {
            if (!$('.tourOptionsSlider').slick('getSlick').$slides[currentSlide]) {
                return;
            }
            var slug = $('.tourOptionsSlider').slick('getSlick').$slides[currentSlide].dataset.slug;
            $.post('/tours/getTour', {slug: slug}, function (data) {
                if (data.status == 'success') {
                    var imgUrl = "url('/content/uploads/tours/" + data.tourImg + "')";
                    console.log(imgUrl);
                    $('[name=review_id]').val(data.tourId);
                    $('[name=redirect]').val("/tours/tour/" + slug);
                    $('#tour-list').val(data.tourId);
                    $('[name=reservationTour]').val(data.tourId);
                    $(".timepickerInputWrapper div.selectedTourImage.media").css("background-image", imgUrl).delay(100).queue(function (removeAnimationTransitionClass) {
                        $(".tourDetailReservationBar .timepickerInputWrapper").removeClass("animationTransitionActive");
                        removeAnimationTransitionClass();
                    });
                    var adults = $('.adultsCountVal').val();
                    var kids = $('.kidsCountVal').val();
                    if (adults > 0 || kids > 0) {
                        $('.reserveBtn').find('p').html('Book this Tour &middot; $' + parseFloat(data.tourPrice * adults + data.tourPrice * kids));
                    } else {
                        $('.reserveBtn').find('p').html('Starting at $' + parseFloat(data.tourPrice));
                    }
                    var datepicker = document.getElementsByClassName('datepickerCellUI');
                    for (var i = 0; i < datepicker.length; i++) {
                        var item = datepicker[i];
                        item.dataset.hours = data.tourHours;
                        item.dataset.week = data.tourWeeks;
                        item.dataset.day = data.tourDays;
                        rome(item).refresh();
                    }
                    $('.tourDetailsOverviewDataWrapper').find('h5').html(data.tourName + '<span class="middot"></span>More Photos from the Tour');
                    $('#tourHighlight').find('h5').html(data.tourName + '<span class="middot"></span>The Highlights');
                    $('.tourDetails_languages').find('span').html(data.languages);
                    $('[name=reservationDateTime]').val('');
                    $('.dateText').removeClass('placeholderInactive').addClass('placeholderActive');
                    $('#language').html(data.tourLanguages);
                    $('#departure').html(data.tourDeparture);
                    $('#duration').html(data.tourDuration);
                    $('#availability').html(data.tourAvailability);
                    $('.descriptionText').html(data.tourDetails);
//					deleteMarkers();
//					var points = $.parseJSON(data.tourRoute);
//					for (var j = 0; j < points.length; j++){
//						var split = points[j].split(', ');
//						addMarker(new google.maps.LatLng(split[0],split[1]));
//						if(j == 0){
//							map.setCenter(new google.maps.LatLng(split[0],split[1]));
//						}
//					}
                    var slider = $('.tourDetailSecondaryMedia_slider');
                    slider.slick('removeSlide', null, null, true);
                    $.each(data.tourGallery, function (i, e) {
                        slider.slick('slickAdd', '<div class="slide"><div class="media" style="background-image:url(\'/content/uploads/tours/' + e + '\');"></div></div>');
                    });
                    var reviewScroller = $('.reviewScroller');
                    var reviewHtml = '';
                    $.each(data.tourReviews, function (i, e) {
                        var stars = '';
                        for (var j = 0; j < 5; j++) {
                            var item = parseFloat(e.rating - j);
                            if (item > .5 || item == 0) {
                                stars += '<i class="material-icons">star_rate</i>';
                            } else if (item == .5) {
                                stars += '<i class="material-icons">star_half</i>';
                            } else {
                                stars += '<i class="material-icons">star_border</i>';
                            }
                        }
                        reviewHtml += '<div class="reviewItem"><div class="quoteMark"></div><div class="quoteText"> <h6 class="medium val">' + e.description + '</h6> <div class="starRating"> ' + stars + ' </div> <p><strong>- ' + e.customer_name + '</strong><span class="middot">&#xb7;</span>' + e.date + '</p> </div> <div class="reviewHelpful"> <a> <h6 class="valB uppercase">Helpful</h6> <icon class="thumbsUp" style="background-image:url(\'content/frontend/images/reviewThumbsUp.png\')"> <span>' + (e.helpful ? e.helpful : 0 ) + '</span> </icon> </a> </div> </div>';
                    });
                    reviewScroller.empty().append(reviewHtml);
                }
            });
            window.history.pushState(null, null, "<?=SITE_URL . 'tours/tour/'?>" + slug);
        });

        $('#tour-list').on('change', function () {
            var index = $(this).prop('selectedIndex');
//			var index = $(this).val();
            $('.tourOptionsSlider').slick('slickGoTo', index).trigger('afterChange');
        })
    })
</script>
<script>
    var map;
    var poly;
    var path;
    var markers = [];
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    function initMap() {
        <?php $routes = json_decode($model->route, true); $ex = explode(',', $routes[0]); $lat = $ex[0]; $lng = $ex[1];?>

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            scrollwheel: false,
            center: {lat: <?=$lat ?: '40.7682729'?>, lng: <?=$lng ?: '-73.9873471'?>}
        });
        var geocoder = new google.maps.Geocoder();

        poly = new google.maps.Polyline({
            strokeColor: '#000000',
            strokeOpacity: 1,
            strokeWeight: 2
        });
        poly.setMap(map);

        <?if($model->route){
        foreach(json_decode($model->route, true) as $index=>$item){
        $ex = explode(',', $item); $lat = $ex[0]; $lng = $ex[1]?>
        addMarker(new google.maps.LatLng(<?=$lat?>, <?=$lng?>));
        <?}?>
        <?}?>

        google.maps.event.addDomListener(map, 'drag', function (e) {
            google.maps.event.trigger(map, 'resize');
            map.setZoom(map.getZoom());
        });
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);

                path = poly.getPath();

                // Because path is an MVCArray, we can simply append a new coordinate
                // and it will automatically appear.
                path.push(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    label: labels[markers.length % labels.length],
                    position: results[0].geometry.location
                });
                markers.push(marker);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function addMarker(location) {
        path = poly.getPath();

        // Because path is an MVCArray, we can simply append a new coordinate
        // and it will automatically appear.
        path.push(location);
        var marker = new google.maps.Marker({
            position: location,
            label: labels[markers.length % labels.length],
            map: map
        });
        markers.push(marker);
    }

    function deleteMarkers() {
        clearMarkers();
        path.clear();
        markers = [];
        $('.markerContainer').empty();
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function mapMarkers() {
        var markerHtml = '';
        $('.markerContainer').empty();
        for (var i = 0; i < markers.length; i++) {
            markerHtml += '<div class="marker_' + i + '"><input class="form-control" name="markers[]" value="' + markers[i].getPosition().lat() + ', ' + markers[i].getPosition().lng() + '"/></div>';
        }
        $('.markerContainer').append(markerHtml);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAPS_API_KEY ?>&callback=initMap"
        type="text/javascript"></script>
<div class="pageWrapper attractionPageWrapper">


	<section class="topSection">
		<div class="heroContainer">
			<div class="absTransCenter heroTextFixed heroText">
				<h1 class="biggest valB"><?=$model->tour->name?></h1>
				<h3 class="valB valUpper">Pair our unforgettable bike tours with this famous NYC tour</h3>
				<a class="btn primaryBtn">Browse Tours</a>
			</div>
			<div class="heroSlideshow heroSlideshowBikeTours">
				<?foreach($model->tour_images as $image){?>
					<div class="slide">
						<div class="blurBar">
							<div class="mediaBlurredBar media" style="background-image:url('<?=UPLOAD_URL.'tours/'.$image->image?>');"></div>
						</div>
						<div class="media" style="background-image:url('<?=UPLOAD_URL.'tours/'.$image->image?>')"></div>
					</div>
				<?}?>
			</div>
		</div>
	</section>	
	<section class="middleSection">
		<div class="row attractionInfo">
			<div class="col right_float imagesCalls">
				<div class="verticalMedia media">
					<img src="<?=UPLOAD_URL.'tours/'.$model->tour->featuredImage()?>">
				</div>
				<div class="attractionDetailMap">
<!--					<iframe src="https://www.google.com/maps/embed/v1/place?key=--><?//=GOOGLE_MAPS_API_KEY?><!--&q=--><?//=htmlentities($model->tour->name)?><!--" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>-->
					<div id="map"></div>
				</div>
				<div class="availablePackagesWrapper">
					<div class="availablePackagesGrid">
						<div class="row packageGridItem">
							<div class="textualData">
								<h2 class="val">MoMA + Bike Rental Package</h2>
								<h4 class="val gray">Friday October 14, 2016 at 10:00 AM <span class="middot"></span><i class="material-icons">timelapse</i>1 Hour</h4>
								<div class="lineSep"></div>
							</div>
							<div class="row row_of_2">
								<div class="pricing col">
									<h6 class="gray uppercase valB">Starting at</h6>
									<h2 class="price"><span>$</span>65</h2>
									<h6 class="gray uppercase valB">per person</h6>
								</div>
								<div class="bookBtnWrapper col">
									<a class="btn fullBtn primaryBtn">Book this Combo</a>
								</div>
							</div>
						</div>
					</div>	
					<div class="availablePackagesGrid">
						<div class="row packageGridItem">
							<div class="textualData">
								<h2 class="val">MoMA + Bike Rental Package</h2>
								<h4 class="val gray">Friday October 15, 2016 at 10:00 AM <span class="middot"></span><i class="material-icons">timelapse</i>1 Hour</h4>
								<div class="lineSep"></div>
							</div>
							<div class="row row_of_2">
								<div class="pricing col">
									<h6 class="gray uppercase valB">Starting at</h6>
									<h2 class="price"><span>$</span>65</h2>
									<h6 class="gray uppercase valB">per person</h6>
								</div>
								<div class="bookBtnWrapper col">
									<a class="btn fullBtn primaryBtn">Book this Combo</a>
								</div>
							</div>
						</div>
					</div>					
					<div class="footer">
						<a class="btn primaryBtn">View More Packages</a>
					</div>
				</div>
			</div>		
			<div class="col left_float textualInfo">
				<div class="row">
					<div class="col left_float">
						<h3 class="val"><?=$model->tour->name?><span class="middot"></span>The Highlights</h3>
					</div>
				</div>
				<div class="descriptionText">
					<?=$model->tour->description?>
				</div>
				<!--<div class="learnMoreLinkWrapper">
					<p>Learn more at</p>
					<a class="invBtn btn arrowBtn storeVisit" href="<?/*=$model->tour->external_link*/?>">
						<?/*=$model->tour->external_link*/?>
						<icon style="background-image:url('<?/*=FRONT_IMG*/?>btnLinkIcon.png')"></icon>
					</a>
				</div>-->
			</div>
		</div>
		
		<div class="additionalAttractionsRow row">
			<div class="left_float col">
				<a>
					<icon class="leftChevron" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon>
					<p class="val">Metropolitan Museum of Art</p>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>met.jpg')">
					</div>
				</a>
			</div>
			<div class="right_float col">
				<a>
					<div class="media" style="background-image:url('<?=FRONT_IMG?>empiresb.jpg')">
					</div>
					<p class="val">Empire State Building</p>
					<icon class="leftChevron" style="background-image:url('<?=FRONT_IMG?>rightChevron.png')"></icon>
				</a>
			</div>
		</div>

	</section>
	<section class="bottomSection">
		<div class="row row_of_3 featuredLinks">
			<?foreach($model->tour_banner as $value){?>
				<div class="col">
					<h4 class="valB medium"><?=$value->title?></h4>
					<div class="media" style="background-image:url('<?=UPLOAD_URL.'banners/'.$value->image?>')"></div>
					<p class="valB"><?=$value->description?></p>
					<a class="invBtn btn arrowBtn learnMore" href="<?=$value->url?>">
						<?=isset(json_decode($value->details,true)['url_text']) ? json_decode($value->details,true)['url_text']: 'Explore our tours here'?>
						<icon style="background-image:url('<?=FRONT_IMG?>btnLinkArrow.png')"></icon>
					</a>
				</div>
			<?}?>
		</div>
	</section>	
</div>
<script>
	var map;
	var poly;
	var path;
	var markers = [];
	var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 8,
			center: {lat: 40.7127837, lng: -74.00594130000002}
		});
		var geocoder = new google.maps.Geocoder();

		poly = new google.maps.Polyline({
			strokeColor: '#000000',
			strokeOpacity:1,
			strokeWeight: 2
		});
		poly.setMap(map);

		<?if($model->route){
        foreach(json_decode($model->route,true) as $index=>$item){
        $ex = explode(',',$item); $lat = $ex[0]; $lng = $ex[1]?>
		addMarker(new google.maps.LatLng(<?=$lat?>, <?=$lng?>));
		<?}?>
		<?}?>

		google.maps.event.addDomListener( map, 'drag', function(e) {
			google.maps.event.trigger(map,'resize');
			map.setZoom(map.getZoom());
		});
	}

	function geocodeAddress(geocoder, resultsMap) {
		var address = document.getElementById('address').value;
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);

				path = poly.getPath();

				// Because path is an MVCArray, we can simply append a new coordinate
				// and it will automatically appear.
				path.push(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: resultsMap,
					label: labels[markers.length % labels.length],
					position: results[0].geometry.location
				});
				markers.push(marker);
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	function addMarker(location){
		path = poly.getPath();

		// Because path is an MVCArray, we can simply append a new coordinate
		// and it will automatically appear.
		path.push(location);
		var marker = new google.maps.Marker({
			position:location,
			label: labels[markers.length % labels.length],
			map:map
		});
		markers.push(marker);
	}

	function deleteMarkers(){
		clearMarkers();
		path.clear();
		markers = [];
		$('.markerContainer').empty();
	}

	function clearMarkers(){
		setMapOnAll(null);
	}

	function setMapOnAll(map){
		for(var i = 0; i < markers.length; i++){
			markers[i].setMap(map);
		}
	}

	function mapMarkers(){
		var markerHtml = '';
		$('.markerContainer').empty();
		for(var i = 0; i < markers.length; i++){
			markerHtml += '<div class="marker_'+i+'"><input class="form-control" name="markers[]" value="'+markers[i].getPosition().lat()+', '+markers[i].getPosition().lng()+'"/></div>';
		}
		$('.markerContainer').append(markerHtml);
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAPS_API_KEY?>&callback=initMap"
		type="text/javascript"></script>
$(document).ready(function () {
    $(document).on('click', '.tabController .tab', function (event) {
        var $tab_controller = $(this).closest(".tabController");
        var $tab_module = $(this).closest('.tabbedView');
        $tab_controller.find(".tab").removeClass("active");
        $(this).addClass("active");
        var tab_content_name = $(this).attr("data-tab_title");



        $tab_module.find(".tab_content").not(".tab_content.tab_content_" + tab_content_name).removeClass("tab_content_active").delay(400).queue(function (nextHidetabs) {
            $tab_module.find(".tab_content").not(".tab_content.tab_content_" + tab_content_name).hide();
            nextHidetabs();
        });

        $tab_module.find(".tab_content.tab_content_" + tab_content_name).show().delay(10).queue(function (nextAddClass) {
            $tab_module.find(".tab_content.tab_content_" + tab_content_name).addClass("tab_content_active");
            nextAddClass();
        });

        if($(this).hasClass("activateSlider")){
            $(".reviewsSlider").slick('reInit'); 
        }

        if($(this).hasClass("sliderLinkedTab")){
            var adultEquipmentCount = $(".sliderLinkedTab_content_adult").length + 1;
            var kidEquipmentCount = $(".sliderLinkedTab_content_kid").length + 1;
            var childEquipmentCount = $(".sliderLinkedTab_content_child").length + 1;
            var tandemEquipmentCount = $(".sliderLinkedTab_content_tandem").length + 1;
            if(tab_content_name==="adult"){
                var jumpToIndex = 0;
                $('.equipmentCustomizationSlider').slick('slickGoTo',jumpToIndex);
                $tab_controller.find(".tab").removeClass("active");
                $tab_controller.find(".tab[data-tab_title='adult']").addClass("active");
            }else if(tab_content_name==="kid"){
                var jumpToIndex = adultEquipmentCount - 1;
                $('.equipmentCustomizationSlider').slick('slickGoTo',jumpToIndex);
                $tab_controller.find(".tab").removeClass("active");
                $tab_controller.find(".tab[data-tab_title='kid']").addClass("active");
            }else if(tab_content_name==="child"){
                var jumpToIndex = kidEquipmentCount + adultEquipmentCount - 1;
                $('.equipmentCustomizationSlider').slick('slickGoTo',jumpToIndex);
                $tab_controller.find(".tab").removeClass("active");
                $tab_controller.find(".tab[data-tab_title='child']").addClass("active");
            }else if(tab_content_name==="tandem"){
                var jumpToIndex = kidEquipmentCount + adultEquipmentCount + tandemEquipmentCount - 1;
                $('.equipmentCustomizationSlider').slick('slickGoTo',jumpToIndex);
                $tab_controller.find(".tab").removeClass("active");
                $tab_controller.find(".tab[data-tab_title='tandem']").addClass("active");
            }
            
        }
    });

    $('.equipmentCustomizationSlider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var $tabBar = $(".equipmentSelectionBox .tabController");
        nextSlide = nextSlide + 1;
        var adultEquipmentTopIndex = $(".sliderLinkedTab_content_adult").length - 1;
        var kidEquipmentTopIndex = $(".sliderLinkedTab_content_kid").length + adultEquipmentTopIndex - 1; 
        var childEquipmentTopIndex = $(".sliderLinkedTab_content_child").length + kidEquipmentTopIndex - 1;
        var tandemEquipmentTopIndex = $(".sliderLinkedTab_content_tandem").length + childEquipmentTopIndex - 1;     
        if(nextSlide <= adultEquipmentTopIndex && nextSlide >= 1){
            $tabBar.find(".tab").removeClass("active");
            $tabBar.find(".tab[data-tab_title='adult']").addClass("active");
        }else if(nextSlide <= kidEquipmentTopIndex && nextSlide > adultEquipmentTopIndex){
            $tabBar.find(".tab").removeClass("active");
            $tabBar.find(".tab[data-tab_title='kid']").addClass("active");
        }else if(nextSlide <= childEquipmentTopIndex && nextSlide > kidEquipmentTopIndex){
            $tabBar.find(".tab").removeClass("active");
            $tabBar.find(".tab[data-tab_title='child']").addClass("active");
        }else if(nextSlide <= tandemEquipmentTopIndex && nextSlide > childEquipmentTopIndex){
            $tabBar.find(".tab").removeClass("active");
            $tabBar.find(".tab[data-tab_title='tandem']").addClass("active");
        }
        console.log(nextSlide);
    });

    
    pageLoad();
    function pageLoad(){
        var $pageWrapper = $(".pageWrapper");
        if($pageWrapper.hasClass("homePageWrapper")){
            $(".homePageWrapper").addClass("pageLoad");
            var randomTimeFrame = Math.floor(Math.random() * 1000) + 350;
            setTimeout(function() { 
                $(".homePageWrapper").addClass("sliderLoad");
            },randomTimeFrame);
            setTimeout(function() { 
                $(".homePageWrapper").addClass("sliderLoaded");
            },1350);
        } if($pageWrapper.hasClass("toursIndexPageWrapper")){
            $(".toursIndexPageWrapper").addClass("pageLoad");
        } if($pageWrapper.hasClass("confirmationPageWrapper")){
            $("#emailAddress").focus();
        }
    }

    var scrollTop = $(window).scrollTop();
    scroll_activated_ui(scrollTop);
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        scroll_activated_ui(scrollTop);
    });

    function  scroll_activated_ui(scrollTop){
        var $pageWrapper = $(".pageWrapper");
        if($pageWrapper.hasClass("homePageWrapper")){
            if(scrollTop>=420){
                $(".fixedReserveRow").addClass("reserveBarFixed");
            }else{
                $(".fixedReserveRow").removeClass("reserveBarFixed");
                if($(window).width>=690){
                    $(".triggerDatepicker").datetimepicker('hide');
                }
            }
        }else if($pageWrapper.hasClass("tourDetailPageWrapper") && $('body').hasClass('tours')){

            var resbarTopDistance=$(".heroContainer .reservationBarWrapper.tourDetailReservationBar").offset().top + $(".bookingSection .reservationBarWrapper.tourDetailReservationBar").height();
            console.log(resbarTopDistance);
            if(scrollTop>=resbarTopDistance){
                $(".fixedReserveRow").addClass("reserveBarFixed");
            }else{
                $(".fixedReserveRow").removeClass("reserveBarFixed");
                if($(window).width>=690){
                    $(".triggerDatepicker").datetimepicker('hide');
                }
            }
        }else if($pageWrapper.hasClass("tourDetailPageWrapper") && $('body').hasClass('rentals')){

            var resbarTopDistance=$(".bookingSection .reservationBarWrapper.tourDetailReservationBar").offset().top + $(".bookingSection .reservationBarWrapper.tourDetailReservationBar").height();
            console.log(resbarTopDistance);
            if(scrollTop>=resbarTopDistance){
                $(".fixedReserveRow").addClass("reserveBarFixed");
            }else{
                $(".fixedReserveRow").removeClass("reserveBarFixed");
                if($(window).width>=690){
                    $(".triggerDatepicker").datetimepicker('hide');
                }
            }
        }else if($pageWrapper.hasClass("cmsPageWrapper")){
            if(scrollTop>=186){
                $(".mainPageContainer > .fixedReserveRow").addClass("reserveBarFixed");
            }else{
                $(".mainPageContainer > .fixedReserveRow").removeClass("reserveBarFixed");
                if($(window).width>=690){
                    $(".triggerDatepicker").datetimepicker('hide');
                }
            }
        }else if($pageWrapper.hasClass("paymentPageWrapper")){
            var colHeight = $(".checkoutFormCol").height() + "px";
            $(".checkoutSummaryCol").css("height",colHeight);
        }
    }

    $('#ccnumber').keyup(function() {
        var $credit_card_icons = $("#creditCardIcons");
        var cc_input = this.value;
        var cc_input_first = cc_input.substr(0, 1);
        var cc_input_first_two = cc_input.substr(0, 2);
        if((cc_input_first === "4") && (cc_input.length <= 19)){
            $credit_card_icons.attr("class","visa");
        } else if((cc_input_first === "3") && (cc_input.length <= 19)){
            if((cc_input_first_two === "34") || (cc_input_first_two === "37")){
                $credit_card_icons.attr("class","amex");
            }
            else{
                $credit_card_icons.attr("class","");
            }
        } else if((cc_input_first === "6") && (cc_input.length <= 19)){
            if((cc_input_first_two === "60") || (cc_input_first_two === "65") || (cc_input_first_two === "62")){
                $credit_card_icons.attr("class","disc");
            }
            else{
                $credit_card_icons.attr("class","");
            }
        } else if((cc_input_first === "5") && (cc_input.length <= 19)){
            if((cc_input_first_two === "51") || (cc_input_first_two === "52") || (cc_input_first_two === "53") || (cc_input_first_two === "54") || (cc_input_first_two === "55")){
                $credit_card_icons.attr("class","mc");
            }
            else{
                $credit_card_icons.attr("class","");
            }
        } else{
            $credit_card_icons.attr("class","");
        }
    });

    if($.browser.chrome) {
        $("body").addClass("br_chr");
    } else if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
        $("body").addClass("br_moz");
    } else if (navigator.userAgent.indexOf("Safari") > -1){
        $("body").addClass("br_saf");
    }

    $("div").on("click", ".checkboxUX", function (event) {
        event.stopImmediatePropagation()
        event.preventDefault();
        var $inner_checkbox = $(this).find("input");
        var $option_clicked = $(this);

        if ($option_clicked.hasClass("optionBoxRadio")) {
            var $option_wrapper = $option_clicked.closest(".radioOptionsModule");
            $option_wrapper.find(".optionBoxRadio").removeClass("selected");
            $option_wrapper.find(".optionBoxRadio").find("input").attr('checked', false);
        }

        if ($option_clicked.hasClass("selected")) {
            $option_clicked.removeClass("selected");
            if ($inner_checkbox.length > 0) {
                $inner_checkbox.attr('checked', false);
            }
        } else {
            $option_clicked.addClass("selected");
            if ($inner_checkbox.length > 0) {
                $inner_checkbox.attr('checked', true);
            }
        }

        if($option_clicked.hasClass('insuranceButton')){
            var $parentActionBtn = $(this).closest(".equipmentCustomizationActionBtn");
            $parentActionBtn.toggleClass("equipmentSelected"); 
        }
    });

    $(document).on("click","html",function(event){
        $(".equipmentSelectionBox.tabbedView.nestedEquipmentCustomizationBox").removeClass("active");
        $(".countStep").each(function () {
            $(this).removeClass("countDropdownActive");
        });
        $(".rd-time").toggleClass('activeTimeDropdown');
        $(".datepickerInputWrapper").each(function(){
            $(this).removeClass("datepickerActive");
        });
         $(".reviewFormExpander").removeClass("activeReviewForm");
         $(".inPageCurtain").removeClass("inPageCurtainActive");
    });

     $(document).on("click",".inPageCurtain",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
         $(".reviewFormExpander").removeClass("activeReviewForm");
         $(".inPageCurtain").removeClass("inPageCurtainActive");
     });

    $(document).on("click",".writeReview",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
        $(".reviewFormExpander").toggleClass("activeReviewForm");
        $(".inPageCurtain").toggleClass("inPageCurtainActive");
        $("#reviewCustomerName").focus();
    });

    $(document).on('click','.submit_review',function(){
        $('#submit_form').submit();
    });

    $(document).on("click",".reviewFormExpander",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });

    $(document).on("click",".equipmentSelectionBox.tabbedView.nestedEquipmentCustomizationBox",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });

    $(document).on("mouseover", ".btn.primaryBtn.equipmentCustomizationActionBtn .addEquipment", function(){
        if(!$(this).closest(".btn.primaryBtn.equipmentCustomizationActionBtn").hasClass("equipmentSelected")){
            $(this).closest(".btn.primaryBtn.equipmentCustomizationActionBtn").addClass("primaryBtnHoverState");
        }
    });

    $(document).on("mouseleave", ".btn.primaryBtn.equipmentCustomizationActionBtn .addEquipment", function(){
        if(!$(this).closest(".btn.primaryBtn.equipmentCustomizationActionBtn").hasClass("equipmentSelected")){
            $(this).closest(".btn.primaryBtn.equipmentCustomizationActionBtn").removeClass("primaryBtnHoverState");
        }
    });


    $(document).on("mouseover", "section.middleSection.toursGrid.toursGridSimplified > .col a:not(.primaryBtn)", function(){
        $("section.middleSection.toursGrid.toursGridSimplified > .col").removeClass("borderStronger");
        $(this).closest("section.middleSection.toursGrid.toursGridSimplified > .col").addClass("borderStronger");
    });

    $(document).on("mouseleave", "section.middleSection.toursGrid.toursGridSimplified > .col a:not(.primaryBtn)", function(){
        $(this).closest("section.middleSection.toursGrid.toursGridSimplified > .col").removeClass("borderStronger");
    });



    $(".questionsSeachWrapper").on("keyup", "input.searchFaqField", function () {
        if ($(".searchFaqField").val().length >= 1) {
            $(".searchFaqField").closest(".questionsSeachWrapper").addClass("faqSearchActive");
        } else {
            $(".searchFaqField").closest(".questionsSeachWrapper").removeClass("faqSearchActive");
        }

    });

    $(document).on("click","a.globalSideMenuTrigger",function(event){
        $("body").toggleClass("sidebarMenuActive");
        $(this).toggleClass("clicked");
        $(".line-wrapper").toggleClass("clicked");
        $(".menu-circle").toggleClass("clicked");
    });

    $(document).on("click","body.sidebarMenuActive div.mainPageContainer div.pageWrapper",function(event){
        $("body").removeClass("sidebarMenuActive");
        $("a.globalSideMenuTrigger").toggleClass("clicked");
        $(".line-wrapper").removeClass("clicked");
        $(".menu-circle").removeClass("clicked");
    });

    $(document).on("click","div#slideDownToBook",function(event){
        $('html, body').animate({
            scrollTop: $("section.bookingSection.middleSection").offset().top + 24
        }, 400);
        $(this).fadeOut(400);
        $("section.bookingSection.middleSection .reservationBarWrapper .container").addClass("activeBookingContainer");
    });

    $(document).on("click","a.triggerFixedReserveBox",function(event){
        if($("body").hasClass("fixedReserveActive")){
            $(this).text("Start Booking!");
        }else{
            $(this).text("Cancel");
        }
        $("body").toggleClass("fixedReserveActive");
    });    

    $(window).resize(function() {
        if($(window).width()>770){
            $("body").removeClass("sidebarMenuActive");
            $("a.globalSideMenuTrigger").removeClass("clicked");
            $(".line-wrapper").removeClass("clicked");
            $(".menu-circle").removeClass("clicked");            
        } 

        if($(window).width()>860){
            $("body").removeClass("fixedReserveActive");
            $("a.triggerFixedReserveBox").text("Cancel");
        }
    });

    $(document).on("click", ".equipmentCustomizationActionBtn .addEquipment", function(){
        var $parentActionBtn = $(this).closest(".equipmentCustomizationActionBtn");
        if(!$parentActionBtn.hasClass("equipmentSelected")){
            $(this).parent().find('.inc').trigger('click');
        }
    });


    $(document).on("click",".datepickerInputWrapper .dateText",function(event){
        $(".countStep").each(function () {
            $(this).removeClass("countDropdownActive");
        });
        $(this).closest(".datepickerInputWrapper").toggleClass("datepickerActive");
        $(".dateTimeConfirmButton").remove();
        if($(this).hasClass("placeholderActive")){
            $(".rd-time").append("<a class='btn disabled primaryBtn dateTimeConfirmButton'>Look's Good!</a>");
        }else{
            $(".rd-time").append("<a class='btn primaryBtn dateTimeConfirmButton'>Look's Good!</a>");
        }




        event.stopImmediatePropagation();
        event.preventDefault();
    });

    $(document).on("click",".rd-time-selected",function(event){
        var $rdContainer = $(this).closest('.rd-container');
        var scrollToPosition = $rdContainer.find('.rd-time').offset().top - 300;
        console.log(scrollToPosition);
        $(this).closest(".rd-time").toggleClass('activeTimeDropdown');
        $('html,body').animate({
            scrollTop: scrollToPosition},
            '1400','easeOutSine');
    });

    $(document).on("click",".dateTimeConfirmButton",function(event){
        $(".datepickerInputWrapper").removeClass("datepickerActive");
        $(".datepickerInputWrapper .dateText").removeClass("placeholderActive").addClass("placeholderInactive");
    });

    $(document).on("click",".countStep div.countDropdownTrigger_data",function(event){
        $(".datepickerInputWrapper").each(function(){
            $(this).removeClass("datepickerActive");
        });
        $(this).closest(".countStep").toggleClass("countDropdownActive");
        event.stopImmediatePropagation();
        event.preventDefault();
    });
    $(document).on("click",".countDropdownMenu a.cancelDropdown",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();        
        $(this).closest(".countStep").removeClass("countDropdownActive");
    });
    $(document).on("click",".countDropdownMenu",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });
    $(document).on("click",".datepickerCellUI .rd-container",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });
    $(document).on("click",".datepickerCellUIConfirm .rd-container",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });

    $(document).on("click","a.triggerEquipmentCustomizationBox",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
        $(this).parent().parent().find(".equipmentSelectionBox.tabbedView.nestedEquipmentCustomizationBox").toggleClass('active');
        //$(".equipmentSelectionBox.tabbedView.nestedEquipmentCustomizationBox").toggleClass("active");
    });

    $('.reserveBtn').on('click',function(){
        var duration = $('[name=reservationTime]').val();
        var adult_count = parseFloat($('[name=reservationQuantity]').val());
        var kid_count = parseFloat($('[name=reservationKidQuantity]').val());
        var tandem_count = isNaN(parseFloat($('[name=reservationTandemQuantity]').val())) ? 0:parseFloat($('[name=reservationTandemQuantity]').val());
        var datetime = $('[name=reservationDateTime]').val();
        if (adult_count + kid_count + tandem_count > 0 && (duration === undefined || duration > 0)) {
            var setDate = new Date();
            setDate.setDate(setDate.getDate()+1);setDate.setHours(12);setDate.setMinutes(0);setDate.setSeconds(0);
            var formatted = setDate.getFullYear()+'/'+(setDate.getMonth()+1)+'/'+setDate.getDate()+' '+setDate.getHours()+':'+setDate.getMinutes()+':'+setDate.getSeconds();
            $('[name=reservationDateTime]').val(formatted);
            $('#book_form').submit();
        } else if (adult_count + kid_count + tandem_count > 0 && datetime != '' && (duration === undefined || duration > 0)) {
            $('#book_form').submit();
        } else if (adult_count + kid_count > 0 && datetime == '') {
            $(".reservationBarWrapper").append('<div class="resBar_error resBar_error_dateTime"><p class="errorText">Please select a package and who is going.</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
        } else if (adult_count + kid_count == 0 && datetime != '') {
            if ($(this).closest(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")) {
                $(".reservationBarWrapper").append('<div class="resBar_error resBar_error_count"><p class="errorText">Please select how many adults and/or kids are going.</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
            } else {
                $(".reservationBarWrapper").append('<div class="resBar_error resBar_error_count"><p class="errorText">Please select how many adults or kids you want to book for this tour</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
            }
        } else {
            if ($(this).closest(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")) {
                $(".reservationBarWrapper").append('<div class="resBar_error resBar_error_all"><p class="errorText">Please make a selection</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
            } else {
                $(".reservationBarWrapper").append('<div class="resBar_error resBar_error_all"><p class="errorText">Please select how many people are going on this tour, and when you would like to book it for</p><span class="separator"></span><a class="closeResBarError"><span></span><span></span></a></div>');
            }
        }
    });

    $("body").on("click",".closeResBarError",function(event){
        $(this).closest(".resBar_error").remove();
    });

    function updateSession(params){
        var pathname = window.location.pathname;
        //do not update session in cart page
        if (pathname != '/reservation/confirm') {
            //differentiate between tour, rental, and package
            var message = 'Book it &middot; $';
            if(pathname.indexOf('/tours') > -1){
                params.type = 'tour';
                message = 'Book this Tour &middot; $';
            } else if (pathname.indexOf('/packages') > -1) {
                params.type = 'package';
                message = 'Book this Package &middot; $';
            } else {
                params.type = $('.reservationTime :selected').data('type');
            }
            if(pathname == '/rentals'){
                params.skipDefault = 1;
            } else {
                params.skipDefault = 0;
            }
            $.post('/cart/updateSession', params, function (data) {
                if (data.status == 'success' && data.newTotal > 0) {
                    $('.reserveBtn').find('p').html(message + data.newTotal.toFixed(0));
                    $('.adultsCount .adultPrice').text('$'+parseFloat(data.defaultPrice));
                    $('.kidsCount .kidPrice').text('$'+parseFloat(data.kidPrice));
                } else {
                    $('.reserveBtn').find('p').html('Starting at $'+parseFloat(data.defaultPrice));
                    $('.adultsCount .adultPrice').text('$'+parseFloat(data.defaultPrice));
                    $('.kidsCount .kidPrice').text('$'+parseFloat(data.kidPrice));
                }
                if(data.appendHtml){
                    var select = !data.tours && !data.packages ? 'selected': '';
                    var html = '<option value="-1" disabled selected hidden'+select+'>Packages</option>';
                    var reservationTime = $('.reservationTime');
                    $.each(data.appendHtml,function(i,e){
                        html += '<optgroup label="'+i+'">';
                        var type = i.toLowerCase();
                        var cart = type+'s';
                        $.each(e,function(a,o){
                            select = data[cart] == o.id ? 'selected': '';
                            html += '<option data-type="'+type+'" data-cart="'+cart+'" value="'+o.id+'" '+select+'>'+o.name+'</option>';
                        });
                        html += '</optgroup>';
                    });
                    reservationTime.empty().append(html);
                }
                // if(data.appendRentals){
                //     var select = -1 == data.rentalId ? 'selected': '';
                //     var rentalTimeHtml = '<option value="-1" disabled '+select+'>How long?</option>';
                //     var reservationTime = $('.reservationTime');
                //     $.each(data.appendRentals,function(i,e){
                //         var select = e.id == data.rentalId ? 'selected': '';
                //         var total = e.total ? "- $"+e.total : '- $'+ e.default;
                //         rentalTimeHtml += "<option value='"+ e.id+"' "+select+">"+e.name+"</option>";
                //     });
                //     reservationTime.empty().append(rentalTimeHtml);
                // }
                //if(data.equipPrice){
                //    $.each(data.equipPrice,function(i,e){
                //        var cpf = $('.comparativePricingFormat[data-id='+i+']');
                //        cpf.find('.online_price p').html('$'+e);
                //        cpf.find('.co_price p').html('$'+e);
                //    })
                //}
            });
            if($('#unique_type').length == 1 && $('#unique_type').val() == 'rental' && $('#rental_ref').length == 1){
                params.ref_num = $('#rental_ref').val();
                $.post('/reservation/updateCart',params);
            }
        }
    }

    function personCountText(adults, kids, tandem, writeElement){
        var text = [];
        var piAdults = parseInt(adults);
        var piKids = parseInt(kids);
        var piTandems = parseInt(tandem);
        if(piAdults){
            text.push("<b>"+piAdults+"</b> "+pluralize(piAdults,'Adult'));
        }
        if(piKids){
            text.push("<b>"+piKids+"</b> "+pluralize(piKids,'Kid'));
        }
        if(window.location.pathname == '/rentals'){
            var otherItems = $(".numberCountVal:not([data-category=Adult]):not([data-category=Kid]):not([data-category=Tandem])");
            $.each(otherItems,function(i,e){
                if(parseInt(e.value)){
                    var c = parseInt(e.value);
                    text.push("<b>"+c+"</b> "+pluralize(c, "<b>"+e.dataset.name+"</b>","<b>"+e.dataset.name+"s</b>"));
                }
            });
        }
        var last = text.pop();
        var str = text.length ? text.join(', ')+" & "+last : last;
        str == '' || typeof str === 'undefined'? placeholderActive(): placeholderInactive();
        writeElement.html(str);
    }

    function pluralize(numVal,singular,plural){
        if(numVal == 1){
            return singular;
        } else {
            return plural || singular+"s";
        }
    }

    function placeholderInactive(){
        $(".countDropdownTrigger_data .text").removeClass("placeholderActive").addClass("placeholderInactive");
    }

    function placeholderActive(){
        $(".countDropdownTrigger_data .text").removeClass("placeholderInactive").addClass("placeholderActive");
    }

    $('.checkInsurance').on('change',function(){
        var qty = $(this).is(':checked') ? 1: 0;
        var params = {equipment:$(this).data('id'),qty:qty};

        var adultVal = $(".adultsCount .adultsCountVal").val();
        var kidVal = $(".kidsCount .kidsCountVal").val();
        var tandemVal = $(".tandemsCount .tandemCountVal").val();

        updateSession(params);
        personCountText(adultVal,kidVal,tandemVal,$(".countDropdownTrigger_data .text p.data"));
    });

    $(document).on("click", ".numberCount div.button",function () {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        var equipCount = 0;
        $.each($('.numberCountVal'), function (i, e) {
            equipCount += parseFloat(e.value);
        });
        var adultVal = $(".adultsCount .adultsCountVal").val();
        var kidVal = $(".kidsCount .kidsCountVal").val();
        var tandemVal = $(".tandemsCount .tandemCountVal").val();

        if ($button.hasClass("inc")) {
            //if (parseFloat($('.adultsCountVal').val()) + parseFloat($('.kidsCountVal').val()) > equipCount) {
            var newVal = parseFloat(oldValue) + 1;

            var id = $(this).data('id');
            updateSession({equipment: id, qty: newVal});
            var numCount = newVal;

            if (newVal >= 1) {
                $button.closest(".equipmentCustomizationActionBtn").addClass("equipmentSelected");
                $button.closest(".numberCount").find("div.button.dec").removeClass("disabled");
            } else {
                $button.closest(".equipmentCustomizationActionBtn").removeClass("equipmentSelected");
                $button.closest(".numberCount").find("div.button.dec").addClass("disabled");
            }

            $button.closest(".numberCount").find("input.numberCountVal").val(newVal);
            switch($(this).data('category')){
                case 'Adult':
                    var totalCount = 0;
                    $.each($('input[data-category="Adult"]'),function(i,e){
                        totalCount += parseFloat(e.value);
                    });
                    if(totalCount > $('.adultsCountVal').val()){
                        $('.adultsCount').find('.inc').first().trigger('click');
                    }
                    break;
                case 'Kid':
                    totalCount = 0;
                    $.each($('input[data-category="Kid"]'),function(i,e){
                        totalCount += parseFloat(e.value);
                    });
                    if(totalCount > $('.kidsCountVal').val()){
                        $('.kidsCount').find('.inc').first().trigger('click');
                    }
                    break;
                case 'Tandem':
                    totalCount = 0;
                    $.each($('input[data-category="Tandem"]'),function(i,e){
                        totalCount += parseFloat(e.value);
                    });
                    if(totalCount > $('.tandemCountVal').val()){
                        $('.tandemsCount').find('.inc').first().trigger('click');
                    }
                    break;
                case 'Accessories':
                case 'Children':
                case 'Insurance':
                    personCountText(adultVal,kidVal,tandemVal,$(".countDropdownTrigger_data .text p.data"));
                    break;
            }
        } else {
            if (oldValue > 0) {
                newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }

            id = $(this).data('id');
            updateSession({equipment: id, qty: newVal});

            if (newVal >= 1) {
                $button.closest(".equipmentCustomizationActionBtn").addClass("equipmentSelected");
                $button.closest(".numberCount").find("div.button.dec").removeClass("disabled");
            } else {
                $button.closest(".equipmentCustomizationActionBtn").removeClass("equipmentSelected");
                $button.closest(".numberCount").find("div.button.dec").addClass("disabled");
            }

            $button.closest(".numberCount").find("input.numberCountVal").val(newVal);
            switch($(this).data('category')){
                case 'Adult':
                    var totalCount = 0;
                    $.each($('input[data-category="Adult"]'),function(i,e){
                        totalCount += parseFloat(e.value);
                    });
                    if(totalCount < $('.adultsCountVal').val()){
                        $('.adultsCount').find('.dec').first().trigger('click');
                    }
                    break;
                case 'Kid':
                    totalCount = 0;
                    $.each($('input[data-category="Kid"]'),function(i,e){
                        totalCount += parseFloat(e.value);
                    });
                    if(totalCount < $('.kidsCountVal').val()){
                        $('.kidsCount').find('.dec').first().trigger('click');
                    }
                    break;
                case 'Tandem':
                    totalCount = 0;
                    $.each($('input[data-category="Tandem"]'),function(i,e){
                        totalCount += parseFloat(e.value);
                    });
                    if(totalCount < $('.tandemCountVal').val()){
                        $('.tandemsCount').find('.dec').first().trigger('click');
                    }
                    break;
                case 'Accessories':
                case 'Insurance':
                    personCountText(adultVal,kidVal,tandemVal,$(".countDropdownTrigger_data .text p.data"));
                    break;
            }
        }
    });

    $(".adultsCount div.button").on("click", function () {
        var $button = $(this);
        var oldValue = $button.closest(".adultsCount").find("input").val();
        // var reservationPplQuantity = parseInt($(this).closest(".step").find($('select[name=reservationQuantity]')).val(),10);
        // console.log(reservationPplQuantity);

        if ($button.hasClass("inc")) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        var kidVal = $(this).closest(".step.countStep").find(".kidsCount .kidsCountVal").val();
        if ((newVal == 2) && (kidVal == 2)) {
            $(".star-badge").remove();
            $(this).closest(".reservationBarWrapper .container").append("<div class='star-badge'><div class='star-badge__inner'><p><span class='star-badge__text--big'>Family Deal!</span><span class='star-badge__text--small'>unlocked 20% off</span></p></div></div>");
        }else{
            $(".star-badge").fadeOut(300).delay(50).queue(function (removeBadge) {
                $(".star-badge").remove();
                removeBadge();
            });
        }

       
        if (newVal >= 1) {
            $button.closest(".adultsCount").find("div.button.dec").removeClass("disabled");
            if($(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")){
                var $inputKeyData=$(".countDropdownTrigger_data .text p.data");
                var tandemVal = $(this).closest(".step.countStep").find(".tandemsCount .tandemCountVal").val();
                newVal = parseInt(newVal);
                kidVal = parseInt(kidVal);
                tandemVal = parseInt(tandemVal);                
                $(".countDropdownTrigger_data .text").removeClass("placeholderActive").addClass("placeholderInactive");

                personCountText(newVal,kidVal,tandemVal,$inputKeyData);
            }
        } else {
            $button.closest(".adultsCount").find("div.button.dec").addClass("disabled");

            if($(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")){
                var $inputKeyData=$(".countDropdownTrigger_data .text p.data");
                var tandemVal = $(this).closest(".step.countStep").find(".tandemsCount .tandemCountVal").val();

                personCountText(newVal,kidVal,tandemVal,$inputKeyData);
            }
        }

        updateSession({adults: newVal});

        // if(newVal<=reservationPplQuantity){
        $button.closest(".adultsCount").find("input.adultsCountVal").val(newVal);
        if(!$(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")){
            if (newVal == 1) {
                $(".adultsCount").find(".adultsCountValWrapper .key").text("adult");
            } else {
                $(".adultsCount").find(".adultsCountValWrapper .key").text("adults");
            }
        }
        var pathname = window.location.pathname;
        if(pathname != '/reservation/confirm') {
            $(".adultsCount input").each(function () {
                $(this).val(newVal);
            });
        } else {
            $(this).siblings().find('input').val(newVal);
        }
        resetCounterBtns();

    });

    resetCounterBtns();
    function resetCounterBtns(){
        $(".kidsCount div.button").each(function(){
            var currentValue = $(this).closest(".kidsCount").find("input").val();
            if(currentValue>=1){
                $(this).closest(".kidsCount").find("div.button.dec").removeClass("disabled");
            }
        });

        $(".adultsCount div.button").each(function(){
            var currentValue = $(this).closest(".adultsCount").find("input").val();
            if(currentValue>=1){
                $(this).closest(".adultsCount").find("div.button.dec").removeClass("disabled");
            }
        });

        $(".tandemsCount div.button").each(function(){
            var currentValue = $(this).closest(".tandemsCount").find("input").val();
            if(currentValue>=1){
                $(this).closest(".tandemsCount").find("div.button.dec").removeClass("disabled");
            }
        });

    }

    $(".kidsCount div.button").on("click", function () {
        var $button = $(this);
        var oldValue = $button.closest(".kidsCount").find("input").val();
        // var reservationPplQuantity = parseInt($(this).closest(".step").find($('select[name=reservationQuantity]')).val(),10);
        // console.log(reservationPplQuantity);

        if ($button.hasClass("inc")) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }


        var adultVal = $(this).closest(".step.countStep").find(".adultsCount .adultsCountVal").val();
        if ((newVal == 2) && (adultVal == 2)) {
            $(".star-badge").remove();
            $(this).closest(".reservationBarWrapper .container").append("<div class='star-badge'><div class='star-badge__inner'><p><span class='star-badge__text--big'>Family Deal!</span><span class='star-badge__text--small'>unlocked 20% off</span></p></div></div>");
            $(this).closest(".reservationBarWrapper .star-badge").addClass(".star-badge__animatein");
        }else{
            $(".star-badge").fadeOut(300).delay(50).queue(function (removeBadge) {
                $(".star-badge").remove();
                removeBadge();
            });
        }

        updateSession({kids: newVal});


        if (newVal >= 1) {
            $button.closest(".kidsCount").find("div.button.dec").removeClass("disabled");

            if($(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")){
                var $inputKeyData=$(".countDropdownTrigger_data .text p.data");
                var tandemVal = $(this).closest(".step.countStep").find(".tandemsCount .tandemCountVal").val();
                adultVal = parseInt(adultVal);
                newVal = parseInt(newVal);
                tandemVal = parseInt(tandemVal);

                personCountText(adultVal,newVal,tandemVal,$inputKeyData);
            }            
        } else {
            $button.closest(".kidsCount").find("div.button.dec").addClass("disabled");

            if($(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")){
                var $inputKeyData=$(".countDropdownTrigger_data .text p.data");
                var tandemVal = $(this).closest(".step.countStep").find(".tandemsCount .tandemCountVal").val();
                personCountText(adultVal,newVal,tandemVal,$inputKeyData);
            }            
        }

        // if(newVal<=reservationPplQuantity){
        $button.closest(".kidsCount").find("input.kidsCountVal").val(newVal);

        if(!$(".reservationBarWrapper").hasClass("reservationBarTandemIncluded")){
            if (newVal == 1) {
                $(".kidsCount").find(".kidsCountValWrapper .key").text("kid");
            } else {
                $(".kidsCount").find(".kidsCountValWrapper .key").text("kids");
            }
        }
        var pathname = window.location.pathname;
        if(pathname != '/reservation/confirm') {
            $(".kidsCount input").each(function () {
                $(this).val(newVal);
            });
        } else {
            $(this).siblings().find('input').val(newVal);
        }
        resetCounterBtns();
    });

    $('.reservationTime').on('change',function(){
        var selectedValue=$(this).val();
        var selectedCart = $(this).find(':selected').data('cart');
        var selectedType = $(this).find(':selected').data('type');
        //$(".reservationTime").each(function(){
        //    $(this).find('option:eq('+(selectedValue)+')').prop('selected', true);
        //})
        $('[name=reservationTimeType]').val(selectedType);
        var arr = {};
        arr[selectedCart] = selectedValue;
        arr['type'] = selectedType;
        updateSession(arr);
    });

    $('[name=reservationDateTime]').on('change',function(){
        updateSession({datetime:$(this).val()});
    });

    $('.heroSlideshow').slick({
        speed: 1000,
        autoplay:true,
        autoplaySpeed:4500,
        pauseOnHover:false,
        fade: true,
        cssEase: 'ease-in-out',
        responsive: [
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                autoplay:false,
                slidesToScroll: 1
              }
            }
        ]
    });

    //Moved configuration to tour.php
    //$('.tourOptionsSlider').slick({
    //    speed: 800,
    //    autoplay:false,
    //    draggable:false,
    //    pauseOnHover:false,
    //    fade:true,
    //    cssEase: 'ease-in-out'
    //});

    // $(".tourMediaSlider").slick({
    //     speed: 0,
    //     autoplay:false,
    //     fade:true,
    //     pauseOnHover:false,
    //     draggable:false
    // });

    $(".tourDetailSecondaryMedia_slider").slick({
        speed: 0,
        autoplay:false,
        fade:true,
        pauseOnHover:false,
        draggable:false
    });

    $(".reviewsSlider").slick({
        speed: 650,
        autoplay:true,
        draggable:false,
        autoplaySpeed:8000,
        slidesToScroll:1,
        pauseOnHover:false,
        arrows:false,
        dots:true,
        infinite:true
    });


    $('body').on('click','.tourOptionsSlider .slide .tourAdjacentName.tourPrev',function(){
        $('.tourOptionsSlider').slick('slickPrev');
    });
    $('body').on('click','.tourOptionsSlider .slide .tourAdjacentName.tourNext',function(){
        $('.tourOptionsSlider').slick('slickNext');
    });


    $(".equipmentSlider").slick({
        speed: 650,
        autoplay:false,
        draggable:false,
        slidesToScroll:1,
        infinite:true
    });

    $(".equipmentCustomizationSlider").slick({
        speed: 650,
        autoplay:false,
        draggable:false,
        slidesToScroll:2,
        slidesToShow:2,
        cssEase: 'ease-in-out',
        infinite:false,
        responsive: [
            {
              breakpoint: 600,
              settings: {
              slidesToScroll:1,
              slidesToShow:1
              }
            }
        ]
    });

    function setupRome() {
        var datepickerCellUI = document.getElementsByClassName('datepickerCellUI');
        var whenSelectorResult = $('.whenSelectorResult');
        for (var i = 0; i < datepickerCellUI.length; i++) {
            var date = moment(Math.ceil((Date.now()/1000)/(30*60))*(30*60)*1000);
            var availableHours = datepickerCellUI[i].dataset.hours ? datepickerCellUI[i].dataset.hours : '';
            var availableWeeks = datepickerCellUI[i].dataset.week ? datepickerCellUI[i].dataset.week : '';
            var availableDate = datepickerCellUI[i].dataset.day ? datepickerCellUI[i].dataset.day : '';
            var datepick = true;
            var timepick = true;
            rome(datepickerCellUI[i], options = {
                "appendTo": document.body,
                "autoClose": true,
                "autoHideOnBlur": true,
                "autoHideOnClick": true,
                "date": true,
                "dateValidator": function (d) {
                    //var index = typeof datepickerCellUI[1] === 'undefined' ? 0: 1;
                    var index = 0;
                    var availableWeeks = datepickerCellUI[index].dataset.week ? datepickerCellUI[index].dataset.week : '';
                    var m = moment(d);
                    if (availableWeeks) {
                        var weekArray = availableWeeks.split(',');
                        var weekMap = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
                        var indexMap = weekArray.map(function (obj) {
                            if (weekMap.indexOf(obj.valueOf()) != -1) {
                                return weekMap.indexOf(obj.valueOf());
                            }
                        });
                        //same date and date is enabled
                        if(m.isSame($(datepickerCellUI[index]).siblings('.triggerDatepicker').val(),'date') && indexMap.indexOf(m.day()) !== -1){
                            //do nothing
                        }
                        //same date and date is disabled
                        else if(m.isSame($(datepickerCellUI[index]).siblings('.triggerDatepicker').val(),'date') && indexMap.indexOf(m.day()) == -1){
                            datepick = false;
                        }
                        //if datepick is false and date is enabled
                        if(datepick == false && indexMap.indexOf(m.day()) !== -1){
                            $('.triggerDatepicker').val(m.format('MMM D, Y h:mm a'));
                            $('.whenSelectorResult').html(m.format('MMM D, Y h:mm a'));
                            datepick = true;
                        }

                        return indexMap.indexOf(m.day()) !== -1;
                    } else {
                        return true;
                    }
                },
                "dayFormat": "D",
                "initialValue": $('[name=reservationDateTime]').length > 0 ? $('[name=reservationDateTime]').val() : date.format('MMM D, YYYY h:mm a'),
                "inputFormat": "MMM D, YYYY h:mm a",
                "invalidate": true,
                "max": null,
                "min": date.format('MMM DD, YYYY h:mm a'),
                "monthFormat": "MMMM 'YY",
                "monthsInCalendar": 1,
                "required": false,
                "strictParse": false,
                "styles": {
                    "back": "rd-back",
                    "container": "rd-container",
                    "date": "rd-date",
                    "dayBody": "rd-days-body",
                    "dayBodyElem": "rd-day-body",
                    "dayConcealed": "rd-day-concealed",
                    "dayDisabled": "rd-day-disabled",
                    "dayHead": "rd-days-head",
                    "dayHeadElem": "rd-day-head",
                    "dayRow": "rd-days-row",
                    "dayTable": "rd-days",
                    "month": "rd-month",
                    "next": "rd-next",
                    "positioned": "rd-container-attachment",
                    "selectedDay": "rd-day-selected",
                    "selectedTime": "rd-time-selected",
                    "time": "rd-time",
                    "timeList": "rd-time-list",
                    "timeOption": "rd-time-option"
                },
                "time": true,
                "timeFormat": "h:mm a",
                "timeInterval": 1800,
                timeValidator: function (d) {
                    //var index = typeof datepickerCellUI[1] === 'undefined' ? 0: 1;
                    var index = 0;
                    var availableHours = datepickerCellUI[index].dataset.hours ? datepickerCellUI[index].dataset.hours : '';
                    var m = moment(d);
                    if (availableHours) {
                        var availableHoursArray = availableHours.toString().split(',');
                        var hoursStringArray = [];
                        for(var i = 0; i < availableHoursArray.length; i++){
                            var thisMins = parseFloat(availableHoursArray[i]) * 60;
                            var h = m.clone().startOf('day').minute(thisMins).second(0);
                            hoursStringArray.push(h.toString());
                        }
                        var comparedM = m.second(0);
                        var inputVal = $(datepickerCellUI[index]).siblings('.triggerDatepicker').val();
                        var validateTime = moment.duration(moment(inputVal).format('HH:mm')).asHours();

                        if(m.isSame(inputVal,'date') && $.inArray(validateTime.toString(),availableHoursArray) == -1){
                            timepick = false;
                        }
                        //if timepick is false and time is enabled
                        if(timepick == false && hoursStringArray.indexOf(comparedM.toString()) != -1){
                            $('.triggerDatepicker').val(m.format('MMM D, Y h:mm a'));
                            $('.whenSelectorResult').html(m.format('MMM D, Y h:mm a'));
                            $('.rd-time-selected').html(m.format('h:mm a'));
                            timepick = true;
                        }
                        return hoursStringArray.indexOf(comparedM.toString()) != -1;
                    } else {
                        var start = m.clone().hour(7).minute(59).second(59);
                        var end = m.clone().hour(20).minute(0).second(1);
                        return m.isAfter(start) && m.isBefore(end);
                    }
                },
                "weekdayFormat": "min",
                "weekStart": moment().weekday(0).day()

            }).on('data', function (value) {
                if(window.location.pathname == '/reservation/confirm'){
                    var ref_num = this.associated.dataset.ref;
                    var data = {ref_num:ref_num,reserve_date:value,upgrade:false};
                    $.post('/reservation/updateCart',data);
                }
                var validateTime = moment.duration(moment(value).format('HH:mm')).asHours();
                var availableHoursArray = [];
                var index = typeof datepickerCellUI[1] === 'undefined' ? 0: 1;
                var availableHours = datepickerCellUI[index].dataset.hours ? datepickerCellUI[index].dataset.hours : '';
                if(availableHours){
                    availableHoursArray = availableHours.toString().split(',');
                } else {
                    for(var i = 8; i < 20.5; i+=.5){
                        availableHoursArray.push(i+"");
                    }
                }
                if(availableHoursArray.indexOf(validateTime.toString()) != -1){
                    //time is good
                } else {
                    //time is bad - set to nearest available
                    var newTime = moment(value);
                    if (newTime.isSame(moment(), 'day')) {
                        var self = this;
                        $.each(availableHoursArray, function (i, e) {
                            if (e > validateTime) {
                                var h = newTime.startOf('day').minute(parseFloat(e) * 60).second(0);
                                self.setValue(h);
                                value = h.format('MMM D, YYYY h:mm a');
                                return false;
                            }
                        })
                    } else {
                        var h = newTime.startOf('day').minute(availableHoursArray[0]*60).second(0);
                        this.setValue(h);
                        value = h.format('MMM D, YYYY h:mm a');
                    }
                }

                whenSelectorResult.html(value);
                $('[name=reservationDateTime]').val(value);
                updateSession({datetime: value});
            }).on('time',function(time){
                if($(".rd-time-list").is(":visible")){
                    $('.rd-time-selected').addClass('timeChosenbyUser');
                    if($('.rd-day-selected').is('.rd-day-disabled')){
                        $('.rd-day-selected').removeClass('rd-day-selected');
                    } else {
                        $(".dateTimeConfirmButton").removeClass("disabled");
                    }
                }
                $(".rd-time").toggleClass('activeTimeDropdown');
            }).on('ready',function(){
                if($('[name=reservationDateTime]').val().length > 0){
                    $('.rd-time-selected').addClass('timeChosenbyUser');
                }
            });
        }
    }
    setupRome();


});

function formValidator(params){
    var submit = true;
    $.each(params,function(i,e){
        var eName = e.attr('name');
        var eErrorText = e.attr('data-ErrorText')
        if(e.val() == ''){
            e.addClass('formInputError');
            if($('.error-'+eName).length == 0){
                e.after('<div class="error-'+eName+' checkoutError"><p>'+eErrorText+'</p></div>');
            }
            submit = false;
        } else if($('.error-'+eName)){
            e.removeClass('formInputError');
            $('.error-'+eName).remove();
        }
    });
    return submit;
}






$(document).ready(function(){
	$(document).keydown(function(e){
	    if (e.keyCode == 40) { 
	    	if($(".productMedia").hasClass("quickSlideInactive")){
	    		var scrollTop = $(window).scrollTop();
	    		if(scrollTop>65){
	    			$(".productMedia").addClass("quickSlideActive").removeClass("quickSlideInactive");	
	    		}
	    	}
	       	return false;
	    }
	});
	var scrollTop = $(window).scrollTop();
	scrollingProductHeader(scrollTop);
	$(window).scroll(function() {
	  var scrollTop = $(window).scrollTop();
	  scrollingProductHeader(scrollTop)
	});
	if($(window).width() < 776){

		$(document).on('click','.col.productMedia.quickSlideActive .legShotsLifestyle > span.trigger_text',function(){
			$(".row.productDetailSection > .col.productMedia").removeClass("quickSlideActive").addClass("quickSlideInactive");	
		});
	}

	function scrollingProductHeader(scrollTop){
		var $mainAddBtn = $(".firstAddBagBtn.btn");
		var $productHeader = $(".productHeader");
		var $scrollingHeaderSection = $(".productHeader .scrollingHeaderSection");
		var top_mainAddBtn = $mainAddBtn.offset().top;
		var top_productHeader = $productHeader.offset().top;
		var top_difference = top_mainAddBtn - top_productHeader;
		var top_difference_from_60 = 60 - top_difference;
		var startTranslate = 60;

		if(top_difference >= 11 && top_difference <= 60){
			var newTranslate = startTranslate - top_difference_from_60;
			$scrollingHeaderSection.css({"-moz-transform":"translateY("+newTranslate+"px)","-webkit-transform":"translateY("+newTranslate+"px)","transform":"translateY("+newTranslate+"px)"});
		}else if(top_difference > 60){
			$scrollingHeaderSection.css({"-moz-transform":"translateY("+startTranslate+"px)","-webkit-transform":"translateY("+startTranslate+"px)","transform":"translateY("+startTranslate+"px)"});
		}else if(top_difference < 11){
			$scrollingHeaderSection.css({"-moz-transform":"translateY(11px)","-webkit-transform":"translateY(11px)","transform":"translateY(11px)"});			
		}
	}



  
    // tile mouse actions
    $('.main_media').on('mouseover', '.media_enlarged_image', function(){
      $(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
      var $itemToZoom = $(this);
    });
    $('.main_media').on('mouseout', '.media_enlarged_image', function(){
      $(this).children('.photo').css({'transform': 'scale(1)'});
      var $itemToZoom = $(this);
    });
    $('.main_media').on('click', '.media_enlarged_image', function(){
    	var $itemToZoom = $(this);
    	if(!$(this).hasClass('zoomUIActive')){
    		activateZoomState($itemToZoom);
    		$(this).addClass('zoomUIActive');
    		$(this).find('.photo').addClass('zoomPhotoActive');
    	}else{
    		$(this).find('.photo').remove();
    		$(this).removeClass('zoomUIActive');
    		$(this).find('.photo').removeClass('zoomPhotoActive');
    	}
    	
      	
      
      
    });
    $('.main_media').on('mousemove', '.media_enlarged_image', function(e){
      $(this).children('.photo').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
      var $itemToZoom = $(this);
    });
    // tiles set up


    function activateZoomState($itemToZoom){
	    $itemToZoom.each(function(){
	      $(this)
	        // add a photo container
	        .append('<div class="photo"></div>')
	        // set up a background image for each tile based on data-image attribute
	        .children('.photo').css({'background-image': 'url('+ $(this).attr('data-image') +')'});
	    });
	}

});
$(document).ready(function(){
	 var $grid = $('.grid').masonry({
	   itemSelector: '.grid-item',
	   isAnimated: true,
		  animationOptions: {
		    duration: 200,
		    easing: 'ease',
		    queue: false
		 },	   
		 percentPosition:true,
		 transitionDuration: '0.1s',
		 columnWidth: '.grid-item'
	 });

	 $grid.imagesLoaded().progress( function(){
	 	$grid.masonry('layout');
	 });

	 var elem = document.querySelector('.grid');
	 var msnry = new Masonry(elem, {
	   // options
	   itemSelector: '.grid-item',
	   isAnimated: true,
		  animationOptions: {
		    duration: 200,
		    easing: 'cubic-bezier(0.19, 1, 0.22, 1)',
		    queue: false
		 },	   
		 percentPosition:true,
		 transitionDuration: '0.1s',
		 columnWidth: '.grid-item'
	 });
});
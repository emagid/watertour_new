$(document).ready(function() {
	var windw = this;
	var $window = $(windw);
	$window.scroll(function(e){
        var window_width = $window.width();
        var scroll_top = $window.scrollTop();

        fix_product_filter_bar(window_width,scroll_top);
    });

    $(window).on('resize', function(){
    	var window_width = $window.width();
        var scroll_top = $window.scrollTop();
        fix_product_filter_bar(window_width,scroll_top);
    });

    function fix_product_filter_bar(window_width,scroll_top){
    	if((window_width > 775) && ($(".filters").length > 0)){
    		if(scroll_top >= 435){
    			$("body").addClass("fixed_products_page_body");
    			$(".filters").addClass("fixed_filter_bar");
    		} else{
    			$("body").removeClass("fixed_products_page_body");
    			$(".filters").removeClass("fixed_filter_bar");
    		}
    	} else{
    		$("body").removeClass("fixed_products_page_body");
    		$(".filters").removeClass("fixed_filter_bar");
    	}
    }
});
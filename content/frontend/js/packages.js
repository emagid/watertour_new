$(document).ready(function(){
	var paginatorWidth = $("body").width() - 56;
	if(paginatorWidth>1080){
		paginatorWidth=1080;
	}else{
		paginatorWidth=paginatorWidth;
	}
	setTimeout(function() { 
		$("div#paginator.datepaginator ul.pagination").css("width",paginatorWidth);
	},10);

	var date = moment();
	setMonthButtons(date);

	var options = {
	    selectedDateFormat:  'YYYY-MM-DD',
	    itemWidth:45,
	    startDate:date,
	    injectStyle:false,
	    width:paginatorWidth,
	    onSelectedDateChanged: function(event, date) {
			$.post('/packages/selectPackageDate'+location.search,{date:date.unix()},function($data){
				if($data.status == 'success'){
					var packGrid = $('.packagesGrid');
					packGrid.empty();
					$.each($data.packages,function(i,e){
						var html = '<div class="packageGridItem row"> ' +
						'<div class="media col" style="background-image:url(\'/content/uploads/packages/'+ e.image+'\');"></div> ' +
						'<div class="textualData col"> ' +
						'<h2 class="val">'+ e.name+'</h2> ' +
						'<h4 class="val gray"> ' +
						'<span class="time"><i class="material-icons">access_time</i>11:00 AM</span> ' +
						'<span class="duration"><i class="material-icons">timelapse</i>'+ e.duration+'</span> ' +
						'<span class="date"><i class="material-icons">date_range</i>'+ e.date+'</span> ' +
						'</h4> ' +
						'<div class="lineSep"></div> ' +
						'<a href="/packages/package/'+ e.slug+'" class="invBtn btn arrowBtn learnMore"> Learn More ' +
						'<icon style="background-image:url(\'/content/frontend/assets/img/btnLinkArrow.png\')"></icon> ' +
						'</a> ' +
						'</div> ' +
						' <div class="pricing col"> ' +
						'<div class="absTransCenter"> ' +
						'<h6 class="gray uppercase valB">Adults</h6> ' +
						'<div class="online_price circle"> ' +
						'<p class="gray uppercase valB">online</p> ' +
						'<h2 class="price"><span>$</span>'+ e.price+'</h2> ' +
						'</div> ' +
						'<div class="walkin circle"> ' +
						'<p class="gray uppercase valB">walkin</p> ' +
						'<h2 class="price"><span>$</span>'+ e.walkin+'</h2> ' +
						'</div> ' +
						'</div> ' +
						'</div>' +
						'<div class="bookBtnWrapper col"> ' +
						'<a class="btn fullBtn primaryBtn" href="packages/reservepackage/'+ e.slug+'?t='+ e.time+'"><p class="absTransCenter">Book this Package</p></a> ' +
						'</div> ' +
						'</div>';
						packGrid.append(html);
					});
				}
			});
	    	setMonthButtons(date);
	    },
	    navItemWidth:30
	};

	function setMonthButtons(date){
		var monthNames = ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		];
		var month=moment(date).month();

		var onePrevMonth=moment(date).subtract(1,'M');
		var onePrevMonth_name=monthNames[onePrevMonth.month()] + ' ' + onePrevMonth.year();

		var twoPrevMonth=moment(date).subtract(2,'M');
		var twoPrevMonth_name=monthNames[twoPrevMonth.month()] + ' ' + twoPrevMonth.year();

		var threePrevMonth=moment(date).subtract(3,'M');
		var threePrevMonth_name=monthNames[threePrevMonth.month()] + ' ' + threePrevMonth.year();

		var oneNextMonth=moment(date).add(1,'M');
		var oneNextMonth_name=monthNames[oneNextMonth.month()] + ' ' + oneNextMonth.year();

		var twoNextMonth=moment(date).add(2,'M');
		var twoNextMonth_name=monthNames[twoNextMonth.month()] + ' ' + twoNextMonth.year();

		var threeNextMonth=moment(date).add(3,'M');
		var threeNextMonth_name=monthNames[threeNextMonth.month()] + ' ' + threeNextMonth.year();

		var $datePaginatorButtonsRow=$(".datePaginatorButtonsRow");

		$datePaginatorButtonsRow.find(".threePrev a").attr("data-dateTime",threePrevMonth);
		$datePaginatorButtonsRow.find(".threePrev a p").text(threePrevMonth_name);

		$datePaginatorButtonsRow.find(".twoPrev a").attr("data-dateTime",twoPrevMonth);
		$datePaginatorButtonsRow.find(".twoPrev a p").text(twoPrevMonth_name);

		$datePaginatorButtonsRow.find(".onePrev a").attr("data-dateTime",onePrevMonth);
		$datePaginatorButtonsRow.find(".onePrev a p").text(onePrevMonth_name);

		$datePaginatorButtonsRow.find(".onePost a").attr("data-dateTime",oneNextMonth);
		$datePaginatorButtonsRow.find(".onePost a p").text(oneNextMonth_name);

		$datePaginatorButtonsRow.find(".twoPost a").attr("data-dateTime",twoNextMonth);
		$datePaginatorButtonsRow.find(".twoPost a p").text(twoNextMonth_name);

		$datePaginatorButtonsRow.find(".threePost a").attr("data-dateTime",threeNextMonth);
		$datePaginatorButtonsRow.find(".threePost a p").text(threeNextMonth_name);

		$(".datePaginatorButtonsRow > div > div > a").each(function(){
			var btnTime=$(this).attr("data-dateTime");
			var btnTimeFormatted=moment(btnTime,'x');
			var dateFormatted=moment(date,'x');
			var dateNow=moment();
			var timeDiff=dateNow.diff(btnTimeFormatted);
			$(this).attr("data-nowTimeDiff",timeDiff);
			if(timeDiff>0){
				$(this).addClass("disabled");
			}else{
				$(this).removeClass("disabled");
			}
		});
	}

	$('body').on('click','.datePaginatorButtonsRow > div > div > a',function(){
		var dateSelected=$(this).attr("data-dateTime");
		var dateSelectedFormatted=moment.utc(dateSelected, 'x').format('YYYY-MM-DD')
		$('#paginator').datepaginator('setSelectedDate', dateSelectedFormatted);
	});

	$('body').on('click','.responsiveMapResultsToggler .togglerWrapper > a',function(){
		if(!$(this).hasClass("active")){
			$('.responsiveMapResultsToggler .togglerWrapper > a.active').removeClass('active');
			$(this).addClass("active");
			$(".attractionsMapSearch").toggleClass("attractionsListingsActive");
		}
	});



    $(window).resize(function() {
    	$('#paginator').datepaginator(options);
    	$("div#paginator.datepaginator ul.pagination").css("width",paginatorWidth);
    });

	$('#paginator').datepaginator(options);

	var attractionsScrollTop = $(".col.attractionsBrowseListings.sidebar").scrollTop();
	attractions_scroll_activated_ui(attractionsScrollTop);
	$(".col.attractionsBrowseListings.sidebar").scroll(function() {
	  var attractionsScrollTop = $(".col.attractionsBrowseListings.sidebar").scrollTop();
	  attractions_scroll_activated_ui(attractionsScrollTop);
	});


	function attractions_scroll_activated_ui(scrollTop){
		var translateY = scrollTop * -1;
		console.log(scrollTop);
		if(scrollTop<47){
			$(".sidebar .filters").css({"-ms-transform":"translateY("+translateY+"px)","-o-transform":"translateY("+translateY+"px)","-moz-transform":"translateY("+translateY+"px)","-webkit-transform":"translateY("+translateY+"px)","transform":"translateY("+translateY+"px)"});
		}else{
			$(".sidebar .filters").css({"-ms-transform":"translateY(-47px)","-o-transform":"translateY(-47px)","-moz-transform":"translateY(-47px)","-webkit-transform":"translateY(-47px)","transform":"translateY(-47px)"});
		}
	}

	$("input.typeahead").focusin(function() {
		var attrIn = $(this).attr("data-placeholder-infocus");
		var attrOut = $(this).attr("data-placeholder-outfocus");
		$(this).attr("placeholder", attrIn);
	});
	$("input.typeahead").focusout(function() {
		var attrIn = $(this).attr("data-placeholder-infocus");
		var attrOut = $(this).attr("data-placeholder-outfocus");
		$(this).attr("placeholder", attrOut);
	});

});
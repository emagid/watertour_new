$(document).ready(function(){
	var scrollTop = $(window).scrollTop();
	scroll_activated_ui_pl(scrollTop);
	$(window).scroll(function() {
	  var scrollTop = $(window).scrollTop();
	  scroll_activated_ui_pl(scrollTop);

    $(".privateLabelSection").each(function() {
    	var number = $(this).attr("data-vertical_screen");
    	var $related_nav_toggle=$("#privateLabelNav li a[data-vertical_screen_nav="+number+"]");
    	var top_position=$(this).offset().top - 150;
    	var height = $(this).height();
    	var bottom_position=top_position+height;
    	var pageHeight = $(document).height();
    	var windowHeight = $(window).height();
    	var footerHeight = $(".mainFooterWrapper").height();
    	var pageFooterDifference = pageHeight - footerHeight;
    	var scrollPageFooterDiff = pageFooterDifference - scrollTop;

    	if(scrollTop>top_position&&scrollTop<bottom_position){
    		$("#privateLabelNav li a").removeClass("activePageSection");
    		$related_nav_toggle.addClass("activePageSection");
    	}

    	if(scrollPageFooterDiff <= windowHeight){
    		$(".privateLabelPageWrapper .show_mdl.floatingActionBtn").addClass("makeAbs");
    	}else{
    		$(".privateLabelPageWrapper .show_mdl.floatingActionBtn").removeClass("makeAbs");
    	}


	});

	});

	function scroll_activated_ui_pl(scrollTop){
		console.log(scrollTop);

		if(scrollTop>211){
			$(".introView.cushion_h1 icon.circleLogoOreo").addClass("fixed");
		}else{
			$(".introView.cushion_h1 icon.circleLogoOreo").removeClass("fixed");
		}

		if(scrollTop>=66){
			$(".floatingNav").addClass("fixed_nav");
			$(".privateLabelPageWrapper .show_mdl.floatingActionBtn").addClass("fixed_fab");
		}else{
			$(".floatingNav").removeClass("fixed_nav");
			$(".privateLabelPageWrapper .show_mdl.floatingActionBtn").removeClass("fixed_fab");			
		}
	}

	$(document).on('click','#privateLabelNav li a',function(e){
		e.preventDefault();
		var number = $(this).attr("data-vertical_screen_nav");
		var $related_section=$(".privateLabelSection[data-vertical_screen="+number+"]");
		goToByScroll($related_section); 

	});

	function goToByScroll($related_section){
		var scrollToPosition = $related_section.offset().top - 55;
	    $('html,body').animate({
	        scrollTop: scrollToPosition},
	        '1400','easeOutSine');
	}
});
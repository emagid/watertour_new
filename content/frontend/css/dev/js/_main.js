$(document).ready(function(){

	$('.ccv a').popover();

	$("nav a").filter(function(){
		return this.href == location.href.replace(/#.*/, "");
	}).closest('li').addClass("active");



	$('.navbar-default .dropdown-toggle').click(function() {
		var location = $(this).attr('href');
		window.location.href = location;
		return false;
	});

	$('input[type="radio"], input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square',
		radioClass: 'iradio_square'
	});

	$(function($) {
		var num_cols = 4,
		container = $('.split-list'),
		listItem = 'li',
		listClass = 'sub-list';
		container.each(function() {
			var items_per_col = new Array(),
			items = $(this).find(listItem),
			min_items_per_col = Math.floor(items.length / num_cols),
			difference = items.length - (min_items_per_col * num_cols);
			for (var i = 0; i < num_cols; i++) {
				if (i < difference) {
					items_per_col[i] = min_items_per_col + 1;
				} else {
					items_per_col[i] = min_items_per_col;
				}
			}
			for (var i = 0; i < num_cols; i++) {
				$(this).append($('<ul ></ul>').addClass(listClass));
				for (var j = 0; j < items_per_col[i]; j++) {
					var pointer = 0;
					for (var k = 0; k < i; k++) {
						pointer += items_per_col[k];
					}
					$(this).find('.' + listClass).last().append(items[j + pointer]);
				}
			}
		});
	});


	$(function($) {
		var num_cols = 3,
		container = $('.brands ul'),
		listItem = 'li',
		listClass = 'col-sm-8';
		container.each(function() {
			var items_per_col = new Array(),
			items = $(this).find(listItem),
			min_items_per_col = Math.floor(items.length / num_cols),
			difference = items.length - (min_items_per_col * num_cols);
			for (var i = 0; i < num_cols; i++) {
				if (i < difference) {
					items_per_col[i] = min_items_per_col + 1;
				} else {
					items_per_col[i] = min_items_per_col;
				}
			}
			for (var i = 0; i < num_cols; i++) {
				$(this).append($('<ul ></ul>').addClass(listClass));
				for (var j = 0; j < items_per_col[i]; j++) {
					var pointer = 0;
					for (var k = 0; k < i; k++) {
						pointer += items_per_col[k];
					}
					$(this).find('.' + listClass).last().append(items[j + pointer]);
				}
			}
		});
	});



	$('.box-three .inner').matchHeight({
		byRow: true,
		property: 'height',
		target: null,
		remove: false
	});


	$( ".search img" ).click(function() {
		$( "form.search .search-wrap" ).fadeToggle( "slow", "linear" );
	});


	$('.flex-product .slides a').magnificPopup({
		removalDelay: 500,
		type: 'image',
		gallery:{
			enabled:true
		},
		mainClass: 'mfp-zoom-in',
		callbacks: {

			open: function() {
                //overwrite default prev + next function. Add timeout for css3 crossfade animation
                $.magnificPopup.instance.next = function() {
                	var self = this;
                	self.wrap.removeClass('mfp-image-loaded');
                	setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
                }
                $.magnificPopup.instance.prev = function() {
                	var self = this;
                	self.wrap.removeClass('mfp-image-loaded');
                	setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
                }
            },
            imageLoadComplete: function() { 
            	var self = this;
            	setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
            }
        },
        closeOnContentClick: true,
        midClick: true
    });


});


$(window).load(function() {
	$('.flex-home').flexslider({
		animation: "fade",
		controlNav: false,
		prevText: "", 
		nextText: "", 
		start: function(){
			$('.flexslider .slides li').fadeIn();
		}
	});
	
});

$(document).ready(function(){
	var submitIcon = $('.searchbox-icon');
	var inputBox = $('.searchbox-input');
	var searchBox = $('.searchbox');
	var isOpen = false;
	submitIcon.click(function(){
		if(isOpen == false){
			searchBox.addClass('searchbox-open');
			inputBox.focus();
			isOpen = true;
		} else {
			searchBox.removeClass('searchbox-open');
			inputBox.focusout();
			isOpen = false;
		}
	});  
	submitIcon.mouseup(function(){
		return false;
	});
	searchBox.mouseup(function(){
		return false;
	});
	$(document).mouseup(function(){
		if(isOpen == true){
			$('.searchbox-icon').css('display','block');
			submitIcon.click();
		}
	});
});
function buttonUp(){
	var inputVal = $('.searchbox-input').val();
	inputVal = $.trim(inputVal).length;
	if( inputVal !== 0){
		$('.searchbox-icon').css('display','none');
	} else {
		$('.searchbox-input').val('');
		$('.searchbox-icon').css('display','block');
	}
}








$(window).load(function() {
  // The slider being synced must be initialized first
  $('.flex-product #carousel').flexslider({
  	animation: "slide",
  	controlNav: false,
  	animationLoop: false,
  	slideshow: false,
  	itemWidth: 191,
  	itemMargin: 30,
  	asNavFor: '#slider'
  });

  $('.flex-product #slider').flexslider({
  	animation: "fade",
  	controlNav: false,
  	animationLoop: false,
  	slideshow: false,
  	sync: "#carousel"
  });
});




$(window).load(function() {
	$('.flex-similar').flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: 190,
		itemMargin: 10,
		minItems: 2,
		maxItems: 4,
		controlNav: false,
		prevText: "",  
		nextText: "",          
	});
});
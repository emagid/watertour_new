<?php
date_default_timezone_set('America/New_York');
error_reporting(0);


require_once("libs/Emagid/emagid.php");
require_once("conf/emagid.conf.php");
require_once('includes/functions.php');
require_once('templates/notification_template.php');
$emagid = new \Emagid\Emagid($emagid_config);

if (session_status() == PHP_SESSION_NONE) {
    \session_start();
}
$emagid->loadMvc($site_routes);

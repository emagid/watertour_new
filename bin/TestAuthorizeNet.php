<?php

include(__DIR__.'/../index.php');
require_once(ROOT_DIR.'libs/authorize/AuthorizeNet.php');

function testAuthorizeAndCapture(){
    $transaction = new AuthorizeNetAIM();
    $transaction->setFields([
        'amount'=>'1000',
        'card_num'=>'4111111111111111',
        'card_code'=>'123',
        'exp_date'=>'01/19',
        'first_name'=>'Haro',
        'last_name'=>'Brimly',
        'email'=>'brimstone@test.com',
        'phone'=>'1234567890',
        'invoice_num'=>'5'
    ]);
    $response = $transaction->authorizeAndCapture();
    $response->authorization_code;
    $response->transaction_id;
    print_r(json_encode($response));
}

testAuthorizeAndCapture();
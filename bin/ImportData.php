<?php

require_once(__DIR__."/../libs/Emagid/emagid.php");
require_once(__DIR__."/../conf/emagid.conf.php");
require_once(__DIR__.'/../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

function import(){
    importTours();
    importPackages();
}

function importTours(){
    echo "\n-----------Tours\n";
    $tours = 'http://www.topviewapi.com/api/tours';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$tours);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $response = curl_exec($ch);

    $tours = json_decode($response,true);
    foreach($tours as $tour){
        $t = new \Model\Tour();
        $t->name = $tour['Name'];

        /**
         * Update tour by name
         */
        if($existingTour = \Model\Tour::getItem(null, ['where' => "lower(name) = '".strtolower($t->name)."'"])){
            $t->id = $existingTour->id;
        }

        $slug = preg_replace('/[^\w-]/','-',strtolower($tour['Name']));
        $slug = preg_replace('/[-]+/','-',$slug);
        $t->slug = $slug;
        if($tour['Validity'] == 'Single'){
            $tour['Validity'] = 'Single Use Only';
        }
        $t->validity = $tour['Validity'] ? array_search($tour['Validity'],\Model\Tour::$validity) : -1;
        $t->tour_code = $tour['ID'];

        //duration hour/min
        $durationParams = ['hour','minute'];
        $duration = strtolower($tour['Duration']);
        if ($duration == null || $duration == 'tour specific') {
            $t->duration_hour = 0;
            $t->duration_minute = 0;
        } else if (strstr($duration, 'hour') || strstr($duration, 'min')) {
            foreach ($durationParams as $durationParam) {
                $arr = explode(' ', str_replace('-', ' ', $duration));
                $index = key(array_filter($arr, function ($a) use ($durationParam) {
                    if (strstr($a, substr($durationParam, 0, 3))) {
                        return true;
                    }
                    return false;
                }));
                $field = "duration_$durationParam";
                if(count($index) > 0){
                    $t->$field = $arr[$index - 1];
                }
            }
        } else if ('all day' == $duration) {
            $t->duration_hour = 24;
            $t->duration_minute = 0;
        }

        //availability
        $availability = strtolower($tour['Availability']);
        if($availability == 'tour specific' || $availability == "null"){
            $t->available_range = json_encode([]);
        } else if(strstr($availability,'daily')){
            $times = [];
            preg_match_all('/([0-9]\w*):?([0-9]\w*)?/', $availability,$times);
            $dbTimes = [];
            if (strpos($availability, '-')) {
                $start = ampm($times[0][0]); $end = ampm($times[0][1]);
                if($start > $end){
                    $end += 24;
                }
                for ($i = $start; $i <= $end; $i++) {
                    if ($i > 24) {
                        $dbTimes[] = $i - 24;
                    } else {
                        $dbTimes[] = $i;
                    }
                }
            } else if (count($times[0]) == 1) {
                $dbTimes[] = ampm($times[0][0]);
            } else if (strpos($availability, ',')) {
                foreach ($times[0] as $time) {
                    $dbTimes[] = ampm($time);
                }
            }
            $a = ['o' => 'Daily', 'h' => $dbTimes];
            $t->available_range = json_encode($a);
        } else if (strstr($availability, 'from')) {
            $times = [];
            preg_match_all('/([0-9]\w*):?([0-9]\w*)?/', $availability,$times);
            $dbTimes = [];
            for ($i = ampm($times[0][0]); $i <= ampm($times[0][1]); $i++) {
                $dbTimes[] = $i;
            }
            $a = ['o'=>'Daily','h'=>$dbTimes];
            $t->available_range = json_encode($a);
        } else if(checkWeek($availability)){
            $times = [];
            preg_match_all('/([0-9]\w*):?([0-9]\w*)?/', $availability,$times);
            $dbTimes = [];
            $dow = ['mon'=>'Monday','tue'=>'Tuesday','wed'=>'Wednesday','thu'=>'Thursday','fri'=>'Friday','sat'=>'Saturday','sun'=>'Sunday'];
            $weeks = [];
            foreach($dow as $short=>$long){
                if(strstr($availability,$short)){
                    $weeks[] = $short;
                }
            }
            if(strstr($availability,' to ')){
                for($i = ampm($times[0][0]); $i <= ampm($times[0][1]); $i++){
                    $dbTimes[] = $i;
                }
            } else {
                foreach ($times[0] as $time) {
                    $i = ampm($time);
                    $dbTimes[] = $i;
                }
            }
            $a = ['o'=>'Weekly','w'=>$weeks,'h'=>$dbTimes];
            $t->available_range = json_encode($a);
        }
        $t->valid_start = date('Y-m-d H:i:s',strtotime($tour['ValidFrom']));
        $t->valid_end = date('Y-m-d H:i:s',strtotime($tour['ValidTo']));

        if($t->save()){
            echo "-----$t->id\n";
//            echo "Name: $t->name\n";
//            echo "Validity: $t->validity\n";
//            echo "Duration: $t->duration_hour : $t->duration_minute\n";
//            echo "Availability: $t->availability_range\n";
//            echo "Valid from - to: $t->valid_start - $t->valid_end\n";
        } else {
            var_dump($t->errors);
        }

    }
    curl_close($ch);
}
function importPackages(){
    echo "\n-----------Packages\n";
    //$packages = 'http://www.topviewapi.com/api/packages';
    $packages = 'http://www.topviewapi.com/api/packages/topview';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$packages);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $response = curl_exec($ch);

    $packages = json_decode($response,true);
    foreach($packages as $package){
        $existingPackage = \Model\Package::getItem(null, ['where' => "lower(name) = '".strtolower($package['Name'])."'"]);
        if($existingPackage){
            continue;
        }
        if($package['Category'] == 'Bus Tours'){
            $tourIds = explode(';',$package['TourCodes']);
            $p = new \Model\Package();
            $p->name = $package['Name'];
            $slug = preg_replace('/[^\w-]/','-',strtolower($package['Name']));
            $slug = preg_replace('/[-]+/','-',$slug);
            $p->slug = $slug;
            $p->status = $package['Status'] ? 1: 0;
            $p->adults_price = $package['AdultPrice'];
            $p->kids_price = $package['KidsPrice'];
            $p->tour_id = json_encode($tourIds);
            $p->package_id = $package['ID'];

            if($p->save()){
                echo "-----$p->id\n";
//                echo "Name: $p->name\n";
//                echo "Status: $p->status\n";
//                echo "Adult Price: $p->adults_price\n";
//                echo "Kids Price: $p->kids_price\n";
//                echo "Tour Id: $p->tour_id\n";
                if(count($tourIds) == 1){
                    $t = \Model\Tour::getItem(null,['where'=>"tour_code = {$package['TourCodes']}"]);
                    $t->external_package_id = $p->package_id;
                    $t->save();
                    echo "-----Update $t->id\n";
                }
            }else {
                var_dump($p->errors);
            }
        }
    }

    curl_close($ch);
}
function ampm($string){
    $ampm = strtolower($string);
    if(strstr($ampm,'am') !== false){
        $time = str_replace('am','',$ampm);
        $ex = explode(':',$time);
        $ret = $ex[0];
        if(isset($ex[1]) && $ex[1] > 0){
            $ret += $ex[1] / 60;
        }
        return $ret;
//        return intval(filter_var($ampm,FILTER_SANITIZE_NUMBER_INT));
    } else {
        $time = str_replace('pm','',$ampm);
        $ex = explode(':',$time);
        $ret = $ex[0];
        if(isset($ex[1]) && $ex[1] > 0){
            $ret += $ex[1] / 60;
        }
        return $ret+12;
//        return intval(filter_var($ampm, FILTER_SANITIZE_NUMBER_INT))+12;
    }
}
function checkWeek($string){
    $dayOfWeek = ['mon','tue','wed','thu','fri','sat','sun'];
    foreach($dayOfWeek as $value){
        if(strstr(strtolower($string),$value)){
            return true;
        }
    } return false;
}

import();
<?php
include __DIR__.'/../index.php';

echo "\n-----------Packages\n";
//$packages = 'http://www.topviewapi.com/api/packages';
$packages = 'http://www.topviewapi.com/api/packages/topview';
$ch = curl_init();
curl_setopt($ch,CURLOPT_URL,$packages);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
$response = curl_exec($ch);

$packages = json_decode($response,true);
$updatingPackages = [];
foreach($packages as $package){
    $p = \Model\Package::getItem(null, ['where' => "lower(name) = '".strtolower($package['Name'])."'"]);
    if(!$p || !$p->id){
        $p = new \Model\Package(); // if existing package not exist, we create new one
    }
    if($package['Category'] == 'Bus Tours'){
        $tourIds = explode(';',$package['TourCodes']);
        $p->active = 1;
        $p->name = $package['Name'];
        $slug = preg_replace('/[^\w-]/','-',strtolower($package['Name']));
        $slug = preg_replace('/[-]+/','-',$slug);
        $p->slug = $slug;
        $p->status = $package['Status'] ? 1: 0;
        $p->adults_price = $package['AdultPrice'];
        $p->kids_price = $package['KidsPrice'];
        $p->tour_id = json_encode($tourIds);
        $p->package_id = $package['ID'];

        if($p->save()){
            echo "-----$p->id\n";
//                echo "Name: $p->name\n";
//                echo "Status: $p->status\n";
//                echo "Adult Price: $p->adults_price\n";
//                echo "Kids Price: $p->kids_price\n";
//                echo "Tour Id: $p->tour_id\n";
            if(count($tourIds) == 1){
                $t = \Model\Tour::getItem(null,['where'=>"tour_code = {$package['TourCodes']}"]);
                $t->external_package_id = $p->package_id;
                $t->save();
                echo "-----Update $t->id\n";
            }
        }else {
            var_dump($p->errors);
        }
    }

    $updatingPackages[$p->id] = 1;
}

foreach(\Model\Package::getList() as $package){
    // package not exist in responded API packages, we inactive the package
    if(!isset($updatingPackages[$package->id])){
        \Model\Package::delete($package->id);
    }
}

curl_close($ch);
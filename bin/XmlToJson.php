<?php

include(__DIR__.'/../index.php');

function xmlToJson(){
    $s = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
            <s:Header />
            <s:Body>
            <GetToursResponse xmlns="http://tempuri.org/">
            <GetToursResult xmlns:a="http://schemas.datacontract.org/2004/07/WcfService1.Proxies" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <a:TourProxy>
            <a:Availability>Daily at 10AM, 1PM, 4PM</a:Availability>
            <a:Duration>2 Hours</a:Duration>
            <a:ID>10</a:ID>
            <a:Name>CENTRAL PARK BIKE TOUR</a:Name>
            <a:ValidFrom>2015-10-01T00:00:00</a:ValidFrom>
            <a:ValidTo>2016-03-31T00:00:00</a:ValidTo>
            <a:Validity>Single Use Only</a:Validity>
            </a:TourProxy>
            <a:TourProxy>
            <a:Availability>Tue.,Thur.,Sun., at 7PM</a:Availability>
            <a:Duration>2 Hours</a:Duration>
            <a:ID>11</a:ID>
            <a:Name>NIGHT BIKE TOUR</a:Name>
            <a:ValidFrom>2015-10-01T00:00:00</a:ValidFrom>
            <a:ValidTo>2016-03-31T00:00:00</a:ValidTo>
            <a:Validity>Single Use Only</a:Validity>
            </a:TourProxy>
            </GetToursResult>
            </GetToursResponse>
            </s:Body>
            </s:Envelope>';
//    $s = '<Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><Header/><wtf>wtf</wtf></Envelope>';
//
    $xml = simplexml_load_string($s);

//    $xml = new SimpleXMLElement($s);
    var_dump(xml2array($xml));

//    $content = file_get_contents(__DIR__.'/Tours.xml');
//    $xml = simplexml_load_string($content);
//    $xml = simplexml_load_file(__DIR__.'/Tours.xml') or die("Error: Cannot create file");
//    $p = xml_parser_create();
//    xml_parse_into_struct($p,$content, $vals, $index);

//    $arr = ['availability','duration','id','name','validfrom','validto','validity'];
//    foreach($vals as $val){
//        foreach($arr as $a){
//            if(strpos(strtolower($val['tag']),$a) != false){
//                echo $val['value']."\n";
//            }
//        }
//    }
//
    var_dump($xml->children());
}

function xml2array ( $xmlObject, $out = array () )
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ||  is_array ( $node ) ) ? xml2array ( $node ) : $node;

    return $out;
}

xmlToJson();
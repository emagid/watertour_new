create table administrator(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
first_name character varying,
last_name character varying,
email character varying,
username character varying,
password character varying,
hash character varying,
permissions character varying);

create table admin_roles(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
admin_id integer not null,
role_id integer not null
);

create table banner(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
title character varying,
description character varying,
image character varying,
url character varying,
featured integer
);

create table brand(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
description character varying,
slug character varying,
menu_display integer,
meta_title character varying,
meta_keywords character varying,
meta_description character varying,
banner character varying,
parent_id integer default 0);

create table category(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
parent_category integer,
description character varying,
slug character varying,
title character varying,
banner character varying,
meta_title character varying,
meta_keywords character varying,
meta_description character varying,
display_order INTEGER
);

create table config(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
value character varying
);

create table public.order(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
bill_first_name character varying,
bill_last_name character varying,
bill_address character varying,
bill_address2 character varying,
bill_city character varying,
bill_state character varying,
bill_country character varying,
bill_zip character varying,
ship_first_name character varying,
ship_last_name character varying,
ship_address character varying,
ship_address2 character varying,
ship_city character varying,
ship_state character varying,
ship_country character varying,
ship_zip character varying,
phone character varying,
tax character varying,
tax_rate character varying,
status character varying,
tracking_number character varying,
shipping_method character varying,
user_id integer,
coupon_code character varying,
gift_card character varying,
email character varying,
shipping_cost real,
payment_method integer,
subtotal real,
total real,
coupon_type character varying,
coupon_amount real,
viewed boolean,
comment character varying,
fraud_status character varying
);

create table page(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
title character varying,
slug character varying,
description character varying,
featured_image character varying,
meta_title character varying,
meta_keywords character varying,
meta_description character varying
);

create table product(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
price numeric(10,2),
description character varying,
mpn character varying,
msrp numeric(10,2),
slug character varying,
tags character varying,
meta_title character varying,
meta_keywords character varying,
meta_description character varying,
featured_image character varying,
availability integer,
age_group character varying,
details CHARACTER VARYING
);
create table role(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying not null
);

create table shipping_method(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
is_default boolean,
name character varying,
cart_subtotal_range_min character varying,
cost character varying
);

create table public.user(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
email character varying not null,
password character varying not null,
hash character varying,
first_name character varying,
last_name character varying
);

create table user_roles(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
user_id integer not null,
role_id integer not null
);

insert into config (name,value) values (
'Meta Title','Bike Tours Title WIP'
);
insert into config (name,value) values (
'Meta Description','Bike Tours Description WIP'
);
insert into config (name,value) values (
'Meta Keywords','Bike Tours Keywords WIP'
);

insert into administrator (first_name, last_name, email, username, password, hash, permissions) VALUES (
'Master','User','admin@admin.com','emagid','5569cb9713e7fed733917f7c465d8c0a091e08a0e20837bdc836989b702fa3aa','591232c9fccd7c9d29c1e090f60964b7','Catalog,Categories,Products,Price alerts,Newproducts,Kenjos,Attributes,Hottest Deal,Attributes,Bezels,Bracestraps,Buckles,Buckle_colors,Casebks,Casematerials,Crowns,Crystals,Funcs,Lug_widths,Movement_drops,Water_ress,Water_res_multis,Order Management,Orders,Customer Center,Wishlist,Users,Questions,Newsletter,Content,Pages,News,Banners,Main,Featured,Deal of the Week,System,Coupons,Shipping Methods,Brands,Colors,Collections,Materials,Configs,Answers,Administrators'
);

INSERT INTO role (name) VALUES ('admin');

INSERT INTO admin_roles(admin_id, role_id) VALUES ('1','1');

-- September 9th
alter table product rename to tour;
alter table category rename to package;
create table tour_images(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null default now(),
tour_id INTEGER,
image CHARACTER VARYING,
display_order INTEGER
);
create table package_images(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null default now(),
package_id INTEGER,
image CHARACTER VARYING,
display_order INTEGER
);
alter table tour drop column mpn, drop column age_group;
alter table tour add column validity INTEGER /*ENUM*/,
  add column duration_hour INTEGER,
  add column duration_minute INTEGER,
  add column start_date TIMESTAMP without time zone,
  add column end_date TIMESTAMP without time zone,
  add column departure_point CHARACTER VARYING,
  add column valid_start TIMESTAMP without time zone,
  add column valid_end TIMESTAMP without time zone,
  add column languages text /*JSON*/,
  add column route CHARACTER VARYING /*OR IMAGE*/;
create table language(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone,
name CHARACTER VARYING
);
alter table package drop column display_order, drop column parent_category;
alter table package add column tour_id text /*JSON*/,
  add column upgrade_options CHARACTER VARYING,
  add column adults_price numeric(10,2),
  add column kids_price numeric(10,2),
  add column status SMALLINT,
  add column start_date TIMESTAMP without time zone,
  add column end_date TIMESTAMP without time zone;

-- September 12th
alter sequence category_id_seq rename to package_id_seq;
alter sequence product_id_seq rename to tour_id_seq;
alter index category_pkey rename to package_pkey;
alter index product_pkey rename to tour_pkey;

alter table language add column flag CHARACTER VARYING;
create table abandoned_cart(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null DEFAULT now(),
user_id INTEGER,
guest_id CHARACTER VARYING,
checkout_fields text,
cart CHARACTER VARYING,
device CHARACTER VARYING,
browser CHARACTER VARYING,
version CHARACTER VARYING
);
create table product(
id serial primary key,
active SMALLINT default 1,
insert_time timestamp without time zone not null default now(),
name CHARACTER VARYING,
slug CHARACTER VARYING,
tags CHARACTER VARYING,
meta_title CHARACTER VARYING,
meta_keywords CHARACTER VARYING,
meta_description CHARACTER VARYING,
price numeric(10,2),
discount numeric (10,2),
featured_image CHARACTER VARYING,
description CHARACTER VARYING
);
alter table public.order add column package_id INTEGER, add column adult_count INTEGER, add COLUMN kid_count INTEGER, add column start_date TIMESTAMP without time zone;
CREATE TABLE bus_package
(
  id serial primary key,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone NOT NULL DEFAULT now(),
  name character varying,
  description character varying,
  slug character varying,
  title character varying,
  banner character varying,
  meta_title character varying,
  meta_keywords character varying,
  meta_description character varying,
  bus_tour_id text,
  upgrade_options character varying,
  adults_price numeric(10,2),
  kids_price numeric(10,2),
  status smallint,
  start_date timestamp without time zone,
  end_date timestamp without time zone
);
CREATE TABLE bus_tour
(
  id serial primary key,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone NOT NULL DEFAULT now(),
  name character varying,
  price numeric(10,2),
  description character varying,
  msrp numeric(10,2),
  slug character varying,
  tags character varying,
  meta_title character varying,
  meta_keywords character varying,
  meta_description character varying,
  featured_image character varying,
  availability integer,
  details character varying,
  validity integer,
  duration_hour integer,
  duration_minute integer,
  start_date timestamp without time zone,
  end_date timestamp without time zone,
  departure_point character varying,
  valid_start timestamp without time zone,
  valid_end timestamp without time zone,
  languages text,
  route character varying
);
CREATE TABLE water_package
(
  id serial primary key,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone NOT NULL DEFAULT now(),
  name character varying,
  description character varying,
  slug character varying,
  title character varying,
  banner character varying,
  meta_title character varying,
  meta_keywords character varying,
  meta_description character varying,
  water_tour_id text,
  upgrade_options character varying,
  adults_price numeric(10,2),
  kids_price numeric(10,2),
  status smallint,
  start_date timestamp without time zone,
  end_date timestamp without time zone
);
CREATE TABLE water_tour
(
  id serial primary key,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone NOT NULL DEFAULT now(),
  name character varying,
  price numeric(10,2),
  description character varying,
  msrp numeric(10,2),
  slug character varying,
  tags character varying,
  meta_title character varying,
  meta_keywords character varying,
  meta_description character varying,
  featured_image character varying,
  availability integer,
  details character varying,
  validity integer,
  duration_hour integer,
  duration_minute integer,
  start_date timestamp without time zone,
  end_date timestamp without time zone,
  departure_point character varying,
  valid_start timestamp without time zone,
  valid_end timestamp without time zone,
  languages text,
  route character varying
);
update config set value = 'Bike Tours Title WIP' where id = 1;
update config set value = 'Bike Tours Description WIP' where id = 2;
update config set value = 'Bike Tours Keywords WIP' where id = 3;
create table route(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
featured_image CHARACTER VARYING,
description CHARACTER VARYING,
points text /*JSON*/
);

-- September 15th
alter table tour add column available_range text;

-- September 22nd
create table rental(
id serial primary key,
active SMALLINT default 1,
insert_time timestamp without time zone not null default now(),
name CHARACTER VARYING,
slug CHARACTER VARYING,
featured_image CHARACTER VARYING,
adult_price numeric(10,2),
kid_price numeric(10,2),
discount_amount numeric(10,2),
discount_type SMALLINT,
description CHARACTER VARYING,
sku CHARACTER VARYING,
meta_title CHARACTER VARYING,
meta_keywords CHARACTER VARYING,
meta_description CHARACTER VARYING
);

create table cart(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
package_id INTEGER,
rentals text, /*JSON*/
adult_count INTEGER,
kid_count INTEGER,
reserve_date TIMESTAMP without time zone,
duration INTEGER,
ref_num CHARACTER VARYING
);

-- September 26th
alter table cart alter column package_id type text using package_id::text;
alter table cart add column tour_id text, add column products text;
alter table public.order alter column package_id type text using package_id::text;
alter table public.order add column card_name CHARACTER VARYING;
alter table public.order add column cc_type CHARACTER VARYING;
alter table public.order add column cc_number NUMERIC(16,0), add column cc_expiration_month INTEGER, add column cc_expiration_year INTEGER, add column cc_ccv INTEGER;
alter table public.order add column guest_id CHARACTER VARYING;
alter table public.order add column ref_num CHARACTER VARYING;
alter table public.order add column note CHARACTER VARYING;
alter table public.order add column error_message CHARACTER VARYING;
create table order_products(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
order_id INTEGER,
products text,
rentals text
);

-- September 27th
alter table public.order add column tour_id text;
alter table banner add column banner_type INTEGER;
alter table banner add column order_num INTEGER;

-- September 28th
create table nav(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
url CHARACTER VARYING,
display_order INTEGER
);
create table sub_nav(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time timestamp without time zone not null default now(),
nav_id INTEGER,
name CHARACTER VARYING,
url CHARACTER VARYING,
display_order INTEGER
);
alter table rental add column category CHARACTER VARYING;
alter table cart add column email CHARACTER VARYING, add column full_name CHARACTER VARYING, add COLUMN phone CHARACTER VARYING;

-- October 3rd
alter table order_products add column tours text, add column packages text;
alter table public.order drop column package_id, drop column tour_id;

-- October 4th
alter table tour alter column duration_hour type numeric(10,1) using duration_hour::numeric(10,1);
alter table tour alter column duration_minute type numeric(10,1) using duration_hour::numeric(10,1);

-- October 14th
alter table public.order add column authorization_code CHARACTER VARYING, add column transaction_id CHARACTER VARYING;
alter table cart add column user_id INTEGER, add column guest_id CHARACTER VARYING;

-- October 19th
create table attraction(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
slug CHARACTER VARYING,
description CHARACTER VARYING,
lat CHARACTER VARYING,
lng CHARACTER VARYING,
featured_image CHARACTER VARYING,
external_link CHARACTER VARYING
);

-- October 20th
create table attraction_images(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null default now(),
attraction_id INTEGER,
image CHARACTER VARYING,
display_order INTEGER
);
alter table attraction add column meta_title CHARACTER VARYING, add column meta_keywords CHARACTER VARYING, add column meta_description CHARACTER VARYING;

-- October 21st
alter table package add column attractions text;

-- October 24th
alter table rental add column displayable SMALLINT;

-- October 25th
alter table tour add column walkin_price NUMERIC(10,2);
insert into banner (title,description,banner_type) values ('WATER ADVENTURES FOR THE CURIOUS','We offer one of most fun and exciting ways to experience Central Park. Whether at your own pace or with one of our professionally guided Central Park tours, you can spend your day gliding over the gentle slopes, spotting the local wildlife, and visiting more than 150 different Hollywood movie sites.',5);
alter table rental rename to equipment;
alter table cart rename rentals to equipment;
alter table equipment rename constraint rental_pkey to equipment_pkey;
alter sequence rental_id_seq rename to equipment_id_seq;
alter table order_products rename rentals to equipment;
create table rental(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING, /*for naming sake*/
duration INTEGER,
adult_price NUMERIC(10,2),
kid_price NUMERIC(10,2),
discount_type SMALLINT,
discount_amount NUMERIC (10,2),
adult_walkin NUMERIC (10,2),
kid_walkin NUMERIC (10,2)
);
alter table cart add column rentals text;

-- October 26th
create table category(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING
);
create table attraction_categories(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time timestamp without time zone not null default now(),
attraction_id INTEGER,
category_id INTEGER
);
create table blog(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
slug CHARACTER VARYING,
description CHARACTER VARYING,
featured_image CHARACTER VARYING,
meta_title CHARACTER VARYING,
meta_keywords CHARACTER VARYING,
meta_description CHARACTER VARYING,
author CHARACTER VARYING,
status SMALLINT,
modified_date TIMESTAMP without time zone default now()
);

-- October 27th
alter table rental add column tandem_price numeric(10,2), add column tandem_walkin numeric(10,2);
alter table cart add column tandem_count INTEGER;

-- October 28th
alter table attraction add column marker_color CHARACTER VARYING;
create table review(
id serial primary KEY,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
review_type SMALLINT,
review_id INTEGER,
rating SMALLINT,
title CHARACTER VARYING,
description CHARACTER VARYING,
create_date TIMESTAMP without time zone not null default now(),
customer_name CHARACTER VARYING,
city CHARACTER VARYING,
state CHARACTER VARYING
);

-- October 31st
alter table review add column status INTEGER;
alter table review add column helpful INTEGER, add column unhelpful INTEGER;
alter table review alter column rating type DECIMAL using rating::DECIMAL;
alter table order_products add COLUMN rental_id INTEGER;

-- November 1st
alter table package add column adult_walkin NUMERIC (10,2),add column kid_walkin NUMERIC (10,2);

-- November 2st
alter table banner add column details text;
insert into banner (title,banner_type,details) values('Bike Tour',2,'{"type":1}'),('Water Tour',2,'{"type":3}'),('Bus Tour',2,'{"type":2}');
create table faq(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
question CHARACTER VARYING,
answer CHARACTER VARYING,
display_order INTEGER
);

-- November 7th
alter table package add column featured SMALLINT;
alter table tour add column summary CHARACTER VARYING;

-- November 8th
create table about(
id serial primary key,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without time zone not null default now(),
title CHARACTER VARYING,
description CHARACTER VARYING,
featured_image CHARACTER VARYING,
url CHARACTER VARYING,
url_text CHARACTER VARYING
);

-- November 9th
alter table package drop column upgrade_options;
alter table package add column subtitle CHARACTER VARYING;

-- November 10th
alter table package add column upgrade_option text;
create table footer_nav(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
url CHARACTER VARYING,
display_order SMALLINT,
parent_bottom_nav INTEGER
);

-- November 18th
alter table tour add column featured SMALLINT;

-- November 21st
alter table cart add column upgrade_option text;

-- November 22nd
alter table order_products add column upgrade_option text;
alter table order_products rename products to product;
alter table order_products rename tours to tour;
alter table order_products rename packages to package;
alter table order_products rename rental_id to rental;

alter table order_products add column adult_count INTEGER, add column kid_count INTEGER, add column tandem_count INTEGER, add column reservation_date TIMESTAMP without time zone;

alter table public.order drop column adult_count, drop column kid_count, drop column start_date;

-- December 1st
alter table order_products add column total NUMERIC (10,2);
alter table equipment add column default_rental SMALLINT;

-- December 7th
alter table package add column rental_id text;
alter table cart add column package_details text;

-- December 8th
alter table order_products add column package_details text;
alter table equipment add column rental_price text;

-- December 12th
alter table rental add column add_ons text;
alter table tour add column add_ons text;
alter table package add column add_ons text;

-- December 13th
alter table cart add column add_ons text;
alter table order_products add column add_ons text;
create table faq_category(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
display_order INTEGER
);
alter table faq add column faq_category_id INTEGER;

-- December 15th
alter table blog add column summary CHARACTER VARYING;

-- December 23rd
alter table order_products add column order_api text;
alter table order_products add column order_api_response text;

-- January 11th
alter table rental add column tour_code INTEGER; alter table tour add column tour_code INTEGER; alter table package add column package_id INTEGER; alter table attraction add column tour_code INTEGER;

-- January 13th
alter table rental add column external_package_id INTEGER; alter table tour add column external_package_id INTEGER; alter table attraction add column external_package_id INTEGER;
alter table rental add column options text;

-- April 18th
update nav set name = 'NYC Tours', url = '/tours' where id = 2;
update nav set name = 'NYC Rentals', url = '/rentals' where id = 3;
update nav set name = 'Things to do in NYC', url = '/attractions' where id = 5;

-- April 20th
alter table tour add column neighborhood SMALLINT;
create table newsletter(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
email CHARACTER VARYING,
coupon_code CHARACTER VARYING,
coupon_redeemed CHARACTER VARYING DEFAULT 0
);
create table coupon(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
start_date CHARACTER VARYING,
end_date CHARACTER VARYING,
coupon_code CHARACTER VARYING,
coupon_amount NUMERIC,
coupon_type SMALLINT,
maximum_use INTEGER,
minimum_amount NUMERIC
);

-- April 21st
alter table tour add column related text;

-- April 24th
alter table tour add column days SMALLINT;
update nav set active = 0 where id = 3;

-- April 25th
alter table route add column street CHARACTER VARYING ;
create table tour_route(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
tour_id INTEGER,
route_id INTEGER,
display_order INTEGER,
route_icon_ids text
);
create table route_icon(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
icon CHARACTER VARYING
);
alter table about add column display_position SMALLINT;
alter table about add column display_order SMALLINT ;

-- April 28th
create table route_path(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
main_route_id INTEGER,
route_id INTEGER,
display_order INTEGER,
route_icon_ids text
);
create table main_route(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING
);
alter table tour add column main_route_id INTEGER;
alter table package add column days INTEGER;

-- May 2nd
alter table attraction drop column tour_code, drop column external_package_id;

-- May 3rd
alter table package add column related text;

-- May 4th

create table popup_screen(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
title CHARACTER VARYING ,
description CHARACTER VARYING ,
image CHARACTER VARYING ,
background_color CHARACTER VARYING
);
create table quote(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
name CHARACTER VARYING,
email CHARACTER VARYING,
content CHARACTER VARYING
);

-- May 14
alter table tour alter column external_package_id type VARCHAR;

-- May 15
alter table package alter column adult_walkin type VARCHAR;
alter table package alter column kid_walkin type VARCHAR;
alter table package add column area text;

--May 30
alter table package add column display_order INTEGER;

--June 12
alter table package add column tickets_sold VARCHAR;

--June 15
drop table quote;
create table quote(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
first_name CHARACTER VARYING,
last_name CHARACTER VARYING,
company CHARACTER VARYING,
email CHARACTER VARYING,
phone VARCHAR ,
comment CHARACTER VARYING,
route CHARACTER VARYING,
pickup_date CHARACTER VARYING ,
pickup_time CHARACTER VARYING ,
pickup_location CHARACTER VARYING,
dropoff_date CHARACTER VARYING ,
dropoff_time CHARACTER VARYING ,
dropoff_location CHARACTER VARYING,
guest INTEGER,
language_request CHARACTER VARYING,
special_request CHARACTER VARYING
);

--June 22
create table contacts(
id serial primary key,
active SMALLINT default 1,
insert_time TIMESTAMP without time zone not null default now(),
full_name CHARACTER VARYING ,
email CHARACTER VARYING ,
subject CHARACTER VARYING ,
message CHARACTER VARYING
);

--June 29
ALTER TABLE public.main_route ADD COLUMN route_data character varying;

-- INSERT INTO main_route (id, active, insert_time, name, route_data) VALUES (1, 1, '2017-05-03 20:07:39.669349', 'Downtown', 'https://www.google.com/maps/d/u/0/embed?mid=1kTDz2-FOuxu0Iryj8fb12EsDq_s') ON CONFLICT(id) DO UPDATE SET route_data = EXCLUDED.route_data;
-- INSERT INTO main_route (id, active, insert_time, name, route_data) VALUES (2, 1, '2017-05-11 17:09:16.135092', 'Uptown', 'https://www.google.com/maps/d/u/0/embed?mid=1EI1ddIkCv3QPi_erYs6BHisXT9c') ON CONFLICT(id) DO UPDATE SET route_data = EXCLUDED.route_data;
-- INSERT INTO main_route (id, active, insert_time, name, route_data) VALUES (3, 1, '2017-05-11 18:11:04.073474', 'Night Tour', 'https://www.google.com/maps/d/u/0/embed?mid=1SuvgjjKRn0UtZN25DbRpjacov9o') ON CONFLICT(id) DO UPDATE SET route_data = EXCLUDED.route_data;
-- INSERT INTO main_route (id, active, insert_time, name, route_data) VALUES (4, 1, '2017-05-11 18:11:15.109789', 'Brooklyn Tour', 'https://www.google.com/maps/d/u/0/embed?mid=1s8vhza-K3WcPsw_khgCFWcwV6HM') ON CONFLICT(id) DO UPDATE SET route_data = EXCLUDED.route_data;
-- INSERT INTO main_route (id, active, insert_time, name, route_data) VALUES (5, 1, '2017-05-11 18:11:27.235195', 'Bronx Tour', 'https://www.google.com/maps/d/u/0/embed?mid=1IIbZ0Nl4a_FomD0CneNKNABCELM') ON CONFLICT(id) DO UPDATE SET route_data = EXCLUDED.route_data;

--July 20
ALTER TABLE public.main_route ADD COLUMN color character varying;

INSERT INTO main_route (id, active, insert_time, name, route_data, color) VALUES (6, 1, '2017-05-11 18:13:43.557958', 'HOP-ON HOP- OFF Liberty Cruise ', '', 'yellow');

INSERT INTO route (active, insert_time, name, featured_image, description, points, street) VALUES (1, '2017-07-19 19:49:20.099938', 'cruise 5', NULL, '', '["40.70445674541597, -74.02034282684326"]', '40.70445674541597, -74.02034282684326');
INSERT INTO route (active, insert_time, name, featured_image, description, points, street) VALUES (1, '2017-07-19 19:49:34.436758', 'cruise 6', NULL, '', '["40.72696606629052, -74.01798248291016"]', '40.72696606629052, -74.01798248291016');
INSERT INTO route (active, insert_time, name, featured_image, description, points, street) VALUES (1, '2017-07-19 19:49:55.013935', 'cruise 7', NULL, '', '["40.743582948460265, -74.01111602783203"]', '40.743582948460265, -74.01111602783203');
INSERT INTO route (active, insert_time, name, featured_image, description, points, street) VALUES (1, '2017-07-19 19:45:28.557694', 'cruise 2', NULL, '', '["40.7451761300463, -74.01832580566406"]', '40.7451761300463, -74.01832580566406');
INSERT INTO route (active, insert_time, name, featured_image, description, points, street) VALUES (1, '2017-07-19 19:48:54.9605', 'cruise 3', NULL, '', '["40.6908889279802, -74.0427017211914"]', '40.6908889279802, -74.0427017211914');
INSERT INTO route (active, insert_time, name, featured_image, description, points, street) VALUES (1, '2017-07-19 19:48:02.396493', 'cruise 8 (end)', NULL, '', '["40.75844032338938, -74.00442123413086"]', '40.706799110400006, -74.03180122375488');

ALTER TABLE public.main_route ADD COLUMN type integer;
ALTER TABLE public.main_route ALTER COLUMN type SET NOT NULL;
ALTER TABLE public.main_route ALTER COLUMN type SET DEFAULT 1;
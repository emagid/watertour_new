<?php

namespace Model;

class Order_Product extends \Emagid\Core\Model {
    static $tablename = "order_products";
  
    public static $fields = [
  		'order_id',
		'product',
		'equipment',
        'tour',
        'package',
        'rental',
        'upgrade_option',
        'adult_count',
        'kid_count',
        'tandem_count',
        'reservation_date',
        'total',
        'package_details',
        'add_ons',
        'order_api',
        'order_api_response'
    ];
	
    public function getAddOns(){
        if($this->add_ons){
            $addOns = json_decode($this->add_ons,true);
            $ar = [];
            foreach($addOns as $type=>$id){
                $m = "\\Model\\".$type;
                foreach($id as $key=>$qty){
                    $obj = $m::getItem($key);
                    $obj->qty = $qty['qty'];
                    $ar[] = $obj;
                }
            }
            return $ar;
        } else {
            return [];
        }
    }
}
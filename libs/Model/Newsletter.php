<?php

namespace Model;

use Emagid\Core\Model;

class Newsletter extends Model{
	static $tablename = 'newsletter';
	static $fields = ["email","coupon_code","coupon_redeemed"];

	public static function checkEmail($email){
	    return self::getItem(null,['where'=>"lower(email) = lower('$email')"]);
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Language extends Model{
    static $tablename = 'language';
    static $fields = [
        'name',
        'flag'
    ];
}
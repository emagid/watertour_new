<?php 

namespace Model;

class Banner extends \Emagid\Core\Model {
	static $tablename = 'banner';

	public static $fields = [
		'title',
		'image',
		'description',
		'url',
//		'options',
		'order_num',
		'featured',
		'banner_type',
		'details'
	];
}
<?php

namespace Model;

use Emagid\Core\Model;

class Route_Path extends Model{
	static $tablename = 'route_path';
	static $fields = ["main_route_id","route_id",'display_order','route_icon_ids'];
}
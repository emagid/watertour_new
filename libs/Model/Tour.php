<?php
namespace Model;

use Carbon\Carbon;

class Tour extends \Emagid\Core\Model
{

    public static $tablename = "tour";

    public static $fields = [
        'name' => ['required' => true],
        'price' => ['type' => 'numeric'],
        'description',
        'msrp' => ['type' => 'numeric'],
        'slug' => ['required' => true, 'unique' => true],
        'tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'featured_image',
        'availability', // TODO st-dev: replaced day_delivery with availability
        'details',
        'validity',
        'duration_hour',
        'duration_minute',
        'start_date',
        'end_date',
        'departure_point',
        'valid_start',
        'valid_end',
        'languages',
        'route',
        'available_range',
        'walkin_price'=>['type'=>'numeric'],
        'summary',
        'featured'=>['type'=>'numeric'],
        'add_ons',
        'tour_code',
        'external_package_id', /*use for storing and sending biketour server packageId*/
        'neighborhood',
        'related',
        'days',
        'main_route_id'
    ];

    /*
     * Available_range legend
     * o - Occurrences: string description of tour occurrences (Daily (h), Weekly (w)(h), Monthly (d)(h))
     * w - Weeks: tour occurs weekly, requires hours (h)
     * d - Days: tour occurs on specific days of the month, requires hours (h)
     * h - Hours: tour occurs at these hours on select days
     * */

    static $validity = [1=>'Single Use Only', 2=>'24 Hours', 3=>'48 Hours', 4=>'72 Hours', 5=>'5 Days', 6=>'7 Days'];
    static $neighborhood = [1=>'Midtown',2=>'Downtown',3=>'Brooklyn'];

    static $relationships = [
        [
            'name' => 'product_category',
            'class_name' => '\Model\Product_Category',
            'local' => 'id',
            'remote' => 'product_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ]
    ];

    static $SORT_MAP = [
        'product.insert_time desc', // newest
        'product.total_sales desc', // best seller
        'product.price desc', // price high to low
        'product.price asc', // price low to high
    ];

    public function getName(){
        return $this->name;
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id, name, featured_image,price, msrp, slug from product where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "CAST(id as TEXT) like '" . $keywords . "%' or ";
        }
        $explodeTags = explode(' ',strtolower(urldecode($keywords)));
        $tags = "'%".implode("%','%",$explodeTags)."%'";
        $sql .= " lower(name) like '%" . strtolower(urldecode($keywords)) . "%' or
            lower(tags) like any(array[". $tags."])
            ) order BY name ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getFutureTours(Carbon $endDate = null){
        $carbon = Carbon::now();
        $query = "valid_start < '$carbon' and valid_end > '$carbon'";
        if($endDate){
            $query .= " and valid_end < '$endDate'";
        }
        return self::getList(['where'=>$query]);
    }

    public static function getPriceSum($productsStr)
    {
        $total = 0;
        foreach (explode(',', $productsStr) as $productId) {
            $product = self::getItem($productId);
            $total += $product->price;
        }
        return $total;
    }

    public function getPrice(){
        return intval($this->msrp) ? : intval($this->price);
    }

    public function getWalkin(){
        return $this->walkin_price;
    }

    public function getAvailability(){
        switch($this->availability){
            case 0:
                return 'Inactive';break;
            case 1:
                return 'In Stock';break;
            case 2:
                return 'Active';break;
        }
        return null;
    }

    public static function generateToken()
    {
        return md5(uniqid(rand(), true));
    }

    public function featuredImage(){
        return $this->featured_image;
    }

    public function getAllImages(){
        $arr = [];
//        if($this->featured_image){
//            $arr[] = $this->featured_image;
//        }
        $where = 'tour_id = '.$this->id;
        $orderBy = 'display_order';
        $tour_images = Tour_Image::getList(['where'=>$where, 'orderBy'=>$orderBy]);
        foreach($tour_images as $tour_image){
            $arr[] = $tour_image->image;
        }
        return $arr;
    }

    public function videoLink()
    {
        if($this->video_link){
            return UPLOAD_URL.'products/'.$this->video_link;
        }

        return false;
    }

    public function getSessionTotal(){
        $price = $this->getPrice();
        $ad = $_SESSION['cart']->adults;
        $ki = $_SESSION['cart']->kids;
        return $ad || $ki ? $price * $ad + $price * $ki: 0;
    }

    public function getRoute(){
        return $this->route ? Route::getItem($this->route): null;
    }

    public function getHours()
    {
        $data = json_decode($this->available_range, true);
        if(!$data || !isset($data['h'])){
            return [];
        } else {
            return $data['h'];
        }
    }

    public function getDay()
    {
        $data = json_decode($this->available_range, true);
        if(!$data || !isset($data['d'])){
            return [];
        } else {
            return $data['d'];
        }
    }

    public function getWeek()
    {
        $data = json_decode($this->available_range, true);
        if(!$data || !isset($data['w'])){
            return [];
        } else {
            return $data['w'];
        }
    }

    public function getDuration(){
        $duration = [];
        if($this->duration_hour){
            $duration[] = round($this->duration_hour).' hours';
        }
        if($this->duration_minute != 0){
            $duration[] = round($this->duration_minute).' minutes';
        }
        return implode(' and ',$duration);
    }

    public function getNextRange(){
        $availability = json_decode($this->available_range, true);
        $carbon = Carbon::now();
        if(isset($availability['o'])){
            switch ($availability['o']){
                case 'Daily':
                    $hours = $availability['h'];
                    $arrs = array_map(function($item) use ($carbon){return $item > $carbon->hour;},$hours);
                    $index = array_search('true',$arrs);
                    if($index !== false){
                        $carbon->hour = $hours[$index];
                        $carbon->minute = $hours[$index] % 1 == 0 ? 0: 30;
                        $carbon->second = 0;
                    } else {
                        $carbon->addDay();
                        $carbon->hour = $hours[0];
                        $carbon->minute = $hours[0] % 1 == 0 ? 0: 30;
                        $carbon->second = 0;
                    }
                    return $carbon->toDateTimeString();
                    break;
                case 'Weekly':
                    $week = $availability['w'];
                    $hours = $availability['h'];
                    $weekMap = ['sun','mon','tue','wed','thu','fri','sat'];
                    $availableWeek = array_map(function($d) use ($weekMap){return array_search($d,$weekMap);},$week);
                    $arrs = array_map(function($item) use ($carbon){return $item > $carbon->hour;},$hours);
                    $index = array_search('true',$arrs);

                    $next = $availableWeek[0];
                    foreach ($availableWeek as $item){
                        if($item > $carbon->dayOfWeek){
                            $next = $item;
                            continue;
                        }
                    }

                    if(array_search($carbon->dayOfWeek,$availableWeek) !== false && $index !== false){
                        $carbon->hour = $hours[$index];
                        $carbon->minute = $hours[$index] % 1 == 0 ? 0: 30;
                    } else {
                        $carbon->next($next);
                        $carbon->hour = $hours[0];
                        $carbon->minute = $hours[0] % 1 == 0 ? 0: 30;
                    }
                    return $carbon->toDateTimeString();
                    break;
                case 'Monthly':
                    break;
            }
        }
        return $carbon->toDateTimeString();
    }
}
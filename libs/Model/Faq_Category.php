<?php

namespace Model;

use Emagid\Core\Model;

class Faq_Category extends Model{
    static $tablename = 'faq_category';
    static $fields = [
        'name',
        'display_order'
    ];

    function getFaq(){
        return Faq::getList(['where'=>"faq_category_id = $this->id", 'orderBy'=>'display_order']);
    }
}
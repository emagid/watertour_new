<?php

namespace Model;

use Emagid\Core\Model;

class Contact extends Model{
    static $tablename = 'contacts';
    static $fields = [
        "full_name",
        "email",
        "subject",
        "message"
    ];


}
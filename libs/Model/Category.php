<?php

namespace Model;

use Emagid\Core\Model;

class Category extends Model{
    static $tablename = 'Category';
    static $fields = [
        'name'=>['unique'=>true]
    ];
    static $relationships = [
        [
            'name' => 'attraction_categories',
            'class_name' => '\Model\Attraction_Categories',
            'local' => 'id',
            'remote' => 'category_id',
            'remote_related' => 'attraction_id',
            'relationship_type' => 'many'
        ]
    ];

    static function getByName($name){
        return self::getItem(null,['where'=>"lower(name) = '".strtolower($name)."'"]);
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Faq extends Model{
    static $tablename = 'faq';
    static $fields = [
        'question',
        'answer',
        'display_order',
        'faq_category_id'
    ];
}
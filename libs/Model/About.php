<?php

namespace Model;

use Emagid\Core\Model;

class About extends Model{
    static $tablename = 'about';
    static $fields = [
        'title',
        'description',
        'featured_image',
        'url',
        'url_text',
        'display_position',
        'display_order'
    ];
}
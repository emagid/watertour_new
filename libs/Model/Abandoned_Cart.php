<?php

namespace Model;

use Emagid\Core\Model;

class Abandoned_Cart extends Model{
    static $tablename = 'abandoned_cart';
    static $fields = [
        'user_id',
        'guest_id',
        'checkout_fields',
        'cart',
        'device',
        'browser',
        'version'
    ];
}
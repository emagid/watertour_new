<?php

namespace Model;

class Package extends \Emagid\Core\Model
{

    static $tablename = "package";

    public static $fields = [
        'active',
        'name' => ['required' => true],
        'description',
        'title',
        'slug' => ['required' => true, 'unique' => true],
        'meta_title',
        'meta_keywords',
        'meta_description',
        'banner',
        'tour_id', /*JSON*/
        'rental_id', /*JSON*/
        'adults_price',
        'kids_price',
        'status',
        'start_date',
        'end_date',
        'attractions',
        'adult_walkin',
        'kid_walkin',
        'featured',
        'subtitle',
        'upgrade_option',
        'add_ons',
        'package_id',
        'days'=>['type'=>'numeric'],
        'related',
        'area',
        'display_order'=>['type'=>'numeric'],
        'tickets_sold'
    ];

    static $status = [1=>'Active',2=>'Inactive'];
    static $featured = [1=>'featured',2=>'not featured'];

    static $relationships = [
        [
            'name' => 'product_category',
            'class_name' => '\Model\Product_Category',
            'local' => 'id',
            'remote' => 'category_id',
            'remote_related' => 'product_id',
            'relationship_type' => 'many'
        ],
    ];

    /**
     * @param $type ('adult' | 'kid')
     */
    function getPrice($type = 'adult'){
        return $type == 'kid' ? $this->kids_price: $this->adults_price;
    }
    function getWalkin($type = 'adult'){
        return $type == 'kid' ? $this->kid_walkin: $this->adult_walkin;
    }
    function getName(){
        return $this->name;
    }
    static function getDays(){
        global $emagid;$db = $emagid->getDb();
        $sql = 'select distinct days from package where days is not null order by days';
        $res = $db->execute($sql);
        return flatten($res);
    }

    static function getByDate($date,$params = []){
        global $emagid;
        $db = $emagid->getDb();
        $formatted = is_numeric($date) ? date('Y-m-d H:i:s',$date) : date('Y-m-d H:i:s',strtotime($date));
        $tourSql = "select id from tour where '{$formatted}' between valid_start and valid_end";
        $tourIds = implode('"%\',\'%"',flatten($db->execute($tourSql)));
        $packageSql = "tour_id like any(array['%\"".$tourIds."\"%']) and status = 1";
        $query = [];
        $query['where'][] = $packageSql;
        $query['orderBy'] = 'adults_price asc';
        if(isset($params['orderBy'])){
            $query['orderBy'] = $params['orderBy'];
        }
        if(isset($params['where'])){
            $query['where'][] = $params['where'];
        }
        if(isset($_GET['f'])){
            $attr = Attraction::getItem(Attraction::getIdBySlug($_GET['f']));
            $query['where'][] = "attractions like any(array['%\"$attr->id\"%'])";
        }
        $query['where'] = implode(' and ',$query['where']);
        $packages = Package::getList($query);
        return $packages;
    }

    public static function array_flatten($array)
    {

        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $return = array_merge($return, self::array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }
        return $return;

    }

    public function featuredImage(){
        return $this->banner;
    }

    public function getDuration($minutes = true){
        $tourIds = json_decode($this->tour_id,true);
        $tours = Tour::getList(['where'=>"id in (".implode(',',$tourIds).")"]);
        $time = [];
        foreach($tours as $tour){
            $hourToMin = $tour->duration_hour * 60;
            $min = $tour->duration_minute;
            $time[] = $hourToMin + $min;
        }
        if($minutes){
            return array_sum($time);
        } else {
            return ['hours'=>floor(array_sum($time)/60),'minutes'=>fmod(array_sum($time),60)];
        }
    }

    public static function getFeatured(){
        return self::getList(['where'=>"featured = 1"]);
    }

    public function getDurationFormatted(){
        $duration = 'Unspecified';
        if($this->getDuration()){
            $duration = [];
            foreach($this->getDuration(false) as $item=>$value){
                $duration[] = $value.' '.$item;
            }
            $duration = implode(', ',$duration);
        }
        return $duration;
    }

    public function getAvailableRange(){
        $ret = [];
        foreach(json_decode($this->tour_id,true) as $value){
            $tour = Tour::getItem($value);
            if($tour->available_range) {
                foreach (json_decode($tour->available_range, true) as $key => $val) {
                    if ($key == 'o') {
                        //do nothing
                    } else if (isset($ret[$key])) {
                        $ret[$key] = array_unique(array_merge($ret[$key], $val));
                    } else {
                        $ret[$key] = $val;
                    }
                }
            }
        }
        return $ret;
    }

    public function getPackageContents(){
        $items = [];
        if($this->tour_id){
            foreach(json_decode($this->tour_id,true) as $value){
                $tour = Tour::getItem($value);
                $tour->days = $tour->getDay();
                $tour->hours = $tour->getHours();
                $tour->weeks = $tour->getWeek();
                $items[] = $tour;
            }
        }
        if($this->rental_id){
            foreach(json_decode($this->rental_id,true) as $value){
                $rental = Rental::getItem($value);
                $rental->days = [];
                $rental->hours = [];
                $rental->weeks = [];
                $items[] = $rental;
            }
        }
        return $items;
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Main_Route extends Model{
	static $tablename = 'main_route';
	static $fields = ["name","route_data","color","type"];
	static $TYPE = [1 => 'Bus', 2=>'Boat'];
}
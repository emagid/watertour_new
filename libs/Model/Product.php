<?php

namespace Model;

use Emagid\Core\Model;

class Product extends Model{
    static $tablename = 'Product';
    static $fields = [
        'name',
        'slug' => ['required' => true, 'unique' => true],
        'tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'price'=>['type'=>'numeric'],
        'discount'=>['type'=>'numeric'],
        'featured_image',
        'description'
    ];

    function getName(){
        return $this->name;
    }

    function getPrice(){
        return $this->discount ? : $this->price;
    }

    function featuredImage(){
        return $this->featured_image;
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Attraction_Categories extends Model{
    static $tablename = 'attraction_categories';
    static $fields = [
        'attraction_id',
        'category_id'
    ];

    static $relationships = [
        [
            'name' => 'attraction',
            'class_name' => '\Model\Attraction',
            'local' => 'attraction_id',
            'remote' => 'id',
            'relation_type' => 'one'
        ],
        [
            'name' => 'category',
            'class_name' => '\Model\Category',
            'local' => 'category_id',
            'remote' => 'id',
            'relation_type' => 'one'
        ],
    ];

    static function getCategories($attraction_id){
        $data = self::getList(['where'=>"attraction_id = $attraction_id"]);
        $arr = [];
        foreach($data as $item){
            $arr[] = $item->category_id;
        }
        return $arr;
    }

    static function getAttractions($category_id){
        $data = self::getList(['where'=>"category_id = $category_id"]);
        $arr = [];
        foreach($data as $item){
            $arr[] = $item->attraction_id;
        }
        return $arr;
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Attraction_Images extends Model{
    static $tablename = 'attraction_images';
    static $fields = [
        'attraction_id',
        'image',
        'display_order'
    ];
}
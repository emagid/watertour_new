<?php

namespace Model;

use Emagid\Core\Model;

class Attraction extends Model{
    static $tablename = 'attraction';
    static $fields = [
        'name',
        'slug',
        'description',
        'lat',
        'lng',
        'featured_image',
        'external_link',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'marker_color',
    ];
    static $relationships = [
        [
            'name' => 'attraction_categories',
            'class_name' => '\Model\Attraction_Categories',
            'local' => 'id',
            'remote' => 'attraction_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ]
    ];

    function getName(){
        return $this->name;
    }

    function getImages(){
        return Attraction_Images::getList(['where'=>"attraction_id = $this->id"]);
    }

    function featuredImage(){
        return $this->featured_image;
    }

    function getPrice(){
        return 0;
    }

    function hasPackage(){
        return Package::getList(['where'=>"attractions like '%\"".$this->id."\"%'"]);
    }

    static function query($term,$cat)
    {
        if ($term || $cat) {
            $im_ex_plode = "'%" . implode("%','%", explode(' ', strtolower($term))) . "%'";
            $where = [];
            if($term){
                $where[] = "lower(name) like all(array[$im_ex_plode])";
            }
            if($cat){
                $where[] = "id in (select attraction_id from attraction_categories where category_id = ".Category::getByName($cat)->id.")";
            }
            $qWhere = implode(' and ',$where);
            $attractions = self::getList(['where' => $qWhere]);
            return $attractions;
        } else {
            return self::getList();
        }
    }
}
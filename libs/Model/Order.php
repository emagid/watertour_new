<?php
namespace Model;

class Order extends \Emagid\Core\Model
{
    static $tablename = "public.order";
    public static $status = ['New', 'Paid', 'Declined', 'Shipped', 'Complete', 'Delivered', 'Canceled', 'Discontinued', 'Returned', 'Processed', 'Refunded', 'Incomplete PayPal'];
    public static $payment_method = [1=>"Credit Card", 2=>"PayPal", 3=>"Cash", 4=>"Deposit", 5=>"Gift Card"];
    //public static $status = ['New', 'Paid', 'Payment Declined', 'Shipped', 'Complete', 'Pending Paypal', 'Canceled', 'Imported as Processing', 'Returned'];

    public static $fields = [
        'insert_time',
        'viewed' => ['type' => 'boolean'],
        // billing info
        'bill_first_name' => ['name' => 'Bill - First Name'],
        'bill_last_name' => ['name' => 'Bill - Last Name'],
        'bill_address' => ['name' => 'Bill - Address'],
        'bill_address2',
        'bill_city' => ['name' => 'Bill - City'],
        'bill_state' => ['name' => 'Bill - State'],
        'bill_country',
        'bill_zip' => ['name' => 'Bill - Zip'],
        //shipping info
        'ship_first_name' => ['name' => 'Shipping - First Name'],
        'ship_last_name' => ['name' => 'Shipping - Last Name'],
        'ship_address' => ['name' => 'Shipping - Address'],
        'ship_address2',
        'ship_city' => ['name' => 'Shipping - City'],
        'ship_state' => ['name' => 'Shipping - State'],
        'ship_country',
        'ship_zip' => ['name' => 'Shipping - Zip'],
        //card info
        'cc_number' => ['type' => 'numeric', 'name' => 'Credit Card Number'],
        'cc_expiration_month' => ['type' => 'numeric', 'name' => 'Credit Card Expiration Month'],
        'cc_expiration_year' => ['type' => 'numeric', 'name' => 'Credit Card Expiration Year'],
        'cc_ccv' => ['type' => 'numeric', 'name' => 'Credit Card CCV'],
        // price info
        'payment_method',
        'shipping_method',
        'shipping_cost' => ['numeric'],
        'subtotal' => ['numeric'],
        'total' => ['numeric'],
        'tax',
        'tax_rate',
        //shipping and tracking info
        'status',
        'tracking_number',
        'user_id',
        'coupon_code',
        'coupon_type',
        'coupon_amount',
        'gift_card',
        'email' => ['type' => 'email'],
        'phone',
        'note',
        'comment',
        'fraud_status',
        'guest_id',
        'card_name',
        'error_message',
        'ref_num',
        'cc_type',
        'authorization_code',
        'transaction_id'
    ];

    function shipName()
    {
        return $this->ship_first_name . ' ' . $this->ship_last_name;
    }

    function billName()
    {
        return $this->bill_first_name . ' ' . $this->bill_last_name;
    }

    function getShippingAddr()
    {
        return $this->ship_address . ' ' . $this->ship_address2 . ' ' . $this->ship_city . ' ' . $this->ship_state . ', ' . $this->ship_zip;
    }

    function getBillingAddr()
    {
        return $this->bill_address . ' ' . $this->bill_address2 . ' ' . $this->bill_city . ' ' . $this->bill_state . ', ' . $this->bill_zip;
    }

    public function beforeValidate()
    {
        if ($this->payment_method == 1) {
            self::$fields['cc_number']['required'] = true;
            self::$fields['cc_expiration_month']['required'] = true;
            self::$fields['cc_expiration_year']['required'] = true;
        }
        if ($this->bill_country == 'United States') {
            self::$fields['bill_state']['required'] = true;
        }
        if ($this->ship_country == 'United States') {
            self::$fields['ship_state']['required'] = true;
        }
    }

    public static function getDashBoardData($year, $month)
    {
        $data = new \stdClass();

        $allStatus = implode("','", Order::$status);

        $sql = " FROM " . self::$tablename . " WHERE ";
        $sql .= " EXTRACT(MONTH FROM insert_time)::INTEGER = " . $month;
        $sql .= " AND EXTRACT(YEAR FROM insert_time)::INTEGER = " . $year;
        $sql .= " AND status in ('" . $allStatus . "')";

        $data->monthly_sales = 0;
        $monthly_sales = self::execute("SELECT SUM(total) AS s " . $sql);
        if (count($monthly_sales) > 0) {
            $data->monthly_sales = $monthly_sales[0]['s'];
        }

        $data->gross_margin = 0;

        $data->monthly_orders = 0;
        $monthly_orders = self::execute("SELECT COUNT(id) AS c " . $sql);
        if (count($monthly_orders) > 0) {
            $data->monthly_orders = $monthly_orders[0]['c'];
        }

        return $data;
    }

    public static function getWeeklyDashboard($present, $past){
        $sql = "SELECT SUM (total) FROM ".self::$tablename." WHERE insert_time between '$past' and '$present'";
        $weekly = self::execute($sql);
        $data = new \stdClass();
        $data->weekly_sales = $weekly[0]['sum'];
        return $data;
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id,ship_first_name, ship_last_name, bill_first_name,bill_last_name from public.order where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " or ";
        }
        $sql .= " lower(email) like '%" . strtolower(urldecode($keywords)) . "%' or lower(ship_first_name) like '%" . strtolower(urldecode($keywords)) . "%' or lower(ship_last_name) like '%" . strtolower(urldecode($keywords)) . "%' or lower(bill_last_name) like '%" . strtolower(urldecode($keywords)) . "%' or lower(bill_first_name) like '%" . strtolower(urldecode($keywords)) . "%') limit " . $limit;

        return self::getList(['sql' => $sql]);
    }

    public function getOrderProducts(){
        $orderProducts = Order_Product::getList(['where'=>"order_id = $this->id"]);
        return $orderProducts ? : null;
    }

}











































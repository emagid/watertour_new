<?php

namespace Model;

use Emagid\Core\Model;

class Blog extends Model{
    static $tablename = 'blog';
    static $fields = [
        'name',
        'slug',
        'description',
        'featured_image',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'author',
        'status',
        'modified_date',
        'summary'
    ];
    static $activity = [1=>'Active',2=>'Inactive'];
}
<?php

namespace Model;

use Emagid\Core\Model;

class Route_Icon extends Model{
	static $tablename = 'route_icon';
	static $fields = ["name","icon"];

    public function icon(){
        return $this->icon ? : '';
    }

    public function fullIconPath(){
        $rc = new \ReflectionClass($this);
        return $this->icon() ? UPLOAD_URL.strtolower($rc->getShortName()).'s'.DS.$this->icon() : '';
    }
}
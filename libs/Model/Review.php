<?php

namespace Model;

use Emagid\Core\Model;

class Review extends Model{
    static $tablename = 'Review';
    static $fields = [
        'review_type'=>['required'=>true, 'type'=>'numeric'],
        'review_id'=>['required'=>true, 'type'=>'numeric'],
        'rating'=>['required'=>true, 'type'=>'numeric'],
        'title',
        'description',
        'create_date',
        'customer_name',
        'city',
        'state',
        'status',
        'helpful',
        'unhelpful'
    ];
    static $review_type = [1=>'Rental',2=>'Tour',3=>'Package'];
    static $status = [1=>'Pending',2=>'Approved',3=>'Disapproved'];

    public static function getType($type){
        if(is_numeric($type)){
            return self::$review_type[$type];
        } else {
            $lowType = ucfirst(strtolower($type));
            return array_search($lowType,self::$review_type);
        }
    }

    public function getReviewItem(){
        $type = "\\Model\\";
        switch($this->review_type){
            case 1:
                $type .= "Rental";
                break;
            case 2:
                $type .= "Tour";
                break;
            case 3:
                $type .= "Package";
                break;
        }
        return $type::getItem($this->review_id);
    }

    public static function getReviewsByType($type_id,$item_id,$limit = null){
        $where = ['review_type = '.self::getType($type_id), 'status = 2'];
        if($item_id){
            $where[] = 'review_id = '.$item_id;
        }
        $whereClause = implode(' and ',$where);
        if($limit && is_numeric($limit)){
            return self::getList(['where'=>$whereClause, 'limit'=>$limit]);
        } else {
            return self::getList(['where'=>$whereClause]);
        }
    }

    public function getStatus(){
        return $this->status ? self::$status[$this->status] : '';
    }
}
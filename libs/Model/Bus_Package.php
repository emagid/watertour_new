<?php

namespace Model;

use Emagid\Core\Model;

class Bus_Package extends Model{
    static $tablename = 'bus_package';
    static $fields = [
        'name' => ['required' => true],
        'description',
        'title',
        'slug' => ['required' => true, 'unique' => true],
        'meta_title',
        'meta_keywords',
        'meta_description',
        'banner',
        'bus_tour_id', /*JSON*/
        'upgrade_options',
        'adults_price',
        'kids_price',
        'status',
        'start_date',
        'end_date'
    ];
}
<?php

namespace Model;

use Emagid\Core\Model;

class Tour_Route extends Model{
	static $tablename = 'tour_route';
	static $fields = ["tour_id","route_id","display_order","route_icon_ids"];
}
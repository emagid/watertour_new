<?php

namespace Model;

use Emagid\Core\Model;

class Footer_Nav extends Model{
    static $tablename = 'footer_nav';
    static $fields = [
        'name',
        'url',
        'display_order',
        'parent_bottom_nav'
    ];

    static function getParents(){
        return self::getList(['where'=>'parent_bottom_nav = 0 or parent_bottom_nav is null']);
    }
    static function footerArray(){
        $parent = self::getList(['where'=>'parent_bottom_nav = 0 or parent_bottom_nav is null','orderBy'=>'display_order']);
        foreach($parent as $p){
            if(($children = self::getList(['where'=>"parent_bottom_nav = $p->id",'orderBy'=>'display_order']))){
                $p->children = $children;
            }
        }
        return $parent;
    }
}
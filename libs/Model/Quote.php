<?php

namespace Model;

use Emagid\Core\Model;

class Quote extends Model{
	static $tablename = 'quote';
	static $fields = [
	    "first_name",
        "last_name",
        "company",
        "email",
        "phone",
        "comment",
        "route",
        "pickup_date",
        "pickup_time",
        "pickup_location",
        "dropoff_date",
        "dropoff_time",
        "dropoff_location",
        "guest",
        "language_request",
        "special_request"
        ];
}
<?php

namespace Model;

use Emagid\Core\Model;

class Sub_Nav extends Model{
    static $tablename = 'sub_nav';
    static $fields = [
        'nav_id',
        'name',
        'url',
        'display_order'
    ];
}
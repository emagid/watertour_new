<?php 

namespace Model; 

class Rental extends \Emagid\Core\Model {

	static $tablename = 'rental';
	
	public static $fields =  [
		'name',
        'duration',
		'adult_price'=>['type'=>'numeric'],
		'kid_price'=>['type'=>'numeric'],
        'discount_type',
        'discount_amount',
        'adult_walkin',
        'kid_walkin',
        'tandem_price',
        'tandem_walkin',
        'add_ons',
        'tour_code',
        'external_package_id', /*use for storing and sending biketour server packageId*/
        'options'
	];
    static $discount_type = [1=>'%', 2=>'-'];

    public function getName(){
        return $this->name;
    }

    public function featuredImage(){
        return '';
    }

    public function getPrice($type = 'adult'){
        switch($type){
            case 'adult':
            default:
                return $this->adult_price;
                break;
            case 'kid':
                return $this->kid_price;
                break;
            case 'tandem':
                return $this->tandem_price;
                break;
        }
    }

    /**
     * Get prices for all rentals and compare to selected rental in $_SESSION
     * Used only for rebuilding dropdown list on frontend
     * @return Array|null
     */
    public static function getSessionPricing(){
        $sessionSelect = 'cart';
        if(isset($_SESSION['rental'])){
            $sessionSelect = 'rental';
        }
        $adults = $_SESSION[$sessionSelect]->adults;
        $kids = $_SESSION[$sessionSelect]->kids;
        $tandem = $_SESSION[$sessionSelect]->tandem;
        $rental = self::getItem($_SESSION[$sessionSelect]->duration); //todo change to rental id
//        if($rental){
            $allRentals = self::getList(['orderBy'=>'duration']);
            $data = [];
            foreach($allRentals as $allRental){
                $total = $allRental->getPrice() * $adults + $allRental->getPrice('kid') * $kids + $allRental->getPrice('tandem') * $tandem;
                $data[] = ['id'=>$allRental->id,'name'=>$allRental->name,'total'=>$total,'default'=>floatval($allRental->getPrice())];
            }
            return $data;
//        } else {
//            return null;
//        }
    }

    public function getDuration(){
        return $this->duration.' '.pluralize($this->duration,'Hour');
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Cart extends Model{
    static $tablename = 'cart';
    static $fields = [
        'package_id',
        'tour_id',
        'products',
        'equipment',
        'adult_count'=>['type'=>'numeric'],
        'kid_count'=>['type'=>'numeric'],
        'tandem_count'=>['type'=>'numeric'],
        'reserve_date',
        'duration'=>['type'=>'numeric'],
        'ref_num',
        'email',
        'full_name',
        'phone',
        'user_id',
        'guest_id',
        'rentals',
        'upgrade_option',
        'package_details',
        'add_ons'
    ];

    public static function getActiveCart($user_id = null,$guest_id = null){
        $where = 'active = 1';
        if($user_id){
            $where .= " and user_id = $user_id";
        } else if($guest_id && $user_id == null){
            $where .= " and guest_id = '{$guest_id}'";
        } else {
        }
        return self::getList(['where'=>$where, 'orderBy'=>'id']);
    }

    public static function getActiveRental($user_id = null,$guest_id = null){
        $where = 'active = 1 and (duration is not null or duration != 0)';
        if($user_id){
            $where .= " and user_id = $user_id";
        } else if($guest_id && $user_id == null){
            $where .= " and guest_id = '{$guest_id}'";
        } else {
        }
        return self::getItem(null,['where'=>$where, 'orderBy'=>'id']);
    }

    public function getProduct(){
        return Product::getItem($this->product_id);
    }

    public function getColor(){
        return json_decode($this->variation,true)['color'];
    }

    public function featuredImage()
    {
        $color_id = json_decode($this->variation, true)['color'];
        $prodImg = Product_Image::getItem(null, ['where' => "product_id = $this->product_id and color_id like '%\"$color_id\"%' and (legshot != 1 or legshot is null)", 'orderBy' => "display_order"]);
        if ($prodImg) {
            return $prodImg->image;
        } else {
            return '';
        }
    }

    public static function validateRefNum($ref_num){
        if(($cart = Cart::getItem(null,['where'=>"ref_num = '$ref_num'"]))){
            return $cart;
        } else {
            return false;
        }
    }

    /**
     * $package->adults_price + $tour->price + ($rentals->adult_price * quantity) + ($products->price * quantity)
     */
    function getTotal(){
        $total = 0;

        //add package
        if($this->package_id){
            $package = Package::getItem($this->package_id);
            $total += $this->adult_count * $package->getPrice();
            $total += $this->kid_count * $package->getPrice('kid');

        }

        //add tour
        else if($this->tour_id){
            $tour = Tour::getItem($this->tour_id);
            $price = $tour->getPrice();
            $adult = $this->adult_count;
            $kid = $this->kid_count;
            $total += $price * $adult + $price * $kid;
        }

        //add products
        else if($this->duration){ //todo replace duration with rental id
            $rental = Rental::getItem($this->duration);
//            $total += $rental->getPrice() * $this->adult_count;
//            $total += $rental->getPrice('kid') * $this->kid_count;
//            $total += $rental->getPrice('tandem') * $this->tandem_count;

        }

        //add equipment
        foreach(json_decode($this->equipment,true) as $id=>$attr){
            $rental = $this->duration ? : 0;
            $equipment = Equipment::getItem($id);
            $obj = Rental::getItem($rental);
            if($equipment && $equipment->category == 'Insurance'){
                $total += $equipment->getPrice('',$rental) * ($this->adult_count + $this->kid_count + $this->tandem_count);
            } else if($equipment && $obj){
                $total += ($equipment->getPrice('',$rental) * $attr['qty']);
//                $total += ($equipment->getPrice('',$rental) * $attr['qty'] * $obj->duration);
            } else {
                $total += 0;
            }
        }

        //add products
        foreach(json_decode($this->products,true) as $id=>$attr){
            $total += ($product = Product::getItem($id)) ? $product->getPrice() * $attr['qty']: 0;
        }

        if($this->upgrade_option || json_decode($this->upgrade_option,true) != []){
            foreach(json_decode($this->upgrade_option,true) as $key=>$idArr){
                foreach($idArr as $id=>$qty){
                    $mod = "\\Model\\".ucfirst($key);
                    $obj = $mod::getItem($id);
                    $total += $obj->getPrice() * $qty['qty'];
                }
            }
        }

        if($this->add_ons && json_decode($this->add_ons,true) != []){
            foreach(json_decode($this->add_ons,true) as $key=>$idArr){
                foreach($idArr as $id=>$qty){
                    $mod = "\\Model\\".ucfirst($key);
                    $obj = $mod::getItem($id);
                    $total += $obj->getPrice() * $qty['qty'];
                }
            }
        }

        return $total;
    }

    static function getSessionTotal($type = ''){
        $parameters = ['adults','kids','tandem','duration','datetime','tours','equipment','packages','products'];
        $total = 0;
        $sessionSelect = 'cart';
        if (isset($_SESSION['rental'])) {
            $sessionSelect = 'rental';
        }
        if($_SESSION[$sessionSelect]->tours){
            $type = 'tour';
        } else if($_SESSION[$sessionSelect]->packages){
            $type = 'package';
        }
        foreach($parameters as $parameter){
            switch($parameter){
                case 'equipment':
                    foreach($_SESSION[$sessionSelect]->$parameter as $id=>$attr){
                        $e = Equipment::getItem($id);
                        $rId = isset($_SESSION[$sessionSelect]->duration) && $_SESSION[$sessionSelect]->duration > 0 ? $_SESSION[$sessionSelect]->duration: 0;
                        if($rId){
                            $r = Rental::getItem($rId);
                            if($e->category == 'Insurance'){
                                $total += ($e->getPrice('',$rId) * ($_SESSION[$sessionSelect]->adults + $_SESSION[$sessionSelect]->kids + $_SESSION[$sessionSelect]->tandem));
                            } else {
//                                $duration = $r->duration;
                                $total += ($e->getPrice('',$rId) * $attr['qty']);
//                                $total += ($e->getPrice('',$rId) * $attr['qty'] * $duration);
                            }
                        }
                    }
                    break;
                case 'products':
                    foreach($_SESSION[$sessionSelect]->$parameter as $id=>$attr){
                        $p = Product::getItem($id);
                        $total += $p->getPrice() * $attr['qty'];
                    }
                    break;
//                case 'duration': //todo change to rental id
//                    if($type == 'rental' || $type == '') {
//                        $r = Rental::getItem($_SESSION[$sessionSelect]->$parameter);
//                        if($r) {
//                            $total += $r->getPrice() * $_SESSION[$sessionSelect]->adults;
//                            $total += $r->getPrice('kid') * $_SESSION[$sessionSelect]->kids;
//                            $total += $r->getPrice('tandem') * $_SESSION[$sessionSelect]->tandem;
//                        }
//                    }
//                    break;
                case 'tours':
                    if($type == 'tour') {
                        $t = Tour::getItem($_SESSION['cart']->$parameter);
                        $total += $t->getPrice() * $_SESSION['cart']->adults;
                        $total += $t->getPrice('kid') * $_SESSION['cart']->kids;
//                    $total += array_sum(array_map(function($id){return Tour::getItem($id)->getPrice();}, $_SESSION['cart']->$parameter));
                    }
                    break;
                case 'packages':
                    if($type == 'package') {
                        $p = Package::getItem($_SESSION['cart']->$parameter);
                        $total += $p->getPrice() * $_SESSION['cart']->adults;
                        $total += $p->getPrice('kid') * $_SESSION['cart']->kids;
//                    $total += array_sum(array_map(function($id){return Package::getItem($id)->getPrice();}, $_SESSION['cart']->$parameter));
                    }
                    break;
            }
        }
        return $total;
    }
}
<?php
namespace Model;

use Carbon\Carbon;

class Water_Tour extends \Emagid\Core\Model
{

    public static $tablename = "water_tour";

    public static $fields = [
        'name' => ['required' => true],
        'price' => ['type' => 'numeric', 'required' => true],
        'description',
        'msrp',
        'slug' => ['required' => true, 'unique' => true],
        'tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'featured_image',
        'availability', // TODO st-dev: replaced day_delivery with availability
        'details',
        'validity',
        'duration_hour',
        'duration_minute',
        'start_date',
        'end_date',
        'departure_point',
        'valid_start',
        'valid_end',
        'languages',
        'route'
    ];

    public function featuredImage($category_id = null, $color_id = null){
        $product = self::getItem($this->id);
        if($category_id && ($prodCat = Product_Category::getItem(null,['where'=>['product_id'=>$this->id,'category_id'=>$category_id]])) && $prodCat->image){
            return $prodCat->image;
        } elseif($color_id){
            return Tour_Image::getItem(null,['where'=>"product_id = {$this->id}", 'orderBy'=>'display_order'])->image;
        } else if($product->featured_image != ''){
            return $product->featured_image;
        } elseif($product = Tour_Image::getItem(null,['where'=>"product_id = {$this->id} and image != ''"])) {
            return $product->image;
        } else {
            return '';
        }
    }

    public function getAvailability(){
        switch($this->availability){
            case 0:
                return 'Inactive';break;
            case 1:
                return 'In Stock';break;
            case 2:
                return 'Active';break;
        }
        return null;
    }
}
<?php

namespace Model;

use Emagid\Core\Model;

class Water_Package extends Model{
    static $tablename = 'water_package';
    static $fields = [
        'name' => ['required' => true],
        'description',
        'title',
        'slug' => ['required' => true, 'unique' => true],
        'meta_title',
        'meta_keywords',
        'meta_description',
        'banner',
        'water_tour_id', /*JSON*/
        'upgrade_options',
        'adults_price',
        'kids_price',
        'status',
        'start_date',
        'end_date'
    ];
}
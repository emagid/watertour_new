<?php

namespace Model;

use Emagid\Core\Model;

class Nav extends Model{
    static $tablename = 'nav';
    static $fields = [
        'name',
        'url',
        'display_order'
    ];

    static function getPresetUrls(){
        $urls = ['Tours'=>['Tour'=>'/tours'], 'Rentals'=>['Rental'=>'/rentals'], 'Packages'=>['Package'=>'/packages']];
        if(($pages = Page::getList())){
            $urls['Pages'] = [];
            foreach($pages as $page){
                $urls['Pages'][$page->title] = '/'.$page->slug;
            }
        }
        return $urls;
    }
}
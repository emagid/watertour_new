<?php 

namespace Model; 

class Admin extends \Emagid\Core\Model {
	
	static $tablename = 'administrator'; 

	//static $role = ['0'=>'custom', '1'=>'super', '2'=>'administrator', '3'=>'manager'];

	static $admin_sections =  ['Content','Attributes','Banners','Pages', 'Administrators','Users',
		'Packages','Tours','Bike Catalog','Products','Equipments','Rentals',
		'Bus Packages','Bus Tours','Bus Catalog','Popup Screen','Quotes','Contacts',
		'Water Packages','Water Tours','Water Catalog','Categories','Reviews','Tour Redirect','Faqs','Faq Categories','Main Footer','Sub Footer','Main Routes',
        'Colors','Attractions','Collections','Materials','System','Order Management','Orders','Customer Center', 'Questions','Abandoned Carts','Routes','Route Icons','Blogs','About',
        'Main Banners', 'Featured Banners','Description', 'Deal of the Week', 'Shipping Methods', 'Newsletter', 'News', 'Configs', 'Languages', 'Navs','Sub Navs'];
	
	static $admin_sections_nested = [
		'Catalog'=>['Packages','Tours','Rentals','Equipments','Products'],
//		'Water Catalog'=>['Water Packages','Water Tours'],
//		'Bus Catalog'=>['Bus Packages','Bus Tours'],
//		'Hottest Deal'=>[],
		'Order Management'=>['Orders'],
		'Customer Center'=>['Users','Newsletter','Popup Screen', 'Quotes','Contacts'],
		'Content'=>['Pages','Attractions','Categories','About','Blogs','Reviews','Faqs','Faq Categories'],
        'Routing'=>['Main Routes','Routes','Route Icons'],
		'Navigation'=>['Navs','Sub Navs','Main Footer','Sub Footer'],
		'Banners'=>['Main', 'Description','Tour Redirect'],
        'System'=>['Abandoned Carts','Coupons','Shipping Methods', 'Languages', 'Configs'],
		'Administrators'=>[],
	];
	
	public static $fields =  [
		'first_name'=>[
			'name'=>'First Name',
			'required'=>true
		], 
		'last_name'=>[
			'name'=>'Last Name',
			'required'=>true,
		],
		'email'=>[
			'type'=>'email',
			'required'=>true,
			'unique'=>true
		],
		'username'=>[
			'required'=>true,
			'unique'=>true
		],
		'password'=>[
			'type'=>'password',
			'required'=>true,
		],
		'hash', 
		'permissions'
	];
	
	function full_name() {
		return $this->first_name.' ' .$this->last_name;
	}
	
	public static function login($username , $password){
		$list = self::getList(['where'=>"username = '{$username}'"]);
		if (count($list) > 0) {
			$admin = $list[0];
			$password = \Emagid\Core\Membership::hash($password, $admin->hash, null );

			if ($password['password'] == $admin->password ) {

				$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and admin_id = '.$admin->id]);
				$rolesIds = [];
				foreach($adminRoles as $role){
					$rolesIds[] = $role->role_id;
				}
				$rolesIds = implode(',', $rolesIds);
				
				$roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
	    		$rolesNames = [];
				foreach($roles as $role){
					$rolesNames[] = $role->name;
				}

	    		\Emagid\Core\Membership::setAuthenticationSession($admin->id,$rolesNames, $admin);

				return true;
			}
		}
		return false;
	}
	
	public static function getNestedSections ($permissions = null){
		if (is_null($permissions)){
			return self::$admin_sections_nested;
		} else {
			$permissions = explode(',', $permissions);			
			$sections = self::$admin_sections_nested;
			$authorized = [];
			foreach ($sections as $parent=>$children){
				if (in_array($parent, $permissions)){
					$authorized[$parent] = [];
					foreach ($children as $child){
						if (in_array($child, $permissions)){
							array_push($authorized[$parent], $child);
						}
					}
				}
			}
			return $authorized;
		}
	}
}
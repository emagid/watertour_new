<?php

namespace Model;

use Emagid\Core\Model;

class Tour_Image extends Model{
    static $tablename = 'tour_images';
    static $fields = [
        'tour_id',
        'image',
        'display_order'
    ];

    public function exists_image($size=[]) {
        $size_str = (count($size)==2) ? implode("_",$size) : "";
//        if($this->image!="" && file_exists(dirname(__FILE__).UPLOAD_URL.'products/'.$size_str.$this->image)) {
        if($this->image!="") {
            return true;
        }
        return false;
    }

    public function get_image_url($size=[]) {
        $size_str = (count($size)==2) ? implode("_",$size) : "";
        return UPLOAD_URL.'tours/'.$size_str.$this->image;
    }
}
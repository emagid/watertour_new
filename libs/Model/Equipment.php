<?php 

namespace Model; 

class Equipment extends \Emagid\Core\Model {

	static $tablename = 'equipment';
	
	public static $fields =  [
		'name',
		'slug'=>['unique'=>true],
		'featured_image',
		'adult_price'=>['type'=>'numeric'],
		'kid_price'=>['type'=>'numeric'],
		'discount_amount'=>['type'=>'numeric'],
		'discount_type',
		'description',
		'sku',
		'meta_title',
		'meta_keywords',
		'meta_description',
		'category',
		'displayable',
		'default_rental',
		'rental_price'
	];

	static $discount_type = [1=>'%', 2=>'-'];
	static $category = ['Adult','Kid','Children','Tandem','Accessories','Insurance','Baby Seat','Trailer'];
	static $displayable = [1=>'Rental',2=>'Tour',3=>'Rental and Tour'];

	function getName(){
		return $this->name;
	}

	/**
	 * @param string $type 'adult'|'kid'
	 * @return float|string
	 */
	function getPrice($type = 'adult',$rental_id = 0){
		$price = $this->applyDiscount($this->adult_price)['newPrice'];
		if($this->rental_price && $rental_id){
			$rp = json_decode($this->rental_price,true);
			$price = array_key_exists($rental_id,$rp) ? $rp[$rental_id]: $price;
		}
		return $price;
//		if($type == 'adult'){
//			return $this->applyDiscount($this->adult_price)['newPrice'];
//		} else if($type == 'kid'){
//			return $this->applyDiscount($this->kid_price)['newPrice'];
//		}
	}

	/**
	 * @param $price
	 * @return array['newPrice'=>'discount applied', 'removed'=>'discount amount from price']
	 */
	private function applyDiscount($price){
		if($price <= 0){
			return null;
		}
		if($this->discount_amount <= 0){
			return ['newPrice'=>$price,'removed'=>0];
		}
		switch($this->discount_type){
			case 1:
				$newPrice = $price*(1-$this->discount_amount/100);
				$removed = $price*($this->discount_amount/100);
				break;
			case 2:
				$newPrice = $price-$this->discount_amount;
				$removed = $this->discount_amount;
				break;
			default:
				$newPrice = $price;
				$removed = 0;
				break;
		}
		return ['newPrice'=>$newPrice,'removed'=>$removed];
	}

	function featuredImage(){
		return $this->featured_image;
	}

	//default rental
	static function getEquipmentByCategory($type = 1){
		$displayable = " and (displayable = $type or displayable = 3)";
		$adults = self::getList(['where'=>"lower(category) = 'adult' $displayable"]);
		$kids = self::getList(['where'=>"lower(category) = 'kid' $displayable"]);
		$children = self::getList(['where'=>"lower(category) = 'children' $displayable"]);
		$tandems = self::getList(['where'=>"lower(category) = 'tandem' $displayable"]);
		$accessories = self::getList(['where'=>"lower(category) = 'accessories' $displayable"]);
		$insurance = self::getList(['where'=>"lower(category) = 'insurance' $displayable"]);
		$babySeat = self::getList(['where'=>"lower(category) = 'baby seat' $displayable"]);
		$trailer = self::getList(['where'=>"lower(category) = 'trailer' $displayable"]);
		return ['adult'=>$adults,'kid'=>$kids,'child'=>array_merge($children,$accessories,$babySeat),'tandem'=>array_merge($tandems,$trailer,$insurance)];
	}

	static function getDefault($category){
		return self::getItem(null,['where'=>"category = '$category' and default_rental = 1"]);
	}
	
}
<?php

namespace Model;

use Emagid\Core\Model;

class Route extends Model{
    static $tablename = 'route';
    static $fields = [
        'name',
        'featured_image',
        'description',
        'points',
        'street'
    ];

    static function getCoordinates($id){
        return self::getItem($id)->points;
    }

    public function featuredImage(){
        return $this->featured_image ? : '';
    }

    public function fullFeaturedImagePath(){
        $rc = new \ReflectionClass($this);
        return $this->featuredImage() ? UPLOAD_URL.strtolower($rc->getShortName()).'s'.DS.$this->featuredImage() : '';
    }
}
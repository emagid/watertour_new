<?php

namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;

class Coupon extends Model{
	static $tablename = 'coupon';
	static $fields = ["name","start_date","end_date","coupon_code","coupon_amount","coupon_type","maximum_use","minimum_amount"];

	static $coupon_type = [1=>'%',2=>'-'];

	public static function newsletterGenerate($email){
	    $coup = new self();
	    $coup->name = 'Newsletter - '.$email;
	    $coup->start_date = Carbon::now()->toDateTimeString();
	    $coup->end_date = Carbon::now()->addYears(2)->toDateTimeString();
	    $coup->coupon_code = uniqid('ns-');
	    $coup->coupon_amount = 10;
	    $coup->coupon_type = 1;
	    $coup->maximum_use = 1;
	    $coup->minimum_amount = 0;
	    if($coup->save()){
            return $coup;
        } else {
	        return null;
        }
    }

    public static function checkCoupon($code){
	    $carbon = Carbon::now()->toDateTimeString();
	    $where = [];
	    $where[] = "lower(coupon_code) = lower('$code')";
	    $where[] = "'$carbon' between start_date and end_date";
	    $where[] = "maximum_use > (select count(id) from public.order where lower(coupon_code) = lower('$code'))";
	    return self::getItem(null,['where'=>implode(' and ',$where)]);
    }
}
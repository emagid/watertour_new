<?php

class toursController extends siteController
{

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Top View | NYC Tours";
        $params = [];
        $params['where'][] = 'availability = 2';
        $params['where'][] = 'now() between valid_start and valid_end';
        if(isset($_GET['o'])){ /*order by*/
            switch ($_GET['o']){
                case 'htl': /*high to low*/
                    $params['orderBy'][] = 'price desc';
                    break;
                case 'lth':
                    $params['orderBy'][] = 'price asc';
                    break;
                case 'new':
                    $params['orderBy'][] = 'insert_time desc';
                    break;
//                case 'best':
//                    $params['orderBy'][] = '';
//                    break;
            }
        }
        if(isset($_GET['n']) && $_GET['n']){ /*neighborhood*/
            $params['where'][] = 'neighborhood = '.$_GET['n'];
        }
        if(isset($_GET['d']) && $_GET['d']){ /*days*/
            $params['where'][] = 'days = '.$_GET['d'];
        }
        $merge = [];
        foreach ($params as $type=>$param) {
            $merge[$type] = implode(' and ',$param);
        }
        $tours = \Model\Tour::getList($merge);
        foreach ($tours as $tour) {
            $tour->images = $tour->getAllImages();
        }

        $featured = \Model\Tour::getList(['where'=>"featured = 1"]);

        $this->viewData->featured = $featured;
        $this->viewData->tours = $tours;
        $this->loadView($this->viewData);
    }

    public function tour(Array $params = [])
    {
        $this->configs['Meta Title'] = "Tour Name | Central Park NYC";
        $this->viewData->tours = \Model\Tour::getList(['where'=>'availability = 2']);
        $this->viewData->tour_index = 0;
        foreach($this->viewData->tours as $index=>$tour){
            if($tour->slug == $params['id']){
                $this->viewData->tour_index = $index;
            }
        }

        $this->viewData->tour = \Model\Tour::getItem(null,['where'=>'id = '.\Model\Tour::getIdBySlug($params['id']) .' and availability = 2']);
        if(!$this->viewData->tour){
            $_SESSION['notification'] = serialize(new \Notification\ErrorHandler('Your selection is not available for selection. Please choose from any of the tours listed below.'));
            redirect('/tours');
        }
        $this->viewData->equipment = \Model\Equipment::getEquipmentByCategory(2);
        $this->viewData->tour_images = $this->viewData->tour->getAllImages();
        $this->viewData->reviews = \Model\Review::getReviewsByType('tour',$this->viewData->tour->id);
//        $this->viewData->tour_routes = \Model\Tour_Route::getList(['where'=>"tour_id = {$this->viewData->tour->id}"]);
        $this->viewData->route_path = \Model\Route_Path::getList(['where'=>"main_route_id = {$this->viewData->tour->main_route_id}"]);
//        $this->viewData->routes = \Model\Route::getList(['where'=>"id in (select route_id from tour_route where tour_id = {$this->viewData->tour->id})"]);
        $_SESSION['cart']->tours = $this->viewData->tour->id;
        $this->loadView($this->viewData);
    }

    public function reservetour(Array $params = []){
        $this->viewData->tour = \Model\Tour::getItem(\Model\Tour::getIdBySlug($params['id']));
        $this->viewData->total = $this->viewData->tour->getPrice() * $_SESSION['cart']->adults + $this->viewData->tour->getPrice() * $_SESSION['cart']->kids;
        $this->loadView($this->viewData);
    }

    public function details(Array $params = []){
        $tour = \Model\Tour::getItem(\Model\Tour::getIdBySlug($params['id']));
        if(!$tour){
            $n = new \Notification\ErrorHandler("Please choose from one of our tours below");
            $_SESSION['notification'] = serialize($n);
            redirect('/tours');
        }
        $this->viewData->tour = $tour;
        $this->viewData->tour_images = \Model\Tour_Image::getList(['where'=>"tour_id = $tour->id"]);
        $this->viewData->route = $tour->getRoute() ? $tour->getRoute()->points: null;
        $this->loadView($this->viewData);
    }

    function getTour()
    {
        $slug = $_POST['slug'];
        $tour = \Model\Tour::getItem(\Model\Tour::getIdBySlug($slug));
        $photos = $tour->getAllImages();
        $reviews = \Model\Review::getReviewsByType('tour',$tour->id);
        foreach($reviews as $review){
            $review->date = date('M jS, Y',strtotime($review->create_date));
        }
        if ($tour) {
            $_SESSION['cart']->tours = $tour->id;
            $occurrence = '';
            if($tour->available_range && json_decode($tour->available_range,true)) {
                $str = [];
                foreach (json_decode($tour->available_range, true) as $syntax => $value) {
                    switch ($syntax) {
                        case 'o':
                            $occurrence = $value;
                            break;
                        case 'w':
                            $str[] = implode(', ', array_map(function ($item) {
                                return ucfirst($item);
                            }, $value));
                            break;
                        case 'd':
                            $str[] = implode(', ', array_map(function ($item) {
                                return ucfirst($item);
                            }, $value));
                            break;
                        case 'h':
                            $str[] = implode(', ', array_map(function ($item) {
                                if ($item < 12) {
                                    $str = fmod($item, 1) == 0 ? $item . 'AM' : floor($item) . ':30AM';
                                    return $str;
                                } else if ($item == 12 || $item == 12.5) {
                                    $str = fmod($item, 1) == 0 ? $item . 'PM' : floor($item) . ':30PM';
                                    return $str;
                                } else if ($item == 24 || $item == 24.5) {
                                    $str = fmod($item, 1) == 0 ? ($item - 12) . 'AM' : floor($item - 12) . ':30AM';
                                    return $str;
                                } else {
                                    $str = fmod($item, 1) == 0 ? ($item - 12) . 'PM' : floor($item - 12) . ':30PM';
                                    return $str;
                                }
                            }, $value));
                            break;
                    }
                }
                $availability = $occurrence . ' (' . implode(' at ', $str) . ')';
            } else {
                $availability = 'Tour Specific';
            }
            $languages = $tour->languages && json_decode($tour->languages,true)? implode(', ',json_decode($tour->languages,true)) : '';
            $duration = 'Tour Specific';
            $dur = [];
            if($tour->duration_hour){
                $dur[] = floatval($tour->duration_hour) .' '. pluralize($tour->duration_hour,'hour');
            }
            if($tour->duration_minute){
                $dur[] = floatval($tour->duration_minute) .' '. pluralize($tour->duration_minute,'hour');
            }
            $duration = $dur ? implode(' and ',$dur) : $duration;
//            if(isset($_SESSION['cart']->datetime) && $_SESSION['cart']->datetime){
//                $_SESSION['cart']->datetime = '';
//            }
            $this->toJson([
                'status' => 'success',
                'languages'=>$tour->languages == 'null' || $tour->languages == '' ? '': implode(', ',json_decode($tour->languages,true)),
                'tourName' => $tour->name,
                'tourId' => $tour->id,
                'tourImg' => $tour->featuredImage(),
                'tourPrice' => $tour->getPrice() ?: 0,
                'tourGallery'=>$photos,
                'tourReviews'=>$reviews,
                'tourAvailability'=>$availability,
                'tourLanguages'=>$languages,
                'tourDuration'=>$duration,
                'tourDetails'=>$tour->details,
                'tourDeparture'=>$tour->departure_point,
                'tourRoute'=>\Model\Route::getCoordinates($tour->route),
                'tourHours'=>implode(',',$tour->getHours()),
                'tourWeeks'=>implode(',',$tour->getWeek()),
                'tourDays'=>implode(',',$tour->getDay())
            ]);
        } else {
            $this->toJson(['status' => 'failed', 'message'=>'Error, did not find the tour.']);
        }
    }
}
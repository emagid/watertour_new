<?php

//For categoriesController AJAX calls
class categoryController extends siteController{

    public function buildCategoryItems(){
        $mCategory = $_POST['mCategory'];
        $categoryId = $_POST['category_id'];
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $query['where'] = [];
        $sortBy = isset($_GET['sort_by'])?\Model\Product::$SORT_MAP[intval($_GET['sort_by'])]:null;

        if($sortBy){
            unset($_GET['sort_by']);
        }
        if(is_numeric($categoryId)){
            $category = \Model\Category::getItem($categoryId);
        } else {
            $category = new stdClass();
            $category->slug = $categoryId;
        }
        $orderByCategory = '';
        if($mCategory && $categoryId) {
            if ($category->slug == 'all' || strpos(strtolower($category->slug), 'shop-by') !== false) {
                $mCatId = is_numeric($mCategory) ? $mCategory: $mCategory->id;
                $query = ['where' => ["id in (select product_id from product_categories where category_id = $mCatId and product_categories.active = 1)"]];
                $orderByCategory = ' and category_id=' . $mCatId;
            } else {
                $query = ['where' => ["id in (select product_id from product_categories where category_id = $category->id and product_categories.active = 1)"]];
                $orderByCategory = ' and category_id=' . $category->id;
            }
        }
        if(isset($_GET)){
            foreach($_GET as $key=>$value){
                if($key == 'term'){
                    $explodeTags = explode(' ', strtolower(urldecode($value)));
                    $tags = "'%" . implode("%','%", $explodeTags) . "%'";
                    $query['where'][] = 'lower(name) like \'%' . strtolower(urldecode($value)) . '%\'';
                    $query['where'][] = 'lower(tags) like any (array[' . $tags . '])';
                    $query['where'] = ["(" . implode(' OR ', $query['where']) . ")"];
                } else if ($key == 'heel_height') {
                    $val = explode(',', urldecode($value));
                    $query['where'][] = "$key in (".implode(',',$val).")";
                } else {
                    $val = explode(',',urldecode($value));
                    $query['where'][] = "$key similar to '%\"(".implode('|',$val).")\"%'";
                }
            }
        }
        $query['where'][] = 'product.active=1';
        $query['orderBy'] = $sortBy ? [$sortBy]: ["(select min(display_order) from product_categories where product.id = product_categories.product_id and product_categories.active=1 $orderByCategory),id"];
        $query['limit'] = $limit;
        $query['offset'] = $offset;
        $products = \Model\Product::getList(['sql'=>\Model\Category::filterSql($query)]);

        $html = '';
        if($products) {
            foreach ($products as $product) {
                $prodAttr = \Model\Product_Attributes::getColorList($product->id, 0);
                $featuredImageParam = $category->slug == 'all' ? null: $category->id;
                $html .=
                    '<div class="col productGridItem">
                        <div id="showQuickView" class="show_mdl" data-id="' . $product->id . '" data-mdl_name="quickView">
                            <h5 class="as_m"><span><i></i><i></i></span>Quick View</h5>
                        </div>';
                if (count($prodAttr)>=5){
                    $html .= '<div class="productGridItemSwatches productGridItemSwatchesSlider">';
                }else{
                    $html .= '<div class="productGridItemSwatches">';
                }
                foreach ($prodAttr as $pa) {
                    $html .= '<div class="productGridItemSwatchWrapper"><img src="' . $pa->swatch() . '" class="productGridItemSwatch" data-color_id="'.$pa->id.'" data-pro_id="'.$product->id.'"/></div>';
                }
                $html .=
                    '</div>
                        <a href="' . SITE_URL . "products/$product->slug" . '">
                            <div class="saleInfoWrapper">';
                $product->isDiscounted()||($product->price)>$product->basePrice() ? $html .= '<small class="saleIndicator">On Sale</small>': $html .= '';
                if(\Model\Event::eventSale($product->id,$product->default_color_id)) {
                    $eventAttr = \Model\Product_Attributes::getEventById($product->id, $product->default_color_id);
                    $event = \Model\Event::getItem($eventAttr->value);
                    $html .= '<div class="saleEventIcon event-icon icon" style="background-image:url(\'' . $event->getIcon() . '\')"></div>';
                }
                $html .= '</div>
                <div class="mediaWrapper">
                    <div class="media" style="background-image:url(' . UPLOAD_URL . 'products/' . $product->featuredImage($featuredImageParam) . ')"></div>
                </div>
                <div class="dataWrapper">
                <h4 class="product_name">' . $product->name . '</h4>';
                $product->isDiscounted() ? $html .= "<small>On Sale</small>" : $html .= "";
                if ($product->msrp > 0.0) {
                    $percent = round(($product->basePrice() - $product->msrp()) * 100 / $product->basePrice());
                    $html .= '
                        <h4 class="product_price">
                            <span class="full_price">
                                <span class="currency">$</span>' . number_format($product->basePrice(), 2) . '
                            </span>
                            <span class="markdown">-' . $percent . '%</span>
                            <span class="value">$' . number_format($product->msrp(), 2) . '</span>
                        </h4>';
                } else if ($product->isDiscounted()) {
                    $percent = round(($product->basePrice() - $product->price()) * 100 / $product->basePrice());
                    $html .=
                        '<h4 class="product_price">
                            <span class="full_price">
                                <span class="currency">$</span>' . number_format($product->basePrice(), 2) . '
                            </span>
                            <span class="markdown">-' . $percent . '%</span>
                            <span class="value">$' . number_format($product->price(), 2) . '</span>
                        </h4>';
                } else if(($product->price)>$product->basePrice()){
                    $percent = round(($product->price - $product->price()) * 100 / $product->price);
                    $html .=
                        '<h4 class="product_price">
                            <span class="full_price">
                                <span class="currency">$</span>' . number_format($product->price, 2) . '
                            </span>
                            <span class="markdown">-' . $percent . '%</span>
                            <span class="value">$' . number_format($product->price(), 2) . '</span>
                        </h4>';
                } else {
                    $html .= '<h4 class="product_price">$'.number_format($product->price(),2).'</h4>';
                }
                $html .=
                    '</div>
                        </a>
                    </div>';
            }
        }
        $status = "success";
        $this->toJson(['itemHtml'=>$html,'status'=>$status]);
    }

    public function buildVariantImage(){
        $product_id = $_POST['product_id'];
        $color_id = $_POST['color_id'];
        $product_image = \Model\Product_Image::getItem(null,['where'=>"product_id = $product_id and color_id like '%\"$color_id\"%' and (legshot = 0 or legshot is null)", 'orderBy'=>'display_order']);
        $image = is_null($product_image)? '': UPLOAD_URL.'products/'.$product_image->image;

        $product = \Model\Product::getItem($product_id);
        $isDiscounted = $product->isDiscounted($color_id);
        $defaultPrice = $product->price;
        $base = $product->basePrice($color_id);
        $icon = '';
        if(\Model\Event::eventSale($product_id,$color_id)){
            $event = \Model\Event::getItem(\Model\Product_Attributes::getEventById($product_id,$color_id)->value);
            $isDiscounted = true;
            $icon = $event->getIcon();
            $price = $product->price($color_id);
            $percent = round(($base - $price) * 100 / $base);
        } else if(($productAttribute = \Model\Product_Attributes::getItem(null,['where'=>"product_id = $product_id and color_id = $color_id and name = 'price'"])) && $productAttribute->value > 0){
            $price = $product->price($color_id);
            if($defaultPrice>$base){
                $isDiscounted = true;
                $base = $defaultPrice;
                $percent = round(($base - $price) * 100 / $base);
            }elseif($price == $base){
                $isDiscounted = false;
                $percent = 0;
            }else{
                $isDiscounted = true;
                $percent = round(($base - $price) * 100 / $base);
            }
        }elseif ($product->msrp>0){
            $price = $product->msrp($color_id);
            $percent = round(($base - $price) * 100 / $base);
        }else{
            $price = $product->price($color_id);
            if($isDiscounted){
                $percent = round(($base - $price) * 100 / $base);
            }else{
                $percent = 0;
            }
        }
        $base = number_format($base,2);
        $price = number_format($price,2);
        $this->toJson(['status'=>'success','image'=>$image,'price'=>$price,'isDiscounted'=>$isDiscounted,'basePrice'=>$base,'percent'=>$percent, 'event'=>$icon]);

    }
}
<?php

require_once(ROOT_DIR.'libs/authorize/AuthorizeNet.php');
include(__DIR__.'/../libs/php-barcode-generator-master/src/BarcodeGenerator.php');
include(__DIR__.'/../libs/php-barcode-generator-master/src/BarcodeGeneratorPNG.php');

class checkoutController extends siteController {
	
    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Bike Tour - Checkout";
        $this->loadView($this->viewData);
//        redirect('/checkout/email');
    }

    public function payment(Array $params = [])
    {

        $this->emptyCartValidation(isset($params['id']) && $params['id'] ? $params['id']: '');
        $this->configs['Meta Title'] = "Checkout &#x2014; Bike Rental Central Park";
        $this->viewData->cart = \Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: null,session_id());
        $this->viewData->subtotal = array_sum(array_map(function($item){return $item->getTotal();},$this->viewData->cart));
        $this->viewData->discount = isset($_SESSION['cart']->coupon) ? ($_SESSION['cart']->coupon['type'] == '%' ? $this->viewData->subtotal * ($_SESSION['cart']->coupon['amt'] / 100): $_SESSION['cart']->coupon['amt']): 0;
        $subWithoutPackage = array_sum(array_map(function($item){if(!$item->package_id){return $item->getTotal();}},$this->viewData->cart));
        $this->viewData->tax = ($subWithoutPackage - $this->viewData->discount) * .08875;
        $this->viewData->total = $this->viewData->subtotal - $this->viewData->discount + $this->viewData->tax;

        if(!isset($_SESSION['aCart'])){
            $cartIds = [];
            if(isset($this->viewData->cart->cart) && $this->viewData->cart->cart != null) {
                $cartIds = array_map(function ($cart) {
                    return $cart->id;
                }, $this->viewData->cart->cart);
            }
            $ac = new \Model\Abandoned_Cart();
            $ac->user_id = $this->viewData->user ? $this->viewData->user->id: 0;
            $ac->guest_id = session_id();
            $ac->cart = json_encode($cartIds);
            $ac->save();
            $_SESSION['aCart'] = $ac;
        } else if(isset($_SESSION['aCart']) && $this->emagid->route['controller'] != 'checkout'){
            unset($_SESSION['aCart']);
        }

        $this->loadView($this->viewData);
    }

    /*
     * Template for sending to different web service
     * public string PutData(
     *  string UserID,
     *  string Interest,
     *  string txnPrice,
     *  string txnTax,
     *  string TXNAmount,
     *  string Type,
     *  string StartTime,
     *  string Duration,
     *  string NoOfMale,
     *  string NoOfFemale,
     *  string NoOfKid,
     *  string BabySeat,
     *  string Trailer,
     *  string Tandem,
     *  string auth_code,
     *  string transactionID,
     *  string First,
     *  string Last,
     *  string Email
     * )
     * */
    function payment_post(Array $params = []){
        $_POST = array_merge($_POST);
        $order = \Model\Order::loadFromPost();
        $date = new DateTime();
        $order->insert_time = $date->format('Y-m-d H:i:s');
        if($this->viewData->user){
            $order->user_id = $this->viewData->user->id;
        } else {
            $order->user_id = 0;
        }
        $order->guest_id = session_id();

        $carts = \Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: 0,session_id());

        $calculatedTotal = $subtotal = array_sum(array_map(function($item){return $item->getTotal();},$carts));
        $subWithoutPackage = array_sum(array_map(function($item){if(!$item->package_id){return $item->getTotal();}},$carts));
        $splitName = explode(' ',$_POST['fullName']);
        $order->bill_first_name = $splitName[0];
        if(count($splitName) > 1){
            unset($splitName[0]);
            $order->bill_last_name = implode(' ',$splitName);
        }

        /** implement coupon*/
        $couponDiscount = 0;
        if(isset($_SESSION['cart']->coupon) && $_SESSION['cart']->coupon && ($coupon = \Model\Coupon::checkCoupon($_SESSION['cart']->coupon['code']))){
            $order->coupon_code = $coupon->code;
            $order->coupon_type = $coupon->coupon_type;
            $order->coupon_amount = $coupon->coupon_amount;
            $couponDiscount = $coupon->coupon_type == 1 ? ($coupon->coupon_amount / 100) * $calculatedTotal: $coupon->coupon_amount;
            $calculatedTotal -= $couponDiscount;
        }
        /** implement tax rate*/
//        if(isset($_POST['tax_rate']) && $_POST['tax_rate']){
            $order->tax = ($subWithoutPackage - $couponDiscount) * .08875;
            $calculatedTotal += $order->tax;
            $order->tax_rate = 8.875;
//        }
        /** implement shipping method */
//        if(isset($_POST['shipping']) && $_POST['shipping']){
//            $calculatedTotal += $_POST['shipping'];
//            $order->shipping_cost = $_POST['shipping'];
//        }
        /** implement gift card*/
//        if(isset($_POST['gift_card']) && $_POST['gift_card']){
////            $calculatedTotal -= $_POST['gift_card'];
//            $order->gift_card = $_POST['gift_card'];
//        }

//        if(isset($_POST['paypal']) && $_POST['paypal'] == 'on'){
//            $order->payment_method = 2;
//        } else {
//            $order->payment_method = 1;
//        }

        $order->status = \Model\Order::$status[0];

        $order->subtotal = $subtotal;
        $order->total = $calculatedTotal;
        $order->ref_num = generateToken();

//        dd($_POST,$_SESSION,$this,$order,$carts);
        if($order->save()) {

            //authorize.net
            $transaction = new AuthorizeNetAIM();
            $transaction->setFields([
                'amount'=>$order->total,
                'card_num'=>$order->cc_number,
                'card_code'=>$order->cc_ccv,
                'exp_date'=>$order->cc_expiration_month.'/'.$order->cc_expiration_year,
                'first_name'=>$order->bill_first_name,
                'last_name'=>$order->bill_last_name,
                'email'=>$order->email,
                'invoice_num'=>$order->id
            ]);
            $response = $transaction->authorizeAndCapture();
            if($response->approved || $response->held) {
                $order->authorization_code = $response->transaction_id;
                $order->transaction_id = $response->authorization_code;
                $order->save();
//            \Model\User::checkoutUser($order);
                $html = '';

                foreach($carts as $cart) {
                    $personStr = implode('<br>',['Adults: '.$cart->adult_count,'Kids: '.$cart->kid_count]);
                    if($cart->tour_id){
                        $t = \Model\Tour::getItem($cart->tour_id);

                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal"><img width="100px!important;" src="https://biketour.mymagid.net' . UPLOAD_URL . 'tours/' . $t->featuredImage() . '"></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $t->name . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($cart->getTotal(),2) . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $personStr . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . date('M d, Y g:i a',strtotime($cart->reserve_date)) . '</span></p>
                                </td>
                            </tr>';
                    }
                    if($cart->package_id){
                        $p = \Model\Package::getItem($cart->package_id);
                        $temp = str_replace(array("[","]","'",'"'), "", $p->tour_id);
                        $tourIds = explode(",",$temp);
        
                        $detailString = '<tr><td>'.$p->name."</td></tr>";
                    $detailString.='<tr><td>'.$p->summary."</td></tr>";
                    foreach($tourIds as $tourId){
                            $tempHour = '';
                            $meridian = 'AM';
                            foreach(json_decode($tour->available_range, true)['h'] as $hour){
                                if($hour/12){
                                    $meridian = "PM";
                                }
                                $tempHour.=intval($hour%12).$meridian."<br/>";
                            }
                            $tour = \Model\Tour::getItem(intval($tourId));
                            $detailString.="<tr><td><strong>".$tour->name."</strong></td></tr>";
                            $detailString.="<tr><td>".$tour->summary."</td></tr>";
                            $detailString.="<tr><td><strong>Tour Duration: </strong>".$tour->getDuration()."</td></tr>";
                            $detailString.="<tr><td><strong>Departure Point: </strong> ".$tour->departure_point."</td></tr>";
                            $detailString.="<tr><td><strong>Availability: </strong>(".json_decode($tour->available_range, true)['o'].") ".$tempHour. "</td></tr><hr/>";
    
                    }
                        foreach(json_decode($cart->package_details,true) as $name=>$detail){
                            $m = "\\Model\\".$name;
                            foreach($detail as $d){
                                $obj = $m::getItem($d['id']);
                                $timeStr[] = "{$obj->getName()} ";/**.date('M d, Y g:i a',strtotime($d['datetime']));**/
                            }
                        }
                        $newTimeStr = implode($timeStr);

                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal"><img width="100px!important;" src="https://biketour.mymagid.net' . UPLOAD_URL . 'packages/' . $p->featuredImage() . '"></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $p->name . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($cart->getTotal(),2) . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $personStr . '</span></p>
                                </td>
                            </tr>
                            
                            <tr id="package_includes">
                                <div width="400px;" style="border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;">
                                <h2><u>Included in Package</u></h2>
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $newTimeStr . '</span></p>
                                </div>
                            </tr>
                            '.$detailString;
                    }
                    if($cart->duration){
                        $r = \Model\Rental::getItem($cart->duration);
                        var_dump("Cheeseburger");
                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal"><img width="100px!important;" src="https://biketour.mymagid.net' . UPLOAD_URL . 'rental/"></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $r->name . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($cart->getTotal(),2) . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $personStr . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . date('M d, Y g:i a',strtotime($cart->reserve_date)) . '</span></p>
                                </td>
                            </tr>';
                    }

                    $babySeat = $trailer = 0;
                    $equip = [];
                    foreach (json_decode($cart->equipment, true) as $id => $attr) {
                        $e = \Model\Equipment::getItem($id);
                        $equip[$id] = array_merge($attr, ['price' => $e->getPrice()]);

                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal"><img width="100px!important;" src="https://biketour.mymagid.net' . UPLOAD_URL . 'equipments/' . $e->featuredImage() . '"></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $e->name . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($e->getPrice() * $attr['qty'], 2) . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $attr['qty'] . '</span></p>
                                </td>
                            </tr>';
                        if($e->category == 'Baby Seat'){
                            $babySeat = $attr['qty'];
                        } else if($e->category == 'Trailer'){
                            $trailer = $attr['qty'];
                        }

                    }

                    $product = [];
                    foreach (json_decode($cart->products, true) as $id => $attr) {
                        $p = \Model\Product::getItem($id);
                        $product[$id] = array_merge($attr, ['price' => $p->getPrice()]);

                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal"><img width="100px!important;" src="https://biketour.mymagid.net' . UPLOAD_URL . 'products/' . $p->featuredImage() . '"></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $p->name . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($p->getPrice() * $attr['qty'], 2) . '</span></p>
                                </td>
                                <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $attr['qty'] . '</span></p>
                                </td>
                            </tr>';
                    }

                    $upgrade = json_decode($cart->upgrade_option,true);
                    if($upgrade) {
                        foreach ($upgrade as $type => $ar) {
                            foreach ($ar as $id => $value) {
                                $mod = "\\Model\\" . ucfirst($type);
                                $obj = $mod::getItem($id);
                                $upgrade[$type][$id]['price'] = $obj->getPrice();
                            }
                        }
                    }

                    $order_product = new \Model\Order_Product();
                    $order_product->order_id = $order->id;
                    $order_product->equipment = json_encode($equip);
                    $order_product->product = json_encode($product);
                    $order_product->tour = $cart->tour_id;
                    $order_product->package = $cart->package_id;
                    $order_product->rental = $cart->duration; //todo change duration to rental id
                    $order_product->upgrade_option = $upgrade;
                    $order_product->adult_count = $cart->adult_count;
                    $order_product->kid_count = $cart->kid_count;
                    $order_product->tandem_count = $cart->tandem_count;
                    $order_product->reservation_date = $cart->reserve_date;
                    $order_product->total = $cart->getTotal();
                    $order_product->package_details = $cart->package_details;
                    $order_product->add_ons = $cart->add_ons;
                    $order_product->save();

                    if($cart->package_id){
                        $packageName = \Model\Package::getItem($cart->package_id)->name;
                        $tourCodes = implode(';',json_decode(\Model\Package::getItem($cart->package_id)->tour_id,true));
                    } else if($cart->tour_id){
                        $tour = \Model\Tour::getItem($cart->tour_id);
                        $packageName = $tour->name;
                        $tourCodes = $tour->tour_code;
                    } else {
                        $rental = \Model\Rental::getItem($cart->duration);
                        if($order_product->tandem_count){
                            $packageName = str_replace('Bike','Tandem',$rental->name); //todo change duration to rental id
                            $tourCodes = json_decode($rental->options,true)['tandem_tour'];
                        } else {
                            $packageName = $rental->name; //todo change duration to rental id
                            $tourCodes = $rental->tour_code;
                        }
                    }

                    if($order_product->rental){
                        $duration = \Model\Rental::getItem($order_product->rental)->getDuration();
                    } else if($order_product->tour){
                        $duration = \Model\Tour::getItem($order_product->tour)->getDuration();
                    } else {
                        $duration = \Model\Package::getItem($order_product->package)->getDurationFormatted();
                    }

                    $adult_count = $order_product->adult_count ? : 0;
                    $adult_count += $order_product->tandem_count * 2;

                    //Send to BikeTour
                    $data = [
                        'OrderID' => $order->id,
                        'PackageName'=>$packageName,
                        'Amount' => round($order->total,2),
                        'Type' => 'TOPVIEW',
                        'StartTime' => date('Y-m-d H:i:s', strtotime($order_product->reservation_date)),
                        'Duration'=>$duration,
                        'Adults' => $adult_count,
                        'Kids' => $order_product->kid_count ? : 0,
                        'AuthCode' => $response->authorization_code,
                        'TransactionID' => $response->transaction_id,
                        'FirstName' => $order->bill_first_name,
                        'LastName' => $order->bill_last_name,
                        'email' => $order->email,
                        'TourCodes'=>$tourCodes,
                        'ClientId'=>ClientId,
                        'BabySeats'=>$babySeat,
                        'Trailers'=>$trailer
                    ];

                    /**$ch = curl_init('http://www.topviewapi.com/api/Order');
                    curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-Type:application/json'));
                    curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                    $order_product->order_api_response = curl_exec($ch);
                    curl_close($ch);**/

                    $order_product->order_api = json_encode($data);
                    $order_product->save();
                }
                $barcode = new \Picqer\Barcode\BarcodeGeneratorPNG();
                $barPng = $barcode->getBarcode($order->authorization_code,$barcode::TYPE_CODE_39);
                $imgStr = 'bar'.$order->authorization_code.'.png';
                file_put_contents(UPLOAD_PATH.'barcode/'.$imgStr,$barPng);
                $img = '<img src="http://biketour.mymagid.net'.UPLOAD_URL.'barcode/'.$imgStr.'"/>';

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER'=>$order->id,
                    'BARCODE'=>$img,
                    'AUTH'=>$order->authorization_code,
                    'DATE'=>date('Y-m-d h:i:s'),
                    'NAME'=>$order->bill_first_name .' '.$order->bill_last_name,
                    'SHIPPING'=>$order->getShippingAddr(),
                    'BILLING'=>$order->getBillingAddr(),
                    'ITEMS'=>$html,
                    'VALIDDATE' => date('M d, Y g:i a',strtotime($cart->reserve_date)),
                    'SUBTOTAL'=>number_format($order->subtotal,2),
                    'DISCOUNT'=>number_format($order->coupon_amount,2), // update later
                    'TAXFEE'=>number_format($order->tax,2),
                    'TOTAL'=>number_format($order->total,2)
                ];
                $email->setTo(['email'=>$order->email,'name'=>$order->bill_first_name.' '.$order->bill_last_name, 'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('water-order-receipt')->send();

                //Unset everything
                if(isset($_SESSION['cart'])){
                    unset($_SESSION['cart']);
                }
                if(isset($_SESSION['rental'])){
                    unset($_SESSION['rental']);
                }
                foreach($carts as $item){
                    \Model\Cart::delete($item->id);
                }

                redirect('/checkout/confirmation/'.$order->ref_num);
            } else {
                $order->status = \Model\Order::$status[2];
                $order->error_message = $response->response_reason_text;
                $order->save();
                $n = new \Notification\ErrorHandler($response->response_reason_text);
                $_SESSION['notification'] = serialize($n);
                redirect('/checkout/payment');
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/checkout/payment');
        }
    }

    function confirmation(Array $params = []){
        $order = \Model\Order::getItem(null,['where'=>"ref_num = '{$params['id']}'"]);
        $orderProducts = $order->getOrderProducts();

        $this->viewData->order = $order;
        $this->viewData->order_products = $orderProducts;
        $this->viewData->total = $order->total;

        $this->loadView($this->viewData);
    }

    function logCheckout(){
        if(isset($_POST) && isset($_SESSION['aCart'])){
            $ac = $_SESSION['aCart'];
            $checkout_fields = $ac->checkout_fields ? json_decode($ac->checkout_fields,true): [];
            foreach($_POST as $key=>$value){
                $checkout_fields[$key] = $value;
            }
            $ac->update_time = date('Y-m-d H:i:s.u');
            $ac->checkout_fields = json_encode($checkout_fields);
            $ac->save();
            echo json_encode(['status'=>"success"]);
        }
    }

    private function emptyCartValidation($ref_num){
        if(!\Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: 0,session_id())){
            redirect('/');
        }
    }

}
<?php

class rentalsController extends siteController {
    
    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Bike Rentals | Central Park NYC";
        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $this->viewData->equipment = \Model\Equipment::getEquipmentByCategory(1);
        $this->viewData->reviews = \Model\Review::getReviewsByType('Rental',null);
        $this->loadView($this->viewData);
    }

}
<?php

class newsletterController extends siteController{
    public function subscribe(){
        $email = isset($_POST['email']) ? $_POST['email']: null;
        if(!$email || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
            $this->toJson(['status'=>'failed','msg'=>'Please enter a valid email address to receive your 10% off!']);
        } else if(\Model\Newsletter::checkEmail($_POST['email'])){
            $this->toJson(['status'=>'failed','msg'=>"You're already signed up! Use your 10% discount while offer last!"]);
        } else {
            $coupon = \Model\Coupon::newsletterGenerate($_POST['email']);

            if($coupon){
                $newsletter = new \Model\Newsletter();
                $newsletter->email = $_POST['email'];
                $newsletter->coupon_code = $coupon->coupon_code;
                if($newsletter->save()){
                    $this->toJson(['status'=>'success','msg'=>"Thank you for subscribing! You'll receive a discount code shortly"]);
                } else {
                    dd($newsletter->errors);
                    $this->toJson(['status'=>'failed','msg'=>"An error occurred while processing your request. Please try again later."]);
                }
            } else {
                dd($coupon->errors);
                $this->toJson(['status'=>'failed','msg'=>"An error occurred while processing your coupon. Please try again later."]);
            }
        }
    }
}
<?php

class attractionsController extends siteController {
    
    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "NYC Attractions &#x2014; Bike Rental Central Park";
        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $attractions = \Model\Attraction::getList(['orderBy'=>'name']);
        $this->viewData->unchunkedAttractions = $attractions;
        $half = ceil(count((array)$attractions)/2);
        $this->viewData->attractions = array_chunk((array)$attractions,$half);
        $categories = [];
        $cPreload = [];
        $increment = 0;
        foreach(\Model\Category::getList() as $value){
            if($increment < 35){
                $cPreload[] = addslashes($value->name);
                $increment++;
            }
            $categories[]['category'] = $value->name;
        }
        $this->viewData->categories = json_encode($categories);
        $this->viewData->preload = implode('\',\'',$cPreload);

        $attr = [];
        $pPreload = [];
        $increment = 0;
        foreach($attractions as $attraction){
            if($increment < 35){
                $pPreload[] = addslashes($attraction->name);
                $increment++;
            }
            $attr[]['attr'] = $attraction->name;
        }
        $this->viewData->attr = json_encode($attr);
        $this->viewData->pPreload = implode('\',\'',$pPreload);
        $this->loadView($this->viewData);
    }

    public function attraction(Array $params = [])
    {
        $this->configs['Meta Title'] = "Bike Rental Central Park";
        $attraction = \Model\Attraction::getItem(\Model\Attraction::getIdBySlug($params['slug']));
        if(!$attraction){
            redirect('/attractions');
        }
        $attractions = \Model\Attraction::getList();
        $attrIds = array_map(function($item){return $item->id;},(array)$attractions);
        if(($index = array_search($attraction->id,$attrIds)) !== false){
            $this->viewData->prev = \Model\Attraction::getItem($index == 0 ? $attrIds[count($attractions)-1] : $attrIds[$index-1]);
            $this->viewData->next = \Model\Attraction::getItem($index == count($attractions)-1 ? $attrIds[0] : $attrIds[$index+1]);
        }

        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $this->viewData->attraction = $attraction;
        $this->viewData->attraction_images = $attraction->getImages();
        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);

        $relatedAll = $attraction->hasPackage();;
        shuffle($relatedAll);
        $related = array_splice($relatedAll,0,2);
        $this->viewData->related = $related;

//        dd($this->viewData);
        $this->loadView($this->viewData);
    }

            
}
<?php

class categoriesController extends siteController {
	
    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Category";
        $this->loadView($this->viewData);
    }

    public function subcategory(Array $params = [])
    {

        $this->configs['Meta Title'] = "Subcategory";
        $this->loadView($this->viewData);
    }

	public function category(Array $params = []){
		$slug = $params['category_slug'];
		$mCategory = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
		// /collections/{main category}
		if(isset($params['category_slug']) && $params['category_slug'] != '' && !isset($params['subcategory'])){
			if($mCategory->meta_title){
				$this->configs['Meta Title'] = $mCategory->meta_title;
			}
			$this->viewData->category = $mCategory;
			$this->viewData->featured_category = \Model\Category::getItem(null,['where'=>"parent_category = {$mCategory->id} and featured_category = 1"]);
			$this->view = 'index';
		}

		// /collections/{main category}/{subcategory}
		if(isset($params['subcategory']) && $params['subcategory'] != '' && $mCategory != ''){
			$slug = $params['subcategory'];
			$category = \Model\Category::getItem(null,['where'=>"slug = '{$slug}' and parent_category = {$mCategory->id}"])?:"all";
			if($category->meta_title){
				$this->configs['Meta Title'] = $category->meta_title;
			}
			if($params['subcategory'] == 'all' || strpos(strtolower($params['subcategory']),'shop-by') !== false){
				//Apply filter on grabbed products
				$query = ['where'=>["id in (select product_id from product_categories where category_id = $mCategory->id and product_categories.active = 1)"]];
				$orderByCategory = ' and category_id='.$mCategory->id;
//				$query = ['where'=>"product_categories.category_id = $mCategory->id and product.active = 1 and product_categories.active = 1"];

				if(strpos(strtolower($params['subcategory']),'shop-by') == 0){
					$filterActive = str_replace('shop-by-','',$params['subcategory']);
					$this->viewData->filterActive = $filterActive;
				}
			} else {
				//Apply filter on grabbed products
				$query = ['where'=>["id in (select product_id from product_categories where category_id = $category->id and product_categories.active = 1)"]];
				$orderByCategory = ' and category_id='.$category->id;
//				$query = ['where'=>"product_categories.category_id = $category->id and product.active = 1 and product_categories.active = 1"];
			}

			$sortBy = isset($_GET['sort_by'])?\Model\Product::$SORT_MAP[intval($_GET['sort_by'])]:null;
			$sortNum = isset($_GET['sort_by']) ? $_GET['sort_by']: null;

			$limit = 8;
			$offset = 0;

			//Filter functionality (build query)
			$query['orderBy'][] = "(select min(display_order) from product_categories where product.id = product_categories.product_id and product_categories.active=1 $orderByCategory),id";
			if($sortBy){
				$query['orderBy'] = [$sortBy];
				unset($_GET['sort_by']);
			}
			if(isset($_GET)){
				foreach($_GET as $key=>$value) {
					if ($key == 'heel_height') {
						$val = explode(',', urldecode($value));
						$query['where'][] = "product.$key in (".implode(',',$val).")";
					} else {
						$val = explode(',', urldecode($value));
						$query['where'][] = "product.$key similar to '%\"(" . implode('|', $val) . ")\"%'";
					}
				}
			}

			$query['where'][] = 'product.active=1';
			$sqlCount = \Model\Category::filterSql($query);
			$query['limit'] = $limit;
			$query['offset'] = $offset;

			$sql = \Model\Category::filterSql($query);
			$productPreLimit = \Model\Product::getList(['sql'=>$sqlCount]);
			$this->viewData->productsCount = count($productPreLimit);
			$products = \Model\Product::getList(['sql'=>$sql]);
			$colorList = [];
			$heel_height = [];
			foreach($productPreLimit as $product){
				$color = \Model\Product_Attributes::getColorList($product->id,0);
				if($color){
					$arrMap = array_map(function($item){return $item->id;},$color);
					$colorList = array_unique(array_merge($colorList,$arrMap));
				}
				$heel_height[$product->heel_height] = $product->heel_height.'MM';
			}
			if($colorList){
				$colorList = array_map(function($item){return \Model\Color::getItem($item);},$colorList);
			}
			if($category == null){
				$n = new Notification\ErrorHandler("Invalid category");
				$_SESSION['notification'] = serialize($n);
				redirect(SITE_URL.'collections/'.$params['category_slug']);
			}
			ksort($heel_height);
			$newArrivals = $category != 'all' && $category->new_arrivals ? array_map(function($item){return \Model\Product::getItem($item);},json_decode($category->new_arrivals,true)): null;
			$this->viewData->filters->colors = $colorList;
			$this->viewData->filters->heel_height = $heel_height;
			$this->viewData->parent = $params['category_slug'];
			$this->viewData->category = $category;
			$this->viewData->mCategory = $mCategory;
			$this->viewData->mainCategory = \Model\Category::getList(['where'=>"parent_category = 0"]);
			$this->viewData->products = $products;
			$this->viewData->new_arrivals = $newArrivals;
			$this->viewData->limit = $limit;
			$this->viewData->offset = $offset;
			$this->viewData->sortBy = $sortNum;
		}
		$this->loadView($this->viewData);
	}

	public function search(Array $params = []){
		$query = [];
		$limit = 8;
		$offset = 0;
		if(!isset($_GET['term']) || !$_GET['term']){
			redirect('/');
		}
		$keywords = $_GET['term'];
		$query['where'] = [];
		$sortBy = isset($_GET['sort_by'])?\Model\Product::$SORT_MAP[intval($_GET['sort_by'])]:null;
		$sortNum = isset($_GET['sort_by']) ? $_GET['sort_by']: null;
		if($sortBy){
			unset($_GET['sort_by']);
		}
		if (is_numeric($keywords)) {
			$query['where'][] = 'CAST (id as TEXT) like \'%'.$keywords.'%\'';
		}
		//Apply filters
		foreach($_GET as $key=>$value) {
			if ($key == 'term') {
				//Search term
				$explodeTags = explode(' ', strtolower(urldecode($keywords)));
				$tags = "'%" . implode("%','%", $explodeTags) . "%'";
				$query['where'][] = 'lower(name) like \'%' . strtolower(urldecode($keywords)) . '%\'';
				$query['where'][] = 'lower(tags) like any (array[' . $tags . '])';
				$query['where'] = ["(" . implode(' OR ', $query['where']) . ")"];
			} else if ($key == 'heel_height') {
				$val = explode(',', urldecode($value));
				$query['where'][] = "product.$key in (".implode(',',$val).")";
			} else {
				$val = explode(',', urldecode($value));
				$query['where'][] = "product.$key similar to '%\"(" . implode('|', $val) . ")\"%'";
			}
		}
		$query['where'][] = 'product.active=1';
		$query['where'] = [implode(' AND ', $query['where'])];
		$query['orderBy'] = $sortBy ? [$sortBy]:'';

		$productsNoLimit = \Model\Product::getList(['sql'=>\Model\Category::filterSql($query)]);
		$productsCount = count($productsNoLimit);
		$query['limit'] = $limit;
		$query['offset'] = $offset;

		$products = \Model\Product::getList(['sql'=>\Model\Category::filterSql($query)]);
		$colorList = [];
		$heel_height = [];
		foreach($productsNoLimit as $product){
			$color = \Model\Product_Attributes::getColorList($product->id,0);
			if($color){
				$arrMap = array_map(function($item){return $item->id;},$color);
				$colorList = array_unique(array_merge($colorList,$arrMap));
				$heel_height[$product->heel_height] = $product->heel_height.'MM';
			}
		}
		if($colorList){
			$colorList = array_map(function($item){return \Model\Color::getItem($item);},$colorList);
		}
		ksort($heel_height);
//		dd($productsCount,$products,\Model\Category::filterSql($query));
		$this->viewData->mCategory = '';
		$this->viewData->filters->heel_height = $heel_height;
		$this->viewData->products = $products;
		$this->viewData->filters->colors = $colorList;
		$this->viewData->productsCount = $productsCount;
		$this->viewData->category = $this->viewData->parent = 'all';
		$this->viewData->limit = $limit;
		$this->viewData->offset = $offset;
		$this->viewData->sortBy = $sortNum;
		parent::index($params);
	}
}
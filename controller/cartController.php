<?php

class cartController extends siteController {

	public function index(Array $params = []){
		$this->configs['Meta Title'] = "Checkout - Shopping Bag";
		if (count($this->viewData->cart->products) > 0){
			$arr = [];
			foreach($this->viewData->cart->products as $prod){
				$prod->details = json_decode($prod->variation,true);
				$arr[] = $prod;
			}
			$this->viewData->products = $arr;
		} else {
			$this->viewData->products = [];
		}

		parent::index($params);
	}

	function updateSession(){
		$parameters = ['adults','kids','tandem','duration','datetime','tours','equipment','packages','products','rentals'];
		if(isset($_POST['type']) && $_POST['type'] == 'rental' && \Model\Cart::getActiveRental($this->viewData->user ? $this->viewData->user->id: 0,session_id())){
			$sessionSelect = 'rental';
		} else {
			$sessionSelect = 'cart';
		}
		if(isset($_POST['tours']) && $_POST['tours']){
		    $_SESSION[$sessionSelect]->packages = 0;
        } else if(isset($_POST['packages']) && $_POST['packages']){
		    $_SESSION[$sessionSelect]->tours = 0;
        }
		foreach($parameters as $parameter){
			if(isset($_POST[$parameter])){
			    if($parameter == 'tours'){
			        $_POST['type'] = 'tour';
                } else if($parameter == 'packages'){
			        $_POST['type'] = 'package';
                }
				if($parameter == 'datetime'){
					$_SESSION[$sessionSelect]->$parameter = date('Y-m-d H:i:s', strtotime($_POST[$parameter]));
				}
				else if(in_array($parameter,['equipment','products'])){
					if(isset($_POST['qty'])){
						//Add item id and qty to session
						if($_POST['qty'] > 0){
							$_SESSION[$sessionSelect]->{$parameter}[$_POST[$parameter]] = ['qty'=>$_POST['qty']];
						}
						//Remove item id and all associative arrays
						else {
							unset($_SESSION[$sessionSelect]->{$parameter}[$_POST[$parameter]]);
						}
					}
				} else if(in_array($parameter,['adults','kids','tandem'])){
					$_SESSION[$sessionSelect]->$parameter = $_POST[$parameter];
//					$map = ['adults'=>'Adult','kids'=>'Kid','tandem'=>'Tandem'];
//					$equip = \Model\Equipment::getDefault($map[$parameter]);
//					if(!$_POST['skipDefault'] && $equip){
//						if($_POST[$parameter] == 0){
//							unset($_SESSION[$sessionSelect]->equipment[$equip->id]);
//						} else {
//							$_SESSION[$sessionSelect]->equipment[$equip->id] = ['qty'=>$_POST[$parameter]];
//						}
//					}
				} else {
					$_SESSION[$sessionSelect]->$parameter = $_POST[$parameter];
				}
			}
		}
		$total = \Model\Cart::getSessionTotal(isset($_POST['type']) && $_POST['type'] ? $_POST['type']: '');
		$appendHtmlArr = ['Package'=>\Model\Package::getList(['where'=>"status = 1"])];
		$defaultPrice = 8;
		$kidPrice = 4;
		if(isset($_POST['type'])){
			switch($_POST['type']){
				case 'tour':
					$defaultPrice = \Model\Tour::getItem($_SESSION['cart']->tours)->getPrice();
					break;
				case 'package':
					$defaultPrice = \Model\Package::getItem($_SESSION['cart']->packages)->getPrice();
					$kidPrice = \Model\Package::getItem($_SESSION['cart']->packages)->getPrice('kid');
					break;
			}
		}
		$equipPrice = [];
		if($_SESSION[$sessionSelect]->duration) {
			foreach (\Model\Equipment::getList() as $item) {
				$rp = json_decode($item->rental_price, true);
				$equipPrice[$item->id] = $rp && array_key_exists($_SESSION[$sessionSelect]->duration, $rp) ? $rp[$_SESSION[$sessionSelect]->duration] : 0;
			}
		}
		$this->toJson([
				'status'=>'success',
				'newTotal'=>$total,
				'appendHtml'=>$appendHtmlArr,
				'tours'=>$_SESSION[$sessionSelect]->tours,
				'packages'=>$_SESSION[$sessionSelect]->packages,
				'defaultPrice'=>$defaultPrice,
				'kidPrice'=>$kidPrice,
				'adults'=>$_SESSION[$sessionSelect]->adults,
				'kids'=>$_SESSION[$sessionSelect]->kids,
//				'tandem'=>$_SESSION[$sessionSelect]->tandem,
				'equipPrice'=>$equipPrice
		]);
	}

	public function remove_once($params){
		foreach ($this->viewData->cart->products as $key=>$product){
			if ($product == $params['id']){
				unset($this->viewData->cart->products[$key]);
				break;
			}
		}
		$this->updateCookies();
		redirect(SITE_URL.'cart');
	}

	public function remove_product($params){
//		unset($this->viewData->cart->products[$params['id']]);
//		unset($this->viewData->cart->productDetails[$params['id']]);
//		$this->updateCookies();
		$id = isset($params['id']) && $params['id'] > 0 ? $params['id']: null;
		if($id){
			$cart = \Model\Cart::getItem($id);
			$cart->delete($id);
		}
		redirect(SITE_URL.'cart');
	}

	public function remove_all(){
		$this->viewData->cart->products = [];
		$this->viewData->cart->productDetails = [];
		$this->updateCookies();
		redirect(SITE_URL.'cart');
	}

	private function updateCookies(){
		setcookie('cartProducts', json_encode($this->viewData->cart->products), time()+60*60*24*100, "/");
		if(isset($this->viewData->cart->productDetails)){
			setcookie('cartProductDetails',json_encode($this->viewData->cart->productDetails), time()+60*60*24*100, "/");
		}

	}

	public function deleteItem(){
		$cart_id = $_POST['cart_id'];
		if($cart_id){
			$cart=\Model\Cart::getItem($cart_id);
			$cart->delete($cart_id);
			$this->toJson(['status'=>"success"]);
		}else{
			$this->toJson(['status'=>"failed",'message'=>"Failed to delete"]);
		}
	}
}
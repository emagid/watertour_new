<?php

class reservationController extends siteController{


    /**
     * $_POST = [reservationQuantity, reservationKidQuantity, reservationDateTime, reservationTime]
     * Reserve # of people/kids, datetime, and duration (hour)
     */
    function resRental(){
        if(isset($_POST['reservationPackage'])){
            $order = [];
            for($i = 0; $i < count($_POST['reservationDateTime']); $i++){
                if(isset($_POST['reservationPackageType'])){
                    $order[$_POST['reservationPackageType'][$i]][] = [
                        'id'=>$_POST['reservationPackageId'][$i],
//                        'datetime'=>$_POST['reservationDateTime'][$i] ? date('Y-m-d H:i:s',strtotime($_POST['reservationDateTime'][$i])) : \Model\Tour::getItem($_POST['reservationPackageId'][$i])->getNextRange(),
                        'datetime'=>\Carbon\Carbon::now()->addMonth(6)->toDateTimeString()
                    ];
                }
            }
            $cart = new \Model\Cart();
            $cart->user_id = $this->viewData->user ? $this->viewData->user->id : 0;
            $cart->guest_id = session_id();

            if(isset($_POST['reservationTime']) && isset($_POST['reservationTimeType'])){
                switch ($_POST['reservationTimeType']){
                    case 'tour':case 'tours':
                        $_POST['reservationTour'] = $_POST['reservationTime'];
                        break;
                    case 'package':case 'packages':
                        $_POST['reservationPackage'] = $_POST['reservationTime'];
                        break;
                }
            }

            $cart->package_id = isset($_POST['reservationPackage']) ? $_POST['reservationPackage'] : null;
            $cart->tour_id = isset($_POST['reservationTour']) ? $_POST['reservationTour'] : null;
            $cart->products = json_encode($_SESSION['cart']->products);
            $cart->equipment = json_encode($_SESSION['cart']->equipment);
            $cart->adult_count = $_POST['reservationQuantity'];
            $cart->kid_count = $_POST['reservationKidQuantity'];
            $cart->tandem_count = isset($_POST['reservationTandemQuantity']) ? $_POST['reservationTandemQuantity'] : 0;
            $cart->reserve_date = date('Y-m-d H:i:s', is_numeric($_POST['reservationDateTime'][0]) ? $_POST['reservationDateTime'][0] : strtotime($_POST['reservationDateTime'][0]));
//            $cart->duration = isset($_POST['reservationTime']) ? $_POST['reservationTime'] : 0;
            $cart->ref_num = generateToken();
            $cart->package_details = json_encode($order);
            $cart->save();
        } else {
            if (isset($_POST['reservationTime']) && $_POST['reservationTime'] && ($cart = \Model\Cart::getActiveRental($this->viewData->user ? $this->viewData->user->id : 0, session_id()))) {
                $sessionSelect = 'rental';
            } else {
                $cart = new \Model\Cart();
                $sessionSelect = 'cart';
                $cart = new \Model\Cart();
            }
            $cart->user_id = $this->viewData->user ? $this->viewData->user->id : 0;
            $cart->guest_id = session_id();

            if(isset($_POST['reservationTime']) && isset($_POST['reservationTimeType'])){
                switch ($_POST['reservationTimeType']){
                    case 'tour':case 'tours':
                        $_POST['reservationTour'] = $_POST['reservationTime'];
                        break;
                    case 'package':case 'packages':
                        $_POST['reservationPackage'] = $_POST['reservationTime'];
                        $package_details = [];
                        foreach (json_decode(\Model\Package::getItem($_POST['reservationPackage'])->tour_id,true) as $item){
                            $tour = \Model\Tour::getItem($item);
                            $package_details['Tour'][] = ['id'=>$tour->id,'datetime'=>\Carbon\Carbon::now()->addMonth(6)->toDateTimeString()];
//                            $package_details['Tour'][] = ['id'=>$tour->id,'datetime'=>$tour->getNextRange()];
                        }
                        $cart->package_details = json_encode($package_details);
                        break;
                }
            }

            $cart->package_id = isset($_POST['reservationPackage']) ? $_POST['reservationPackage'] : null;
            $cart->tour_id = isset($_POST['reservationTour']) ? $_POST['reservationTour'] : null;
            $cart->products = json_encode($_SESSION[$sessionSelect]->products);
            $cart->equipment = json_encode($_SESSION[$sessionSelect]->equipment);
            $cart->adult_count = $_POST['reservationQuantity'];
            $cart->kid_count = $_POST['reservationKidQuantity'];
            $cart->tandem_count = isset($_POST['reservationTandemQuantity']) ? $_POST['reservationTandemQuantity'] : 0;
            $cart->reserve_date = date('Y-m-d H:i:s', is_numeric($_POST['reservationDateTime']) ? $_POST['reservationDateTime'] : strtotime($_POST['reservationDateTime']));
//            $cart->duration = isset($_POST['reservationTime']) ? $_POST['reservationTime'] : 0;
            $cart->ref_num = generateToken();
            $cart->save();
        }
        if(isset($_SESSION['cart'])){
            unset($_SESSION['cart']);
        }
        redirect('/reservation/confirm');
    }

    function confirm(Array $params = []){
        $cart = \Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: 0, session_id());
        if(!$cart){
            $n = new \Notification\ErrorHandler('Reserve a tour or package to proceed to checkout');
            $_SESSION['notification'] = serialize($n);
            redirect('/');
        }
        $this->viewData->tours = \Model\Tour::getFutureTours();
        $this->viewData->cart = $cart;
        $total = 0;
        foreach($cart as $item){
            $total += $item->getTotal();
        }
        $this->viewData->total = $total;

        $this->viewData->featured = \Model\Package::getFeatured();
        parent::index($params);
    }

    function updateCart(){
        $cart = \Model\Cart::validateRefNum($_POST['ref_num']);
        //translate session variables to Cart obj fields
        if(isset($_POST['adults'])){
            $_POST['adult_count'] = $_POST['adults'];
        }
        if(isset($_POST['kids'])){
            $_POST['kid_count'] = $_POST['kids'];
        }
        if(isset($_POST['tandem'])){
            $_POST['tandem_count'] = $_POST['tandem'];
        }
        if(isset($_POST['category']) && isset($_POST['cat_id'])){
            //item is a package, update values for package_details
            $details = json_decode($cart->package_details,true);
            $items = $details[$_POST['category']];
            for($i = 0; $i < count($items); $i++){
                if($items[$i]['id'] == $_POST['cat_id']){
                    $items[$i]['datetime'] = date('Y-m-d H:i:s', strtotime($_POST['reserve_date']));
                }
            }
            $details[$_POST['category']] = $items;
            $cart->package_details = json_encode($details);
            $cart->save();
        }
        $parameters = \Model\Cart::getSelectFields();
        //update columns in db
        if(isset($_POST['upgrade']) && $_POST['upgrade'] == 'false' && isset($_POST['add_ons']) && $_POST['add_ons'] == 'false') {
            foreach ($parameters as $parameter) {
                if (isset($_POST[$parameter])) {
                    switch ($parameter) {
                        case 'package_id':
                        case 'tour_id':
                            $cart->$parameter = json_encode($_POST[$parameter]);
                            break;
                        case 'equipment':
                        case 'products':
                            $var = json_decode($cart->{$parameter}, true);
                            if ($_POST['qty'] == 0) {
                                unset($var[$_POST[$parameter]]);
                            } else {
                                $var[$_POST[$parameter]] = ['qty' => $_POST['qty']];
                            }
                            $cart->{$parameter} = json_encode($var);
                            break;
                        case 'reserve_date':
                            $cart->$parameter = date('Y-m-d H:i:s', strtotime($_POST[$parameter]));
                            break;
                        default:
                            $cart->$parameter = $_POST[$parameter];
                            break;
                    }
                    $cart->save();
                }
            }
        }
        //update upgrade_options
        else if(isset($_POST['upgrade']) && $_POST['upgrade'] == 'true'){
            $upgradeParams = ['equipment','rental','tour'];
            $upgradeOptions = json_decode($cart->upgrade_option,true);
            foreach($upgradeParams as $param){
                if(isset($_POST[$param])) {
                    if($_POST['qty']) {
                        $upgradeOptions[$param][$_POST[$param]] = ['qty' => $_POST['qty']];
                    } else {
                        unset($upgradeOptions[$param][$_POST[$param]]);
                        if($upgradeOptions[$param] == []){
                            unset($upgradeOptions[$param]);
                        }
                    }
                }
            }
            $cart->upgrade_option = $upgradeOptions ? json_encode($upgradeOptions) : '';
            $cart->save();
        }
        else if(isset($_POST['add_ons']) && $_POST['add_ons'] == 'true'){
            $addOnParams = ['Rental','Equipment','Tour','Package','Product','Attraction'];
            $addOns = json_decode($cart->add_ons,true);
            foreach($addOnParams as $addOn){
                if(isset($_POST[$addOn])){
                    if($_POST['qty']){
                        $addOns[$addOn][$_POST[$addOn]] = ['qty'=>$_POST['qty']];
                    } else {
                        unset($addOns[$addOn][$_POST[$addOn]]);
                        if($addOns[$addOn] == []){
                            unset($addOns[$addOn]);
                        }
                    }
                }
            }
            $cart->add_ons = $addOns ? json_encode($addOns) : '';
            $cart->save();
        } else {
            foreach ($parameters as $parameter) {
                if (isset($_POST[$parameter])) {
                    switch ($parameter) {
                        case 'package_id':
                        case 'tour_id':
                            $cart->$parameter = json_encode($_POST[$parameter]);
                            break;
                        case 'equipment':
                        case 'products':
                            $var = json_decode($cart->{$parameter}, true);
                            if ($_POST['qty'] == 0) {
                                unset($var[$_POST[$parameter]]);
                            } else {
                                $var[$_POST[$parameter]] = ['qty' => $_POST['qty']];
                            }
                            $cart->{$parameter} = json_encode($var);
                            break;
                        case 'reserve_date':
                            $cart->$parameter = date('Y-m-d H:i:s', strtotime($_POST[$parameter]));
                            break;
                        default:
                            $cart->$parameter = $_POST[$parameter];
                            break;
                    }
                    $cart->save();
                }
            }
        }
        if(($activeRental = \Model\Cart::getActiveRental($this->viewData->user ? $this->viewData->user->id: 0,session_id())) && $activeRental->id == $cart->id){
            $this->resetRentalSession($activeRental);
        }
        $lineItemTotal = $cart->getTotal();

//        $equipment = [];
//        foreach(json_decode($cart->equipment,true) as $key=>$qty){
//            $r = $cart->duration ? : 0;
//            $equip = \Model\Equipment::getItem($key);
//            $equipment[] = ['id'=>$equip->id,'name'=>$equip->name,'featuredImage'=>$equip->featuredImage(),'walk_in'=>intval($equip->getPrice('',$r)),'online_price'=>intval($equip->getPrice('',$r)),'ref'=>$cart->ref_num,'category'=>$equip->category,'qty'=>$qty['qty']];
//        }
        $allCarts = \Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: null,session_id());
        $total = 0;
        foreach($allCarts as $carts){
            $total += $carts->getTotal();
        }
        echo json_encode(['status'=>'success','newTotal'=>$total,'lineItemTotal'=>$lineItemTotal/*, 'equipment'=>$equipment*/]);
    }

    function updateCartUpgradeOption(){
        $cart = \Model\Cart::getItem($_POST['cart']);
        $decode = json_decode($cart->upgrade_options,true);
        if(isset($decode[$_POST['type']]) && $decode[$_POST['type']]){
            if($_POST['function'] == 'add' && in_array($_POST['id'],$decode[$_POST['type']])){
                //do nothing
            } else if($_POST['function'] == 'add' && !in_array($_POST['id'],$decode[$_POST['type']])){
                $decode[$_POST['type']][] = $_POST['id'];
            } else if($_POST['function'] == 'delete' && in_array($_POST['id'],$decode[$_POST['type']])){
                $key = array_search($_POST['id'],$decode[$_POST['type']]);
                unset($decode[$_POST['type']][$key]);
            } else {
                //do nothing
            }
        }
        $cart->upgrade_option = json_encode($decode);
        $cart->save();
    }

    function addSession(){
        foreach($_POST as $key=>$value){
            $_SESSION[$key] = $value;
        }
        echo json_encode(['status'=>'success']);
    }

    function delete(Array $params = []){
        if($params['id']){
            \Model\Cart::delete($params['id']);
            if(isset($_SESSION['rental']) && $_SESSION['rental']->id == $params['id']){
                unset($_SESSION['rental']);
            }
        }
        redirect('/reservation/confirm');
    }
}
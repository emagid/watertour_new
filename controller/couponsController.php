<?php

class couponsController extends siteController{
    public function add(){
        $code = isset($_POST['code']) ? $_POST['code']: null;
        $email = isset($_POST['email']) ? $_POST['email']: null;
        $coupon = \Model\Coupon::checkCoupon($code);
        /*Validate later*/
        $newsletterCoupon = \Model\Newsletter::getItem(null,['where'=>"lower(coupon_code) = lower('$code')"]);

        if(!$code){
            $this->toJson(['status'=>'failed','msg'=>'Enter a coupon code to proceed.']);
            return;
        }
        /*Cart must exist*/
        if(!isset($_SESSION['cart'])){
            $this->toJson(['status'=>'failed','msg'=>'There was an error processing this coupon. Please try again later']);
            return;
        }
        if($coupon){
            $_SESSION['cart']->coupon = ['id'=>$coupon->id,'code'=>$coupon->coupon_code,'amt'=>$coupon->coupon_amount,'type'=>\Model\Coupon::$coupon_type[$coupon->coupon_type]];
            $cart = \Model\Cart::getActiveCart(null,session_id());
            $subtotal = array_sum(array_map(function($item){return $item->getTotal();},$cart));
            $discount = $coupon->coupon_type == 1 ? ($coupon->coupon_amount / 100) * $subtotal: $coupon->coupon_amount;
            $subWithoutPackage = array_sum(array_map(function($item){if(!$item->package_id){return $item->getTotal();}},$cart));
            $tax = ($subWithoutPackage-$discount) * .08875;
            $total = $subtotal - $discount + $tax;

            $this->toJson(['status'=>'success','subtotal'=>intval($subtotal),'discount'=>number_format($discount,2),'tax'=>number_format($tax,2),'total'=>number_format($total,2)]);
            return;
        } else {
            $this->toJson(['status'=>'failed','msg'=>'Invalid Coupon. Please enter a valid code.']);
            return;
        }
    }
}
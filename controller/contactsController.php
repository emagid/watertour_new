<?php

/**
 * Created by PhpStorm.
 * User: Gee
 * Date: 6/22/17
 * Time: 11:57 AM
 */
class contactsController
{
    function add(){
        $fields = \Model\Contact::getSelectFields();
        $contacts= new \Model\Contact();
        foreach($fields as $field){
            if(isset($_POST[$field]) && $_POST[$field]){
                $contacts->$field = $_POST[$field];
            }
        }
        if($contacts->save()) {
            $n = new \Notification\MessageHandler('Thank you for your submission!');
        } else {
            $n = new \Notification\MessageHandler('Error: Could not save your submission!.');
        }
        $_SESSION['notification'] = serialize($n);
        redirect($_POST['redirect']);
    }
}
<?php

class loginController extends siteController {

	public function index_post(Array $params = []){
		\Model\User::login($_POST['email'],$_POST['password']);

		redirect($_POST['redirect-url']);
	}

	public function register_post(Array $params = [])
	{
		$user = \Model\User::loadFromPost();
		$hash = \Emagid\Core\Membership::hash($user->password);
		$user->password = $hash['password'];
		$user->hash = $hash['salt'];
        $emails = \Model\Newsletter::getItem(null,['where'=>"active = 1 and email = '".$user->email."'"]);

		if ($user->save()) {

			$customerRole = new \Model\User_Roles();
			$customerRole->role_id = 2;
			$customerRole->user_id = $user->id;
			$customerRole->save();
            if(!$emails){
                $newsletter = \Model\Newsletter::loadFromPost();
                $newsletter->save();
            }
			$n = new \Notification\MessageHandler('Welcome to Bike Tour! Your account was successfully created.');
			$_SESSION["notification"] = serialize($n);

//			$email = new \Emagid\Email();
//			$email->addTo($user->email);
//			$email->subject('Welcome, ' . $user->full_name() . '!');
//			$email->body = '<p><a href="www.watertour.com"><img src="https://watertour.com/content/frontend/img/logo.png" /></a></p>'
//					. '<p><b>Dear ' . $user->full_name() . '</b></p>'
//					. '<p>Welcome to <a href="www.watertour.com">watertour.com</a>. To log in when visiting our site just click My Account at the top of every page, and then enter your e-mail address and password.</p>'
//					. '<p>When you log in to your account, you will be able to do the following:</p>'
//					. '<ul>'
//					. '<li>Proceed through checkout faster when making a purchase</li>'
//					. '<li>Check the status of orders</li>'
//					. '<li>View past orders</li>'
//					. '<li>Make changes to your account information</li>'
//					. '<li>Change your password</li>'
//					. '<li>Store alternative addresses (for shipping to multiple family members and friends!)</li>'
//					. '</ul>'
//					. '<p>If you have any questions about your account or any other matter, please feel free to contact us at support@watertour.com or by phone at 877.752.6919.</p>'
//					. '<p>Thanks again!</p>'
//					. '<p>Find us on <a href="//TODO st-dev Bike Tour facebook URL">Facebook</a> and <a href="//TODO st-dev Bike Tour twitter url">Twitter</a>.</p>';
//			$email->send();

			$this->index_post($params);
		} else {
			$n = new \Notification\ErrorHandler($user->errors);
			$_SESSION["notification"] = serialize($n);
		};

		redirect($_POST['redirect-url']);
	}

	public function logout(){
		\Emagid\Core\Membership::destroyAuthenticationSession();
		redirect(SITE_URL);
	}

}
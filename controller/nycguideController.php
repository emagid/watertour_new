<?php

class nycguideController extends siteController{
    public function index(){
        $this->configs['Meta Title'] = "Cruise Routes | Watertours Sightseeing";
        $this->configs['Meta Keywords'] = "Routes, directions, options Bus, Double Decker, Tour, cruise, Hop-on hop-off, Manhattan, New York City, NYC, ride, sightseeing";
        $this->configs['Meta Description'] = "Take a Cruise and see the City from the water! Tickets start at just $29. ";

        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $this->viewData->routes_data = \Model\Main_Route::getList(["where"=>"route_data IS NOT NULL AND type = 2",'orderBy' => 'name asc']);
        $attractions = \Model\Attraction::getList(['orderBy'=>'name']);
        $googleAttractions = [];
        foreach($attractions as $attraction){

            $googleAttractions[] = ["coordinates"=> array("lat"=> $attraction->lat, "lng"=>$attraction->lng),
                'name'=>$attraction->name,'description'=>$attraction->description];
        }
        $this->viewData->attractions = $googleAttractions;
        $this->loadView($this->viewData);
    }

    public function getRoutesData(){
        $mainRoutes = \Model\Main_Route::getList(["where"=>"route_data IS NOT NULL AND type = 2",'orderBy' => 'name asc']);
        $routesData = []; // routeName => ['routes' => ['coordinates' => [], 'name' => ''], 'icons' => [['name' => '', 'image' => '']]]
        foreach($mainRoutes as $mr){
            $connections = \Model\Route_Path::getList(['where' => ['main_route_id' => $mr->id], 'orderBy' => 'display_order asc']);
            //$routesData[$mr->id]['color'] = $this->routeColors($mr->id);
            $routesData[$mr->id]['color'] = $mr->color;
            $googlePath = [];
            foreach($connections as $key => $connect){
                $route = \Model\Route::getItem($connect->route_id);
                $points = json_decode($route->points, true);
                if(count($points) == 1){
                    $pointArray = array_map('trim', explode(',', $points[0]));
                    $googlePath[] = array("lat"=> $pointArray[0], "lng"=> $pointArray[1]);
                    if(json_decode($connect->route_icon_ids,true) || $key === 0 || $route->name == 'Slip 5'){
                        $routesData[$mr->id]['attractions'][] = ['coordinates' => array("lat"=> $pointArray[0], "lng"=> $pointArray[1]),
                            'name' => $route->name, 'street' => $route->display_order];
                    }
                }
                $iconIds = json_decode($connect->route_icon_ids, true);
                foreach($iconIds as $iconId){
                    $iconModel = \Model\Route_Icon::getItem($iconId);
                    $routesData[$mr->id]['icons'][] = ['name' => $iconModel->name, 'image' => $iconModel->icon];
                }
            }
            $routesData[$mr->id]['googlePath']=$googlePath;
        }

        $this->toJson(['data' => $routesData]);
    }

    public function getAttractionsData(){
        $attractions = \Model\Attraction::getList(['orderBy'=>'name']);
        $googleAttractions = [];
        foreach($attractions as $attraction){

            $googleAttractions[] = ["coordinates"=> array("lat"=> $attraction->lat, "lng"=>$attraction->lng),
                'name'=>$attraction->name,'description'=>$attraction->description];
        }
        $this->toJson(['data' => $googleAttractions]);
    }

    public function routeColors($id){
        $colorArray = [
            'yellow',
            'orange',
            'green',
            'purple',
            'red',
            'blue',
            '#cc0066',
            '#cc6666'
        ];
        return $colorArray[$id];
    }
}
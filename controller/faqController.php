<?php

class faqController extends siteController {
    
    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "FAQ &#x2014; Bike Rental Central Park";
//        $faq = \Model\Faq::getList(['orderBy'=>'display_order']);
        $fc = \Model\Faq_Category::getList(['orderBy'=>'display_order']);
        $ar = [];
        foreach($fc as $item){
            $ar[$item->name] = array_chunk((array)$item->getFaq(),3);
        }
        $this->viewData->faqs = $ar;
//        $this->viewData->faqs = array_chunk((array)$faq,3);
        $this->loadView($this->viewData);
    }
            
}
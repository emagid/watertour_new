<?php

class aboutController extends siteController {
    
    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "About Us &#x2014; Bike Rental Central Park";
        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $about = \Model\About::getList(['orderBy'=>'display_order']);
        $this->viewData->about = $about;


        $this->loadView($this->viewData);
    }

    public function sights(Array $params = [])
    {
        $this->configs['Meta Title'] = "Unique Sights in NYC &#x2014; Bike Rental Central Park";
        $this->loadView($this->viewData);
    }

    public function location(Array $params = [])
    {
        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $this->configs['Meta Title'] = "Pick-Up &#x26; Drop-Off Locations &#x2014; Bike Rental Central Park";
        $this->loadView($this->viewData);
    }
            
}
<?php

class packagesController extends siteController {
    
    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Attraction Packages &#x2014; Bike Rental Central Park";
        $params = [];
        $order = [];
        if(isset($_GET['sort'])){
            switch($_GET['sort']){
                case 'lth':
                    $params['orderBy'] = 'adults_price asc';
                    break;
                case 'htl':
                    $params['orderBy'] = 'adults_price desc';
                    break;
            }
        } else {
            $params['orderBy'] = 'display_order asc';
        }
        if(isset($_GET['day'])){
            //$params['where'][] = 'days = '.$_GET['day'];
        }
        if(isset($_GET['f']) && $_GET['f']){
            $attraction = \Model\Attraction::getItem(\Model\Attraction::getIdBySlug($_GET['f']));
            if($attraction){
                $params['where'][] = $_GET['f'];
            }
        }
        if(isset($params['where'])){
            //$params['where'] = implode(' and ',$params['where']);
        }


        $params['where']['status'] = 1;
        //$params['orderBy'] = 'display_order';

        //$this->viewData->packages = \Model\Package::getList(['orderBy'=>'display_order']);
        $this->viewData->packages = \Model\Package::getList($params);

        // $this->viewData->packages = \Model\Package::getByDate(time(),$params);
        $this->loadView($this->viewData);
    }

    public function package(Array $params = []){
        $package = \Model\Package::getItem(\Model\Package::getIdBySlug($params['id']));
        $packages = \Model\Package::getByDate(time());
        $total = 0;
        if(isset($_SESSION['cart'])){
            $total += $_SESSION['cart']->adults * $package->adults_price;
            $total += $_SESSION['cart']->kids * $package->kids_price;
        }

        $this->viewData->total = $total;
        $packIds = array_map(function($item){return $item->id;},(array)$packages);
        if(($index = array_search($package->id,$packIds)) !== false){
            $this->viewData->prev = \Model\Package::getItem($index == 0 ? $packIds[count($packages)-1] : $packIds[$index-1]);
            $this->viewData->next = \Model\Package::getItem($index == count($packages)-1 ? $packIds[0] : $packIds[$index+1]);
        }
        if(!$package){
            $n = new \Notification\ErrorHandler("Unknown Package. Please select from our list below");
            $_SESSION['notification'] = serialize($n);
            redirect('/packages');
        }
        $relatedAll = \Model\Package::getList(['where'=>"id != $package->id"]);
        shuffle($relatedAll);
        $related = $package->related ? array_map(function($val){return \Model\Package::getItem($val);},json_decode($package->related,true)): array_slice($relatedAll,0,2);
        $this->viewData->reviews = \Model\Review::getReviewsByType('Package',$package->id);
        $this->viewData->related = $related;
        $this->viewData->package = $package;
        $this->viewData->items = $package->getPackageContents();
        $this->viewData->featuredPackages = \Model\Package::getList(['where'=>'featured = 1']);
        $this->viewData->package_images = \Model\Package_Image::getList(['where'=>"package_id = $package->id"]);
        $this->loadView($this->viewData);
    }

    public function reservepackage(Array $params = [])
    {
        $this->configs['Meta Title'] = "Reserve your Package &#x2014; Bike Rental Central Park";
        $this->viewData->package = \Model\Package::getItem(\Model\Package::getIdBySlug($params['id']));
        $total = 0;
        if(isset($_SESSION['cart'])){
            $total += $_SESSION['cart']->adults * $this->viewData->package->adults_price;
            $total += $_SESSION['cart']->kids * $this->viewData->package->kids_price;
        }
        $this->viewData->range = $this->viewData->package->getAvailableRange();
        $this->viewData->total = $total;
        $this->viewData->days = $this->viewData->weeks = $this->viewData->hours = [];
        $this->viewData->items = $this->viewData->package->getPackageContents();
        $this->loadView($this->viewData);
    }

    function selectPackageDate(){
        $params = [];
        if(isset($_GET['sort'])){
            switch($_GET['sort']){
                case 'lth':
                    $params['orderBy'] = 'adults_price asc';
                    break;
                case 'htl':
                    $params['orderBy'] = 'adults_price desc';
                    break;
            }
        }
        if(isset($_GET['f']) && $_GET['f']){
            $attraction = \Model\Attraction::getItem(\Model\Attraction::getIdBySlug($_GET['f']));
            if($attraction){
                $params['where'] = $_GET['f'];
            }
        }
        $packages = \Model\Package::getByDate($_POST['date'],$params);
        $arr = [];
        if($packages){
            $packArray = [];
            $time = is_numeric($_POST['date'])? $_POST['date']:strtotime($_POST['date']);
            foreach($packages as $package){
                $packArray[] = ['name'=>$package->name,'duration'=>$package->getDurationFormatted(),'date'=>date('l F d, Y',is_numeric($_POST['date'])? $_POST['date']:strtotime($_POST['date'])),'time'=>$time,'image'=>$package->banner,'price'=>intval($package->getPrice()),'walkin'=>intval($package->getWalkin()),'slug'=>$package->slug];
            }
            $arr['status'] = 'success';
            $arr['packages'] = $packArray;
        } else {
            $arr['status'] = 'failed';
        }
        $this->toJson($arr);
    }

    function updatePrice(){
        $slug = $_POST['slug'];
        $package = \Model\Package::getItem(\Model\Package::getIdBySlug($slug));

    }
}
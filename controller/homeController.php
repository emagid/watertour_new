<?php

class homeController extends siteController {
	
	public function index(Array $params = []){

		$this->viewData->banners = \Model\Banner::getList(['where'=>"banner_type = 1"]);
		$this->viewData->categoryBanners = \Model\Banner::getList(['where'=>"banner_type = 2"]);
		$this->viewData->selectionBanners = \Model\Banner::getList(['where'=>"banner_type = 3"]);
		$this->viewData->descriptionText = \Model\Banner::getItem(null,['where'=>"banner_type = 4"]);
		$this->viewData->specialityBanners = \Model\Banner::getList(['where'=>"banner_type = 5"]);

		$this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
		$this->viewData->equipment = \Model\Equipment::getList(['limit'=>3]);
		$this->viewData->featuredTours = \Model\Tour::getList(['where'=>'featured = 1']);
		$this->viewData->featuredPackages = \Model\Package::getList(['where'=>'featured = 1','orderby'=>'display_order ASC']);

		$featuredPackage = \Model\Package::getList(['where'=>"featured = 1"]);
        shuffle($featuredPackage);
		$this->viewData->randomPackage = $featuredPackage[0];
		$this->loadView($this->viewData);
	}

	public function query()
	{
		$query = $_POST['q'];

		$products = \Model\Product::search($query, 20);

		$res = [];
		foreach($products as $product){
//			$category = $product->getMainCategory();
			$res[] = ['label' => $product->name, 'value' => "/products/{$product->slug}", 'image' => $product->featured_image];
		}

		$this->toJson($res);
	}

}
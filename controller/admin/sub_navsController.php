<?php

class sub_navsController extends adminController{
    public function __construct()
    {
        parent::__construct('Sub_Nav');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['orderBy'] = 'display_order';
        parent::index($params); // TODO: Change the autogenerated stub
    }

    public function update(Array $arr = [])
    {
        $ar = [];
        foreach(\Model\Nav::getList() as $item){
            $ar[$item->id] = $item->name;
        }
        $this->_viewData->navs = $ar;
        $this->_viewData->urls = \Model\Nav::getPresetUrls();
        parent::update($arr); // TODO: Change the autogenerated stub
    }

}
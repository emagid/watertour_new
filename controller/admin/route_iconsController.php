<?php

class route_iconsController extends adminController{

	function __construct(){
        parent::__construct('Route_Icon');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
}
<?php

class quotesController extends adminController{

	function __construct(){
        parent::__construct('Quote');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
    public function update_post(Array $params = []){
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0){
            $quote = \Model\Quote::getItem($_POST['id']);


            if ($quote->save()){
                $n = new \Notification\MessageHandler('Quote saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($quote->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL.$this->_content.'/update/'.$quote->id);
            }
        }
        redirect(ADMIN_URL.'quotes');
    }
}
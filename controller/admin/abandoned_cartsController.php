<?php

class abandoned_cartsController extends adminController{
    function __construct()
    {
        parent::__construct('Abandoned_Cart');
    }

    function sendEmail(Array $params = []){
        if(isset($params['id']) && $params['id']){
            $ac = \Model\Abandoned_Cart::getItem($params['id']);
            if($ac->getEmail()) {
                $email = new \Email\MailMaster();
                $email->setTo(['email' => $ac->getEmail(), 'name'=>ucwords($ac->getFullName()), 'type'=>'to'])->setTemplate('modern-vice-abandoned-cart');
                $email->send();
                $ac->send_email = 1;
                $ac->save();

                $n = new \Notification\MessageHandler("Email sent");
            } else {
                $n = new \Notification\ErrorHandler("This person's email is not in the system");
            }
        } else {
            $n = new \Notification\ErrorHandler("Invalid ID");
        }
        $buildQueryString = isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING']: '';
        $_SESSION['notification'] = serialize($n);
        redirect(ADMIN_URL.'abandoned_cart'.$buildQueryString);
    }
}
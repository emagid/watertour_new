<?php

class newslettersController extends adminController{

	function __construct(){
        parent::__construct('Newsletter');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
}
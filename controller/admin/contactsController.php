<?php

/**
 * Created by PhpStorm.
 * User: Gee
 * Date: 6/22/17
 * Time: 11:59 AM
 */
class contactsController extends adminController
{

    function __construct(){
        parent::__construct('Contact');
    }

    function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    public function update_post(Array $params = []){
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0){
            $contact = \Model\Contact::getItem($_POST['id']);



//			$question_answer  = new \Model\Question_Answer();
//			$question_answer->answer = $_POST['answer'];
//			$question_answer->author_id = \Emagid\Core\Membership::userId();
//			$question_answer->question_id = $_POST['id'];
//			$question_answer->save();
//			if ($question->status =="Approved")
//			{
//				TODO st-dev uncomment when preparing to send email
//				$email = new \Emagid\Email();
//				$product = \Model\Product::getItem($question->product_id);
//
//				$email->addBcc($question->reply_email);
//				$email->addBcc('info@watertour.com'); //TODO st-dev get valid email
//				$email->subject('You have the one new answer!');
//				$email->body .= '<p><a href="www.watertour.com"><img src="https://watertour.com/content/frontend/img/logo.png" /></a></p><br />'
//							   .'<p>You  have the answer about your question:</p>';
//
//				$email->body .= '<h2>';
//				$email->body .=	'<a href="http://watertour.com/product/'.$product->slug.'">'.$product->name.'</a>';
//				$email->body .= '</h2>';
//				$email->body .= '<p>Subject:'.$question->subject.'</p>';
//				$email->body .= '<p>'.$question->text_question.'</p>';
//				$email->body .= '<p>------------------------------------</p>';
//				$email->body .= '<p>Answer:</p>';
//				$email->body .= '<p>'.$question->answer.'</p>';
//
//				$email->send();
//			}


            if ($contact->save()){
                $n = new \Notification\MessageHandler('Submission saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($contact->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL.$this->_content.'/update/'.$contact->id);
            }
        }
        redirect(ADMIN_URL.'contacts');
    }

}
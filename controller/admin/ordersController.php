<?php

use Emagid\Html\Form;
use Emagid\Core\Membership;

//require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class ordersController extends adminController
{

    function __construct()
    {
        parent::__construct("Order");
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    public function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->order->viewed) || !$this->_viewData->order->viewed) {
            $this->_viewData->order->viewed = true;
            $this->_viewData->order->save();
        }

        $this->_viewData->order_status = \Model\Order::$status;

        $this->_viewData->shipping_methods = [];

        $this->_viewData->order_products = \Model\Order_Product::getList(['where' => "order_id=" . $this->_viewData->order->id]);

        if (is_null($this->_viewData->order->user_id)||!($this->_viewData->order->user_id)) {
            $this->_viewData->order->user = null;
        } else {
            $this->_viewData->order->user = \Model\User::getItem($this->_viewData->order->user_id);
        }

        if (is_null($this->_viewData->order->coupon_code)) {
            $this->_viewData->coupon = null;
            $this->_viewData->savings = '0';
        } else {
            if ($this->_viewData->order->coupon_type == 1) {//$
                $this->_viewData->savings = $this->_viewData->order->coupon_amount;
            } else if ($this->_viewData->order->coupon_type == 2) {//%
                $this->_viewData->savings = $this->_viewData->order->subtotal * $this->_viewData->order->coupon_amount / 100;
            }
        }

        if (!is_null($this->_viewData->order->cc_number)) {
            $this->_viewData->order->cc_number = '****' . substr($this->_viewData->order->cc_number, -4);
        }
    }

    public function mail(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        echo $mail->text;
        $this->template = FALSE;


    }

    public function print_packing_slip(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $this->slip_pack($id);
        $this->template = FALSE;


    }


     public function upd_fraud(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';
 
         $post_array = array(
                                'ApiLogin' => 'YwZzFUKX66lx',
                                'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                                'Action' => 'getOrderStatus',
                                'OrderNumber' => $id
                            );

// Convert post array into a query string
                            $post_query = http_build_query($post_array);

// Do the POST
                            $ch = curl_init('https://www.eye4fraud.com/api/');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            preg_match_all('~<value>(.*)</value>~', $response, $out_status);
                             
                                $order = \Model\Order::getItem($id);
                                $order->fraud_status =  $out_status[0][2];
                                $order->save();
                                redirect($_SERVER["HTTP_REFERER"]);
        $this->template = FALSE;


    }

    public function getVariations(){
        $product_id = isset($_GET['product_id']) ? $_GET['product_id']: null;
        if($product_id){
            $product = \Model\Product::getItem($product_id);
            $colors = $product->getColors();
            $sizes = $product->getSizes();
            $colorHtml = $sizeHtml ='<option value="0">None</option>';
            $price = $product->getPrice($colors[0]->id);
            foreach($colors as $color){
                $colorHtml .= '<option value="'.$color->id.'">'.$color->name.'</option>';
            }
            foreach($sizes as $size){
                $sizeHtml .= '<option value="'.$size->id.'">'.$size->us_size.'</option>';
            }
            echo json_encode(['status'=>"success", 'colorHtml'=>$colorHtml, 'sizeHtml'=>$sizeHtml, 'subtotal'=>$price]);
        }
    }

    public function setColorVariant(){
        $product_id = isset($_GET['product_id']) ? $_GET['product_id']: null;
        $color_id = isset($_GET['color_id']) ? $_GET['color_id']: null;
        if($product_id && $color_id){
            $product = \Model\Product::getItem($product_id);
            echo json_encode(['status'=>"success", 'price'=>$product->getPrice($color_id)]);
        } else {
            echo json_encode(['status'=>"failed"]);
        }
    }

    public function getShippingCost(){
        $country = $_GET['country'];
        $shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '{$country}'"]);
        if($shipping){
            echo json_encode(['status'=>'success', 'shipping'=>json_decode($shipping->cost,true)[0]]);
        } else {
            echo json_encode(['status'=>'failed', 'shipping'=>80]);
        }
    }

    public function create_order(Array $arr = []){
        parent::update($arr);
    }

    //Admin checkout flow -- because we're crazy
    public function create_order_post(){
        //Set Subtotal
        $subtotal = 0;
        for($i = 0; $i < count($_POST['product_id']); $i++){
            if($_POST['product_id'][$i] > 0 && $_POST['custom_price'][$i] == ''){
                $product = \Model\Product::getItem($_POST['product_id'][$i]);
                $subtotal += $product->getPrice($_POST['color'][$i]);
            } else if($_POST['custom_price'] > 0){
                $subtotal += $_POST['custom_price'][$i];
            }
        }
        $_POST['subtotal'] = $subtotal;

        //Set shipping cost
        if($_POST['ship_country'] == 'United States'){
            $shipping_cost = 0;
            $shipping_cost = json_decode($shipping->cost,true)[0];
        } else {
            $shipping_cost = 80;
        }
        $_POST['shipping_cost'] = round($shipping_cost,2);

        //Set Taxes
        if(isset($_POST['tax-free']) && $_POST['tax-free']){
            $_POST['tax_rate'] = 0;
            $_POST['tax'] = $tax = 0;
        } else {
            $_POST['tax_rate'] = 8.875;
            $_POST['tax'] = $tax = round($subtotal * .08875,2);
        }

        //Set discount -- N/A
        $discount = 0;
//        $coupon = \Model\Coupon::getItem(null,['where'=>"code = '{$_POST['coupon_code']}'"]);
//        $_POST['coupon_type'] = $coupon->discount_type;
//        $_POST['coupon_amount'] = $coupon->discount_amount;
//        $discount = $coupon->applyCoupon($subtotal);

        //Set guest_id
        $_POST['guest_id'] = session_id();

        $_POST['status'] = 'New';
        $_POST['ref_num'] = generateToken();

        //Set total
        $_POST['total'] = $total = $subtotal + $shipping_cost + $tax - $discount;

        $_POST['insert_time'] = date('Y-m-d H:i:s.u');
        $_POST['payment_method'] = 1;
        $order = \Model\Order::loadFromPost();
        $order->product_id=$order->color=$order->size=$order->misc_name=$order->custom_price='';
        if($order->save()){
            $html = '';
            for($i = 0; $i < count($_POST['product_id']); $i++){
                $orderProduct = new \Model\Order_Product();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $_POST['product_id'][$i];
                $orderProduct->quantity = 1; //add quantity
                if($_POST['product_id'][$i] > 0){ //id is product
                    $product = \Model\Product::getItem($_POST['product_id'][$i]);
                    $product->total_sales += 1;
                    $product->save();
                    $unit_price = $_POST['custom_price'][$i] ? : $product->price($_POST['color'][$i]);
                    $product_image = $product->featuredImage(null,$_POST['color'][$i]);
                    $product_name = $product->name;
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = \Model\Color::getItem($_POST['color'][$i])->name;
                    $product_size = \Model\Size::getItem($_POST['size'][$i]);

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['color'=>$_POST['color'][$i], 'size'=>$_POST['size'][$i]]);
                    $orderProduct->save();
                } else { //id is miscellaneous charge
                    $unit_price = $_POST['custom_price'][$i];
                    $product_image = '';
                    $product_name = $_POST['misc_name'][$i];
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = '';
                    $product_size = new stdClass(); $product_size->us_size = ''; $product_size->eur_size = '';

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['misc_name'=>$_POST['misc_name'][$i]]);
                }
                $orderProduct->save();

                $html .=
                    '<tr style="height:75.0pt">
                            <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p class="MsoNormal"><img width="100px!important;" src="https://watertour.com'.UPLOAD_URL.'products/'.$product_image.'"></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_name.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($product_price,2).'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_color.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">US: '.$product_size->us_size.'<br>EU: '.$product_size->eur_size.'</span></p>
                            </td>
                        </tr>';
            }

            $expiry = str_pad($order->cc_expiration_month,2,0).substr($order->cc_expiration_year,-2);
            $firstData = new Payment\FirstData(PAYEEZY_ID,PAYEEZY_PASSWORD);
            $firstData->setAmount($order->total);
            $firstData->setCreditCardNumber($order->cc_number);
            $firstData->setCreditCardName($order->card_name);
            $firstData->setCreditCardType($_POST['cc_type']);
            $firstData->setCreditCardVerification($order->ccv);
            $firstData->setCreditCardExpiration($expiry);
            $firstData->setCurrency('USD');
            $firstData->setReferenceNumber($order->id);
            if($order->user_id){
                $firstData->setCustomerReferenceNumber($order->user_id);
            }
            $firstData->process();
            if($firstData->isSuccess()){
                $order->status = \Model\Order::$status[1];
                $order->save();

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER'=>"1".str_pad($order->id,5,0,STR_PAD_LEFT),
                    'DATE'=>date('Y-m-d h:i:s'),
                    'NAME'=>$order->ship_first_name .' '.$order->ship_last_name,
                    'SHIPPING'=>$order->getShippingAddr(),
                    'BILLING'=>$order->getBillingAddr(),
                    'ITEMS'=>$html,
                    'SUBTOTAL'=>number_format($order->subtotal,2),
                    'DISCOUNT'=>number_format($order->coupon_amount,2), // update later
                    'SHIPPINGFEE'=>number_format($order->shipping_cost,2),
                    'TAXFEE'=>number_format($order->tax,2),
                    'TOTAL'=>number_format($order->total,2)
                ];
                $email->setTo(['email' => $order->email, 'name' => $order->first_name .' '.$order->last_name,  'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/'.$order->id);
            } else {
                $order->status = \Model\Order::$status[2];
                $order->error_message = $firstData->getErrorMessage();
                $order->save();
                $n = new \Notification\ErrorHandler($firstData->getErrorMessage());
                $_SESSION["notification"] = serialize($n);
                redirect('/admin/orders/create_order');
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/admin/orders/create_orders');
        }
    }

    public function update_post()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $order = \Model\Order::getItem($_POST['id']);

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/

            if (isset($_POST['status'])) {
                $order->status = $_POST['status'];
                if ($_POST['status'] !== $_POST['old_status']) {
                    $log_status = new \Model\Log_Status();
                    $log_status->order_id = $_POST['id'];
                    $log_status->status = $_POST['status'];
                    $log_status->admin_id = \Emagid\Core\Membership::userId();
                    $log_status->save();
                }


            }

            $order->tracking_number = $_POST['tracking_number'];
            if (isset($_POST['cc_number'])) {
                if ('****' . substr($order->cc_number, -4) != $_POST['cc_number']) {
                    $order->cc_number = $_POST['cc_number'];
                }
            }
            if (isset($_POST['cc_expiration_month'])) {
                $order->cc_expiration_month = $_POST['cc_expiration_month'];
            }
            if (isset($_POST['cc_expiration_year'])) {
                $order->cc_expiration_year = $_POST['cc_expiration_year'];
            }

            if (isset($_POST['cc_ccv'])) {
                $order->cc_ccv = $_POST['cc_ccv'];
            }


            if (isset($_POST['payment_method'])) {
                $order->payment_method = $_POST['payment_method'];
            }

            $order->bill_first_name = $_POST['bill_first_name'];
            $order->bill_last_name = $_POST['bill_last_name'];
            $order->bill_address = $_POST['bill_address'];
            $order->bill_address2 = $_POST['bill_address2'];


            if (isset($_POST['bill_state'])) {
                $order->bill_state = $_POST['bill_state'];
            }
            if (isset($_POST['bill_country'])) {
                $order->bill_country = $_POST['bill_country'];
            }


            $order->bill_city = $_POST['bill_city'];


            $order->bill_zip = $_POST['bill_zip'];

            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];


            if (isset($_POST['ship_state'])) {
                $order->ship_state = $_POST['ship_state'];
            }
            if (isset($_POST['ship_country'])) {
                $order->ship_country = $_POST['ship_country'];
            }


            $order->ship_zip = $_POST['ship_zip'];
            $order->note = $_POST['note'];

            $order->phone = $_POST['phone'];
            $subtotal = $order->subtotal;
            if (!is_null($order->coupon_code)) {
                if ($order->coupon_type == 1) {
                    $subtotal = $subtotal - $order->coupon_amount;
                } else if ($order->coupon_type == 2) {
                    $subtotal = $subtotal * (1 - ($order->coupon_amount / 100));
                }
            }

            /*$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());  
            $orderchange = new \Model\Orderchange();  
            $orderchange->order_id = $_POST['id'];
            $orderchange->user_id =  $logged_admin->id;
            $orderchange->save() ; */


            if (!empty($_POST['mail'])) {
                $email = new \Emagid\Email();
                global $emagid;
                $emagid->email->from->email = 'orders@watertour.com'; //TODO st-dev get email
                $mail_log = new \Model\Mail_Log();
                $mail_log->from_ = "orders@watertour.com"; //TODO st-dev get email

                if (is_null($order->user_id)) {
                    $email->addTo($order->email);
                    $mail_log->to_ = $order->email;
                } else {
                    $email->addTo(\Model\User::getItem($order->user_id)->email);
                    $mail_log->to_ = \Model\User::getItem($order->user_id)->email;
                }
                $email->addBcc("orders@watertour.com"); //TODO st-dev get email
                if ($_POST['status'] !== $_POST['old_status']) {
                    $subject_array = Array();
                    foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
                        $product = \Model\Product::getItem($order_product->product_id);
                        $brand = \Model\Brand::getItem($product->brand);
                        $subject_array[] = $brand->name;
                        $subject_array[] = $product->mpn;

                        //$subject_array[] = $order_product->id;
                    }
                    if ($_POST['status'] == "Shipped") {

                        $email->subject('Your order has shipped! ' . implode(" ", $subject_array) . " watertour Order #" . $order->id);
                        $mail_log->subject = 'Your order has shipped! ' . implode(" ", $subject_array) . "watertour Order #" . $order->id;
                    } else {
                        $email->subject('Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " watertour Order #" . $order->id);
                        $mail_log->subject = 'Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " watertour Order #" . $order->id;
                    }
                }
                $subject = $_POST['subject'];
                $mailtitle = $_POST['mailtitle'];
                $status = $_POST['status'];
                $text = $_POST['mail'];
                $email->subject($subject);
                $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="https://watertour.com/content/frontend/img/logo.png" alt="Water Tour"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">' . $mailtitle . '</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</a></span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Client:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:green">' . $order->ship_first_name . ' ' . $order->ship_last_name . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';


                $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
                <tbody>
                     <p align="justify">' . $text . '</p>
                </tbody>
                </table>';

                $mail_log->text = $email->body;

                $mail_log->order_id = $order->id;
                $mail_log->save();
                // echo $email->body;
                // echo $email->subject;
                $email->send();

            }

            if(isset($_POST['tracking_email']) && $_POST['tracking_email'] == 'on'){
                $email = new \Email\MailMaster();
                $mergeFields = [
                    'REF_NUM'=>$order->ref_num,
                    'TRACKING_NUMBER'=>$order->tracking_number
                ];
                $email->setTo(['email'=>$order->email,'name'=>$order->shipName(), 'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-tracking-email')->send();
            }

           /* if (isset($_POST['shipping_method'])) {
                if ($order->shipping_method != $_POST['shipping_method']) {
                    $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
                    $costs = json_decode($order->shipping_method->cost);
                    $order->shipping_method->cost = 0;
                    $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                    foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                        if (is_null($range) || trim($range) == '') {
                            $range = 0;
                        }
                        if ($subtotal >= $range && isset($costs[$key])) {
                            $order->shipping_cost = $costs[$key];
                        }
                    }
                    $order->shipping_method = $order->shipping_method->id;
                }
            }*/
           if(isset($_POST['shipping_cost'])){
               if($order->shipping_cost != $_POST['shipping_cost']){
                   $order->shipping_cost = $_POST['shipping_cost'];
               }
           }

            if (isset($_POST['ship_state'])) {
                $shippingState = preg_replace('/\s+/', '', strtolower($_POST['ship_state']));
                if ($shippingState == 'ny' ||$shippingState=='newyork') {
                    $order->tax_rate = 8.875;
                    $order->tax = $subtotal * ($order->tax_rate / 100);
                } else {
                    $order->tax_rate = null;
                    $order->tax = null;
                }
            }

            $order->total = $subtotal + $order->shipping_cost + $order->tax - $order->coupon_amount;

            if ($order->save()) {
                $n = new \Notification\MessageHandler('Order saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }




    /*  public function update_post_status() {   
            $id = $_POST['id'];
            $status = $_POST['status'];
            $order = \Model\Order::getItem($_POST['id']);
            $order->status = $status;
            $order->save();
        }*/


    /*     private function sendShippedEmail($order, $trackingNumber){
            $email = new \Emagid\Email();
            if (is_null($order->user_id)){
                $email->addTo($order->email);
            } else {
                $email->addTo(\Model\User::getItem($order->user_id)->email);
            }
            $email->bccTo("orders@watertour.com");
            $email->subject('Order shipped');
            $email->body = '<p><a href="www.watertour.com"><img src="http://watertour.com/content/frontend/img/logo.png" /></a></p>'
                          .'<p>Your watertour order was shipped!</p>'
                          .'<p>The tracking number is: <b>'.$trackingNumber.'</b></p>'
                          .'<p>Regards,<br />watertour Team</p>'
                          .'<p>Find us on <a href="https://www.facebook.com/pages/watertourcom/202088860134">Facebook</a> and <a href="https://twitter.com/watertour">Twitter</a>.</p>';
            $email->send();
        } */

    public function getCcNumber()
    {
        if (isset($_POST['password']) && $_POST['password'] == 'test5343') {
            echo json_encode(\Model\Order::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function unlock()
    {
        if (isset($_POST['pwd']) && $_POST['pwd'] == 'test5343') {
            echo json_encode("ok");
        } else {
            echo json_encode("false");
        }
    }

    public function search()
    {
        $orders = \Model\Order::Search($_GET['keywords']);

        echo '[';
        foreach ($orders as $key => $order) {
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);

            echo '{ "id": "' . $order->id . '", 
            "status": "' . $order->status . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '", 
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "'; ?><? foreach ($order_products as $op) {
                $product = \Model\Product::getItem($op->product_id);
                echo $product->name;
                echo "<br>";
            } ?><? echo '",' ?><? echo '"brands": "'; ?><? foreach ($order_products as $op) {
                $product = \Model\Product::getItem($op->product_id);
                $brand = \Model\Brand::getItem($product->brand);
                echo $brand->name;
                echo "<br>";
            } ?><? echo '","total": "' . number_format($order->total, 2) . '"}';
            if ($key < (count($orders) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }


    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {

                    $a[] = '{ "id": "' . $order->id . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '",
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "' . $pr->name . '",
            "total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }


    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }

    private function slip_pack($order)
    {
        global $emagid;
        $emagid->email->from->email = 'orders@watertour.com';

        $email = new \Emagid\Email();

        $order = \Model\Order::getItem($order);


        $email->subject('Order packing slip!');///wtf?
        $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://watertour.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Packing slip</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Number:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Date:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->insert_time . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        /* if ($order->payment_method == 1) {
            $email->body .= 'We have received your order and we are currently processing it.<br>';
            $email->body .= 'Your credit card will not be charged until the item’s availability has been established, at which time you will receive a confirmation email.';
            $email->body .= '<p>Then, as soon as your item ships you will receive another email with the tracking information.</p>';
            $email->body .= '<p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@watertour.com</p>';
        } else if ($order->payment_method == 2) {
            $email->body .= '<p>We will contact you shortly for more information. If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@watertour.com</p>';
        } */


        $shp = array($order->ship_first_name, $order->ship_last_name, $order->ship_address, $order->ship_address2,
            $order->ship_country, $order->ship_city, $order->ship_state, $order->ship_zip);


        $blng = array($order->bill_first_name, $order->bill_last_name, $order->bill_address, $order->bill_address2,
            $order->bill_country, $order->bill_city, $order->bill_state, $order->bill_zip);


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="50%" style="width:50.0%;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping to:</span></b> </p>
                        </td>
                        <td width="50%" style="width:50.0%;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Billing to:</span></b> </p>
                        </td>
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>
                    <tr>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">
                            ' . $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . ' <br>
                            ' . $order->ship_zip . ' ' . $order->ship_country . '                       
                            </span></p>
                        </td>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">';
        if (count(array_diff($shp, $blng)) > 0) {
            $email->body .= $order->bill_first_name . ' ' . $order->bill_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->bill_address . ' ' . $order->bill_address2 . '<br>
                            ' . $order->bill_city . ' ' . $order->bill_state . '<br>
                            ' . $order->bill_zip . ' ' . $order->bill_country;
        } else {
            $email->body .= $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . '<br>
                            ' . $order->ship_zip . ' ' . $order->ship_country;
        }
        $email->body .= '</p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        if($order->note) {
            $email->body .= '<h4>Note: ' . $order->note . '</h4>';
        }
        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="255" colspan="2" style="width:191.25pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Items</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Qty</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Color</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Size</span></b> </p>
                        </td>
                         
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="6" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>';

        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
            $variant = json_decode($order_product->details,true);
            $color = isset($variant['color']) && $variant['color'] ? \Model\Color::getItem($variant['color']) : null;
            $size = isset($variant['size']) && $variant['size'] ? \Model\Size::getItem($variant['size']) : null;

            $product = $order_product->product_id ? \Model\Product::getItem($order_product->product_id) : null;
            $productName = $product ? $product->name: $variant['misc_name'];
            $productImage = $product ? $product->featuredImage(null,$color->id) : '';
            $custom = !$product && $variant['misc_name'] ? '(Custom Item)': '';

            $colorName = $color ? $color->name: '';
            $sizeHtml = $size ? 'US: '.$size->us_size.' EU: '.$size->eur_size : '';

            $email->body .= '<tr style="height:75.0pt">'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal"><img  width="400px!important;" src="https://watertour.com/content/uploads/products/' . $productImage . '"></p></td>'
                . '<td width="100" valign="top" style="width:127.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 0in;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042"> ' . $productName . ' ' . $custom . '</span></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order_product->quantity . '</span></b></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $colorName . '</span></b></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">'.$sizeHtml.'</span></b></p>
                        </td> </tr>
                ';
        }
        $email->body .= '</tbody>
            </table>
            <p class="MsoNormal">&nbsp;</p>';
        if (!is_null($order->coupon_code)) {
            $savings = ($order->coupon_type == 1) ? '$' : '';
            $savings .= number_format($order->coupon_amount, 2);
            $savings .= ($order->coupon_type == 2) ? '%' : '';
        } else {
            $savings = 0;
        }

        $show_saving = 'display:none;';

        $a = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt;padding-left: 604px;">
                <tbody>
                    <tr>
                         
                        <td style="padding:0in 0in 0in 0in">
                            <table border="0" cellspacing="0" cellpadding="0" width="200" style="width:150.0pt">
                                <tbody>
                                    <tr style="height:20.25pt">
                                        <td width="200" colspan="2" style="width:150.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order Summary</span></b></p>
                                        </td>
                                    </tr><tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sub Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->subtotal, 2) . '</span></p>
                                        </td>
                                    </tr>
                                     
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->shipping_cost, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sales Tax:</span></b> </p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->tax, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->total, 2) . '</span></b></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';


        /*  $email->addTo($order->email);
          $email->addBcc('info@watertour.com');
          $email->addBcc('orders@watertour.com');
          $mail_log = new \Model\Mail_Log();
          $mail_log->to_ = $order->email;
          $mail_log->from_ = "orders@watertour.com";
          $mail_log->subject = "Order packing slip";
          $mail_log->order_id = $order->id;
          $mail_log->text = $email->body;
          $mail_log->save();*/
        echo $email->body; //sends to the customer


    }

}

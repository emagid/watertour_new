<?php

class bus_packagesController extends adminController
{

    function __construct()
    {
        parent::__construct("Bus_Package");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    function update(Array $arr = [])
    {
        $this->_viewData->tours = \Model\Bus_Tour::getList();
        parent::update($arr);
    }

    function update_post()
    {
        $range = explode('-', $_POST['package_range']);
        $_POST['start_date'] = trim($range[0]);
        $_POST['end_date'] = trim($range[1]);
        $_POST['bus_tour_id'] = json_encode($_POST['bus_tour_id']);
        parent::update_post(); // TODO: Change the autogenerated stub
    }

    function sort($arr = [])
    {
        $this->_viewData->categories = \Model\Category::getNested(0);
//		dd($this->_viewData->categories[1]->children[1]);
        $this->loadView($this->_viewData);
//		parent::index($arr);
    }

    function orderPreview()
    {
        $category = \Model\Category::getItem($_POST['id']);
        $category->product_preview_ids = json_encode($_POST['array']);
        $category->save();

        echo 'success';
    }

    function orderProduct()
    {
        $increment = 1;
        foreach ($_POST['ids'] as $id) {
            $prodcat = \Model\Product_Category::getItem($id);
            $prodcat->display_order = $increment;
            $prodcat->save();
            $increment++;
        }
        echo 'fail';
    }

    function updateCategory()
    {
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach ($data as $da) {
            \Model\Category::updateRelationship($da->id, $da->children[0]);
            $category = \Model\Category::getItem($da->id);
            $category->parent_category = 0;
            $category->display_order = $display;
            $category->save();

            $display++;
        }
    }

    function afterObjSave($obj)
    {
        if (!is_null($obj->banner) && $obj->banner != "") {
            $image = new \Emagid\Image();
            $image->fileName = $obj->banner;
            $image->fileType = explode(".", $image->fileName);
            $image->fileType = strtolower(array_pop($image->fileType));
            $image->fileDir = UPLOAD_PATH . 'categories' . DS;
            $image->resize('1200_344' . $image->fileName, false, 1200, 344);
        }
    }

    function updateCategoryProductImage()
    {
        $product_id = $_POST['product_id'];
        $category_id = $_POST['category_id'];
        $image = $_POST['image'];

        $prodCatImg = \Model\Product_Category::getItem(null, ['where' => ['product_id' => $product_id, 'category_id' => $category_id]]);
        $prodCatImg->image = $image;
        $prodCatImg->save();

        echo json_encode(['status' => 'success']);
    }

    function upload_images($params)
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $data['redirect'] = false;
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id'] > 0) ? $params['id'] : 0;
        if ((int)$id > 0) {
            $item = \Model\Package::getItem($id);
            if ($item == null) {
                $data['redirect'] = true;
            }
            $this->save_upload_images($_FILES['file'], $item->id, UPLOAD_PATH . 'packages' . DS, []);
            $data['success'] = true;
        }

        //print_r($_FILES);
        echo json_encode($data);
        exit();
    }

    public function save_upload_images($files,$obj_id,$upload_path,$sizes=[]) {
        $counter = 1;
        // get highest display order counter if any images exist for specified listing
        $image_high = \Model\Package_Image::getItem(null,['where'=>'package_id='.$obj_id,'orderBy'=>'display_order','sort'=>'DESC']);
        if($image_high!=null) {
            $counter = $image_high->display_order+1;
        }
        foreach($files['name'] as $key=>$val) {
            $item_image = new \Model\Package_Image();
            $temp_file_name = $files['tmp_name'][$key];
            $file_name = uniqid() . "_" . basename($files['name'][$key]);
            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('-', '_', $file_name);

            $allow_format = array('jpg', 'png', 'gif','JPG');
            $filetype = explode("/", $files['type'][$key]);
            $filetype = strtolower(array_pop($filetype));
            //$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
            //$filetype = array_pop($filetype);
            if($filetype == 'jpeg' || $filetype=="JPG"){$filetype = 'jpg';}

            if(in_array($filetype, $allow_format)){

                $img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
                if($img_path===false) {
                    move_uploaded_file($temp_file_name, $upload_path . $file_name);
                }
                //move_uploaded_file($temp_file_name, $upload_path . $file_name);

                if(count($sizes)>0) {
                    foreach($sizes as $key=>$val) {
                        if(is_array($val) && count($val)==2) {
                            resize_image_by_minlen($upload_path,
                                $file_name, min($val), $filetype);

                            $path = images_thumb($file_name,min($val),
                                $val[0],$val[1], file_format($file_name));
                            $image_size_str = implode('_',$val);
                            copy($path, $upload_path.$image_size_str.$file_name);
                        }
                    }
                }
//				$variant_image = new \Model\Product_Attributes();
//				$variant_image->product_id = $obj_id;
//				$variant_image->name = 'image';
//				$variant_image->value = $file_name;
//				$variant_image->save();

                $item_image->image = $file_name;
                $item_image->package_id = $obj_id;
                $item_image->display_order = $counter;
                $item_image->save();
            }
            $counter++;
        }
    }

}
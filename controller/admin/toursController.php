<?php

use Emagid\Html\Form;

class toursController extends adminController {
	
	function __construct(){
		parent::__construct("Tour");
	}

	function index(Array $params = [])
	{
		if (!Empty($_GET['how_many'])) {

			$params['limit'] = $_GET['how_many'];

		} else {
			$params['limit'] = 10;
		}
		$this->_viewData->hasCreateBtn = true;

		parent::index($params);

	}

	public function import(Array $params = [])
	{
		set_time_limit(0);
		$handle = fopen($_FILES['csv']['tmp_name'], "r");
		$headSkip = true;
		$id1 = '';
		$id2 = '';
		$id3 = '';
		$allArr = [];
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if (empty($data[0])) {
				} else {
					if($headSkip){
						$headSkip = false;
					} else {
						if(!array_key_exists($data[0],$allArr)){
							$id1 = strtolower($data[7]);$id2 = strtolower($data[9]);$id3 = strtolower($data[11]);
							$allArr[$data[0]] = ['slug'=>$data[0],'name'=>$data[1],'description'=>$data[2],'tags'=>$data[5],'price'=>$data[19],'msrp'=>$data[20],'meta_title'=>isset($data[31])?$data[31]:'', 'meta_description'=>isset($data[32])?$data[32]:'',
												'options'=>[$id1=>[$data[8]],$id2=>[$data[10]],$id3=>[$data[12]]], 'images'=>[$data[24]]];
						} else {
							if(!in_array($data[8],$allArr[$data[0]]['options'][$id1])){
								$allArr[$data[0]]['options'][$id1][] = $data[8];
							}
							if(!in_array($data[10],$allArr[$data[0]]['options'][$id2])){
								$allArr[$data[0]]['options'][$id2][] = $data[10];
							}
							if(!in_array($data[12],$allArr[$data[0]]['options'][$id3])){
								$allArr[$data[0]]['options'][$id3][] = $data[12];
							}
							if(isset($data[24]) && $data[24] != ''){
								$allArr[$data[0]]['images'][] = $data[24];
							}
						}
					}
				}
			}
			fclose($handle);
		}
		foreach($allArr as $arr){
			$product = new \Model\Product();
			$product->slug = $arr['slug'];
			$product->name = $arr['name'];
			$product->description = $arr['description'];
			$product->tags = $arr['tags'];
			$product->price = $arr['price'];
			$product->msrp = $arr['msrp'] != ''?$arr['msrp']:$arr['price'];
			$product->meta_title = $arr['meta_title'];
			$product->meta_description= $arr['meta_description'];
			$product->options = json_encode($arr['options']);
			if(array_key_exists('',$arr['options'])){
				unset($arr['options'][""]);
			}
			if(isset($arr['options']['size'])){
				$product->size = json_encode($arr['options']['size']);
			}
			$arrColor = [];
			if(isset($arr['options']['color'])){
				foreach($arr['options']['color'] as $color){
					if($c = \Model\Color::getItem(null,['where'=>"name = '{$color}'"])){
						$arrColor[] = $c->id;
					} else {
						$strReplace = str_replace(',','-',trim($color));
						$strReplace = str_replace('/','-',trim($strReplace));
						$strReplace = str_replace('&','-',trim($strReplace));
						$strReplace = str_replace(' ','-',trim($strReplace));
						$c = new \Model\Color();
						$c->name = $color;
						$c->slug = $strReplace;
						$c->save();
						$arrColor[] = $c->id;
					}
				}
				$product->color = json_encode($arrColor);
			}
			$product->save();
			foreach($arr['images'] as $image){
				$prod_image = new \Model\Product_Image();
				$uniqueId = uniqid($product->slug);
				file_put_contents(__DIR__ . '/../../content/uploads/products/' . $uniqueId . '.jpg', file_get_contents($image));
				$prod_image->product_id = $product->id;
				$prod_image->image = $uniqueId . '.jpg';
				$prod_image->save();
			}
		}


		$n = new \Notification\MessageHandler("Import success!");
		$_SESSION["notification"] = serialize($n);
		redirect(SITE_URL . 'admin/products');


	}
	function export(Array $params = []){
		$this->template = false;
		$br = $_GET['brand'];
		$this->_viewData->hasCreateBtn = true;
		$this->_viewData->product = \Model\Product::getList(['where'=>' brand = '.$br]);
		$brand = \Model\Brand::getItem($br);
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$brand->name.'.csv');
		$output = fopen('php://output', 'w');
		$t=array("Name","Mpn","Sell price","MSRP", "Availability", 'Image File', 'Mpn 2', 'Mpn 3');
		fputcsv($output, $t);
		$row ='';
		foreach($this->_viewData->product as $b) {
		$row = array($b->name,$b->mpn,$b->price,$b->msrp, $b->getAvailability(), $b->featured_image,$b->mpn2,$b->mpn3);
		fputcsv($output, $row);  
		}   
		parent::index($params);
	}

	public function update(Array $arr = []) {
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);

		$carbon = \Carbon\Carbon::now();

		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_images = [];
		if(isset($arr['id'])){
			$this->_viewData->tours = \Model\Tour::getList(['where'=>"id not in ({$arr['id']})",'orderBy'=>'name']);
		} else {
		    $this->_viewData->tours = \Model\Tour::getList(['orderBy'=>"name"]);
        }
//		$this->_viewData->routes = \Model\Route::getList();
        $mr = \Model\Main_Route::getList();
		$main_routes = [];
		foreach ($mr as $m){
		    $main_routes[$m->id] = $m;
        }
        $this->_viewData->main_routes = $main_routes;
		$this->_viewData->packages = \Model\Package::getList(['orderBy'=>'name']);
		$this->_viewData->languages = \Model\Language::getList();

		parent::update($arr);
	}

    protected function beforeLoadUpdateView()
    {
        $sql = "select r.*,tr.display_order,tr.route_icon_ids from route r inner join tour_route tr on tr.route_id = r.id where tr.active = 1 and tr.tour_id = {$this->_viewData->tour->id} order by display_order";
        $this->_viewData->tour_routes = \Model\Route::getList(['sql'=>$sql]);
        parent::beforeLoadUpdateView(); // TODO: Change the autogenerated stub
    }


    public function update_post()
	{
		$ava = [];
		$stuff = ['o','w','d','h'];
		foreach($stuff as $s){
			if(isset($_POST[$s])){
				$ava[$s] = $_POST[$s];
			}
		}
		$arr = ['rentals','tours','packages','attractions','equipment','products'];
		$addOns = [];
		foreach($arr as $a){
			if(isset($_POST[$a]) && $_POST[$a]){
				$addOns[$a] = $_POST[$a];
			}
		}
		$_POST['related'] = json_encode($_POST['related']);
		$_POST['add_ons'] = json_encode($addOns);
		$_POST['available_range'] = json_encode($ava);
//		$availability = explode('-',$_POST['availability_range']);
		$validity = explode('-',$_POST['validity_range']);
//		$_POST['start_date'] = $availability[0];
//		$_POST['end_date'] = $availability[1];
		$_POST['valid_start'] = $validity[0];
		$_POST['valid_end'] = $validity[1];
		$_POST['languages'] = json_encode($_POST['languages']);
		parent::update_post();
	}

	public function sort_images()
	{
		$display_order = 1;
		foreach($_POST['ids'] as $id){
			$prod_img = \Model\Product_Image::getItem($id);
			$prod_img->display_order = $display_order;
			$prod_img->save();
			$display_order++;
		}
	}

	public function save_variant(){
		$material = [$_POST['material']];
		$color = $_POST['color'];
		$sole = $_POST['sole'];
		$price = $_POST['price'] ? : 0.0;
		$quantity = $_POST['quantity'] ? : 0.0;
		$madeToOrder = $_POST['mto'] ? : 'off';
		$productId = $_POST['productId'];

		$echo = ['redirect'=>$_POST['redirect']];
		if($material && $color && $sole){
			$arr = \Model\Variation::$lowerVariations;
			$arrList = [];
			foreach($arr as $array){
				if(isset($$array)){
					$arrList[] = $$array;
				}
			}
			$list = \Model\Variation::combinations($arrList);
			foreach($list as $combo){
				$variant = [];
				for($i = 0; $i < count($combo); $i++){
					$variant[$arr[$i]] = $combo[$i];
				}
				$variation = \Model\Variation::addVariation($variant);
				\Model\Product_Attributes::addAttribute($productId,$variation->id,['price'=>$price, 'quantity'=>$quantity, 'madeToOrder'=>$madeToOrder]);
			}
		} else {
			$n = new \Notification\ErrorHandler("Requirements unmet");
			$_SESSION['notification'] = serialize($n);
		}
		echo json_encode($echo);
	}

	public function delete_variant()
	{
		$id = isset($_POST['id']) && $_POST['id'] != '' ? $_POST['id'] : null;
		$prod_id = isset($_POST['prod_id']) && $_POST['prod_id'] != '' ? $_POST['prod_id'] : null;
		if ($id && $prod_id) {
			$variation = \Model\Variation::getItem($id);
			$pAttr = \Model\Product_Attributes::getList(['where' => "variation_id = $variation->id and product_id = $prod_id"]);
			foreach ($pAttr as $pa) {
				\Model\Product_Attributes::delete($pa->id);
			}
			echo json_encode(['status' => "success"]);
		} else {
			echo json_encode(['status' => "failed", 'message' => "Failed to delete"]);
		}
	}

	public function save_attribute(){
		$product_id = $_POST['product_id'] ? : null;
		$color_id = $_POST['color_id'] ? : null;
		$price = $_POST['price'];
		$mto = $_POST['mto'];
		$event = $_POST['event'];
		$arr = ['price'=>$price, 'mto'=>$mto, 'event'=>$event];
		if($product_id && $color_id){
			foreach($arr as $key=>$ar){
				$prod_attr = \Model\Product_Attributes::getItem(null,['where'=>"product_id = $product_id and color_id = $color_id and name='{$key}'"]) ? : new \Model\Product_Attributes();
				$prod_attr->product_id = $product_id;
				$prod_attr->color_id = $color_id;
				$prod_attr->name = $key;
				$prod_attr->value = $ar;
				$prod_attr->save();
			}
			echo json_encode(['status'=>'success', 'message'=>'Saved']);
		} else {
			echo json_encode(['status'=>'failed', 'message'=>'Variant failed to save']);
		}
	}

	public function multi_update(Array $arr = []) {
		$id_array = $_GET;
		
		$q = implode("," , $id_array);

		if (count($_GET) <= 1){
			$n = new \Notification\ErrorHandler('You must select at least one product to Multi Edit.');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'products');
		}

		$this->_viewData->q=$q;
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_notes as $note){
			$note->admin = \Model\Admin::getItem($note->admin_id);
			$note->insert_time = new DateTime($note->insert_time);
			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList();
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->colors = \Model\Color::getList();
		$this->_viewData->collections = \Model\Collection::getList();
		$this->_viewData->materials = \Model\Material::getList();

		parent::update($arr);
	}
	
	public function multi_update_post(){

		$q = explode(",", $_POST['id']);
		$a = $_POST;
		
		foreach($q as $productId){
			if (is_numeric($productId)){
				$product = Model\Product::getItem($productId);
				
				if ((isset($a['product_category'])) && (count($a['product_category'])>0)  )  {
					for ($x = 0; $x < count($a['product_category']); $x++){
						$category = \Model\Product_Category::getItem(null, ['where' => "product_id = '" . $productId . "' and category_id = ".$a['product_category'][$x]]);
						if (!isset($category))	{
							$category = new Model\Product_Category();
							$category->product_id = $productId;
							$category->category_id = $a['product_category'][$x];
							$category->save();
						}
					}
				}

				if ((isset($a['product_collection'])) && (count($a['product_collection'])>0)  )  {
					for ($x = 0; $x < count($a['product_collection']); $x++){
						$collection = \Model\Product_Collection::getItem(null, ['where' => "product_id = '" . $productId . "' and collection_id = ".$a['product_collection'][$x]]);
						if (!isset($collection))	{
							$collection = new Model\Product_Collection();
							$collection->product_id = $productId;
							$collection->collection_id = $a['product_collection'][$x];
							$collection->save();
						}
					}
				} 

				if ((isset($a['product_material'])) && (count($a['product_material'])>0)  )  {
					for ($x = 0; $x < count($a['product_material']); $x++){
						$material = \Model\Product_Material::getItem(null, ['where' => "product_id = '" . $productId . "' and material_id = ".$a['product_material'][$x]]);
						if (!isset($material))	{
							$material = new Model\Product_Material();
							$material->product_id = $productId;
							$material->material_id = $a['product_material'][$x];
							$material->save();
						}
					}
				}

				foreach($a as $key1 => $value1)	{
					if (!empty($value1) && $key1 != "id"){
						$product->$key1 = $value1;
						$product->save();
					}
				}
			}
		}
		redirect(ADMIN_URL.'products');
	}

	 

	protected function afterObjSave($obj){
        $activeRoutes = [];
	    if(isset($_POST['tour_route'])){
            foreach ($_POST['tour_route'] as $item){
                $trArr = ['route_id'=>$item['route_id'],'tour_id'=>$obj->id];
                $tourRoute = \Model\Tour_Route::getItem(null,['where'=>"route_id = {$item['route_id']} and tour_id = $obj->id"]) ? : new \Model\Tour_Route($trArr);
                $tourRoute->display_order = $item['display_order'];
                if(isset($item['route_icon_id']) && is_array($item['route_icon_id'])){
                    $tourRoute->route_icon_ids = json_encode($item['route_icon_id']);
                } else {
                    $tourRoute->route_icon_ids = json_encode([]);
                }
                $tourRoute->save();
                $activeRoutes[] = $tourRoute->id;
            }

        }
        foreach (\Model\Tour_Route::getList(['where'=>"tour_id = $obj->id"]) as $item){
	        if(!in_array($item->id,$activeRoutes)){
                \Model\Tour_Route::delete($item->id);
            }
        }

		if (isset($_POST['note'])){
			$note = new \Model\Note();
			$note->name = $_POST['note']['name'];
			$note->description = $_POST['note']['description'];
			$note->admin_id = \Emagid\Core\Membership::userId();
			$note->product_id = $obj->id;
			$note->save();
		}
	}

	public function save_variant_image(){
		$img_id = $_POST['product_image_id'];
		$color_id = $_POST['color_id'];
		$prod_img = \Model\Product_Image::getItem($img_id);
		if($prod_img){
			$prod_img->color_id = json_encode($color_id);
			if($prod_img->save()){
				echo json_encode(['status'=>"success",'message'=>"Success"]);
			} else {
				echo json_encode(['status'=>"failed",'message'=>"Failed to save"]);
			}
		} else {
			echo json_encode(['status'=>"failed",'message'=>"Product Image does not exist... somehow"]);
		}
	}

	public function save_legshot_image(){
		$id = $_POST['product_image_id'];
		$colId = $_POST['color_id'];
		$prodId = $_POST['product_id'];

		$prodImg = \Model\Product_Image::getList(['where'=>"product_id = $prodId and color_id like '%$colId%'"]);
		if($prodImg) {
			foreach ($prodImg as $pi) {
				if ($pi->id != $id || ($pi->id == $id && $pi->legshot == 1)) {
					$pi->legshot = 0;
				} else {
					$pi->legshot = 1;
				}
				$pi->save();
			}
			echo json_encode(['status' => "success", 'message' => "Success"]);
		} else {
			echo json_encode(['status'=>"failed",'message'=>"Failed"]);
		}
	}

	function upload_images($params) {
		header("Content-type:application/json");
		$data = [];
		$data['success'] = false;
		$data['redirect'] = false;
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
		if((int)$id>0) {
			$product = \Model\Tour::getItem($id);
			if($product==null) {$data['redirect']=true;}
			$this->save_upload_images($_FILES['file'], $product->id, UPLOAD_PATH.'tours'.DS,[]);
			$data['success'] = true;
		}
	
		//print_r($_FILES);
		echo json_encode($data);exit();
	}
	
	public function save_upload_images($files,$obj_id,$upload_path,$sizes=[]) {
		$counter = 1;
		// get highest display order counter if any images exist for specified listing
		$image_high = \Model\Tour_Image::getItem(null,['where'=>'tour_id='.$obj_id,'orderBy'=>'display_order','sort'=>'DESC']);
		if($image_high!=null) {
			$counter = $image_high->display_order+1;
		}
		foreach($files['name'] as $key=>$val) {
			$product_image = new \Model\Tour_Image();
			$temp_file_name = $files['tmp_name'][$key];
			$file_name = uniqid() . "_" . basename($files['name'][$key]);
			$file_name = str_replace(' ', '_', $file_name);
			$file_name = str_replace('-', '_', $file_name);
	
			$allow_format = array('jpg', 'png', 'gif','JPG');
			$filetype = explode("/", $files['type'][$key]);
			$filetype = strtolower(array_pop($filetype));
			//$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
			//$filetype = array_pop($filetype);
			if($filetype == 'jpeg' || $filetype=="JPG"){$filetype = 'jpg';}
	
			if(in_array($filetype, $allow_format)){
	
				$img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
				if($img_path===false) {
					move_uploaded_file($temp_file_name, $upload_path . $file_name);
				}
				 //move_uploaded_file($temp_file_name, $upload_path . $file_name);
				 
				if(count($sizes)>0) {
					foreach($sizes as $key=>$val) {
						if(is_array($val) && count($val)==2) {
							resize_image_by_minlen($upload_path,
							$file_name, min($val), $filetype);
	
							$path = images_thumb($file_name,min($val),
									$val[0],$val[1], file_format($file_name));
							$image_size_str = implode('_',$val);
							copy($path, $upload_path.$image_size_str.$file_name);
						}
					}
				}
//				$variant_image = new \Model\Product_Attributes();
//				$variant_image->product_id = $obj_id;
//				$variant_image->name = 'image';
//				$variant_image->value = $file_name;
//				$variant_image->save();
	
				$product_image->image = $file_name;
				$product_image->tour_id = $obj_id;
				$product_image->display_order = $counter;
				$product_image->save();
			}
			$counter++;
		}
	}

	public function search(){
		$products = \Model\Tour::search($_GET['keywords']);

		echo '[';
		foreach($products as $key=>$product){
			echo '{ "id": "'.$product->id.'", "name": "'.$product->name.'", "featured_image": "'.$product->featured_image.'", "price":"'.$product->price.'", "msrp":"'.$product->msrp.'", "availability":"'.$product->getAvailability().'" }';
			if ($key < (count($products)-1)){
				echo ",";
			}
		}
		echo ']';
	}   

	public function check_slug(){
		$slug = $_POST['slug'];
		$id = $_POST['id'];
		if($id>0){
			$list = \Model\Tour::getItem(null,['where'=>"slug = '".$slug."' and id = '".$id."'"]);
				 if (isset($list) && count($list) == 1){
				 	echo "0";
				 }else{
					 	$list = \Model\Tour::getList(['where'=>"slug = '".$slug."' "]);
					 if (isset($list) && count($list) > 0){
					 	echo "1";
					 }else{
					 	echo "0";
					 }
				 }
		}else{
			$list = \Model\Tour::getList(['where'=>"slug = '".$slug."' "]);
				 if (isset($list) && count($list) > 0){
				 	echo "1";
				 }else{
				 	echo "0";
				 }
		}
		
	}

	public function updateFeatured(){
		foreach(\Model\Tour::getList() as $item){
			if(in_array($item->id,json_decode($_POST['featured'],true))){
				$item->featured = 1;
			} else {
				$item->featured = 0;
			}
			$item->save();
		}
	}
     
}
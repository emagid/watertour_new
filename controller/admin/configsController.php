<?php

class configsController extends adminController {
	
	function __construct(){
		parent::__construct("Config");
	}

	public function index(Array $params = []){
		$params['queryOptions']['orderBy'] = 'config.id';
		parent::index($params);
	}

	public function update_post() {
    	$obj = \Model\Config::getItem($_POST['id']); 
    	
    	$obj->value = $_POST['value'];

    	if ($obj->save()){
            $this->update_relationships($obj);
            $this->afterObjSave($obj);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
           	$_SESSION["notification"] = serialize($n);
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
    	}

    	if (isset($_POST['redirectTo'])){
    		redirect($_POST['redirectTo']);
    	} else {
    		redirect(ADMIN_URL.$this->_content);
    	}
    }

    function generateSiteMap(){
    	$links = [];

    	$home = "https://watertour.com/";
    	$links[] = $home;
    	$links[] = $home."home";
    	$links[] = $home."home/index";

    	foreach (\Model\Page::getList() as $page){
    		$links[] = $home.'page/'.$page->slug;
    	}

    	$links[] = $home."brands";
    	foreach (\Model\Brand::getList() as $brand){
    		$links[] = $home.'brand/'.$brand->slug;

    		$nPages = $this->emagid->db->execute('SELECT count(id) as c_product FROM product WHERE active = 1 AND brand = '.$brand->id)[0]['c_product'];
    		$nPages = ceil($nPages/12);
    		if ($nPages > 1){
    			for($i = 2; $i <= $nPages; $i++){
    				$links[] = $home.'brand/'.$brand->slug.'?page='.$i;
    			}
    		}
    	}
    	

		foreach (\Model\Category::getList() as $category){
    		$links[] = $home.'category/'.$category->slug;

    		$nPages = $this->emagid->db->execute('SELECT count(product_id) as c_product FROM product_categories INNER JOIN product ON (product_id = product.id) WHERE product.active = 1 AND category_id = '.$category->id)[0]['c_product'];
    		$nPages = ceil($nPages/12);
    		if ($nPages > 1){
    			for($i = 2; $i <= $nPages; $i++){
    				$links[] = $home.'category/'.$category->slug.'?page='.$i;
    			}
    		}
    	}

    	$links[] = $home."news";
    	foreach (\Model\Article::getList() as $article){
    		$links[] = $home.'news/article/'.$article->slug;
    	}

    	$products = $this->emagid->db->execute('SELECT slug FROM product WHERE active = 1');
    	foreach ($products as $product){
    		$links[] = $home.'product/'.$product['slug'];
    	}

    	//Generate the file

    	$header = '<url><loc>';
    	$footer = '</loc><lastmod>'.date("Y-m-d").'T'.date("H:i:s").'00:00</lastmod></url>';

    	$f = fopen(ROOT_DIR."/sitemap.xml","w"); fclose($f); //clear the file
        $f = fopen(ROOT_DIR."/sitemap.xml","a"); //open with append write mode

        fwrite($f, '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
        foreach ($links as $link){
        	fwrite($f, $header);
        	fwrite($f, $link);
        	fwrite($f, $footer);
        }
        fwrite($f, '</urlset>');

        fclose($f);
    }

	function delete(Array $arr = []){}
  	
}












































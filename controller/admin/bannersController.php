<?php

class bannersController extends adminController {
	
	function __construct(){
		parent::__construct("Banner");
	}

	function update(Array $params = []){}
	function update_post(Array $params = []){}

	function main(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		$params['queryOptions'] = ['where'=>"banner_type = 1"];
		parent::index($params);
	}

	function main_update(Array $params = []){
		$params['overrideView'] = 'main_update';
		parent::update($params);
	}

	function main_update_post(){
//		$_POST['options'] = [];
//		//buttons
//		for($i = 0;$i < count($_POST['button_title']); $i++){
//			$_POST['options']['buttons'][] = ['title'=>$_POST['button_title'][$i],'url'=>$_POST['button_url'][$i],'button_order'=>$_POST['button_order'][$i]];
//		}
//		$_POST['options'] = json_encode($_POST['options']);
		$obj = new $this->_model($_POST);

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$image->resize('1200_540'.$image->fileName, true, 1200);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/main');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/main_update/'.$obj->id);
    	}
	}

	function main_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/main';
		parent::delete($params);
	}

	function main_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem($params['id']);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/main_update/'.$banner->id);
	}

	function description(Array $params = []){
		$params['queryOptions'] = ['where'=>"banner_type = 4"];
		parent::index($params);
	}

	function description_update(Array $params = []){
		$params['overrideView'] = 'description_update';
		parent::update($params);
	}

	function description_update_post(){
//		$_POST['options'] = [];
//		//buttons
//		for($i = 0;$i < count($_POST['button_title']); $i++){
//			$_POST['options']['buttons'][] = ['title'=>$_POST['button_title'][$i],'url'=>$_POST['button_url'][$i],'button_order'=>$_POST['button_order'][$i]];
//		}
//		$_POST['options'] = json_encode($_POST['options']);
		$obj = new $this->_model($_POST);

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$image->resize('1200_540'.$image->fileName, true, 1200);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/description');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/description_update/'.$obj->id);
    	}
	}

	function description_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/description';
		parent::delete($params);
	}

	function description_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem($params['id']);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/description_update/'.$banner->id);
	}

	function tour_redirect(Array $params = []){
		$params['queryOptions'] = ['where'=>"banner_type = 2",'orderBy'=>'order_num'];
		parent::index($params);
	}

	function tour_redirect_update(Array $params = []){
		$params['overrideView'] = 'tour_redirect_update';
		parent::update($params);
	}

	function tour_redirect_update_post(){
		$tr = \Model\Banner::getItem($_POST['id']);
		$details = json_decode($tr->details,true);
		$details['url_text'] = $_POST['url_text'];
		$_POST['details'] = json_encode($details);
		$obj = new $this->_model($_POST);

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$image->resize('1200_540'.$image->fileName, true, 1200);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/tour_redirect');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/tour_redirect_update/'.$obj->id);
    	}
	}

	function tour_redirect_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/tour_redirect';
		parent::delete($params);
	}

	function tour_redirect_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem($params['id']);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/tour_redirect_update/'.$banner->id);
	}

}
<?php

class tour_routesController extends adminController{

	function __construct(){
        parent::__construct('Tour_Route');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
}
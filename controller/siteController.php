<?php

class siteController extends \Emagid\Mvc\Controller {	
	
	protected $viewData;
	protected $filters;

	function __construct(){
		parent::__construct();
//dd($this,\Model\Product::getIdBySlug($this->emagid->route['product_slug']),http_build_query(['term'=>'hello_world']));
		//Begin redirecting before content loading
		$this->legacyRedirection();

		$this->configs = \Model\Config::getItems();

		$this->viewData = (object)[
			'pages' => \Model\Page::getList(),
			'cart' => new stdClass(),
			'params' => [],
			'user' => null,
			'blog' => \Model\Blog::getItem(null,['orderBy'=>'id desc']),
			'tour_banner'=>\Model\Banner::getList(['where'=>"banner_type = 2",'orderBy'=>"order_num"]),
            'booking_selection'=>['Package'=>\Model\Package::getList(['where'=>'status = 1', 'orderBy' => 'display_order ASC'])]
		];


		$this->viewData->footer = \Model\Footer_Nav::footerArray();

		if(\Emagid\Core\Membership::isAuthenticated() && \Emagid\Core\Membership::isInRoles(['customer'])){
			$this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
		}
		if(!isset($_SESSION['cart'])){
			$rental = \Model\Rental::getItem(null,['orderBy'=>'duration']);
			$carbon = \Carbon\Carbon::now();
			//check for opening hours
			if($carbon->hour < 20 && $carbon->hour > 8){
				$date = date('Y-m-d H:i:s',ceil(time()/(15*60)) * (15*60));
			} else if($carbon->hour > 20){
				$tmr = $carbon->tomorrow(); $tmr->hour = 8;
				$date = date('Y-m-d H:i:s',$tmr->timestamp);
			} else {
				$later = $carbon->today(); $later->hour = 8;
				$date = date('Y-m-d H:i:s',$later->timestamp);
			}
			$_SESSION['cart'] = new stdClass();
			$_SESSION['cart']->adults = 0;
			$_SESSION['cart']->kids = 0;
			$_SESSION['cart']->tandem = 0;
			$_SESSION['cart']->duration = -1; //todo replace duration with rental_id
			$_SESSION['cart']->datetime = null;
			$_SESSION['cart']->tours = 0;
			$_SESSION['cart']->packages = 0;
			$_SESSION['cart']->equipment = [];
			$_SESSION['cart']->products = [];
			$_SESSION['cart']->rentals = []; //todo remove and change duration to rental id
		}
		if(($rentalCart = \Model\Cart::getActiveRental($this->viewData->user ? $this->viewData->user->id:0,session_id())) && !isset($_SESSION['rental'])){
			$this->resetRentalSession($rentalCart);
		}
		//only relevant when choosing between cart and rental sessions
		$this->viewData->sessionSelect = 'cart';
		if(isset($_SESSION['rental'])){
			$this->viewData->sessionSelect = 'rental';
		}
//		if(strtotime($_SESSION['cart']->datetime) < time()){
//			$_SESSION['cart']->datetime = date('Y-m-d H:i:s',ceil(time()/(15*60)) * (15*60));
//		}

//		if($this->viewData->user){
//			if($cart = \Model\Cart::getList(['where'=>"guest_id = '".session_id()."' and user_id = 0"])) {
//				foreach ($cart as $c) {
//					$c->user_id = $this->viewData->user->id;
//					$c->save();
//				}
//			}
//			$cart = \Model\Cart::getActiveCart($this->viewData->user->id,session_id());
//		} else {
//			$cart = \Model\Cart::getActiveCart(null,session_id());
//		}
//		if($cart){
//			$this->viewData->cart->products = array_map(function($item){
//				return \Model\Product::getItem($item->product_id);
//			},$cart);
//			$this->viewData->cart->cart = $cart;
//			$totals = array_map(function($item){
//				$prod = \Model\Product::getItem($item->product_id);
//				$color = json_decode($item->variation,true)['color'];
//				return $prod->getPrice($color);
//			},$cart);
//			$this->viewData->cart->discount = isset($_SESSION['coupon']) && \Model\Coupon::checkDiscount()? \Model\Coupon::checkDiscount()->getDiscountAmount(array_sum($totals)): 0;
//			$this->viewData->cart->total = array_sum($totals);
//			$this->viewData->cart->minPrice = min($totals);
//		} else {
//			$this->viewData->cart->products = [];
//			$this->viewData->cart->discount = 0;
//			$this->viewData->cart->total = 0;
//			$this->viewData->cart->minPrice = 0;
//		}
		//TODO inelegant solution to abandoned cart
		if(!isset($_SESSION['aCart']) && $this->emagid->route['controller'] == 'checkout'){
			$cartIds = [];
			if(isset($this->viewData->cart->cart) && $this->viewData->cart->cart != null) {
				$cartIds = array_map(function ($cart) {
					return $cart->id;
				}, $this->viewData->cart->cart);
			}
			$ac = new \Model\Abandoned_Cart();
			$ac->user_id = $this->viewData->user ? $this->viewData->user->id: 0;
			$ac->guest_id = session_id();
			$ac->cart = json_encode($cartIds);
			$ac->save();
			$_SESSION['aCart'] = $ac;
		} else if(isset($_SESSION['aCart']) && $this->emagid->route['controller'] != 'checkout'){
			unset($_SESSION['aCart']);
		}
//		if ($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'checkout'){
//
//        }elseif($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'checkout'){
//
//        }else{
//        	unset($_SESSION['post']);
//        }
//		if (isset($_COOKIE['cartProducts'])){
//			$this->viewData->cart->products = json_decode($_COOKIE['cartProducts'],true);
//			if (count($this->viewData->cart->products) > 0){
//				$this->viewData->cart->total = \Model\Product::getPriceSum(implode(',', $this->viewData->cart->products));
//			} else {
//				$this->viewData->cart->total = 0;
//			}
//		} else {
//			$this->viewData->cart->products = [];
//			$this->viewData->cart->total = 0;
//		}
//		if(isset($_COOKIE['cartProductDetails'])){
//			$this->viewData->cart->productDetails = json_decode($_COOKIE['cartProductDetails'], true);
//		} else {
//			$this->viewData->cart->productDetails = [];
//		}
//		$this->viewData->cart->total = number_format($this->viewData->cart->total, 2);

		$this->setFilters();
	}
	
	public function index(Array $params = []){

		$this->loadView($this->viewData);
	}

	public function setFilters(){
		$this->filters = (object)[
			'where' => [],
			'genders' => ['Unisex'=>1, 'Men'=>2, 'Women'=>3],
			'price' => ['1000', '5000', '10000', '15000'],
			'filter' =>['lowToHigh'=>'Lowest to Highest','highToLow'=>'Highest to Lowest']
		];
		if (isset($_GET) && count($_GET) > 0){
			foreach($_GET as $filter=>$keyword){
				switch($filter){
					case 'price':
						$this->filters->where['price'] = 'product.price < '.$keyword;
						$this->filters->$filter = $keyword;
						$this->viewData->params['price'] = $keyword;
						break;
					case 'gender':
						$this->filters->where['gender'] = 'product.gender = '.$this->filters->genders[$keyword];
						$this->filters->$filter = $keyword;
						$this->viewData->params['gender'] = $keyword;
						break;
					case 'material':
						$this->filters->where['material'] = 'product.id in ('.implode(',', \Model\Product_Material::get_material_products($keyword)).')';
						$this->filters->$filter = $keyword;
						$this->viewData->params['material'] = $keyword;
						break;
					case 'collection':
						$this->filters->where['collection'] = 'product.id in ('.implode(',', \Model\Product_Collection::get_collection_products($keyword)).')';
						$this->filters->$filter = $keyword;
						$this->viewData->params['collection'] = $keyword;
						break;
					case 'color':
						$this->filters->where['color'] = 'product.color = '.\Model\Color::getIdBySlug($keyword);
						$this->filters->$filter = $keyword;
						$this->viewData->params['color'] = $keyword;
						break;
				}
			}
			if(isset($_GET['filter'])){
				$this->filters->priceFilter = $_GET['filter'];
			}
		}
		$this->viewData->filters = $this->filters;
	}

	public function toJson($array)
	{
		header('Content-Type: application/json');
		echo json_encode($array);
		exit();
	}
	/**
	 * Intercept previously existing URL and redirect (directly|algorithmically)
	 **/
	public function legacyRedirection(){
		/**
		 * URI => Redirection
		 **/
		$hardLinkRedirection = ['/pages/shoe-store'=>'/nycstore'];
		if(array_key_exists($this->emagid->uri,$hardLinkRedirection)){
			redirect($hardLinkRedirection[$this->emagid->uri]);
		}

		/**
		 * /products/ redirection to search page
		 **/
		if(isset($this->emagid->route['product_slug']) && \Model\Product::getIdBySlug($this->emagid->route['product_slug']) == 0){
			$slugReplace = str_replace('-',' ',$this->emagid->route['product_slug']);
			$search = http_build_query(['term'=>$slugReplace]);
			redirect('/categories/search?'.$search);
		}
	}

	public function resetRentalSession($rentalCart){
		if(isset($_SESSION['rental'])){
			unset($_SESSION['rental']);
		}
		$_SESSION['rental'] = new stdClass();
		$_SESSION['rental']->id = $rentalCart->id;
		$_SESSION['rental']->adults = $rentalCart->adult_count ? : 0;
		$_SESSION['rental']->kids = $rentalCart->kid_count ? : 0;
		$_SESSION['rental']->tandem = $rentalCart->tandem_count ? : 0;
		$_SESSION['rental']->duration = $rentalCart->duration;
		$_SESSION['rental']->datetime = $rentalCart->reserve_date;
		$_SESSION['rental']->tours = $rentalCart->tour_id;
		$_SESSION['rental']->packages = $rentalCart->package_id;
		$_SESSION['rental']->equipment = json_decode($rentalCart->equipment,true);
		$_SESSION['rental']->products = json_decode($rentalCart->products,true);
	}
}
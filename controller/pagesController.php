<?php

class pagesController extends siteController {

	public function index(Array $params = []) {
		redirect(SITE_URL);
	}

	public function page(Array $params = []) {
		$slug = (isset($params['page_slug'])) ? $params['page_slug'] : '';
		$this->viewData->page  = \Model\Page::getItem(null,['where'=>'page.active = 1 AND page.slug like \''.$slug.'\'']);

		if (!is_null($this->viewData->page->meta_keywords) && $this->viewData->page->meta_keywords != ''){
			$this->configs['Meta Keywords'] = $this->viewData->page->meta_keywords;
		}
		if (!is_null($this->viewData->page->meta_description) && $this->viewData->page->meta_description != ''){
			$this->configs['Meta Description'] = $this->viewData->page->meta_description;
		}
		if (!is_null($this->viewData->page->meta_title) && $this->viewData->page->meta_title != ''){
			$this->configs['Meta Title'] = $this->viewData->page->meta_title;
		}

		$this->loadView($this->viewData);
	}

}
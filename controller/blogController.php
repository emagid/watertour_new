<?php

class blogController extends siteController
{

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Our Blog &#x2014; Bike Rental Central Park";
        $this->viewData->blog = \Model\Blog::getList(['where' => 'status = 1']);
        $this->viewData->rentals = \Model\Rental::getList(['orderBy'=>'duration']);
        $this->loadView($this->viewData);
    }

    public function blog(Array $params = [])
    {
        $this->viewData->blog = \Model\Blog::getItem(\Model\Blog::getIdBySlug($params['slug']));
        $this->loadView($this->viewData);
    }

}
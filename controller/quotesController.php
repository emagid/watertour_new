<?php

class quotesController extends siteController{
    /*public function add(){
        if(isset($_POST['first_name']) && $_POST['first_name'] &&
            isset($_POST['last_name']) && $_POST['last_name'] &&
            isset($_POST['email']) && $_POST['email'] &&
            isset($_POST['comment']) && $_POST['comment'] &&
            isset($_POST['phone']) && $_POST['phone'] &&
            isset($_POST['company']) && $_POST['company'] &&
            isset($_POST['route']) && $_POST['route'] &&
            isset($_POST['pickup_date']) && $_POST['pickup_date'] &&
            isset($_POST['pickup_time']) && $_POST['pickup_time'] &&
            isset($_POST['pickup_location']) && $_POST['pickup_location'] &&
            isset($_POST['dropoff_date']) && $_POST['dropoff_date'] &&
            isset($_POST['dropoff_time']) && $_POST['dropoff_time'] &&
            isset($_POST['dropoff_location']) && $_POST['dropoff_location'] &&
            isset($_POST['guest']) && $_POST['guest'] &&
            isset($_POST['special_request']) && $_POST['special_request'] &&
            isset($_POST['language_request']) && $_POST['language_request']){
            $quote = new \Model\Quote($_POST);
            $quote->save();
            $this->toJson(['status'=>'success','msg'=>"Done"]);
        } else {
            $this->toJson(['status'=>'failed','msg'=>"Please complete the form before submitting."]);
        }
    }*/
    function add(){
        $fields = \Model\Quote::getSelectFields();
        $quotes= new \Model\Quote();
        foreach($fields as $field){
            if(isset($_POST[$field]) && $_POST[$field]){
                $quotes->$field = $_POST[$field];
            }
        }
        if($quotes->save()) {
            $n = new \Notification\MessageHandler('Thank you for your submission!');
        } else {
            $n = new \Notification\MessageHandler('Error: Could not save your submission');
        }
        $_SESSION['notification'] = serialize($n);
        redirect($_POST['redirect']);
    }
}
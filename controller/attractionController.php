<?php

//Ajax Controller
class attractionController extends siteController{

    public function query(){
        $term = $_POST['term'];
        $cat = $_POST['cat'];
        if(($attraction = \Model\Attraction::query($term,$cat))){
            $attractArr = [];
            foreach($attraction as $item){
                $attractArr[] = ['id'=>$item->id,'name'=>$item->name,'slug'=>$item->slug,'description'=>$item->description,'lat'=>$item->lat,'lng'=>$item->lng,'featured_image'=>$item->featured_image,'marker_color'=>$item->marker_color,'hasPackage'=>$item->hasPackage() ? true: false];
            }

            $half = ceil(count($attractArr)/2);
            $return = array_chunk((array)$attractArr,$half);
            $this->toJson($return);
        } else {
            $this->toJson([]);
        }
    }
}